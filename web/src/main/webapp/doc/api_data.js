define({ "api": [
  {
    "type": "post",
    "url": "/rest/tenant_user/info.htm",
    "title": "1.02 用户基本信息",
    "version": "0.0.1",
    "name": "info",
    "group": "1user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "tenant",
            "description": "<p>平台id(3)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userToken",
            "description": "<p>令牌</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>用户基本信息</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码(默认为0)</p>"
          }
        ]
      }
    },
    "filename": "./1user.js",
    "groupTitle": "1.0 用户模块",
    "sampleRequest": [
      {
        "url": "https://umall.haoxuer.com/bigworld/rest/tenant_user/info.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/saas_user/loginOauth.htm",
    "title": "1.01 通过第三方登陆",
    "version": "0.0.1",
    "name": "loginOauth",
    "group": "1user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "tenant",
            "description": "<p>平台id(3)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>令牌</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>第三方类型（wxapp）</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>通过第三方登陆,要是没有用户信息，系统会创建一份用户信息</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码(默认为0)</p>"
          }
        ]
      }
    },
    "filename": "./1user.js",
    "groupTitle": "1.0 用户模块",
    "sampleRequest": [
      {
        "url": "https://umall.haoxuer.com/bigworld/rest/saas_user/loginOauth.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/message/delete.htm",
    "title": "1.06 删除消息",
    "version": "0.0.1",
    "name": "message_delete",
    "group": "1user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "tenant",
            "description": "<p>平台id(3)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userToken",
            "description": "<p>令牌</p>"
          },
          {
            "group": "Parameter",
            "type": "long",
            "optional": false,
            "field": "id",
            "description": "<p>消息id</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>删除消息</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码(默认为0)</p>"
          }
        ]
      }
    },
    "filename": "./1user.js",
    "groupTitle": "1.0 用户模块",
    "sampleRequest": [
      {
        "url": "https://umall.haoxuer.com/bigworld/rest/message/delete.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/message/page.htm",
    "title": "1.04 消息记录列表",
    "version": "0.0.1",
    "name": "message_page",
    "group": "1user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "tenant",
            "description": "<p>平台id(3)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userToken",
            "description": "<p>令牌</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "no",
            "description": "<p>页码</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "size",
            "description": "<p>大小</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>消息记录列表</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码(默认为0)</p>"
          }
        ]
      }
    },
    "filename": "./1user.js",
    "groupTitle": "1.0 用户模块",
    "sampleRequest": [
      {
        "url": "https://umall.haoxuer.com/bigworld/rest/message/page.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/message/read.htm",
    "title": "1.05 阅读消息",
    "version": "0.0.1",
    "name": "message_read",
    "group": "1user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "tenant",
            "description": "<p>平台id(3)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userToken",
            "description": "<p>令牌</p>"
          },
          {
            "group": "Parameter",
            "type": "long",
            "optional": false,
            "field": "id",
            "description": "<p>消息id</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>阅读消息</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码(默认为0)</p>"
          }
        ]
      }
    },
    "filename": "./1user.js",
    "groupTitle": "1.0 用户模块",
    "sampleRequest": [
      {
        "url": "https://umall.haoxuer.com/bigworld/rest/message/read.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/tenant_user/pageForReturn.htm",
    "title": "1.03 返现记录",
    "version": "0.0.1",
    "name": "pageForReturn",
    "group": "1user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "tenant",
            "description": "<p>平台id(3)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userToken",
            "description": "<p>令牌</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "no",
            "description": "<p>页码</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "size",
            "description": "<p>大小</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>返现记录</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码(默认为0)</p>"
          }
        ]
      }
    },
    "filename": "./1user.js",
    "groupTitle": "1.0 用户模块",
    "sampleRequest": [
      {
        "url": "https://umall.haoxuer.com/bigworld/rest/tenant_user/pageForReturn.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/tenant/document.htm",
    "title": "1.07 获取文档",
    "version": "0.0.1",
    "name": "tenant_document",
    "group": "1user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "tenant",
            "description": "<p>平台id(3)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userToken",
            "description": "<p>令牌</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "key",
            "description": "<p>文件key(about关于我们，license用户协议)</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>获取文档</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码(默认为0)</p>"
          }
        ]
      }
    },
    "filename": "./1user.js",
    "groupTitle": "1.0 用户模块",
    "sampleRequest": [
      {
        "url": "https://umall.haoxuer.com/bigworld/rest/tenant/document.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/activity/page.htm",
    "title": "2.02 获取活动页列表数据",
    "version": "0.0.1",
    "name": "page",
    "group": "2activity",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "tenant",
            "description": "<p>平台id(3)</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "no",
            "description": "<p>页码</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "size",
            "description": "<p>分页大小</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>活动详情</p>",
    "filename": "./2_activity.js",
    "groupTitle": "2 活动模块",
    "sampleRequest": [
      {
        "url": "https://umall.haoxuer.com/bigworld/rest/activity/page.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/activity/top.htm",
    "title": "2.01 首页活动列表",
    "version": "0.0.1",
    "name": "top",
    "group": "2activity",
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>获取程序的token</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "tenant",
            "description": "<p>平台id(3)</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "size",
            "description": "<p>分页大小</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "total",
            "description": "<p>总共多少条数据</p>"
          },
          {
            "group": "Success 200",
            "type": "totalPage",
            "optional": false,
            "field": "size",
            "description": "<p>总共多少页</p>"
          }
        ]
      }
    },
    "filename": "./2_activity.js",
    "groupTitle": "2 活动模块",
    "sampleRequest": [
      {
        "url": "https://umall.haoxuer.com/bigworld/rest/activity/top.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/activity/view.htm",
    "title": "2.03 活动详情",
    "version": "0.0.1",
    "name": "view",
    "group": "2activity",
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>购买活动券</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Long",
            "optional": false,
            "field": "id",
            "description": "<p>活动id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>状态消息</p>"
          }
        ]
      }
    },
    "filename": "./2_activity.js",
    "groupTitle": "2 活动模块",
    "sampleRequest": [
      {
        "url": "https://umall.haoxuer.com/bigworld/rest/activity/view.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/venue/searchByCircle.htm",
    "title": "3.01 获取附近的场馆",
    "version": "0.0.1",
    "name": "searchByCircle",
    "group": "3venue",
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>获取程序的token</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "tenant",
            "description": "<p>平台id(3)</p>"
          },
          {
            "group": "Parameter",
            "type": "float",
            "optional": false,
            "field": "lng",
            "description": "<p>经度</p>"
          },
          {
            "group": "Parameter",
            "type": "float",
            "optional": false,
            "field": "lat",
            "description": "<p>纬度</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userToken",
            "description": "<p>用户token</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "list",
            "description": "<p>数据集合</p>"
          },
          {
            "group": "Success 200",
            "type": "long",
            "optional": false,
            "field": "list.id",
            "description": "<p>场馆id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "list.name",
            "description": "<p>场馆名称</p>"
          },
          {
            "group": "Success 200",
            "type": "float",
            "optional": false,
            "field": "list.lng",
            "description": "<p>场馆经度</p>"
          },
          {
            "group": "Success 200",
            "type": "float",
            "optional": false,
            "field": "list.lat",
            "description": "<p>场馆纬度</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "list.phone",
            "description": "<p>场馆联系电话</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "list.money",
            "description": "<p>人均消费</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "list.zoom",
            "description": "<p>地图显示等级</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "list.logo",
            "description": "<p>场馆logo图片</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "list.privilege",
            "description": "<p>场馆会员特权</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "list.catalog",
            "description": "<p>场馆类型</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "list.address",
            "description": "<p>店铺地址</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "total",
            "description": "<p>总共多少条数据</p>"
          },
          {
            "group": "Success 200",
            "type": "totalPage",
            "optional": false,
            "field": "size",
            "description": "<p>总共多少页</p>"
          }
        ]
      }
    },
    "filename": "./3venue.js",
    "groupTitle": "3 场馆模块",
    "sampleRequest": [
      {
        "url": "https://umall.haoxuer.com/bigworld/rest/venue/searchByCircle.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/venue/view.htm",
    "title": "3.02 商家详情",
    "version": "0.0.1",
    "name": "view",
    "group": "3venue",
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>商家详情</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "long",
            "optional": false,
            "field": "tenant",
            "description": "<p>平台id(1)</p>"
          },
          {
            "group": "Parameter",
            "type": "long",
            "optional": false,
            "field": "id",
            "description": "<p>场馆id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userToken",
            "description": "<p>用户token</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>状态消息</p>"
          },
          {
            "group": "Success 200",
            "type": "long",
            "optional": false,
            "field": "id",
            "description": "<p>场馆id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>场馆名称</p>"
          },
          {
            "group": "Success 200",
            "type": "float",
            "optional": false,
            "field": "lng",
            "description": "<p>场馆经度</p>"
          },
          {
            "group": "Success 200",
            "type": "float",
            "optional": false,
            "field": "lat",
            "description": "<p>场馆纬度</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>场馆联系电话</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "money",
            "description": "<p>人均消费</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "note",
            "description": "<p>场馆介绍</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "logo",
            "description": "<p>场馆logo图片</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "images",
            "description": "<p>场馆效果图集合</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>店铺地址</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "privilege",
            "description": "<p>场馆会员特权</p>"
          },
          {
            "group": "Success 200",
            "type": "float",
            "optional": false,
            "field": "rebate",
            "description": "<p>折扣</p>"
          }
        ]
      }
    },
    "filename": "./3venue.js",
    "groupTitle": "3 场馆模块",
    "sampleRequest": [
      {
        "url": "https://umall.haoxuer.com/bigworld/rest/venue/view.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/order/page.htm",
    "title": "4.02 订单列表功能",
    "version": "0.0.1",
    "name": "page",
    "group": "4order",
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>订单列表功能</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "tenant",
            "description": "<p>平台id(3)</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "no",
            "description": "<p>页码</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "size",
            "description": "<p>分页大小</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userToken",
            "description": "<p>用户token</p>"
          }
        ]
      }
    },
    "filename": "./4order.js",
    "groupTitle": "4 订单模块",
    "sampleRequest": [
      {
        "url": "https://umall.haoxuer.com/bigworld/rest/order/page.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/order/pay.htm",
    "title": "4.01 付款接口",
    "version": "0.0.1",
    "name": "pay",
    "group": "4order",
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>付款接口</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "tenant",
            "description": "<p>平台id(3)</p>"
          },
          {
            "group": "Parameter",
            "type": "long",
            "optional": false,
            "field": "venue",
            "description": "<p>场馆id</p>"
          },
          {
            "group": "Parameter",
            "type": "float",
            "optional": false,
            "field": "payType",
            "description": "<p>付款方式 cash现金余额，wxapp微信付款</p>"
          },
          {
            "group": "Parameter",
            "type": "float",
            "optional": false,
            "field": "money",
            "description": "<p>付款金额</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userToken",
            "description": "<p>用户token</p>"
          }
        ]
      }
    },
    "filename": "./4order.js",
    "groupTitle": "4 订单模块",
    "sampleRequest": [
      {
        "url": "https://umall.haoxuer.com/bigworld/rest/order/pay.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/order/view.htm",
    "title": "4.03 订单详情",
    "version": "0.0.1",
    "name": "view",
    "group": "4order",
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>订单详情</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "tenant",
            "description": "<p>平台id(3)</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>订单id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userToken",
            "description": "<p>用户token</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>店铺名称</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "icon",
            "description": "<p>店铺logo</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "address",
            "description": "<p>店铺地址</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "date",
            "description": "<p>付款时间</p>"
          },
          {
            "group": "Success 200",
            "type": "float",
            "optional": false,
            "field": "money",
            "description": "<p>付款金额</p>"
          },
          {
            "group": "Success 200",
            "type": "float",
            "optional": false,
            "field": "backMoney",
            "description": "<p>返款金额</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "payType",
            "description": "<p>支付方式</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "venueType",
            "description": "<p>店铺类型</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "orderType",
            "description": "<p>订单类型(0普通订单,1充电宝订单)</p>"
          }
        ]
      }
    },
    "filename": "./4order.js",
    "groupTitle": "4 订单模块",
    "sampleRequest": [
      {
        "url": "https://umall.haoxuer.com/bigworld/rest/order/view.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/cash/page.htm",
    "title": "5.03 提现记录",
    "version": "0.0.1",
    "name": "page",
    "group": "5cash",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "tenant",
            "description": "<p>平台id(3)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userToken",
            "description": "<p>令牌</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "no",
            "description": "<p>页码</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "size",
            "description": "<p>大小</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>提现记录</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码(默认为0)</p>"
          }
        ]
      }
    },
    "filename": "./5cash.js",
    "groupTitle": "5.0 资金模块",
    "sampleRequest": [
      {
        "url": "https://umall.haoxuer.com/bigworld/rest/cash/page.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/cash/stream.htm",
    "title": "5.01 资金记录",
    "version": "0.0.1",
    "name": "stream",
    "group": "5cash",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "no",
            "description": "<p>页码</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "size",
            "description": "<p>大小</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "tenant",
            "description": "<p>平台id(3)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userToken",
            "description": "<p>令牌</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>资金记录</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码(默认为0)</p>"
          }
        ]
      }
    },
    "filename": "./5cash.js",
    "groupTitle": "5.0 资金模块",
    "sampleRequest": [
      {
        "url": "https://umall.haoxuer.com/bigworld/rest/cash/stream.htm"
      }
    ]
  },
  {
    "type": "post",
    "url": "/rest/cash/withdrawal.htm",
    "title": "5.02 提现",
    "version": "0.0.1",
    "name": "withdrawal",
    "group": "5cash",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "tenant",
            "description": "<p>平台id(3)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userToken",
            "description": "<p>令牌</p>"
          },
          {
            "group": "Parameter",
            "type": "float",
            "optional": false,
            "field": "money",
            "description": "<p>金额</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>真实姓名</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>提现</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码(默认为0)</p>"
          }
        ]
      }
    },
    "filename": "./5cash.js",
    "groupTitle": "5.0 资金模块",
    "sampleRequest": [
      {
        "url": "https://umall.haoxuer.com/bigworld/rest/cash/withdrawal.htm"
      }
    ]
  }
] });
