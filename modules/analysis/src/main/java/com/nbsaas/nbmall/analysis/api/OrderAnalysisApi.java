package com.nbsaas.nbmall.analysis.api;

import com.nbsaas.nbmall.analysis.request.OrderNumRequest;
import com.haoxuer.bigworld.analysis.list.LongList;

public interface OrderAnalysisApi {

    LongList num(OrderNumRequest request);

    LongList month(OrderNumRequest request);

}
