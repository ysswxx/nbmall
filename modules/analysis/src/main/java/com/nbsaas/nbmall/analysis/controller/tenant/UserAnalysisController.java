package com.nbsaas.nbmall.analysis.controller.tenant;

import com.nbsaas.nbmall.analysis.api.UserAnalysisApi;
import com.nbsaas.nbmall.analysis.request.OrderNumRequest;
import com.haoxuer.bigworld.analysis.list.LongList;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/tenantRest/user")
@RestController
public class UserAnalysisController extends BaseTenantRestController {

    @RequestMapping("num")
    public LongList num(OrderNumRequest request) {
        initTenant(request);
        return api.num(request);
    }

    @RequestMapping("month")
    public LongList month(OrderNumRequest request) {
        initTenant(request);
        return api.month(request);
    }

    @Autowired
    private UserAnalysisApi api;
}
