package com.nbsaas.nbmall.analysis.mapper;

import com.nbsaas.nbmall.analysis.request.OrderNumRequest;
import com.haoxuer.bigworld.analysis.simple.LongName;

import java.util.List;

public interface UserMapper {

    List<LongName> num(OrderNumRequest request);

    List<LongName> month(OrderNumRequest request);
}
