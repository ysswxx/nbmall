package com.nbsaas.nbmall.shop.controller.rest;

import com.nbsaas.nbmall.shop.api.apis.ShopHourApi;
import com.nbsaas.nbmall.shop.api.domain.list.ShopHourList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopHourPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopHourResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/shophour")
@RestController
public class ShopHourRestController extends BaseRestController {


    @RequestMapping("create")
    public ShopHourResponse create(ShopHourDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

    @RequestMapping("delete")
    public ShopHourResponse delete(ShopHourDataRequest request) {
        initTenant(request);
        ShopHourResponse result = new ShopHourResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public ShopHourResponse update(ShopHourDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public ShopHourResponse view(ShopHourDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public ShopHourList list(ShopHourSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public ShopHourPage search(ShopHourSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private ShopHourApi api;

}
