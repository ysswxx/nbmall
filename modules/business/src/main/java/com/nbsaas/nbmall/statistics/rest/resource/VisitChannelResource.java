package com.nbsaas.nbmall.statistics.rest.resource;

import com.nbsaas.nbmall.statistics.api.apis.VisitChannelApi;
import com.nbsaas.nbmall.statistics.api.domain.list.VisitChannelList;
import com.nbsaas.nbmall.statistics.api.domain.page.VisitChannelPage;
import com.nbsaas.nbmall.statistics.api.domain.request.*;
import com.nbsaas.nbmall.statistics.api.domain.response.VisitChannelResponse;
import com.nbsaas.nbmall.statistics.data.dao.VisitChannelDao;
import com.nbsaas.nbmall.statistics.data.entity.VisitChannel;
import com.nbsaas.nbmall.statistics.rest.convert.VisitChannelResponseConvert;
import com.nbsaas.nbmall.statistics.rest.convert.VisitChannelSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class VisitChannelResource implements VisitChannelApi {

    @Autowired
    private VisitChannelDao dataDao;



    @Override
    public VisitChannelResponse create(VisitChannelDataRequest request) {
        VisitChannelResponse result = new VisitChannelResponse();

        VisitChannel bean = new VisitChannel();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new VisitChannelResponseConvert().conver(bean);
        return result;
    }

    @Override
    public VisitChannelResponse update(VisitChannelDataRequest request) {
        VisitChannelResponse result = new VisitChannelResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        VisitChannel bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new VisitChannelResponseConvert().conver(bean);
        return result;
    }

    private void handleData(VisitChannelDataRequest request, VisitChannel bean) {
       TenantBeanUtils.copyProperties(request,bean);

    }

    @Override
    public VisitChannelResponse delete(VisitChannelDataRequest request) {
        VisitChannelResponse result = new VisitChannelResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public VisitChannelResponse view(VisitChannelDataRequest request) {
        VisitChannelResponse result=new VisitChannelResponse();
        VisitChannel bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new VisitChannelResponseConvert().conver(bean);
        return result;
    }
    @Override
    public VisitChannelList list(VisitChannelSearchRequest request) {
        VisitChannelList result = new VisitChannelList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<VisitChannel> organizations = dataDao.list(0, request.getSize(), filters, orders);

        VisitChannelSimpleConvert convert=new VisitChannelSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public VisitChannelPage search(VisitChannelSearchRequest request) {
        VisitChannelPage result=new VisitChannelPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<VisitChannel> page=dataDao.page(pageable);

        VisitChannelSimpleConvert convert=new VisitChannelSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
