package com.nbsaas.nbmall.order.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.order.data.entity.OrderForm;

import java.math.BigDecimal;
import java.util.Date;

/**
* Created by imake on 2021年12月24日00:04:24.
*/
public interface OrderFormDao extends BaseDao<OrderForm,Long>{

	 OrderForm findById(Long id);

	 OrderForm save(OrderForm bean);

	 OrderForm updateByUpdater(Updater<OrderForm> updater);

	 OrderForm deleteById(Long id);

	 OrderForm findById(Long tenant, Long id);

     OrderForm deleteById(Long tenant, Long id);

	BigDecimal day(Long id, Date today);

	Long dayNum(Long id, Date yesterday);

	BigDecimal dayAll(Long tenant, Date today);

	Long dayNumAll(Long tenant, Date yesterday);
}