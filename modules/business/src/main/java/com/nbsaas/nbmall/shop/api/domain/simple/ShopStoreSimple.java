package com.nbsaas.nbmall.shop.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by BigWorld on 2021年12月10日17:38:00.
*/
@Data
public class ShopStoreSimple implements Serializable {

    private Long id;

     private Long shop;
     private Integer area;
     private String address;
     private Integer city;
     private Double lng;
     private Long creator;
     private String shopName;
     private Integer province;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private String phone;
     private String logo;
     private String name;
     private Double lat;


}
