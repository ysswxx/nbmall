package com.nbsaas.nbmall.video.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.video.data.dao.VideoCatalogDao;
import com.nbsaas.nbmall.video.data.entity.VideoCatalog;
import com.haoxuer.discover.data.core.CatalogDaoImpl;

/**
* Created by imake on 2022年03月16日21:25:37.
*/
@Repository

public class VideoCatalogDaoImpl extends CatalogDaoImpl<VideoCatalog, Integer> implements VideoCatalogDao {

	@Override
	public VideoCatalog findById(Integer id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public VideoCatalog save(VideoCatalog bean) {

		add(bean);
		
		
		return bean;
	}

    @Override
	public VideoCatalog deleteById(Integer id) {
		VideoCatalog entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<VideoCatalog> getEntityClass() {
		return VideoCatalog.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public VideoCatalog findById(Long tenant,Integer id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public VideoCatalog deleteById(Long tenant,Integer id) {
		VideoCatalog entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}