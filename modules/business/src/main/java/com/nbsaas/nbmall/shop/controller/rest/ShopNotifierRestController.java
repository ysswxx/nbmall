package com.nbsaas.nbmall.shop.controller.rest;

import com.nbsaas.nbmall.shop.api.apis.ShopNotifierApi;
import com.nbsaas.nbmall.shop.api.domain.list.ShopNotifierList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopNotifierPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopNotifierResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/shopnotifier")
@RestController
public class ShopNotifierRestController extends BaseRestController {


    @RequestMapping("create")
    public ShopNotifierResponse create(ShopNotifierDataRequest request) {
        initTenant(request);
        request.setCreator(request.getUser());
        return api.create(request);
    }

    @RequestMapping("delete")
    public ShopNotifierResponse delete(ShopNotifierDataRequest request) {
        initTenant(request);
        ShopNotifierResponse result = new ShopNotifierResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public ShopNotifierResponse update(ShopNotifierDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public ShopNotifierResponse view(ShopNotifierDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public ShopNotifierList list(ShopNotifierSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public ShopNotifierPage search(ShopNotifierSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private ShopNotifierApi api;

}
