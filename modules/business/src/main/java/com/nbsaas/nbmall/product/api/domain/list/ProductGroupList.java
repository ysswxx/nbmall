package com.nbsaas.nbmall.product.api.domain.list;


import com.nbsaas.nbmall.product.api.domain.simple.ProductGroupSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年12月28日17:59:58.
*/

@Data
public class ProductGroupList  extends ResponseList<ProductGroupSimple> {

}