package com.nbsaas.nbmall.shop.api.apis;


import com.nbsaas.nbmall.shop.api.domain.list.ShopCatalogList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopCatalogPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopCatalogResponse;

public interface ShopCatalogApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    ShopCatalogResponse create(ShopCatalogDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    ShopCatalogResponse update(ShopCatalogDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    ShopCatalogResponse delete(ShopCatalogDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     ShopCatalogResponse view(ShopCatalogDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    ShopCatalogList list(ShopCatalogSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    ShopCatalogPage search(ShopCatalogSearchRequest request);

}