package com.nbsaas.nbmall.promote.api.domain.page;


import com.nbsaas.nbmall.promote.api.domain.simple.FullReductionItemSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年12月25日12:45:48.
*/

@Data
public class FullReductionItemPage  extends ResponsePage<FullReductionItemSimple> {

}