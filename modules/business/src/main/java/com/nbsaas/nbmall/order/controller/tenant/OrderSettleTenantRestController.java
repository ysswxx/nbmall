package com.nbsaas.nbmall.order.controller.tenant;

import com.nbsaas.nbmall.order.api.apis.OrderSettleApi;
import com.nbsaas.nbmall.order.api.domain.list.OrderSettleList;
import com.nbsaas.nbmall.order.api.domain.page.OrderSettlePage;
import com.nbsaas.nbmall.order.api.domain.request.*;
import com.nbsaas.nbmall.order.api.domain.response.OrderSettleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/ordersettle")
@RestController
public class OrderSettleTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("ordersettle")
    @RequestMapping("create")
    public OrderSettleResponse create(OrderSettleDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

	@RequiresPermissions("ordersettle")
    @RequestMapping("delete")
    public OrderSettleResponse delete(OrderSettleDataRequest request) {
        initTenant(request);
        OrderSettleResponse result = new OrderSettleResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("ordersettle")
    @RequestMapping("update")
    public OrderSettleResponse update(OrderSettleDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("ordersettle")
    @RequestMapping("view")
    public OrderSettleResponse view(OrderSettleDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("ordersettle")
    @RequestMapping("list")
    public OrderSettleList list(OrderSettleSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("ordersettle")
    @RequestMapping("search")
    public OrderSettlePage search(OrderSettleSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private OrderSettleApi api;

}
