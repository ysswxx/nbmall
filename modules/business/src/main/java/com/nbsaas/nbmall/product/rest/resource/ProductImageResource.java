package com.nbsaas.nbmall.product.rest.resource;

import com.nbsaas.nbmall.product.api.apis.ProductImageApi;
import com.nbsaas.nbmall.product.api.domain.list.ProductImageList;
import com.nbsaas.nbmall.product.api.domain.page.ProductImagePage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductImageResponse;
import com.nbsaas.nbmall.product.data.dao.ProductImageDao;
import com.nbsaas.nbmall.product.data.entity.ProductImage;
import com.nbsaas.nbmall.product.rest.convert.ProductImageResponseConvert;
import com.nbsaas.nbmall.product.rest.convert.ProductImageSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.nbsaas.nbmall.product.data.dao.ProductDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class ProductImageResource implements ProductImageApi {

    @Autowired
    private ProductImageDao dataDao;

    @Autowired
    private ProductDao productDao;


    @Override
    public ProductImageResponse create(ProductImageDataRequest request) {
        ProductImageResponse result = new ProductImageResponse();

        ProductImage bean = new ProductImage();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new ProductImageResponseConvert().conver(bean);
        return result;
    }

    @Override
    public ProductImageResponse update(ProductImageDataRequest request) {
        ProductImageResponse result = new ProductImageResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        ProductImage bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new ProductImageResponseConvert().conver(bean);
        return result;
    }

    private void handleData(ProductImageDataRequest request, ProductImage bean) {
       TenantBeanUtils.copyProperties(request,bean);
           if(request.getProduct()!=null){
              bean.setProduct(productDao.findById(request.getProduct()));
           }

    }

    @Override
    public ProductImageResponse delete(ProductImageDataRequest request) {
        ProductImageResponse result = new ProductImageResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public ProductImageResponse view(ProductImageDataRequest request) {
        ProductImageResponse result=new ProductImageResponse();
        ProductImage bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new ProductImageResponseConvert().conver(bean);
        return result;
    }
    @Override
    public ProductImageList list(ProductImageSearchRequest request) {
        ProductImageList result = new ProductImageList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<ProductImage> organizations = dataDao.list(0, request.getSize(), filters, orders);

        ProductImageSimpleConvert convert=new ProductImageSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public ProductImagePage search(ProductImageSearchRequest request) {
        ProductImagePage result=new ProductImagePage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<ProductImage> page=dataDao.page(pageable);

        ProductImageSimpleConvert convert=new ProductImageSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
