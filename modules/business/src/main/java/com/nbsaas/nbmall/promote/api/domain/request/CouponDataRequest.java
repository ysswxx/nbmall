package com.nbsaas.nbmall.promote.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import com.nbsaas.nbmall.promote.data.enums.CouponState;
import com.nbsaas.nbmall.promote.data.enums.CouponCatalog;
import java.util.Date;
import java.math.BigDecimal;

/**
*
* Created by imake on 2021年12月25日12:45:48.
*/

@Data
public class CouponDataRequest extends TenantRequest {

    private Long id;

     private Long tenantUser;
     private Long shop;
     private Date useEndTime;
     private Long couponRule;
     private Date useBeginTime;
     private BigDecimal money;
     private CouponCatalog couponCatalog;
     private CouponState couponState;

}