package com.nbsaas.nbmall.promote.rest.convert;

import com.nbsaas.nbmall.promote.api.domain.simple.FullReductionSimple;
import com.nbsaas.nbmall.promote.data.entity.FullReduction;
import com.haoxuer.discover.data.rest.core.Conver;
public class FullReductionSimpleConvert implements Conver<FullReductionSimple, FullReduction> {


    @Override
    public FullReductionSimple conver(FullReduction source) {
        FullReductionSimple result = new FullReductionSimple();

            result.setId(source.getId());
            if(source.getShop()!=null){
               result.setShop(source.getShop().getId());
            }
             result.setFullReductionWay(source.getFullReductionWay());
             result.setEndTime(source.getEndTime());
             if(source.getShop()!=null){
                result.setShopName(source.getShop().getName());
             }
             result.setBeginTime(source.getBeginTime());
             result.setName(source.getName());
             result.setAddDate(source.getAddDate());

             result.setFullReductionWayName(source.getFullReductionWay()+"");
        return result;
    }
}
