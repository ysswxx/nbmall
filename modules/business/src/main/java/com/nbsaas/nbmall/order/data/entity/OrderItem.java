package com.nbsaas.nbmall.order.data.entity;

import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.FormField;
import com.nbsaas.nbmall.product.data.entity.Product;
import com.nbsaas.nbmall.product.data.entity.ProductSku;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;


@Data
@FormAnnotation(title = "商品信息")
@Entity
@Table(name = "bs_tenant_order_item")
public class OrderItem extends TenantEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    private OrderForm orderForm;

    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @ManyToOne(fetch = FetchType.LAZY)
    private ProductSku sku;

    @FormField(title = "购买数量", sortNum = "1", grid = true, col = 12)
    private Integer size;

    @FormField(title = "退货数量", sortNum = "1", grid = true, col = 12)
    private Integer returnNum;

    //使用数量
    private Integer useNum;

    //剩余
    private Integer surplusNum;

    @FormField(title = "退款金额", sortNum = "1", grid = true, col = 12)
    private BigDecimal returnAmount;

    //售后状态 0无 1部分退款 2全部退款 3部分换货 4全部换货 5部分退货退款 6全部退货退款 7综合售后
    @FormField(title = "售后状态", sortNum = "1", grid = true, col = 12)
    private Integer refundState;

    @FormField(title = "单位", sortNum = "1", grid = true, col = 12)
    private String unit;

    @FormField(title = "重量", sortNum = "1", grid = true, col = 12)
    private BigDecimal weight;

    @FormField(title = "单价", sortNum = "1", grid = true, col = 12)
    private BigDecimal price;

    @FormField(title = "成交单价", sortNum = "1", grid = true, col = 12)
    private BigDecimal realPrice;

    @FormField(title = "运费", sortNum = "1", grid = true, col = 12)
    private BigDecimal freight;


    @FormField(title = "小计", sortNum = "1", grid = true, col = 12)
    private BigDecimal subtotal;


    @FormField(title = "优惠金额", sortNum = "1", grid = true, col = 12)
    private BigDecimal discountAmount;

    @FormField(title = "平台优惠", sortNum = "1", grid = true, col = 12)
    private BigDecimal platformDiscount;

    @FormField(title = "积分抵扣", sortNum = "1", grid = true, col = 12)
    private BigDecimal integralDiscount;

    @FormField(title = "使用积分", sortNum = "1", grid = true, col = 12)
    private BigDecimal useIntegral;

    @FormField(title = "使用红包", sortNum = "1", grid = true, col = 12)
    private BigDecimal useRedPacket;

    @FormField(title = "单件商品红包抵扣", sortNum = "1", grid = true, col = 12)
    private BigDecimal redPacketDiscount;

    //商品小计实际成交金额(扣除优惠分摊后的价格)不含运费
    @FormField(title = "实际成交金额", sortNum = "1", grid = true, col = 12)
    private BigDecimal realAmount;


    @FormField(title = "已经支付", sortNum = "1", grid = true, col = 12)
    private BigDecimal paidAmount;

    //商品小计实际用户支付金额(成交金额-积分抵扣-红包抵扣-已经支付)不含运费
    @FormField(title = "实际用户支付金额", sortNum = "1", grid = true, col = 12)
    private BigDecimal payAmount;

    @FormField(title = "是否是赠品", sortNum = "1", grid = true, col = 12)
    private Boolean giveType;

    @FormField(title = "商品sku", sortNum = "1", grid = true, col = 12)
    private String skuAttr;

    @FormField(title = "商品名称", sortNum = "1", grid = true, col = 12)
    private String name;

    @FormField(title = "商品封面", sortNum = "1", grid = true, col = 12)
    private String logo;


}
