package com.nbsaas.nbmall.cart.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.cart.data.dao.CartDao;
import com.nbsaas.nbmall.cart.data.entity.Cart;

/**
* Created by imake on 2021年12月30日22:38:35.
*/
@Repository

public class CartDaoImpl extends CriteriaDaoImpl<Cart, Long> implements CartDao {

	@Override
	public Cart findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public Cart save(Cart bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public Cart deleteById(Long id) {
		Cart entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<Cart> getEntityClass() {
		return Cart.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public Cart findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public Cart deleteById(Long tenant,Long id) {
		Cart entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}