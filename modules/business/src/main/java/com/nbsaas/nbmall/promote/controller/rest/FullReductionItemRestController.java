package com.nbsaas.nbmall.promote.controller.rest;

import com.nbsaas.nbmall.promote.api.apis.FullReductionItemApi;
import com.nbsaas.nbmall.promote.api.domain.list.FullReductionItemList;
import com.nbsaas.nbmall.promote.api.domain.page.FullReductionItemPage;
import com.nbsaas.nbmall.promote.api.domain.request.*;
import com.nbsaas.nbmall.promote.api.domain.response.FullReductionItemResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/fullreductionitem")
@RestController
public class FullReductionItemRestController extends BaseRestController {


    @RequestMapping("create")
    public FullReductionItemResponse create(FullReductionItemDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

    @RequestMapping("delete")
    public FullReductionItemResponse delete(FullReductionItemDataRequest request) {
        initTenant(request);
        FullReductionItemResponse result = new FullReductionItemResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public FullReductionItemResponse update(FullReductionItemDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public FullReductionItemResponse view(FullReductionItemDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public FullReductionItemList list(FullReductionItemSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public FullReductionItemPage search(FullReductionItemSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private FullReductionItemApi api;

}
