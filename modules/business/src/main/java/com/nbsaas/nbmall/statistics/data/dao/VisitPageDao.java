package com.nbsaas.nbmall.statistics.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.statistics.data.entity.VisitPage;

/**
* Created by imake on 2021年12月23日11:45:35.
*/
public interface VisitPageDao extends BaseDao<VisitPage,Long>{

	 VisitPage findById(Long id);

	 VisitPage save(VisitPage bean);

	 VisitPage updateByUpdater(Updater<VisitPage> updater);

	 VisitPage deleteById(Long id);

	 VisitPage findById(Long tenant, Long id);

     VisitPage deleteById(Long tenant, Long id);
}