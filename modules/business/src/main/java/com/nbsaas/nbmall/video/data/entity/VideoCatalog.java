package com.nbsaas.nbmall.video.data.entity;

import com.haoxuer.bigworld.tenant.data.entity.TenantCatalogEntity;
import com.nbsaas.codemake.annotation.CatalogClass;
import com.nbsaas.codemake.annotation.FieldConvert;
import com.nbsaas.codemake.annotation.FieldName;
import com.nbsaas.codemake.annotation.FormAnnotation;
import lombok.Data;

import javax.persistence.*;
import java.util.List;


@Data
@FormAnnotation(title = "视频分类")
@Entity
@Table(name = "bs_tenant_video_catalog")
@CatalogClass
public class VideoCatalog extends TenantCatalogEntity {


    @FieldConvert(classType = "Integer")
    @FieldName
    @ManyToOne(fetch = FetchType.LAZY)
    private VideoCatalog parent;


    @OneToMany(fetch = FetchType.LAZY,mappedBy = "parent")
    private List<VideoCatalog> children;

    @Override
    public Integer getParentId() {
        if (parent != null) {
            return parent.getId();
        }
        return null;
    }
}
