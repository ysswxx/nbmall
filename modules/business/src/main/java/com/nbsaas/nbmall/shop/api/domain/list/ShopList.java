package com.nbsaas.nbmall.shop.api.domain.list;


import com.nbsaas.nbmall.shop.api.domain.simple.ShopSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年12月10日17:38:00.
*/

@Data
public class ShopList  extends ResponseList<ShopSimple> {

}