package com.nbsaas.nbmall.product.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import com.nbsaas.nbmall.product.api.domain.simple.ProductImageSimple;
import com.nbsaas.nbmall.product.api.domain.simple.ProductSkuSimple;
import com.nbsaas.nbmall.product.api.domain.simple.ProductSpecSimple;
import lombok.Data;

import java.math.BigDecimal;

import com.nbsaas.nbmall.product.data.enums.ProductState;
import com.haoxuer.discover.data.enums.StoreState;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Created by BigWorld on 2021年12月28日18:00:18.
 */

@Data
public class ProductResponse extends ResponseObject {

    private Long id;

    private String summary;
    private String thumbnail;
    private StoreState storeState;
    private Long creator;
    private BigDecimal salePrice;
    private Double weight;
    private String productGroupName;
    private Long productGroup;
    private Double length;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date addDate;
    private ProductState productState;
    private BigDecimal costPrice;
    private Boolean skuEnable;
    private Double volume;
    private Double netWeight;
    private String unit;
    private String name;
    private Integer warningValue;
    private Boolean invoice;
    private Double width;
    private Long shop;
    private String note;
    private BigDecimal marketPrice;
    private String productCatalogName;
    private String shopName;
    private Integer inventory;
    private Integer productCatalog;
    private String demo;
    private String barCode;
    private BigDecimal score;
    private Integer sortNum;
    private String logo;
    private Double height;
    private BigDecimal vipPrice;

    private String productStateName;
    private String storeStateName;

    private List<ProductSpecSimple> specs;

    private List<ProductImageSimple> images;

    private List<ProductSkuSimple> skus;

    private List<String> catalogs;
}