package com.nbsaas.nbmall.shop.rest.convert;

import com.nbsaas.nbmall.shop.api.domain.simple.ShopCatalogSimple;
import com.nbsaas.nbmall.shop.data.entity.ShopCatalog;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import lombok.Data;


@Data
public class ShopCatalogSimpleConvert implements Conver<ShopCatalogSimple, ShopCatalog> {

    private int fetch;

    @Override
    public ShopCatalogSimple conver(ShopCatalog source) {
        ShopCatalogSimple result = new ShopCatalogSimple();

         result.setLabel(source.getName());
         result.setValue(""+source.getId());
         if (fetch!=0&&source.getChildren()!=null&&source.getChildren().size()>0){
             result.setChildren(ConverResourceUtils.converList(source.getChildren(),this));
         }
            result.setId(source.getId());
            if(source.getParent()!=null){
               result.setParent(source.getParent().getId());
            }
             result.setCode(source.getCode());
             result.setLevelInfo(source.getLevelInfo());
             if(source.getParent()!=null){
                result.setParentName(source.getParent().getName());
             }
             result.setSortNum(source.getSortNum());
             result.setIds(source.getIds());
             result.setLastDate(source.getLastDate());
             result.setName(source.getName());
             result.setAddDate(source.getAddDate());

        return result;
    }
}
