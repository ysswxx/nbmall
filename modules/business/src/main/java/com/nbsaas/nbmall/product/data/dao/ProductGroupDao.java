package com.nbsaas.nbmall.product.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.product.data.entity.ProductGroup;

/**
* Created by imake on 2021年12月28日17:59:58.
*/
public interface ProductGroupDao extends BaseDao<ProductGroup,Long>{

	 ProductGroup findById(Long id);

	 ProductGroup save(ProductGroup bean);

	 ProductGroup updateByUpdater(Updater<ProductGroup> updater);

	 ProductGroup deleteById(Long id);

	 ProductGroup findById(Long tenant, Long id);

     ProductGroup deleteById(Long tenant, Long id);
}