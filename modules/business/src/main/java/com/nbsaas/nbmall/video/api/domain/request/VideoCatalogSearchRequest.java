package com.nbsaas.nbmall.video.api.domain.request;

import com.haoxuer.bigworld.member.api.domain.request.TenantPageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2022年03月16日21:25:37.
*/

@Data
public class VideoCatalogSearchRequest extends TenantPageRequest {



     private int fetch;

     @Search(name = "levelInfo",operator = Filter.Operator.eq)
     private Integer level;
}