package com.nbsaas.nbmall.third.service;

import me.chanjar.weixin.mp.api.WxMpService;

public interface WxService {
    WxMpService getWxMpService(Long tenant);
}
