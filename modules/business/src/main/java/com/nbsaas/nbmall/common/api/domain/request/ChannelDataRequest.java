package com.nbsaas.nbmall.common.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月10日15:46:16.
*/

@Data
public class ChannelDataRequest extends BaseRequest {

    private Long id;

     private String note;

     private Long creator;

     private String logo;

     private String name;

     private String url;


}