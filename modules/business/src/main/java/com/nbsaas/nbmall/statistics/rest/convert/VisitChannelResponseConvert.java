package com.nbsaas.nbmall.statistics.rest.convert;

import com.nbsaas.nbmall.statistics.api.domain.response.VisitChannelResponse;
import com.nbsaas.nbmall.statistics.data.entity.VisitChannel;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class VisitChannelResponseConvert implements Conver<VisitChannelResponse, VisitChannel> {
    @Override
    public VisitChannelResponse conver(VisitChannel source) {
        VisitChannelResponse result = new VisitChannelResponse();
        TenantBeanUtils.copyProperties(source,result);


        return result;
    }
}
