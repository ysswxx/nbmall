package com.nbsaas.nbmall.order.controller.rest;

import com.nbsaas.nbmall.order.api.apis.OrderRefundApi;
import com.nbsaas.nbmall.order.api.domain.list.OrderRefundList;
import com.nbsaas.nbmall.order.api.domain.page.OrderRefundPage;
import com.nbsaas.nbmall.order.api.domain.request.*;
import com.nbsaas.nbmall.order.api.domain.response.OrderRefundResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/orderrefund")
@RestController
public class OrderRefundRestController extends BaseRestController {


    @RequestMapping("create")
    public OrderRefundResponse create(OrderRefundDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

    @RequestMapping("delete")
    public OrderRefundResponse delete(OrderRefundDataRequest request) {
        initTenant(request);
        OrderRefundResponse result = new OrderRefundResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public OrderRefundResponse update(OrderRefundDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public OrderRefundResponse view(OrderRefundDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public OrderRefundList list(OrderRefundSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public OrderRefundPage search(OrderRefundSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private OrderRefundApi api;

}
