package com.nbsaas.nbmall.shop.data.enums;

public enum SubjectType {
    shop,platform;
    @Override
    public String toString() {
        if (name().equals("shop")) {
            return "商家";
        } else if (name().equals("platform")) {
            return "平台";
        }
        return super.toString();
    }
}
