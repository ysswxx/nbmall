package com.nbsaas.nbmall.cart.api.apis;


import com.haoxuer.bigworld.gather.api.domain.response.MapResponse;
import com.nbsaas.nbmall.cart.api.domain.list.CartList;
import com.nbsaas.nbmall.cart.api.domain.page.CartPage;
import com.nbsaas.nbmall.cart.api.domain.request.*;
import com.nbsaas.nbmall.cart.api.domain.response.CartResponse;

public interface CartApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    CartResponse create(CartDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    CartResponse update(CartDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    CartResponse delete(CartDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     CartResponse view(CartDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    CartList list(CartSearchRequest request);


    MapResponse my(CartSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    CartPage search(CartSearchRequest request);

}