package com.nbsaas.nbmall.shop.data.entity;

import com.haoxuer.bigworld.member.data.entity.TenantUser;
import com.haoxuer.discover.data.enums.StoreState;
import com.nbsaas.codemake.annotation.FieldConvert;
import com.nbsaas.codemake.annotation.FieldName;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.SearchItem;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Data
@FormAnnotation(title = "商家员工管理", model = "商家员工", menu = "1,101,102")
@Entity
@Table(name = "bs_tenant_shop_staff")
public class ShopStaff  extends ShopEntity {


    @SearchItem(label = "用户",name = "tenantUser",key = "tenantUser.id",classType = "Long",operator = "eq")
    @FieldName
    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private TenantUser tenantUser;

    private StoreState storeState;
}
