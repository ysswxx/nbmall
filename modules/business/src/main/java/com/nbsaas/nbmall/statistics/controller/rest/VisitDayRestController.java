package com.nbsaas.nbmall.statistics.controller.rest;

import com.nbsaas.nbmall.statistics.api.apis.VisitDayApi;
import com.nbsaas.nbmall.statistics.api.domain.list.VisitDayList;
import com.nbsaas.nbmall.statistics.api.domain.page.VisitDayPage;
import com.nbsaas.nbmall.statistics.api.domain.request.*;
import com.nbsaas.nbmall.statistics.api.domain.response.VisitDayResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/visitday")
@RestController
public class VisitDayRestController extends BaseRestController {


    @RequestMapping("create")
    public VisitDayResponse create(VisitDayDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

    @RequestMapping("delete")
    public VisitDayResponse delete(VisitDayDataRequest request) {
        initTenant(request);
        VisitDayResponse result = new VisitDayResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public VisitDayResponse update(VisitDayDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public VisitDayResponse view(VisitDayDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public VisitDayList list(VisitDaySearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public VisitDayPage search(VisitDaySearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private VisitDayApi api;

}
