package com.nbsaas.nbmall.product.rest.resource;

import com.nbsaas.nbmall.product.api.apis.FreightTemplateApi;
import com.nbsaas.nbmall.product.api.domain.list.FreightTemplateList;
import com.nbsaas.nbmall.product.api.domain.page.FreightTemplatePage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.FreightTemplateResponse;
import com.nbsaas.nbmall.product.data.dao.FreightTemplateDao;
import com.nbsaas.nbmall.product.data.entity.FreightTemplate;
import com.nbsaas.nbmall.product.rest.convert.FreightTemplateResponseConvert;
import com.nbsaas.nbmall.product.rest.convert.FreightTemplateSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class FreightTemplateResource implements FreightTemplateApi {

    @Autowired
    private FreightTemplateDao dataDao;



    @Override
    public FreightTemplateResponse create(FreightTemplateDataRequest request) {
        FreightTemplateResponse result = new FreightTemplateResponse();

        FreightTemplate bean = new FreightTemplate();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new FreightTemplateResponseConvert().conver(bean);
        return result;
    }

    @Override
    public FreightTemplateResponse update(FreightTemplateDataRequest request) {
        FreightTemplateResponse result = new FreightTemplateResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        FreightTemplate bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new FreightTemplateResponseConvert().conver(bean);
        return result;
    }

    private void handleData(FreightTemplateDataRequest request, FreightTemplate bean) {
       TenantBeanUtils.copyProperties(request,bean);

    }

    @Override
    public FreightTemplateResponse delete(FreightTemplateDataRequest request) {
        FreightTemplateResponse result = new FreightTemplateResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public FreightTemplateResponse view(FreightTemplateDataRequest request) {
        FreightTemplateResponse result=new FreightTemplateResponse();
        FreightTemplate bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new FreightTemplateResponseConvert().conver(bean);
        return result;
    }
    @Override
    public FreightTemplateList list(FreightTemplateSearchRequest request) {
        FreightTemplateList result = new FreightTemplateList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<FreightTemplate> organizations = dataDao.list(0, request.getSize(), filters, orders);

        FreightTemplateSimpleConvert convert=new FreightTemplateSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public FreightTemplatePage search(FreightTemplateSearchRequest request) {
        FreightTemplatePage result=new FreightTemplatePage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<FreightTemplate> page=dataDao.page(pageable);

        FreightTemplateSimpleConvert convert=new FreightTemplateSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
