package com.nbsaas.nbmall.promote.api.domain.page;


import com.nbsaas.nbmall.promote.api.domain.simple.FullReductionSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年12月26日12:04:21.
*/

@Data
public class FullReductionPage  extends ResponsePage<FullReductionSimple> {

}