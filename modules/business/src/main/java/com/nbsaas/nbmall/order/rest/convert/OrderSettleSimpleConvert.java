package com.nbsaas.nbmall.order.rest.convert;

import com.nbsaas.nbmall.order.api.domain.simple.OrderSettleSimple;
import com.nbsaas.nbmall.order.data.entity.OrderSettle;
import com.haoxuer.discover.data.rest.core.Conver;
public class OrderSettleSimpleConvert implements Conver<OrderSettleSimple, OrderSettle> {


    @Override
    public OrderSettleSimple conver(OrderSettle source) {
        OrderSettleSimple result = new OrderSettleSimple();

            result.setId(source.getId());
            if(source.getShop()!=null){
               result.setShop(source.getShop().getId());
            }
             result.setMealSubject(source.getMealSubject());
             result.setFreight(source.getFreight());
             result.setPlatformBasic(source.getPlatformBasic());
             result.setMoney(source.getMoney());
             result.setPayAmount(source.getPayAmount());
             result.setPlatformFee(source.getPlatformFee());
             result.setAddDate(source.getAddDate());
             result.setPlatformBalance(source.getPlatformBalance());
            if(source.getOrderForm()!=null){
               result.setOrderForm(source.getOrderForm().getId());
            }
             result.setBalance(source.getBalance());
             result.setPlatformRate(source.getPlatformRate());
             result.setFreightSubject(source.getFreightSubject());
             result.setSettleType(source.getSettleType());

             result.setFreightSubjectName(source.getFreightSubject()+"");
             result.setSettleTypeName(source.getSettleType()+"");
             result.setMealSubjectName(source.getMealSubject()+"");
        return result;
    }
}
