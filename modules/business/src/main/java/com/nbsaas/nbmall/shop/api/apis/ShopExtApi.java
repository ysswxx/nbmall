package com.nbsaas.nbmall.shop.api.apis;


import com.nbsaas.nbmall.shop.api.domain.list.ShopExtList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopExtPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopExtResponse;

public interface ShopExtApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    ShopExtResponse create(ShopExtDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    ShopExtResponse update(ShopExtDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    ShopExtResponse delete(ShopExtDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     ShopExtResponse view(ShopExtDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    ShopExtList list(ShopExtSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    ShopExtPage search(ShopExtSearchRequest request);

}