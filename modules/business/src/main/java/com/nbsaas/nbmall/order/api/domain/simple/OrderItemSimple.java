package com.nbsaas.nbmall.order.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by BigWorld on 2021年12月10日17:38:01.
*/
@Data
public class OrderItemSimple implements Serializable {

    private Long id;

     private Integer refundState;
     private BigDecimal platformDiscount;
     private BigDecimal weight;
     private BigDecimal payAmount;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private Boolean giveType;
     private BigDecimal subtotal;
     private Integer size;
     private String unit;
     private Integer returnNum;
     private String name;
     private BigDecimal paidAmount;
     private BigDecimal realPrice;
     private String skuAttr;
     private BigDecimal integralDiscount;
     private BigDecimal freight;
     private BigDecimal discountAmount;
     private BigDecimal useIntegral;
     private Integer surplusNum;
     private BigDecimal useRedPacket;
     private BigDecimal returnAmount;
     private BigDecimal realAmount;
     private Integer useNum;
     private BigDecimal price;
     private String logo;
     private BigDecimal redPacketDiscount;


}
