package com.nbsaas.nbmall.device.rest.convert;

import com.nbsaas.nbmall.device.api.domain.simple.DeviceTypeSimple;
import com.nbsaas.nbmall.device.data.entity.DeviceType;
import com.haoxuer.discover.data.rest.core.Conver;
public class DeviceTypeSimpleConvert implements Conver<DeviceTypeSimple, DeviceType> {


    @Override
    public DeviceTypeSimple conver(DeviceType source) {
        DeviceTypeSimple result = new DeviceTypeSimple();

            result.setId(source.getId());
             result.setBeginDate(source.getBeginDate());
             result.setNote(source.getNote());
             result.setWebsite(source.getWebsite());
            if(source.getCreator()!=null){
               result.setCreator(source.getCreator().getId());
            }
             result.setDoc(source.getDoc());
             result.setName(source.getName());
             if(source.getCreator()!=null){
                result.setCreatorName(source.getCreator().getName());
             }
             result.setClassName(source.getClassName());
             result.setAddDate(source.getAddDate());

        return result;
    }
}
