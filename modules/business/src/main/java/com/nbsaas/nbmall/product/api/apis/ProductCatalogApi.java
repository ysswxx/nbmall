package com.nbsaas.nbmall.product.api.apis;


import com.nbsaas.nbmall.product.api.domain.list.ProductCatalogList;
import com.nbsaas.nbmall.product.api.domain.page.ProductCatalogPage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductCatalogResponse;

public interface ProductCatalogApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    ProductCatalogResponse create(ProductCatalogDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    ProductCatalogResponse update(ProductCatalogDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    ProductCatalogResponse delete(ProductCatalogDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     ProductCatalogResponse view(ProductCatalogDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    ProductCatalogList list(ProductCatalogSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    ProductCatalogPage search(ProductCatalogSearchRequest request);

}