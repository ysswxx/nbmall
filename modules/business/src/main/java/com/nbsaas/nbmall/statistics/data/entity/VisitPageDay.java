package com.nbsaas.nbmall.statistics.data.entity;


import com.haoxuer.bigworld.member.data.entity.TenantUser;
import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.ComposeView;
import com.nbsaas.codemake.annotation.FieldConvert;
import com.nbsaas.codemake.annotation.FormAnnotation;
import lombok.Data;

import javax.persistence.*;

@ComposeView
@Data
@FormAnnotation(title = "用户每日访问", model = "用户每日访问", menu = "1,101,102")
@Entity
@Table(name = "bs_tenant_visit_page_day")
public class VisitPageDay extends TenantEntity {

    //2021-07-12
    @Column(name = "data_key", length = 12)
    private String key;

    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private TenantUser user;

    private Long pageNum;

}
