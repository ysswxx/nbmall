package com.nbsaas.nbmall.promote.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.promote.data.dao.FullReductionItemDao;
import com.nbsaas.nbmall.promote.data.entity.FullReductionItem;

/**
* Created by imake on 2021年12月25日12:45:48.
*/
@Repository

public class FullReductionItemDaoImpl extends CriteriaDaoImpl<FullReductionItem, Long> implements FullReductionItemDao {

	@Override
	public FullReductionItem findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public FullReductionItem save(FullReductionItem bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public FullReductionItem deleteById(Long id) {
		FullReductionItem entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<FullReductionItem> getEntityClass() {
		return FullReductionItem.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public FullReductionItem findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public FullReductionItem deleteById(Long tenant,Long id) {
		FullReductionItem entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}