package com.nbsaas.nbmall.video.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.List;

/**
*
* Created by BigWorld on 2022年03月16日21:25:37.
*/

@Data
public class VideoCatalogSimple implements Serializable {
    private Integer id;
    private String value;
    private String label;
    private List<VideoCatalogSimple> children;

     private Integer parent;
     private String code;
     private Integer levelInfo;
     private String parentName;
     private Integer sortNum;
     private String ids;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date lastDate;
     private String name;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;

}
