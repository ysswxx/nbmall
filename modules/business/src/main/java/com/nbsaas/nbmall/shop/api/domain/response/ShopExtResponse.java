package com.nbsaas.nbmall.shop.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
/**
*
* Created by BigWorld on 2021年12月10日17:38:00.
*/

@Data
public class ShopExtResponse extends ResponseObject {

    private Long id;

     private Long shop;
     private String note;
     private String cardNoFront;
     private String contact;
     private String cardNo;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private String license;
     private String letter;
     private String phone;
     private String cardNoReverse;
     private String name;
     private String permit;
     private String cardNoHand;

}