package com.nbsaas.nbmall.shop.api.domain.request;

import com.haoxuer.bigworld.member.api.domain.request.TenantPageRequest;
import com.haoxuer.discover.data.enums.StoreState;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import com.nbsaas.nbmall.shop.data.enums.AuditState;
import com.nbsaas.nbmall.shop.data.enums.ShopState;
import lombok.Data;

import java.util.Date;

/**
 * Created by imake on 2021年12月10日17:38:00.
 */

@Data
public class ShopSearchRequest extends TenantPageRequest {

    //商家名称
    @Search(name = "name", operator = Filter.Operator.like)
    private String name;

    //地址
    @Search(name = "address", operator = Filter.Operator.like)
    private String address;

    //状态
    @Search(name = "shopState", operator = Filter.Operator.eq)
    private ShopState shopState;

    @Search(name = "auditState", operator = Filter.Operator.eq)
    private AuditState auditState;

    @Search(name = "shopCatalog.id", operator = Filter.Operator.eq)
    private Integer shopCatalog;

    @Search(name = "storeState", operator = Filter.Operator.eq)
    private StoreState storeState=StoreState.normal;

    private Long shop;

    private String sortType;

    private Double lat;

    private Double lng;


}