package com.nbsaas.nbmall.device.api.apis;


import com.nbsaas.nbmall.device.api.domain.list.DeviceTypeList;
import com.nbsaas.nbmall.device.api.domain.page.DeviceTypePage;
import com.nbsaas.nbmall.device.api.domain.request.*;
import com.nbsaas.nbmall.device.api.domain.response.DeviceTypeResponse;

public interface DeviceTypeApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    DeviceTypeResponse create(DeviceTypeDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    DeviceTypeResponse update(DeviceTypeDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    DeviceTypeResponse delete(DeviceTypeDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     DeviceTypeResponse view(DeviceTypeDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    DeviceTypeList list(DeviceTypeSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    DeviceTypePage search(DeviceTypeSearchRequest request);

}