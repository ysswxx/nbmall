package com.nbsaas.nbmall.shop.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.shop.data.dao.ShopCatalogDao;
import com.nbsaas.nbmall.shop.data.entity.ShopCatalog;
import com.haoxuer.discover.data.core.CatalogDaoImpl;

/**
* Created by imake on 2021年12月10日17:38:00.
*/
@Repository

public class ShopCatalogDaoImpl extends CatalogDaoImpl<ShopCatalog, Integer> implements ShopCatalogDao {

	@Override
	public ShopCatalog findById(Integer id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public ShopCatalog save(ShopCatalog bean) {

		add(bean);
		
		
		return bean;
	}

    @Override
	public ShopCatalog deleteById(Integer id) {
		ShopCatalog entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<ShopCatalog> getEntityClass() {
		return ShopCatalog.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public ShopCatalog findById(Long tenant,Integer id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public ShopCatalog deleteById(Long tenant,Integer id) {
		ShopCatalog entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}