package com.nbsaas.nbmall.order.rest.convert;

import com.nbsaas.nbmall.order.api.domain.simple.OrderFormSimple;
import com.nbsaas.nbmall.order.data.entity.OrderForm;
import com.haoxuer.discover.data.rest.core.Conver;
public class OrderFormSimpleConvert implements Conver<OrderFormSimple, OrderForm> {


    @Override
    public OrderFormSimple conver(OrderForm source) {
        OrderFormSimple result = new OrderFormSimple();

            result.setId(source.getId());
             result.setReturnCount(source.getReturnCount());
             result.setConsignee(source.getConsignee());
             result.setOrderSource(source.getOrderSource());
             result.setEditPriceTime(source.getEditPriceTime());
             result.setSysReceiveTime(source.getSysReceiveTime());
             result.setAddress(source.getAddress());
            if(source.getCreator()!=null){
               result.setCreator(source.getCreator().getId());
            }
             result.setPlatformDiscount(source.getPlatformDiscount());
             result.setStoreState(source.getStoreState());
             result.setWeight(source.getWeight());
             result.setPaySource(source.getPaySource());
             result.setPayAmount(source.getPayAmount());
             result.setAddDate(source.getAddDate());
             result.setAmount(source.getAmount());
             result.setReceiveTime(source.getReceiveTime());
             result.setCommentState(source.getCommentState());
             result.setProductCount(source.getProductCount());
             result.setOutTradeNum(source.getOutTradeNum());
             result.setSendIntegral(source.getSendIntegral());
             result.setTotalAmount(source.getTotalAmount());
             result.setPhone(source.getPhone());
            if(source.getUser()!=null){
               result.setUser(source.getUser().getId());
            }
             result.setIntegralDiscount(source.getIntegralDiscount());
            if(source.getShop()!=null){
               result.setShop(source.getShop().getId());
            }
             result.setNo(source.getNo());
             result.setNote(source.getNote());
             result.setChangeAmount(source.getChangeAmount());
             result.setNewRefundId(source.getNewRefundId());
             result.setPayTime(source.getPayTime());
             result.setDeliveryTime(source.getDeliveryTime());
             result.setFreight(source.getFreight());
             if(source.getShop()!=null){
                result.setShopName(source.getShop().getName());
             }
             result.setDiscount(source.getDiscount());
             result.setUseIntegral(source.getUseIntegral());
             result.setPayState(source.getPayState());
             result.setOrderState(source.getOrderState());
             result.setOrderType(source.getOrderType());
             result.setMergeOrderCode(source.getMergeOrderCode());
             result.setUseRedPacket(source.getUseRedPacket());
             result.setReturnAmount(source.getReturnAmount());
             result.setRemindTime(source.getRemindTime());
             result.setOrderTime(source.getOrderTime());
             result.setUserState(source.getUserState());
             result.setState(source.getState());
             result.setRedPacketDiscount(source.getRedPacketDiscount());
             result.setFinishTime(source.getFinishTime());

             result.setPayStateName(source.getPayState()+"");
             result.setOrderStateName(source.getOrderState()+"");
             result.setStoreStateName(source.getStoreState()+"");
        return result;
    }
}
