package com.nbsaas.nbmall.shop.controller.tenant;

import com.nbsaas.nbmall.shop.api.apis.ShopCollectApi;
import com.nbsaas.nbmall.shop.api.domain.list.ShopCollectList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopCollectPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopCollectResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/shopcollect")
@RestController
public class ShopCollectTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("shopcollect")
    @RequestMapping("create")
    public ShopCollectResponse create(ShopCollectDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

	@RequiresPermissions("shopcollect")
    @RequestMapping("delete")
    public ShopCollectResponse delete(ShopCollectDataRequest request) {
        initTenant(request);
        ShopCollectResponse result = new ShopCollectResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("shopcollect")
    @RequestMapping("update")
    public ShopCollectResponse update(ShopCollectDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("shopcollect")
    @RequestMapping("view")
    public ShopCollectResponse view(ShopCollectDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("shopcollect")
    @RequestMapping("list")
    public ShopCollectList list(ShopCollectSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("shopcollect")
    @RequestMapping("search")
    public ShopCollectPage search(ShopCollectSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private ShopCollectApi api;

}
