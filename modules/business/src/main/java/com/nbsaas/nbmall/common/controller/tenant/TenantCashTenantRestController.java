package com.nbsaas.nbmall.common.controller.tenant;

import com.nbsaas.nbmall.common.api.apis.TenantCashApi;
import com.nbsaas.nbmall.common.api.domain.list.TenantCashList;
import com.nbsaas.nbmall.common.api.domain.page.TenantCashPage;
import com.nbsaas.nbmall.common.api.domain.request.*;
import com.nbsaas.nbmall.common.api.domain.response.TenantCashResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/tenantcash")
@RestController
public class TenantCashTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("tenantcash")
    @RequestMapping("create")
    public TenantCashResponse create(TenantCashDataRequest request) {
        initTenant(request);
        request.setCreator(request.getCreateUser());
        return api.create(request);
    }

	@RequiresPermissions("tenantcash")
    @RequestMapping("delete")
    public TenantCashResponse delete(TenantCashDataRequest request) {
        initTenant(request);
        TenantCashResponse result = new TenantCashResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("tenantcash")
    @RequestMapping("update")
    public TenantCashResponse update(TenantCashDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("tenantcash")
    @RequestMapping("view")
    public TenantCashResponse view(TenantCashDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("tenantcash")
    @RequestMapping("list")
    public TenantCashList list(TenantCashSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("tenantcash")
    @RequestMapping("search")
    public TenantCashPage search(TenantCashSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @RequiresPermissions("tenantcash")
    @RequestMapping("back")
    public TenantCashResponse back(TenantCashDataRequest request) {
        initTenant(request);
        return api.back(request);
    }

    @RequiresPermissions("tenantcash")
    @RequestMapping("send")
    public TenantCashResponse send(TenantCashDataRequest request) {
        initTenant(request);
        return api.send(request);
    }

    @Autowired
    private TenantCashApi api;

}
