package com.nbsaas.nbmall.order.api.apis;


import com.nbsaas.nbmall.order.api.domain.list.OrderRefundList;
import com.nbsaas.nbmall.order.api.domain.page.OrderRefundPage;
import com.nbsaas.nbmall.order.api.domain.request.*;
import com.nbsaas.nbmall.order.api.domain.response.OrderRefundResponse;

public interface OrderRefundApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    OrderRefundResponse create(OrderRefundDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    OrderRefundResponse update(OrderRefundDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    OrderRefundResponse delete(OrderRefundDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     OrderRefundResponse view(OrderRefundDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    OrderRefundList list(OrderRefundSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    OrderRefundPage search(OrderRefundSearchRequest request);

}