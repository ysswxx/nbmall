package com.nbsaas.nbmall.product.rest.convert;

import com.nbsaas.nbmall.product.api.domain.simple.ProductImageSimple;
import com.nbsaas.nbmall.product.data.entity.ProductImage;
import com.haoxuer.discover.data.rest.core.Conver;
public class ProductImageSimpleConvert implements Conver<ProductImageSimple, ProductImage> {


    @Override
    public ProductImageSimple conver(ProductImage source) {
        ProductImageSimple result = new ProductImageSimple();

            result.setId(source.getId());
            if(source.getProduct()!=null){
               result.setProduct(source.getProduct().getId());
            }
             result.setAddDate(source.getAddDate());
             result.setUrl(source.getUrl());

        return result;
    }
}
