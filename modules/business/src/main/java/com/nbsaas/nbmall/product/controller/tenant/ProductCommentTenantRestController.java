package com.nbsaas.nbmall.product.controller.tenant;

import com.nbsaas.nbmall.product.api.apis.ProductCommentApi;
import com.nbsaas.nbmall.product.api.domain.list.ProductCommentList;
import com.nbsaas.nbmall.product.api.domain.page.ProductCommentPage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductCommentResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/productcomment")
@RestController
public class ProductCommentTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("productcomment")
    @RequestMapping("create")
    public ProductCommentResponse create(ProductCommentDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

	@RequiresPermissions("productcomment")
    @RequestMapping("delete")
    public ProductCommentResponse delete(ProductCommentDataRequest request) {
        initTenant(request);
        ProductCommentResponse result = new ProductCommentResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("productcomment")
    @RequestMapping("update")
    public ProductCommentResponse update(ProductCommentDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("productcomment")
    @RequestMapping("view")
    public ProductCommentResponse view(ProductCommentDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("productcomment")
    @RequestMapping("list")
    public ProductCommentList list(ProductCommentSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("productcomment")
    @RequestMapping("search")
    public ProductCommentPage search(ProductCommentSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private ProductCommentApi api;

}
