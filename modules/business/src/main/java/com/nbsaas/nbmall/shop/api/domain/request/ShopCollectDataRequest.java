package com.nbsaas.nbmall.shop.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import com.haoxuer.discover.data.enums.StoreState;
import java.util.Date;

/**
*
* Created by imake on 2021年12月10日17:38:00.
*/

@Data
public class ShopCollectDataRequest extends TenantRequest {

    private Long id;

     private Long tenantUser;
     private Long shop;
     private StoreState storeState;

}