package com.nbsaas.nbmall.promote.controller.admin;


import com.haoxuer.bigworld.tenant.controller.admin.TenantBaseAction;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
*
* Created by imake on 2021年12月26日12:04:21.
*/


@Scope("prototype")
@Controller
public class FullReductionAction extends TenantBaseAction {

	@RequiresPermissions("fullreduction")
	@RequestMapping("/tenant/fullreduction/view_list")
	public String list(ModelMap model) {
		return getView("fullreduction/list");
	}

}