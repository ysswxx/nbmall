package com.nbsaas.nbmall.product.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.product.data.dao.ProductSpecDao;
import com.nbsaas.nbmall.product.data.entity.ProductSpec;

/**
* Created by imake on 2021年12月10日17:38:01.
*/
@Repository

public class ProductSpecDaoImpl extends CriteriaDaoImpl<ProductSpec, Long> implements ProductSpecDao {

	@Override
	public ProductSpec findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public ProductSpec save(ProductSpec bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public ProductSpec deleteById(Long id) {
		ProductSpec entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<ProductSpec> getEntityClass() {
		return ProductSpec.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public ProductSpec findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public ProductSpec deleteById(Long tenant,Long id) {
		ProductSpec entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public void deleteByProduct(Long id) {
		getSession().createQuery("delete from ProductSpec d where d.product.id="+id).executeUpdate();

	}
}