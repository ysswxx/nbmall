package com.nbsaas.nbmall.product.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.product.data.dao.ProductCatalogDao;
import com.nbsaas.nbmall.product.data.entity.ProductCatalog;
import com.haoxuer.discover.data.core.CatalogDaoImpl;

/**
* Created by imake on 2021年12月10日17:38:01.
*/
@Repository

public class ProductCatalogDaoImpl extends CatalogDaoImpl<ProductCatalog, Integer> implements ProductCatalogDao {

	@Override
	public ProductCatalog findById(Integer id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public ProductCatalog save(ProductCatalog bean) {

		add(bean);
		
		
		return bean;
	}

    @Override
	public ProductCatalog deleteById(Integer id) {
		ProductCatalog entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<ProductCatalog> getEntityClass() {
		return ProductCatalog.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public ProductCatalog findById(Long tenant,Integer id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public ProductCatalog deleteById(Long tenant,Integer id) {
		ProductCatalog entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}