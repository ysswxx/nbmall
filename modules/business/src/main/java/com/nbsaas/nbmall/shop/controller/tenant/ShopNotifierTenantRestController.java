package com.nbsaas.nbmall.shop.controller.tenant;

import com.nbsaas.nbmall.shop.api.apis.ShopNotifierApi;
import com.nbsaas.nbmall.shop.api.domain.list.ShopNotifierList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopNotifierPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopNotifierResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/shopnotifier")
@RestController
public class ShopNotifierTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("shop")
    @RequestMapping("create")
    public ShopNotifierResponse create(ShopNotifierDataRequest request) {
        initTenant(request);
        request.setCreator(request.getCreateUser());
        return api.create(request);
    }

	@RequiresPermissions("shop")
    @RequestMapping("delete")
    public ShopNotifierResponse delete(ShopNotifierDataRequest request) {
        initTenant(request);
        ShopNotifierResponse result = new ShopNotifierResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("shop")
    @RequestMapping("update")
    public ShopNotifierResponse update(ShopNotifierDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("shop")
    @RequestMapping("view")
    public ShopNotifierResponse view(ShopNotifierDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("shop")
    @RequestMapping("list")
    public ShopNotifierList list(ShopNotifierSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("shop")
    @RequestMapping("search")
    public ShopNotifierPage search(ShopNotifierSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @RequiresPermissions("shop")
    @RequestMapping("test")
    public ShopNotifierResponse test(ShopNotifierDataRequest request) {
        initTenant(request);
        return api.test(request);
    }

    @Autowired
    private ShopNotifierApi api;

}
