package com.nbsaas.nbmall.statistics.api.apis;


import com.nbsaas.nbmall.statistics.api.domain.list.VisitPageDayList;
import com.nbsaas.nbmall.statistics.api.domain.page.VisitPageDayPage;
import com.nbsaas.nbmall.statistics.api.domain.request.*;
import com.nbsaas.nbmall.statistics.api.domain.response.VisitPageDayResponse;

public interface VisitPageDayApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    VisitPageDayResponse create(VisitPageDayDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    VisitPageDayResponse update(VisitPageDayDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    VisitPageDayResponse delete(VisitPageDayDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     VisitPageDayResponse view(VisitPageDayDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    VisitPageDayList list(VisitPageDaySearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    VisitPageDayPage search(VisitPageDaySearchRequest request);

}