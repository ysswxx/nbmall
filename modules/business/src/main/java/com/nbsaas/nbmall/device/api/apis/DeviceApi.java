package com.nbsaas.nbmall.device.api.apis;


import com.nbsaas.nbmall.device.api.domain.list.DeviceList;
import com.nbsaas.nbmall.device.api.domain.page.DevicePage;
import com.nbsaas.nbmall.device.api.domain.request.*;
import com.nbsaas.nbmall.device.api.domain.response.DeviceResponse;

public interface DeviceApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    DeviceResponse create(DeviceDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    DeviceResponse update(DeviceDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    DeviceResponse delete(DeviceDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     DeviceResponse view(DeviceDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    DeviceList list(DeviceSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    DevicePage search(DeviceSearchRequest request);

}