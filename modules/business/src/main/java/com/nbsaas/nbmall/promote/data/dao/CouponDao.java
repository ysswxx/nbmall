package com.nbsaas.nbmall.promote.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.promote.data.entity.Coupon;

/**
* Created by imake on 2021年12月25日12:45:48.
*/
public interface CouponDao extends BaseDao<Coupon,Long>{

	 Coupon findById(Long id);

	 Coupon save(Coupon bean);

	 Coupon updateByUpdater(Updater<Coupon> updater);

	 Coupon deleteById(Long id);

	 Coupon findById(Long tenant, Long id);

     Coupon deleteById(Long tenant, Long id);
}