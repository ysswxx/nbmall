package com.nbsaas.nbmall.order.data.enums;

public enum RefundState {

    init,success;


    @Override
    public String toString() {
        if (name().equals("init")) {
            return "待退款";
        } else if (name().equals("success")) {
            return "已退款";
        }
        return super.toString();
    }
}
