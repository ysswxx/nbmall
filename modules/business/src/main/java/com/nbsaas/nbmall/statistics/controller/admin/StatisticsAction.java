package com.nbsaas.nbmall.statistics.controller.admin;

import com.haoxuer.bigworld.tenant.controller.admin.TenantBaseAction;
import com.haoxuer.discover.data.page.Pageable;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;


@Scope("prototype")
@Controller
public class StatisticsAction extends TenantBaseAction {

    @RequiresPermissions("statistics_home")
    @RequestMapping("/tenant/statistics/home")
    public String list(Pageable pageable, ModelMap model) {
        return getView("statistics/home");
    }

    @RequiresPermissions("statistics_customer")
    @RequestMapping("/tenant/statistics/customer")
    public String customer(Pageable pageable, ModelMap model) {
        return getView("statistics/customer");
    }


    @RequiresPermissions("statistics_kpi")
    @RequestMapping("/tenant/statistics/kpi")
    public String kpi(Pageable pageable, ModelMap model) {
        return getView("statistics/kpi");
    }

    @RequiresPermissions("statistics_goods")
    @RequestMapping("/tenant/statistics/goods")
    public String goods(Pageable pageable, ModelMap model) {
        return getView("statistics/goods");
    }

    @RequiresPermissions("statistics_area")
    @RequestMapping("/tenant/statistics/area")
    public String area(Pageable pageable, ModelMap model) {
        return getView("statistics/area");
    }
}
