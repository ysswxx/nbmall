package com.nbsaas.nbmall.statistics.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.statistics.data.entity.VisitChannel;

/**
* Created by imake on 2021年12月23日11:45:35.
*/
public interface VisitChannelDao extends BaseDao<VisitChannel,Long>{

	 VisitChannel findById(Long id);

	 VisitChannel save(VisitChannel bean);

	 VisitChannel updateByUpdater(Updater<VisitChannel> updater);

	 VisitChannel deleteById(Long id);

	 VisitChannel findById(Long tenant, Long id);

     VisitChannel deleteById(Long tenant, Long id);
}