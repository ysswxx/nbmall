package com.nbsaas.nbmall.product.rest.convert;

import com.nbsaas.nbmall.product.api.domain.simple.ProductCatalogSimple;
import com.nbsaas.nbmall.product.data.entity.ProductCatalog;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import lombok.Data;


@Data
public class ProductCatalogSimpleConvert implements Conver<ProductCatalogSimple, ProductCatalog> {

    private int fetch;

    @Override
    public ProductCatalogSimple conver(ProductCatalog source) {
        ProductCatalogSimple result = new ProductCatalogSimple();

         result.setLabel(source.getName());
         result.setValue(""+source.getId());
         if (fetch!=0&&source.getChildren()!=null&&source.getChildren().size()>0){
             result.setChildren(ConverResourceUtils.converList(source.getChildren(),this));
         }
            result.setId(source.getId());
            if(source.getParent()!=null){
               result.setParent(source.getParent().getId());
            }
             result.setCode(source.getCode());
             result.setLevelInfo(source.getLevelInfo());
             if(source.getParent()!=null){
                result.setParentName(source.getParent().getName());
             }
             result.setSortNum(source.getSortNum());
             result.setIds(source.getIds());
             result.setLastDate(source.getLastDate());
             result.setName(source.getName());
             result.setAddDate(source.getAddDate());

        return result;
    }
}
