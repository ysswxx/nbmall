package com.nbsaas.nbmall.shop.rest.convert;

import com.nbsaas.nbmall.shop.api.domain.response.ShopNotifierResponse;
import com.nbsaas.nbmall.shop.data.entity.ShopNotifier;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class ShopNotifierResponseConvert implements Conver<ShopNotifierResponse, ShopNotifier> {
    @Override
    public ShopNotifierResponse conver(ShopNotifier source) {
        ShopNotifierResponse result = new ShopNotifierResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getShop()!=null){
           result.setShop(source.getShop().getId());
        }
        if(source.getCreator()!=null){
           result.setCreator(source.getCreator().getId());
        }
         if(source.getShop()!=null){
            result.setShopName(source.getShop().getName());
         }

        return result;
    }
}
