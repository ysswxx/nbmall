package com.nbsaas.nbmall.statistics.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.statistics.data.dao.VisitChannelDao;
import com.nbsaas.nbmall.statistics.data.entity.VisitChannel;

/**
* Created by imake on 2021年12月23日11:45:35.
*/
@Repository

public class VisitChannelDaoImpl extends CriteriaDaoImpl<VisitChannel, Long> implements VisitChannelDao {

	@Override
	public VisitChannel findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public VisitChannel save(VisitChannel bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public VisitChannel deleteById(Long id) {
		VisitChannel entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<VisitChannel> getEntityClass() {
		return VisitChannel.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public VisitChannel findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public VisitChannel deleteById(Long tenant,Long id) {
		VisitChannel entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}