package com.nbsaas.nbmall.common.data.entity;


import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.FormAnnotation;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@FormAnnotation(title = "编号")
@Entity
@Table(name = "bs_tenant_serial_number")
public class SerialNumber extends TenantEntity {

    @Column(name = "data_key")
    private String key;

    private Long num;

    private Integer step;
}
