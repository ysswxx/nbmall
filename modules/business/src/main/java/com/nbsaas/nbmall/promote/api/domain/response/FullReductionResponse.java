package com.nbsaas.nbmall.promote.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import com.nbsaas.nbmall.promote.api.domain.simple.FullReductionItemSimple;
import lombok.Data;
import com.nbsaas.nbmall.promote.data.enums.FullReductionWay;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Created by BigWorld on 2021年12月26日12:04:21.
 */

@Data
public class FullReductionResponse extends ResponseObject {

    private Long id;

    private Long shop;
    private FullReductionWay fullReductionWay;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date endTime;
    private String shopName;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date beginTime;
    private String name;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date addDate;

    private String fullReductionWayName;

    private List<FullReductionItemSimple> items;

}