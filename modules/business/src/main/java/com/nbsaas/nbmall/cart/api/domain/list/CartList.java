package com.nbsaas.nbmall.cart.api.domain.list;


import com.nbsaas.nbmall.cart.api.domain.simple.CartSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年12月30日22:38:35.
*/

@Data
public class CartList  extends ResponseList<CartSimple> {

}