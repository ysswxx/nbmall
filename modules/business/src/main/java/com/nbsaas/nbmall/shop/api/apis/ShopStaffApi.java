package com.nbsaas.nbmall.shop.api.apis;


import com.nbsaas.nbmall.shop.api.domain.list.ShopStaffList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopStaffPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopStaffResponse;

public interface ShopStaffApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    ShopStaffResponse create(ShopStaffDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    ShopStaffResponse update(ShopStaffDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    ShopStaffResponse delete(ShopStaffDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     ShopStaffResponse view(ShopStaffDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    ShopStaffList list(ShopStaffSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    ShopStaffPage search(ShopStaffSearchRequest request);

}