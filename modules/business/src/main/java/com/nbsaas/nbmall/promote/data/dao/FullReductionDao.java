package com.nbsaas.nbmall.promote.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.promote.data.entity.FullReduction;

/**
* Created by imake on 2021年12月26日12:04:21.
*/
public interface FullReductionDao extends BaseDao<FullReduction,Long>{

	 FullReduction findById(Long id);

	 FullReduction save(FullReduction bean);

	 FullReduction updateByUpdater(Updater<FullReduction> updater);

	 FullReduction deleteById(Long id);

	 FullReduction findById(Long tenant, Long id);

     FullReduction deleteById(Long tenant, Long id);
}