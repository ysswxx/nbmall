package com.nbsaas.nbmall.device.api.domain.page;


import com.nbsaas.nbmall.device.api.domain.simple.DeviceTypeSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年12月10日17:40:16.
*/

@Data
public class DeviceTypePage  extends ResponsePage<DeviceTypeSimple> {

}