package com.nbsaas.nbmall.device.api.domain.list;


import com.nbsaas.nbmall.device.api.domain.simple.DeviceTypeSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年12月10日17:40:16.
*/

@Data
public class DeviceTypeList  extends ResponseList<DeviceTypeSimple> {

}