package com.nbsaas.nbmall.product.data.entity;

import com.haoxuer.bigworld.member.data.entity.TenantUser;
import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.FieldConvert;
import com.nbsaas.codemake.annotation.FormAnnotation;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Data
@FormAnnotation(title = "运费模板管理",model = "运费模板",menu = "1,107,113")
@Entity
@Table(name = "bs_tenant_product_comment")
public class ProductComment extends TenantEntity {

    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private TenantUser tenantUser;

    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    private String note;

    private Float score;

    private Float logistics;

    private Float service;

}
