package com.nbsaas.nbmall.common.api.domain.list;


import com.nbsaas.nbmall.common.api.domain.simple.ChannelSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年12月10日15:46:16.
*/

@Data
public class ChannelList  extends ResponseList<ChannelSimple> {

}