package com.nbsaas.nbmall.order.api.apis;


import com.nbsaas.nbmall.order.api.domain.list.OrderItemList;
import com.nbsaas.nbmall.order.api.domain.page.OrderItemPage;
import com.nbsaas.nbmall.order.api.domain.request.*;
import com.nbsaas.nbmall.order.api.domain.response.OrderItemResponse;

public interface OrderItemApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    OrderItemResponse create(OrderItemDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    OrderItemResponse update(OrderItemDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    OrderItemResponse delete(OrderItemDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     OrderItemResponse view(OrderItemDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    OrderItemList list(OrderItemSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    OrderItemPage search(OrderItemSearchRequest request);

}