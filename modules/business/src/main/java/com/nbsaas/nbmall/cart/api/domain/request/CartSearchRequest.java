package com.nbsaas.nbmall.cart.api.domain.request;

import com.haoxuer.bigworld.member.api.domain.request.TenantPageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月30日22:38:35.
*/

@Data
public class CartSearchRequest extends TenantPageRequest {

    //用户
     @Search(name = "tenantUser.id",operator = Filter.Operator.eq)
     private Long tenantUser;

    //商家
     @Search(name = "shop.id",operator = Filter.Operator.eq)
     private Long shop;



}