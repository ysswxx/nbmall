package com.nbsaas.nbmall.customer.rest.convert;

import com.nbsaas.nbmall.customer.api.domain.simple.CustomerAddressSimple;
import com.nbsaas.nbmall.customer.data.entity.CustomerAddress;
import com.haoxuer.discover.data.rest.core.Conver;
public class CustomerAddressSimpleConvert implements Conver<CustomerAddressSimple, CustomerAddress> {


    @Override
    public CustomerAddressSimple conver(CustomerAddress source) {
        CustomerAddressSimple result = new CustomerAddressSimple();

            result.setId(source.getId());
            if(source.getArea()!=null){
               result.setArea(source.getArea().getId());
            }
             result.setNote(source.getNote());
             result.setUpdateDate(source.getUpdateDate());
             result.setAddress(source.getAddress());
            if(source.getCity()!=null){
               result.setCity(source.getCity().getId());
            }
             result.setLng(source.getLng());
             result.setStoreState(source.getStoreState());
             result.setPostalCode(source.getPostalCode());
            if(source.getProvince()!=null){
               result.setProvince(source.getProvince().getId());
            }
             result.setLabel(source.getLabel());
             result.setAddDate(source.getAddDate());
             if(source.getCustomer()!=null){
                result.setCustomerName(source.getCustomer().getName());
             }
             result.setPhone(source.getPhone());
             if(source.getCity()!=null){
                result.setCityName(source.getCity().getName());
             }
             result.setHouseNo(source.getHouseNo());
             result.setName(source.getName());
             if(source.getProvince()!=null){
                result.setProvinceName(source.getProvince().getName());
             }
             if(source.getArea()!=null){
                result.setAreaName(source.getArea().getName());
             }
             result.setTel(source.getTel());
            if(source.getCustomer()!=null){
               result.setCustomer(source.getCustomer().getId());
            }
             result.setLat(source.getLat());

             result.setStoreStateName(source.getStoreState()+"");
        return result;
    }
}
