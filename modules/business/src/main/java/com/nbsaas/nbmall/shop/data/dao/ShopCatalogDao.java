package com.nbsaas.nbmall.shop.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.shop.data.entity.ShopCatalog;

/**
* Created by imake on 2021年12月10日17:38:00.
*/
public interface ShopCatalogDao extends BaseDao<ShopCatalog,Integer>{

	 ShopCatalog findById(Integer id);

	 ShopCatalog save(ShopCatalog bean);

	 ShopCatalog updateByUpdater(Updater<ShopCatalog> updater);

	 ShopCatalog deleteById(Integer id);

	 ShopCatalog findById(Long tenant, Integer id);

     ShopCatalog deleteById(Long tenant, Integer id);
}