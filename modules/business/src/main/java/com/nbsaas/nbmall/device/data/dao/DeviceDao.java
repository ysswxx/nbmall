package com.nbsaas.nbmall.device.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.device.data.entity.Device;

/**
* Created by imake on 2021年12月10日17:38:01.
*/
public interface DeviceDao extends BaseDao<Device,Long>{

	 Device findById(Long id);

	 Device save(Device bean);

	 Device updateByUpdater(Updater<Device> updater);

	 Device deleteById(Long id);

	 Device findById(Long tenant, Long id);

     Device deleteById(Long tenant, Long id);
}