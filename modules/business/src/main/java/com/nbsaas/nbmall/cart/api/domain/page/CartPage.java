package com.nbsaas.nbmall.cart.api.domain.page;


import com.nbsaas.nbmall.cart.api.domain.simple.CartSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年12月30日22:38:35.
*/

@Data
public class CartPage  extends ResponsePage<CartSimple> {

}