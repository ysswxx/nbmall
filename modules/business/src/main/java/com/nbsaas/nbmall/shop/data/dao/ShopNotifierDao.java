package com.nbsaas.nbmall.shop.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.shop.data.entity.ShopNotifier;

/**
* Created by imake on 2021年12月10日17:38:00.
*/
public interface ShopNotifierDao extends BaseDao<ShopNotifier,Long>{

	 ShopNotifier findById(Long id);

	 ShopNotifier save(ShopNotifier bean);

	 ShopNotifier updateByUpdater(Updater<ShopNotifier> updater);

	 ShopNotifier deleteById(Long id);

	 ShopNotifier findById(Long tenant, Long id);

     ShopNotifier deleteById(Long tenant, Long id);
}