package com.nbsaas.nbmall.shop.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.shop.data.dao.ShopHourDao;
import com.nbsaas.nbmall.shop.data.entity.ShopHour;

/**
* Created by imake on 2021年12月10日17:38:01.
*/
@Repository

public class ShopHourDaoImpl extends CriteriaDaoImpl<ShopHour, Long> implements ShopHourDao {

	@Override
	public ShopHour findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public ShopHour save(ShopHour bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public ShopHour deleteById(Long id) {
		ShopHour entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<ShopHour> getEntityClass() {
		return ShopHour.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public ShopHour findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public ShopHour deleteById(Long tenant,Long id) {
		ShopHour entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}