package com.nbsaas.nbmall.test.rest.resource;

import com.haoxuer.bigworld.pay.data.dao.PayUserDao;
import com.haoxuer.bigworld.pay.data.entity.PayUser;
import com.haoxuer.bigworld.tenant.api.domain.request.TenantRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.trade.api.domain.request.TradeRequest;
import com.haoxuer.discover.trade.data.dao.BasicTradeAccountDao;
import com.haoxuer.discover.trade.data.dao.TradeAccountDao;
import com.haoxuer.discover.trade.data.entity.TradeAccount;
import com.haoxuer.discover.trade.data.enums.ChangeType;
import com.nbsaas.nbmall.test.api.apis.TestApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


@Transactional
@Service
public class TestResource implements TestApi {

    @Autowired
    private PayUserDao payUserDao;

    @Autowired
    private TradeAccountDao accountDao;

    @Autowired
    private BasicTradeAccountDao tradeAccountDao;


    @Override
    public ResponseObject testTrade(TenantRequest request) {
        ResponseObject result = new ResponseObject();

        Random random = new Random();
        long num = random.nextLong();
        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.le("id", num));
        List<Order> orders = new ArrayList<>();
        orders.add(Order.desc("id"));
        List<PayUser> users = payUserDao.list(1, 2, filters, orders);
        if (users == null || users.isEmpty()) {
            return result;
        }
        PayUser user = users.get(0);
        TradeAccount account = user.getTradeAccount();
        if (account == null) {
            account = accountDao.initNormal();
            user.setTradeAccount(account);
        }
        TradeAccount from =   tradeAccountDao.special("test");
        TradeRequest req=new TradeRequest();
        req.setAmount(new BigDecimal(new Random().nextDouble()*100000));
        req.setFrom(from.getId());
        req.setTo(account.getId());
        req.setNote("充值");
        req.setChangeType(ChangeType.from(1,"充值"));
        accountDao.trade(req);

        return result;
    }
}
