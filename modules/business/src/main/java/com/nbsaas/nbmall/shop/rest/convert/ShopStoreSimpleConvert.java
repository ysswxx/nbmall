package com.nbsaas.nbmall.shop.rest.convert;

import com.nbsaas.nbmall.shop.api.domain.simple.ShopStoreSimple;
import com.nbsaas.nbmall.shop.data.entity.ShopStore;
import com.haoxuer.discover.data.rest.core.Conver;
public class ShopStoreSimpleConvert implements Conver<ShopStoreSimple, ShopStore> {


    @Override
    public ShopStoreSimple conver(ShopStore source) {
        ShopStoreSimple result = new ShopStoreSimple();

            result.setId(source.getId());
            if(source.getShop()!=null){
               result.setShop(source.getShop().getId());
            }
            if(source.getArea()!=null){
               result.setArea(source.getArea().getId());
            }
             result.setAddress(source.getAddress());
            if(source.getCity()!=null){
               result.setCity(source.getCity().getId());
            }
             result.setLng(source.getLng());
            if(source.getCreator()!=null){
               result.setCreator(source.getCreator().getId());
            }
             if(source.getShop()!=null){
                result.setShopName(source.getShop().getName());
             }
            if(source.getProvince()!=null){
               result.setProvince(source.getProvince().getId());
            }
             result.setAddDate(source.getAddDate());
             result.setPhone(source.getPhone());
             result.setLogo(source.getLogo());
             result.setName(source.getName());
             result.setLat(source.getLat());

        return result;
    }
}
