package com.nbsaas.nbmall.shop.controller.admin;


import com.haoxuer.bigworld.tenant.controller.admin.TenantBaseAction;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
*
* Created by imake on 2021年12月10日17:38:01.
*/


@Scope("prototype")
@Controller
public class ShopCashAction extends TenantBaseAction {

	@RequiresPermissions("shopcash")
	@RequestMapping("/tenant/shopcash/view_list")
	public String list(ModelMap model) {
		return getView("shopcash/list");
	}

}