package com.nbsaas.nbmall.shop.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.shop.data.dao.ShopCashDao;
import com.nbsaas.nbmall.shop.data.entity.ShopCash;

/**
* Created by imake on 2021年12月10日17:38:01.
*/
@Repository

public class ShopCashDaoImpl extends CriteriaDaoImpl<ShopCash, Long> implements ShopCashDao {

	@Override
	public ShopCash findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public ShopCash save(ShopCash bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public ShopCash deleteById(Long id) {
		ShopCash entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<ShopCash> getEntityClass() {
		return ShopCash.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public ShopCash findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public ShopCash deleteById(Long tenant,Long id) {
		ShopCash entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}