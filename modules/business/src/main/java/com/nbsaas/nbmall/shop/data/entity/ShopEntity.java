package com.nbsaas.nbmall.shop.data.entity;

import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.*;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;


@Setter
@Getter
@MappedSuperclass
public class ShopEntity extends TenantEntity {

    @SearchItem(label = "商家名称",name = "shop",key = "shop.id",classType = "Long",type = InputType.select,operator = "eq")
    @FormField(title = "商家",  grid = false, type = InputType.select,option = "shop")
    @FieldConvert
    @FieldName
    @ManyToOne(fetch = FetchType.LAZY)
    private Shop shop;
}
