package com.nbsaas.nbmall.promote.controller.tenant;

import com.nbsaas.nbmall.promote.api.apis.CouponApi;
import com.nbsaas.nbmall.promote.api.domain.list.CouponList;
import com.nbsaas.nbmall.promote.api.domain.page.CouponPage;
import com.nbsaas.nbmall.promote.api.domain.request.*;
import com.nbsaas.nbmall.promote.api.domain.response.CouponResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/coupon")
@RestController
public class CouponTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("coupon")
    @RequestMapping("create")
    public CouponResponse create(CouponDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

	@RequiresPermissions("coupon")
    @RequestMapping("delete")
    public CouponResponse delete(CouponDataRequest request) {
        initTenant(request);
        CouponResponse result = new CouponResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("coupon")
    @RequestMapping("update")
    public CouponResponse update(CouponDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("coupon")
    @RequestMapping("view")
    public CouponResponse view(CouponDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("coupon")
    @RequestMapping("list")
    public CouponList list(CouponSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("coupon")
    @RequestMapping("search")
    public CouponPage search(CouponSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private CouponApi api;

}
