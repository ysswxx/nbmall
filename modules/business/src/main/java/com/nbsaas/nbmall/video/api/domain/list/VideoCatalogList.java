package com.nbsaas.nbmall.video.api.domain.list;


import com.nbsaas.nbmall.video.api.domain.simple.VideoCatalogSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2022年03月16日21:25:37.
*/

@Data
public class VideoCatalogList  extends ResponseList<VideoCatalogSimple> {

}