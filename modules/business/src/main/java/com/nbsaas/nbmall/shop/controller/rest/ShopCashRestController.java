package com.nbsaas.nbmall.shop.controller.rest;

import com.nbsaas.nbmall.shop.api.apis.ShopCashApi;
import com.nbsaas.nbmall.shop.api.domain.list.ShopCashList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopCashPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopCashResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/shopcash")
@RestController
public class ShopCashRestController extends BaseRestController {


    @RequestMapping("create")
    public ShopCashResponse create(ShopCashDataRequest request) {
        initTenant(request);
        request.setCreator(request.getUser());
        return api.create(request);
    }

    @RequestMapping("delete")
    public ShopCashResponse delete(ShopCashDataRequest request) {
        initTenant(request);
        ShopCashResponse result = new ShopCashResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public ShopCashResponse update(ShopCashDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public ShopCashResponse view(ShopCashDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public ShopCashList list(ShopCashSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public ShopCashPage search(ShopCashSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private ShopCashApi api;

}
