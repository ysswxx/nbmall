package com.nbsaas.nbmall.device.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
/**
*
* Created by BigWorld on 2021年12月10日17:38:01.
*/

@Data
public class DeviceResponse extends ResponseObject {

    private Long id;

     private Long shop;
     private Integer printNum;
     private String secretKey;
     private Long creator;
     private String shopName;
     private String deviceTypeName;
     private String creatorName;
     private String deviceCode;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private String name;
     private String state;
     private String model;
     private Long deviceType;

}