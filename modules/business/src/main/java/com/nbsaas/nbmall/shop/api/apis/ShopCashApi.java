package com.nbsaas.nbmall.shop.api.apis;


import com.nbsaas.nbmall.shop.api.domain.list.ShopCashList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopCashPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopCashResponse;

public interface ShopCashApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    ShopCashResponse create(ShopCashDataRequest request);

    ShopCashResponse cash(ShopCashDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    ShopCashResponse update(ShopCashDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    ShopCashResponse delete(ShopCashDataRequest request);


    /**
     * 详情
     *
     * @param request
     * @return
     */
    ShopCashResponse view(ShopCashDataRequest request);

    ShopCashResponse back(ShopCashDataRequest request);

    ShopCashResponse send(ShopCashDataRequest request);

    /**
     *
     * @param request
     * @return
     */
    ShopCashList list(ShopCashSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    ShopCashPage search(ShopCashSearchRequest request);

}