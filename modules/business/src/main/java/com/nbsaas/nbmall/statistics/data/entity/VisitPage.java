package com.nbsaas.nbmall.statistics.data.entity;


import com.haoxuer.bigworld.member.data.entity.TenantUser;
import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.ComposeView;
import com.nbsaas.codemake.annotation.FieldConvert;
import com.nbsaas.codemake.annotation.FormAnnotation;
import lombok.Data;

import javax.persistence.*;

@ComposeView
@Data
@FormAnnotation(title = "访问页面", model = "访问页面", menu = "1,101,102")
@Entity
@Table(name = "bs_tenant_visit_user_page")
public class VisitPage extends TenantEntity {

    //2021-07-12
    @Column(name = "data_key", length = 12)
    private String key;

    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private TenantUser user;

    @Column(length = 50)
    private String path;

    private Long num;

}
