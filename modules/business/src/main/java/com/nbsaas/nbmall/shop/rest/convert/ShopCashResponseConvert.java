package com.nbsaas.nbmall.shop.rest.convert;

import com.nbsaas.nbmall.shop.api.domain.response.ShopCashResponse;
import com.nbsaas.nbmall.shop.data.entity.ShopCash;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class ShopCashResponseConvert implements Conver<ShopCashResponse, ShopCash> {
    @Override
    public ShopCashResponse conver(ShopCash source) {
        ShopCashResponse result = new ShopCashResponse();
        TenantBeanUtils.copyProperties(source,result);

         if(source.getCashConfig()!=null){
            result.setCashConfigName(source.getCashConfig().getName());
         }
        if(source.getShop()!=null){
           result.setShop(source.getShop().getId());
        }
        if(source.getCreator()!=null){
           result.setCreator(source.getCreator().getId());
        }
         if(source.getShop()!=null){
            result.setShopName(source.getShop().getName());
         }
        if(source.getCashConfig()!=null){
           result.setCashConfig(source.getCashConfig().getId());
        }

         result.setSendStateName(source.getSendState()+"");
        return result;
    }
}
