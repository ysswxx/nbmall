package com.nbsaas.nbmall.video.api.domain.page;


import com.nbsaas.nbmall.video.api.domain.simple.VideoSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2022年03月16日21:26:00.
*/

@Data
public class VideoPage  extends ResponsePage<VideoSimple> {

}