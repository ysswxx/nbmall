package com.nbsaas.nbmall.product.rest.convert;

import com.nbsaas.nbmall.product.api.domain.simple.ProductSkuSimple;
import com.nbsaas.nbmall.product.data.entity.ProductSku;
import com.haoxuer.discover.data.rest.core.Conver;
public class ProductSkuSimpleConvert implements Conver<ProductSkuSimple, ProductSku> {


    @Override
    public ProductSkuSimple conver(ProductSku source) {
        ProductSkuSimple result = new ProductSkuSimple();

            result.setId(source.getId());
             result.setCode(source.getCode());
             result.setMarketPrice(source.getMarketPrice());
            if(source.getProduct()!=null){
               result.setProduct(source.getProduct().getId());
            }
             result.setSalePrice(source.getSalePrice());
             result.setLogo(source.getLogo());
             result.setName(source.getName());
             result.setInventory(source.getInventory());
             result.setAddDate(source.getAddDate());
             if(source.getProduct()!=null){
                result.setProductName(source.getProduct().getName());
             }
             result.setWarning(source.getWarning());
             result.setCostPrice(source.getCostPrice());

        return result;
    }
}
