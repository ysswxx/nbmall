package com.nbsaas.nbmall.statistics.controller.tenant;

import com.nbsaas.nbmall.statistics.api.apis.VisitPageDayApi;
import com.nbsaas.nbmall.statistics.api.domain.list.VisitPageDayList;
import com.nbsaas.nbmall.statistics.api.domain.page.VisitPageDayPage;
import com.nbsaas.nbmall.statistics.api.domain.request.*;
import com.nbsaas.nbmall.statistics.api.domain.response.VisitPageDayResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/visitpageday")
@RestController
public class VisitPageDayTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("visitpageday")
    @RequestMapping("create")
    public VisitPageDayResponse create(VisitPageDayDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

	@RequiresPermissions("visitpageday")
    @RequestMapping("delete")
    public VisitPageDayResponse delete(VisitPageDayDataRequest request) {
        initTenant(request);
        VisitPageDayResponse result = new VisitPageDayResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("visitpageday")
    @RequestMapping("update")
    public VisitPageDayResponse update(VisitPageDayDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("visitpageday")
    @RequestMapping("view")
    public VisitPageDayResponse view(VisitPageDayDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("visitpageday")
    @RequestMapping("list")
    public VisitPageDayList list(VisitPageDaySearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("visitpageday")
    @RequestMapping("search")
    public VisitPageDayPage search(VisitPageDaySearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private VisitPageDayApi api;

}
