package com.nbsaas.nbmall.shop.api.apis;


import com.nbsaas.nbmall.shop.api.domain.list.ShopHourList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopHourPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopHourResponse;

public interface ShopHourApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    ShopHourResponse create(ShopHourDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    ShopHourResponse update(ShopHourDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    ShopHourResponse delete(ShopHourDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     ShopHourResponse view(ShopHourDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    ShopHourList list(ShopHourSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    ShopHourPage search(ShopHourSearchRequest request);

}