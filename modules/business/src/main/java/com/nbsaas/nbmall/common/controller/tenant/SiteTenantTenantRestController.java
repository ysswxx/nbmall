package com.nbsaas.nbmall.common.controller.tenant;

import com.haoxuer.bigworld.member.shiro.domain.ShiroTenantUser;
import com.haoxuer.bigworld.member.utils.TenantUserUtils;
import com.haoxuer.bigworld.pay.api.domain.page.TradeStreamPage;
import com.nbsaas.nbmall.common.api.apis.SiteTenantApi;
import com.nbsaas.nbmall.common.api.domain.list.SiteTenantList;
import com.nbsaas.nbmall.common.api.domain.page.SiteTenantPage;
import com.nbsaas.nbmall.common.api.domain.request.*;
import com.nbsaas.nbmall.common.api.domain.response.SiteTenantResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@RequestMapping("/tenantRest/sitetenant")
@RestController
public class SiteTenantTenantRestController extends BaseTenantRestController {


    @RequiresPermissions("sitetenant")
    @RequiresUser
    @RequestMapping("create")
    public SiteTenantResponse create(SiteTenantDataRequest request) {
        init(request);
        request.setCreator(request.getCreateUser());
        return api.create(request);
    }

    @RequiresPermissions("sitetenant")
    @RequiresUser
    @RequestMapping("update")
    public SiteTenantResponse update(SiteTenantDataRequest request) {
        init(request);
        return api.update(request);
    }

    @RequiresPermissions("sitetenant")
    @RequiresUser
    @RequestMapping("delete")
    public SiteTenantResponse delete(SiteTenantDataRequest request) {
        init(request);
        SiteTenantResponse result = new SiteTenantResponse();
        try {
            result = api.delete(request);
        } catch (Exception e) {
            result.setCode(501);
            result.setMsg("删除失败!");
        }
        return result;
    }

    @RequiresPermissions("sitetenant")
    @RequiresUser
    @RequestMapping("view")
    public SiteTenantResponse view(SiteTenantDataRequest request) {
        init(request);
        return api.view(request);
    }

    @RequiresPermissions("sitetenant")
    @RequiresUser
    @RequestMapping("list")
    public SiteTenantList list(SiteTenantSearchRequest request) {
        init(request);
        return api.list(request);
    }

    @RequiresPermissions("sitetenant")
    @RequiresUser
    @RequestMapping("search")
    public SiteTenantPage search(SiteTenantSearchRequest request) {
        init(request);
        return api.search(request);
    }

    @RequiresUser
    @RequestMapping("updateCur")
    public SiteTenantResponse updateCur(SiteTenantDataRequest request) {
        SiteTenantResponse result = new SiteTenantResponse();
        init(request);
        ShiroTenantUser tenant = TenantUserUtils.getCurrentShiroUser();
        if (tenant == null) {
            result.setCode(501);
            result.setMsg("无效信息");
            return result;
        }
        request.setExpireDate(null);
        request.setId(tenant.getTenant());
        return api.update(request);
    }

    @RequiresUser
    @RequestMapping("cur")
    public SiteTenantResponse cur(SiteTenantDataRequest request) {
        SiteTenantResponse result = new SiteTenantResponse();
        init(request);
        ShiroTenantUser tenant = TenantUserUtils.getCurrentShiroUser();
        if (tenant == null) {
            result.setCode(501);
            result.setMsg("无效信息");
            return result;
        }
        request.setId(tenant.getTenant());
        return api.view(request);
    }

    @RequiresUser
    @RequestMapping("stream")
    public TradeStreamPage stream(SiteTenantSearchRequest request) {
        TradeStreamPage result = new TradeStreamPage();
        init(request);
        ShiroTenantUser tenant = TenantUserUtils.getCurrentShiroUser();
        if (tenant == null) {
            result.setCode(501);
            result.setMsg("无效信息");
            return result;
        }
        request.setTenant(tenant.getTenant());
        return api.stream(request);
    }

    @Autowired
    private SiteTenantApi api;

}
