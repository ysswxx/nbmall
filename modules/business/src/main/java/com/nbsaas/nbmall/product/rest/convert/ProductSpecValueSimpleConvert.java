package com.nbsaas.nbmall.product.rest.convert;

import com.nbsaas.nbmall.product.api.domain.simple.ProductSpecValueSimple;
import com.nbsaas.nbmall.product.data.entity.ProductSpecValue;
import com.haoxuer.discover.data.rest.core.Conver;

public class ProductSpecValueSimpleConvert implements Conver<ProductSpecValueSimple, ProductSpecValue> {


    @Override
    public ProductSpecValueSimple conver(ProductSpecValue source) {
        ProductSpecValueSimple result = new ProductSpecValueSimple();

        result.setId(source.getId());
        result.setValue(source.getValue());
        result.setName(source.getValue());
        result.setAddDate(source.getAddDate());
        if (source.getProductSpec() != null) {
            result.setProductSpec(source.getProductSpec().getId());
        }

        return result;
    }
}
