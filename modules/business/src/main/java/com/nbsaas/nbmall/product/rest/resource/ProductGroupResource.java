package com.nbsaas.nbmall.product.rest.resource;

import com.nbsaas.nbmall.product.api.apis.ProductGroupApi;
import com.nbsaas.nbmall.product.api.domain.list.ProductGroupList;
import com.nbsaas.nbmall.product.api.domain.page.ProductGroupPage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductGroupResponse;
import com.nbsaas.nbmall.product.data.dao.ProductGroupDao;
import com.nbsaas.nbmall.product.data.entity.ProductGroup;
import com.nbsaas.nbmall.product.rest.convert.ProductGroupResponseConvert;
import com.nbsaas.nbmall.product.rest.convert.ProductGroupSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.nbsaas.nbmall.shop.data.dao.ShopDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class ProductGroupResource implements ProductGroupApi {

    @Autowired
    private ProductGroupDao dataDao;

    @Autowired
    private ShopDao shopDao;


    @Override
    public ProductGroupResponse create(ProductGroupDataRequest request) {
        ProductGroupResponse result = new ProductGroupResponse();

        ProductGroup bean = new ProductGroup();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new ProductGroupResponseConvert().conver(bean);
        return result;
    }

    @Override
    public ProductGroupResponse update(ProductGroupDataRequest request) {
        ProductGroupResponse result = new ProductGroupResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        ProductGroup bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new ProductGroupResponseConvert().conver(bean);
        return result;
    }

    private void handleData(ProductGroupDataRequest request, ProductGroup bean) {
       TenantBeanUtils.copyProperties(request,bean);
           if(request.getShop()!=null){
              bean.setShop(shopDao.findById(request.getShop()));
           }

    }

    @Override
    public ProductGroupResponse delete(ProductGroupDataRequest request) {
        ProductGroupResponse result = new ProductGroupResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public ProductGroupResponse view(ProductGroupDataRequest request) {
        ProductGroupResponse result=new ProductGroupResponse();
        ProductGroup bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new ProductGroupResponseConvert().conver(bean);
        return result;
    }
    @Override
    public ProductGroupList list(ProductGroupSearchRequest request) {
        ProductGroupList result = new ProductGroupList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<ProductGroup> organizations = dataDao.list(0, request.getSize(), filters, orders);

        ProductGroupSimpleConvert convert=new ProductGroupSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public ProductGroupPage search(ProductGroupSearchRequest request) {
        ProductGroupPage result=new ProductGroupPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<ProductGroup> page=dataDao.page(pageable);

        ProductGroupSimpleConvert convert=new ProductGroupSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
