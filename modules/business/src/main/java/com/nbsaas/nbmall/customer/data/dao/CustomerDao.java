package com.nbsaas.nbmall.customer.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.customer.data.entity.Customer;

/**
* Created by imake on 2021年12月10日22:56:18.
*/
public interface CustomerDao extends BaseDao<Customer,Long>{

	 Customer findById(Long id);

	 Customer save(Customer bean);

	 Customer updateByUpdater(Updater<Customer> updater);

	 Customer deleteById(Long id);

	 Customer findById(Long tenant, Long id);

     Customer deleteById(Long tenant, Long id);
}