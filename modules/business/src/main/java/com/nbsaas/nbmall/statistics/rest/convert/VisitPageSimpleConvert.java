package com.nbsaas.nbmall.statistics.rest.convert;

import com.nbsaas.nbmall.statistics.api.domain.simple.VisitPageSimple;
import com.nbsaas.nbmall.statistics.data.entity.VisitPage;
import com.haoxuer.discover.data.rest.core.Conver;
public class VisitPageSimpleConvert implements Conver<VisitPageSimple, VisitPage> {


    @Override
    public VisitPageSimple conver(VisitPage source) {
        VisitPageSimple result = new VisitPageSimple();

            result.setId(source.getId());
             result.setPath(source.getPath());
             result.setNum(source.getNum());
            if(source.getUser()!=null){
               result.setUser(source.getUser().getId());
            }
             result.setAddDate(source.getAddDate());
             result.setKey(source.getKey());

        return result;
    }
}
