package com.nbsaas.nbmall.statistics.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.statistics.data.entity.VisitPageDay;

/**
* Created by imake on 2021年12月23日11:45:35.
*/
public interface VisitPageDayDao extends BaseDao<VisitPageDay,Long>{

	 VisitPageDay findById(Long id);

	 VisitPageDay save(VisitPageDay bean);

	 VisitPageDay updateByUpdater(Updater<VisitPageDay> updater);

	 VisitPageDay deleteById(Long id);

	 VisitPageDay findById(Long tenant, Long id);

     VisitPageDay deleteById(Long tenant, Long id);
}