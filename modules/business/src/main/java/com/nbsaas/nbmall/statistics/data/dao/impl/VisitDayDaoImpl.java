package com.nbsaas.nbmall.statistics.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.statistics.data.dao.VisitDayDao;
import com.nbsaas.nbmall.statistics.data.entity.VisitDay;

/**
* Created by imake on 2021年12月23日11:45:35.
*/
@Repository

public class VisitDayDaoImpl extends CriteriaDaoImpl<VisitDay, Long> implements VisitDayDao {

	@Override
	public VisitDay findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public VisitDay save(VisitDay bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public VisitDay deleteById(Long id) {
		VisitDay entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<VisitDay> getEntityClass() {
		return VisitDay.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public VisitDay findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public VisitDay deleteById(Long tenant,Long id) {
		VisitDay entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}