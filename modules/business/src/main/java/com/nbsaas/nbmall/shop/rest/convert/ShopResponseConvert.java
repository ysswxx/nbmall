package com.nbsaas.nbmall.shop.rest.convert;

import com.nbsaas.nbmall.product.data.entity.ProductCatalog;
import com.nbsaas.nbmall.shop.api.domain.response.ShopResponse;
import com.nbsaas.nbmall.shop.data.entity.Shop;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.nbsaas.nbmall.shop.data.entity.ShopCatalog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ShopResponseConvert implements Conver<ShopResponse, Shop> {
    @Override
    public ShopResponse conver(Shop source) {
        ShopResponse result = new ShopResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getArea()!=null){
           result.setArea(source.getArea().getId());
        }
        if(source.getCreator()!=null){
           result.setCreator(source.getCreator().getId());
        }
         if(source.getBoss()!=null){
            result.setBossName(source.getBoss().getName());
         }
         if(source.getChannel()!=null){
            result.setUrl(source.getChannel().getUrl());
         }
         if(source.getShopCatalog()!=null){
            result.setShopCatalogName(source.getShopCatalog().getName());
         }
        if(source.getBoss()!=null){
           result.setBoss(source.getBoss().getId());
        }
        if(source.getCity()!=null){
           result.setCity(source.getCity().getId());
        }
        if(source.getChannel()!=null){
           result.setChannel(source.getChannel().getId());
        }
        if(source.getExt()!=null){
           result.setExt(source.getExt().getId());
        }
         if(source.getTradeAccount()!=null){
            result.setAmount(source.getTradeAccount().getAmount());
         }
        if(source.getProvince()!=null){
           result.setProvince(source.getProvince().getId());
        }
        if(source.getShopCatalog()!=null){
           result.setShopCatalog(source.getShopCatalog().getId());
            List<String> catalogs = new ArrayList<>();
            ShopCatalog catalog = source.getShopCatalog();
            catalogs.add(catalog.getId() + "");
            while (catalog.getParent() != null) {
                catalogs.add(catalog.getParent().getId() + "");
                catalog = catalog.getParent();
            }
            Collections.reverse(catalogs);
            result.setCatalogs(catalogs);
        }

         result.setFreightSubjectName(source.getFreightSubject()+"");
         result.setAuditStateName(source.getAuditState()+"");
         result.setReceivingModeName(source.getReceivingMode()+"");
         result.setShopStateName(source.getShopState()+"");
         result.setMealSubjectName(source.getMealSubject()+"");
         result.setStoreStateName(source.getStoreState()+"");
        return result;
    }
}
