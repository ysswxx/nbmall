package com.nbsaas.nbmall.shop.controller.rest;

import com.nbsaas.nbmall.shop.api.apis.ShopStaffApi;
import com.nbsaas.nbmall.shop.api.domain.list.ShopStaffList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopStaffPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopStaffResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/shopstaff")
@RestController
public class ShopStaffRestController extends BaseRestController {


    @RequestMapping("create")
    public ShopStaffResponse create(ShopStaffDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

    @RequestMapping("delete")
    public ShopStaffResponse delete(ShopStaffDataRequest request) {
        initTenant(request);
        ShopStaffResponse result = new ShopStaffResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public ShopStaffResponse update(ShopStaffDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public ShopStaffResponse view(ShopStaffDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public ShopStaffList list(ShopStaffSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public ShopStaffPage search(ShopStaffSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private ShopStaffApi api;

}
