package com.nbsaas.nbmall.cart.controller.tenant;

import com.nbsaas.nbmall.cart.api.apis.CartApi;
import com.nbsaas.nbmall.cart.api.domain.list.CartList;
import com.nbsaas.nbmall.cart.api.domain.page.CartPage;
import com.nbsaas.nbmall.cart.api.domain.request.*;
import com.nbsaas.nbmall.cart.api.domain.response.CartResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/cart")
@RestController
public class CartTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("cart")
    @RequestMapping("create")
    public CartResponse create(CartDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

	@RequiresPermissions("cart")
    @RequestMapping("delete")
    public CartResponse delete(CartDataRequest request) {
        initTenant(request);
        CartResponse result = new CartResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("cart")
    @RequestMapping("update")
    public CartResponse update(CartDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("cart")
    @RequestMapping("view")
    public CartResponse view(CartDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("cart")
    @RequestMapping("list")
    public CartList list(CartSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("cart")
    @RequestMapping("search")
    public CartPage search(CartSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private CartApi api;

}
