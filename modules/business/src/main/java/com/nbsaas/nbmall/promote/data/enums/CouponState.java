package com.nbsaas.nbmall.promote.data.enums;

public enum CouponState {
    init, used, expire;

    public String toString() {
        if (this.name().equals("init")) {
            return "待使用";
        } else if (this.name().equals("used")) {
            return "已使用";
        } else if (this.name().equals("expire")) {
            return "已过期";
        } else {
            return this.name();
        }
    }
}
