package com.nbsaas.nbmall.shop.controller.admin;


import com.haoxuer.bigworld.tenant.controller.admin.TenantBaseAction;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
*
* Created by imake on 2021年12月10日17:38:00.
*/


@Scope("prototype")
@Controller
public class ShopStaffAction extends TenantBaseAction {

	@RequiresPermissions("shopstaff")
	@RequestMapping("/tenant/shopstaff/view_list")
	public String list(ModelMap model) {
		return getView("shopstaff/list");
	}

}