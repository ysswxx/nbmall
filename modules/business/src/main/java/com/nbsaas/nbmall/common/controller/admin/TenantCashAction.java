package com.nbsaas.nbmall.common.controller.admin;


import com.haoxuer.bigworld.tenant.controller.admin.TenantBaseAction;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
*
* Created by imake on 2021年12月27日16:40:36.
*/


@Scope("prototype")
@Controller
public class TenantCashAction extends TenantBaseAction {

	@RequiresPermissions("tenantcash")
	@RequestMapping("/tenant/tenantcash/view_list")
	public String list(ModelMap model) {
		return getView("tenantcash/list");
	}

}