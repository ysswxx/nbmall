package com.nbsaas.nbmall.shop.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.haoxuer.discover.data.enums.StoreState;

/**
*
* Created by BigWorld on 2021年12月10日17:38:00.
*/
@Data
public class ShopCollectSimple implements Serializable {

    private Long id;

     private Long tenantUser;
     private Long shop;
     private StoreState storeState;
     private String shopName;
     private String tenantUserName;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;

     private String storeStateName;

}
