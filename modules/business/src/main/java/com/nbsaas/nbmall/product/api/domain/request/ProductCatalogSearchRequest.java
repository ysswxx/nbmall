package com.nbsaas.nbmall.product.api.domain.request;

import com.haoxuer.bigworld.member.api.domain.request.TenantPageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月10日17:38:01.
*/

@Data
public class ProductCatalogSearchRequest extends TenantPageRequest {

    //分类
     @Search(name = "parent.id",operator = Filter.Operator.eq)
     private Integer parent;



     private int fetch;

     @Search(name = "levelInfo",operator = Filter.Operator.eq)
     private Integer level;
}