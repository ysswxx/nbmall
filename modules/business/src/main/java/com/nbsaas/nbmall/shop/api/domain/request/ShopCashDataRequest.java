package com.nbsaas.nbmall.shop.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import com.haoxuer.bigworld.pay.data.enums.SendState;
import java.util.Date;
import java.math.BigDecimal;

/**
*
* Created by imake on 2021年12月10日17:38:01.
*/

@Data
public class ShopCashDataRequest extends TenantRequest {

    private Long id;

     private Long shop;
     private String no;
     private String note;
     private BigDecimal fee;
     private Long creator;
     private SendState sendState;
     private String openId;
     private String idNo;
     private BigDecimal money;
     private String demo;
     private String appId;
     private String bussNo;
     private String phone;
     private String name;
     private Long cashConfig;
     private BigDecimal cash;
 private String key;

}