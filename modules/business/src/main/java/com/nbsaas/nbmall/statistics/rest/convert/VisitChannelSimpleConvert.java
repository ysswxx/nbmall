package com.nbsaas.nbmall.statistics.rest.convert;

import com.nbsaas.nbmall.statistics.api.domain.simple.VisitChannelSimple;
import com.nbsaas.nbmall.statistics.data.entity.VisitChannel;
import com.haoxuer.discover.data.rest.core.Conver;
public class VisitChannelSimpleConvert implements Conver<VisitChannelSimple, VisitChannel> {


    @Override
    public VisitChannelSimple conver(VisitChannel source) {
        VisitChannelSimple result = new VisitChannelSimple();

            result.setId(source.getId());
             result.setUserNum(source.getUserNum());
             result.setName(source.getName());
             result.setPageNum(source.getPageNum());
             result.setAddDate(source.getAddDate());

        return result;
    }
}
