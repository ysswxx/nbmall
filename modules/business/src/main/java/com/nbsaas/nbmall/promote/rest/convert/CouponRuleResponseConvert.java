package com.nbsaas.nbmall.promote.rest.convert;

import com.nbsaas.nbmall.promote.api.domain.response.CouponRuleResponse;
import com.nbsaas.nbmall.promote.data.entity.CouponRule;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class CouponRuleResponseConvert implements Conver<CouponRuleResponse, CouponRule> {
    @Override
    public CouponRuleResponse conver(CouponRule source) {
        CouponRuleResponse result = new CouponRuleResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getShop()!=null){
           result.setShop(source.getShop().getId());
        }
         if(source.getShop()!=null){
            result.setShopName(source.getShop().getName());
         }

         result.setCouponStateName(source.getCouponState()+"");
         result.setExpireTypeName(source.getExpireType()+"");
         result.setCouponCatalogName(source.getCouponCatalog()+"");
         result.setShowTypeName(source.getShowType()+"");
         result.setStoreStateName(source.getStoreState()+"");
        return result;
    }
}
