package com.nbsaas.nbmall.video.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.video.data.dao.VideoDao;
import com.nbsaas.nbmall.video.data.entity.Video;

/**
* Created by imake on 2022年03月16日21:26:00.
*/
@Repository

public class VideoDaoImpl extends CriteriaDaoImpl<Video, Long> implements VideoDao {

	@Override
	public Video findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public Video save(Video bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public Video deleteById(Long id) {
		Video entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<Video> getEntityClass() {
		return Video.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public Video findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public Video deleteById(Long tenant,Long id) {
		Video entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}