package com.nbsaas.nbmall.device.rest.convert;

import com.nbsaas.nbmall.device.api.domain.simple.DeviceSimple;
import com.nbsaas.nbmall.device.data.entity.Device;
import com.haoxuer.discover.data.rest.core.Conver;
public class DeviceSimpleConvert implements Conver<DeviceSimple, Device> {


    @Override
    public DeviceSimple conver(Device source) {
        DeviceSimple result = new DeviceSimple();

            result.setId(source.getId());
            if(source.getShop()!=null){
               result.setShop(source.getShop().getId());
            }
             result.setPrintNum(source.getPrintNum());
             result.setSecretKey(source.getSecretKey());
            if(source.getCreator()!=null){
               result.setCreator(source.getCreator().getId());
            }
             if(source.getShop()!=null){
                result.setShopName(source.getShop().getName());
             }
             if(source.getDeviceType()!=null){
                result.setDeviceTypeName(source.getDeviceType().getName());
             }
             if(source.getCreator()!=null){
                result.setCreatorName(source.getCreator().getName());
             }
             result.setDeviceCode(source.getDeviceCode());
             result.setAddDate(source.getAddDate());
             result.setName(source.getName());
             result.setState(source.getState());
             result.setModel(source.getModel());
            if(source.getDeviceType()!=null){
               result.setDeviceType(source.getDeviceType().getId());
            }

        return result;
    }
}
