package com.nbsaas.nbmall.shop.rest.resource;

import com.nbsaas.nbmall.shop.api.apis.ShopStoreApi;
import com.nbsaas.nbmall.shop.api.domain.list.ShopStoreList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopStorePage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopStoreResponse;
import com.nbsaas.nbmall.shop.data.dao.ShopStoreDao;
import com.nbsaas.nbmall.shop.data.entity.ShopStore;
import com.nbsaas.nbmall.shop.rest.convert.ShopStoreResponseConvert;
import com.nbsaas.nbmall.shop.rest.convert.ShopStoreSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.haoxuer.bigworld.member.data.dao.TenantUserDao;
import com.nbsaas.nbmall.shop.data.dao.ShopDao;
import com.haoxuer.discover.area.data.dao.AreaDao;
import com.haoxuer.discover.area.data.dao.AreaDao;
import com.haoxuer.discover.area.data.dao.AreaDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class ShopStoreResource implements ShopStoreApi {

    @Autowired
    private ShopStoreDao dataDao;

    @Autowired
    private TenantUserDao creatorDao;
    @Autowired
    private ShopDao shopDao;
    @Autowired
    private AreaDao provinceDao;
    @Autowired
    private AreaDao areaDao;
    @Autowired
    private AreaDao cityDao;


    @Override
    public ShopStoreResponse create(ShopStoreDataRequest request) {
        ShopStoreResponse result = new ShopStoreResponse();

        ShopStore bean = new ShopStore();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new ShopStoreResponseConvert().conver(bean);
        return result;
    }

    @Override
    public ShopStoreResponse update(ShopStoreDataRequest request) {
        ShopStoreResponse result = new ShopStoreResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        ShopStore bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new ShopStoreResponseConvert().conver(bean);
        return result;
    }

    private void handleData(ShopStoreDataRequest request, ShopStore bean) {
       TenantBeanUtils.copyProperties(request,bean);
           if(request.getShop()!=null){
              bean.setShop(shopDao.findById(request.getShop()));
           }
           if(request.getArea()!=null){
              bean.setArea(areaDao.findById(request.getArea()));
           }
           if(request.getCity()!=null){
              bean.setCity(cityDao.findById(request.getCity()));
           }
           if(request.getCreator()!=null){
              bean.setCreator(creatorDao.findById(request.getCreator()));
           }
           if(request.getProvince()!=null){
              bean.setProvince(provinceDao.findById(request.getProvince()));
           }

    }

    @Override
    public ShopStoreResponse delete(ShopStoreDataRequest request) {
        ShopStoreResponse result = new ShopStoreResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public ShopStoreResponse view(ShopStoreDataRequest request) {
        ShopStoreResponse result=new ShopStoreResponse();
        ShopStore bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new ShopStoreResponseConvert().conver(bean);
        return result;
    }
    @Override
    public ShopStoreList list(ShopStoreSearchRequest request) {
        ShopStoreList result = new ShopStoreList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<ShopStore> organizations = dataDao.list(0, request.getSize(), filters, orders);

        ShopStoreSimpleConvert convert=new ShopStoreSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public ShopStorePage search(ShopStoreSearchRequest request) {
        ShopStorePage result=new ShopStorePage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<ShopStore> page=dataDao.page(pageable);

        ShopStoreSimpleConvert convert=new ShopStoreSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
