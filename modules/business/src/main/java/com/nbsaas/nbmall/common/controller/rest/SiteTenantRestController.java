package com.nbsaas.nbmall.common.controller.rest;

import com.nbsaas.nbmall.common.api.apis.SiteTenantApi;
import com.nbsaas.nbmall.common.api.domain.list.SiteTenantList;
import com.nbsaas.nbmall.common.api.domain.page.SiteTenantPage;
import com.nbsaas.nbmall.common.api.domain.request.*;
import com.nbsaas.nbmall.common.api.domain.response.SiteTenantResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.rest.BaseRestController;

@RequestMapping("/rest/sitetenant")
@RestController
public class SiteTenantRestController extends BaseRestController {


    @RequestMapping("view")
    public SiteTenantResponse view(SiteTenantDataRequest request) {
        init(request);
        return api.view(request);
    }
    @Autowired
    private SiteTenantApi api;

}
