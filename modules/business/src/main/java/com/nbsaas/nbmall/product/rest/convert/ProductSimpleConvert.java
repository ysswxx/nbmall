package com.nbsaas.nbmall.product.rest.convert;

import com.nbsaas.nbmall.product.api.domain.simple.ProductSimple;
import com.nbsaas.nbmall.product.data.entity.Product;
import com.haoxuer.discover.data.rest.core.Conver;
import com.nbsaas.nbmall.product.data.enums.ProductState;

public class ProductSimpleConvert implements Conver<ProductSimple, Product> {


    @Override
    public ProductSimple conver(Product source) {
        ProductSimple result = new ProductSimple();
        if (source.getProductState() == null) {
            source.setProductState(ProductState.lowerShelf);
        }

        result.setId(source.getId());
        result.setSummary(source.getSummary());
        result.setThumbnail(source.getThumbnail());
        result.setStoreState(source.getStoreState());
        if (source.getCreator() != null) {
            result.setCreator(source.getCreator().getId());
        }
        result.setSalePrice(source.getSalePrice());
        result.setWeight(source.getWeight());
        if (source.getProductGroup() != null) {
            result.setProductGroupName(source.getProductGroup().getName());
        }
        if (source.getProductGroup() != null) {
            result.setProductGroup(source.getProductGroup().getId());
        }
        result.setLength(source.getLength());
        result.setAddDate(source.getAddDate());
        result.setProductState(source.getProductState());
        result.setCostPrice(source.getCostPrice());
        result.setSkuEnable(source.getSkuEnable());
        result.setVolume(source.getVolume());
        result.setNetWeight(source.getNetWeight());
        result.setUnit(source.getUnit());
        result.setName(source.getName());
        result.setWarningValue(source.getWarningValue());
        result.setInvoice(source.getInvoice());
        result.setWidth(source.getWidth());
        if (source.getShop() != null) {
            result.setShop(source.getShop().getId());
        }
        result.setNote(source.getNote());
        result.setMarketPrice(source.getMarketPrice());
        if (source.getProductCatalog() != null) {
            result.setProductCatalogName(source.getProductCatalog().getName());
        }
        if (source.getShop() != null) {
            result.setShopName(source.getShop().getName());
        }
        result.setInventory(source.getInventory());
        if (source.getProductCatalog() != null) {
            result.setProductCatalog(source.getProductCatalog().getId());
        }
        result.setDemo(source.getDemo());
        result.setBarCode(source.getBarCode());
        result.setScore(source.getScore());
        result.setSortNum(source.getSortNum());
        result.setLogo(source.getLogo());
        result.setHeight(source.getHeight());
        result.setVipPrice(source.getVipPrice());

        result.setProductStateName(source.getProductState() + "");
        result.setStoreStateName(source.getStoreState() + "");
        return result;
    }
}
