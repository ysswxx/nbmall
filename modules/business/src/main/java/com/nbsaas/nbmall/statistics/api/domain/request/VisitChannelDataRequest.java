package com.nbsaas.nbmall.statistics.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月23日11:45:35.
*/

@Data
public class VisitChannelDataRequest extends TenantRequest {

    private Long id;

     private Long userNum;
     private String name;
     private Long pageNum;

}