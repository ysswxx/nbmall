package com.nbsaas.nbmall.cart.rest.convert;

import com.nbsaas.nbmall.cart.api.domain.simple.CartSimple;
import com.nbsaas.nbmall.cart.data.entity.Cart;
import com.haoxuer.discover.data.rest.core.Conver;
public class CartSimpleConvert implements Conver<CartSimple, Cart> {


    @Override
    public CartSimple conver(Cart source) {
        CartSimple result = new CartSimple();

            result.setId(source.getId());
            if(source.getTenantUser()!=null){
               result.setTenantUser(source.getTenantUser().getId());
            }
            if(source.getShop()!=null){
               result.setShop(source.getShop().getId());
            }
            if(source.getProduct()!=null){
               result.setProduct(source.getProduct().getId());
            }
             result.setTotal(source.getTotal());
             result.setPrice(source.getPrice());
             result.setChecked(source.getChecked());
             result.setAddDate(source.getAddDate());
            if(source.getSku()!=null){
               result.setSku(source.getSku().getId());
            }
             result.setAmount(source.getAmount());

        return result;
    }
}
