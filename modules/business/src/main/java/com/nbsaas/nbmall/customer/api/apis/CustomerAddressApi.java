package com.nbsaas.nbmall.customer.api.apis;


import com.nbsaas.nbmall.customer.api.domain.list.CustomerAddressList;
import com.nbsaas.nbmall.customer.api.domain.page.CustomerAddressPage;
import com.nbsaas.nbmall.customer.api.domain.request.*;
import com.nbsaas.nbmall.customer.api.domain.response.CustomerAddressResponse;

public interface CustomerAddressApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    CustomerAddressResponse create(CustomerAddressDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    CustomerAddressResponse update(CustomerAddressDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    CustomerAddressResponse delete(CustomerAddressDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     CustomerAddressResponse view(CustomerAddressDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    CustomerAddressList list(CustomerAddressSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    CustomerAddressPage search(CustomerAddressSearchRequest request);

}