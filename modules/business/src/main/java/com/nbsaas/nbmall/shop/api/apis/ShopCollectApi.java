package com.nbsaas.nbmall.shop.api.apis;


import com.nbsaas.nbmall.shop.api.domain.list.ShopCollectList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopCollectPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopCollectResponse;

public interface ShopCollectApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    ShopCollectResponse create(ShopCollectDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    ShopCollectResponse update(ShopCollectDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    ShopCollectResponse delete(ShopCollectDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     ShopCollectResponse view(ShopCollectDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    ShopCollectList list(ShopCollectSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    ShopCollectPage search(ShopCollectSearchRequest request);

}