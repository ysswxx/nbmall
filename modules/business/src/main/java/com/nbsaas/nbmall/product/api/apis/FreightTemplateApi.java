package com.nbsaas.nbmall.product.api.apis;


import com.nbsaas.nbmall.product.api.domain.list.FreightTemplateList;
import com.nbsaas.nbmall.product.api.domain.page.FreightTemplatePage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.FreightTemplateResponse;

public interface FreightTemplateApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    FreightTemplateResponse create(FreightTemplateDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    FreightTemplateResponse update(FreightTemplateDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    FreightTemplateResponse delete(FreightTemplateDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     FreightTemplateResponse view(FreightTemplateDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    FreightTemplateList list(FreightTemplateSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    FreightTemplatePage search(FreightTemplateSearchRequest request);

}