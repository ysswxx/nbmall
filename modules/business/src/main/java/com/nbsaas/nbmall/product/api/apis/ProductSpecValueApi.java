package com.nbsaas.nbmall.product.api.apis;


import com.nbsaas.nbmall.product.api.domain.list.ProductSpecValueList;
import com.nbsaas.nbmall.product.api.domain.page.ProductSpecValuePage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductSpecValueResponse;

public interface ProductSpecValueApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    ProductSpecValueResponse create(ProductSpecValueDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    ProductSpecValueResponse update(ProductSpecValueDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    ProductSpecValueResponse delete(ProductSpecValueDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     ProductSpecValueResponse view(ProductSpecValueDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    ProductSpecValueList list(ProductSpecValueSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    ProductSpecValuePage search(ProductSpecValueSearchRequest request);

}