package com.nbsaas.nbmall.common.rest.convert;

import com.nbsaas.nbmall.common.api.domain.response.TenantCashResponse;
import com.nbsaas.nbmall.common.data.entity.TenantCash;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class TenantCashResponseConvert implements Conver<TenantCashResponse, TenantCash> {
    @Override
    public TenantCashResponse conver(TenantCash source) {
        TenantCashResponse result = new TenantCashResponse();
        TenantBeanUtils.copyProperties(source,result);

         if(source.getCashConfig()!=null){
            result.setCashConfigName(source.getCashConfig().getName());
         }
        if(source.getCreator()!=null){
           result.setCreator(source.getCreator().getId());
        }
        if(source.getCashConfig()!=null){
           result.setCashConfig(source.getCashConfig().getId());
        }

         result.setSendStateName(source.getSendState()+"");
        return result;
    }
}
