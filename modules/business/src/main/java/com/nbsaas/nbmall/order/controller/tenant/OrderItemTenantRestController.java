package com.nbsaas.nbmall.order.controller.tenant;

import com.nbsaas.nbmall.order.api.apis.OrderItemApi;
import com.nbsaas.nbmall.order.api.domain.list.OrderItemList;
import com.nbsaas.nbmall.order.api.domain.page.OrderItemPage;
import com.nbsaas.nbmall.order.api.domain.request.*;
import com.nbsaas.nbmall.order.api.domain.response.OrderItemResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/orderitem")
@RestController
public class OrderItemTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("orderitem")
    @RequestMapping("create")
    public OrderItemResponse create(OrderItemDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

	@RequiresPermissions("orderitem")
    @RequestMapping("delete")
    public OrderItemResponse delete(OrderItemDataRequest request) {
        initTenant(request);
        OrderItemResponse result = new OrderItemResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("orderitem")
    @RequestMapping("update")
    public OrderItemResponse update(OrderItemDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("orderitem")
    @RequestMapping("view")
    public OrderItemResponse view(OrderItemDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("orderitem")
    @RequestMapping("list")
    public OrderItemList list(OrderItemSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("orderitem")
    @RequestMapping("search")
    public OrderItemPage search(OrderItemSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private OrderItemApi api;

}
