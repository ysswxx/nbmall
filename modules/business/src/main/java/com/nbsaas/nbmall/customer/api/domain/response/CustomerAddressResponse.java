package com.nbsaas.nbmall.customer.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import com.haoxuer.discover.data.enums.StoreState;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
/**
*
* Created by BigWorld on 2021年12月10日22:56:19.
*/

@Data
public class CustomerAddressResponse extends ResponseObject {

    private Long id;

     private Integer area;
     private String note;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date updateDate;
     private String address;
     private Integer city;
     private Double lng;
     private StoreState storeState;
     private String postalCode;
     private Integer province;
     private String label;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private String customerName;
     private String phone;
     private String cityName;
     private String houseNo;
     private String name;
     private String provinceName;
     private String areaName;
     private String tel;
     private Long customer;
     private Double lat;

     private String storeStateName;
}