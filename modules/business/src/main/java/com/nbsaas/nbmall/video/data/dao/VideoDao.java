package com.nbsaas.nbmall.video.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.video.data.entity.Video;

/**
* Created by imake on 2022年03月16日21:26:00.
*/
public interface VideoDao extends BaseDao<Video,Long>{

	 Video findById(Long id);

	 Video save(Video bean);

	 Video updateByUpdater(Updater<Video> updater);

	 Video deleteById(Long id);

	 Video findById(Long tenant, Long id);

     Video deleteById(Long tenant, Long id);
}