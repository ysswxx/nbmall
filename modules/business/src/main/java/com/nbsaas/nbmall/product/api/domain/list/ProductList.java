package com.nbsaas.nbmall.product.api.domain.list;


import com.nbsaas.nbmall.product.api.domain.simple.ProductSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年12月28日18:00:18.
*/

@Data
public class ProductList  extends ResponseList<ProductSimple> {

}