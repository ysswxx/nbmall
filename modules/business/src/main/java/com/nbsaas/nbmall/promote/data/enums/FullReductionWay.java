package com.nbsaas.nbmall.promote.data.enums;

public enum FullReductionWay {
    all,part;
    public String toString() {
        if (this.name().equals("all")) {
            return "全部参与";
        } else if (this.name().equals("part")) {
            return "限时折扣不参与";
        }  else {
            return this.name();
        }
    }
}
