package com.nbsaas.nbmall.shop.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月10日17:38:01.
*/

@Data
public class ShopHourDataRequest extends TenantRequest {

    private Long id;

     private Long shop;
     private String endTime;
     private String beginTime;
     private String name;

}