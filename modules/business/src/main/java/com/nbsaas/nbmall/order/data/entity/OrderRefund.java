package com.nbsaas.nbmall.order.data.entity;


import com.haoxuer.bigworld.member.data.entity.TenantUser;
import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.FieldConvert;
import com.nbsaas.codemake.annotation.FieldName;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.FormField;
import com.nbsaas.nbmall.order.data.enums.RefundState;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@FormAnnotation(title = "退款管理",model = "退款单",menu = "1,107,112")
@Entity
@Table(name = "bs_tenant_order_refund")
public class OrderRefund extends TenantEntity {


    @Column(length = 50)
    @FormField(title = "退款单号",width = "180",grid = true, col = 12)
    private String no;

    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private OrderForm orderForm;


    @FieldName
    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private TenantUser creator;

    @FormField(title = "退款金额",width = "180",grid = true, col = 12)
    private BigDecimal money;

    @FormField(title = "商家退款金额",width = "180",grid = true, col = 12)
    private BigDecimal shopMoney;

    @FormField(title = "平台退款金额",width = "180",grid = true, col = 12)
    private BigDecimal platformMoney;

    @FormField(title = "退款原因",width = "1000",grid = true, col = 12)
    private String reason;

    private RefundState refundState;

    private RefundState shopState;

    private RefundState platformState;

}
