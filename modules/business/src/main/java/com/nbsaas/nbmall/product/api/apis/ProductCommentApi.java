package com.nbsaas.nbmall.product.api.apis;


import com.nbsaas.nbmall.product.api.domain.list.ProductCommentList;
import com.nbsaas.nbmall.product.api.domain.page.ProductCommentPage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductCommentResponse;

public interface ProductCommentApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    ProductCommentResponse create(ProductCommentDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    ProductCommentResponse update(ProductCommentDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    ProductCommentResponse delete(ProductCommentDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     ProductCommentResponse view(ProductCommentDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    ProductCommentList list(ProductCommentSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    ProductCommentPage search(ProductCommentSearchRequest request);

}