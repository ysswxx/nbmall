package com.nbsaas.nbmall.video.data.entity;

import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.FormField;

import javax.persistence.Entity;
import javax.persistence.Table;

@FormAnnotation(title = "视频")
@Entity
@Table(name = "bs_tenant_video_type")
public class VideoType  extends TenantEntity {

    @FormField(title = "分类名称", grid = true, col = 24)
    private String name;
}
