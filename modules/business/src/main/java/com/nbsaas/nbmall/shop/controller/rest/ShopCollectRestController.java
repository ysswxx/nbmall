package com.nbsaas.nbmall.shop.controller.rest;

import com.nbsaas.nbmall.shop.api.apis.ShopCollectApi;
import com.nbsaas.nbmall.shop.api.domain.list.ShopCollectList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopCollectPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopCollectResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/shopcollect")
@RestController
public class ShopCollectRestController extends BaseRestController {


    @RequestMapping("create")
    public ShopCollectResponse create(ShopCollectDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

    @RequestMapping("delete")
    public ShopCollectResponse delete(ShopCollectDataRequest request) {
        initTenant(request);
        ShopCollectResponse result = new ShopCollectResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public ShopCollectResponse update(ShopCollectDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public ShopCollectResponse view(ShopCollectDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public ShopCollectList list(ShopCollectSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public ShopCollectPage search(ShopCollectSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private ShopCollectApi api;

}
