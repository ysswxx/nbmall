package com.nbsaas.nbmall.product.api.domain.request;

import com.haoxuer.bigworld.member.api.domain.request.TenantPageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月28日18:04:52.
*/

@Data
public class ProductImageSearchRequest extends TenantPageRequest {



}