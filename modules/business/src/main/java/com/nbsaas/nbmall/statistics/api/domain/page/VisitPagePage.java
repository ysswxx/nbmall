package com.nbsaas.nbmall.statistics.api.domain.page;


import com.nbsaas.nbmall.statistics.api.domain.simple.VisitPageSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年12月23日11:45:35.
*/

@Data
public class VisitPagePage  extends ResponsePage<VisitPageSimple> {

}