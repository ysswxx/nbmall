package com.nbsaas.nbmall.device.api.domain.request;

import com.haoxuer.bigworld.member.api.domain.request.TenantPageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月10日17:38:01.
*/

@Data
public class DeviceSearchRequest extends TenantPageRequest {

    //商家名称
     @Search(name = "shop.id",operator = Filter.Operator.eq)
     private Long shop;

    //打印机名称
     @Search(name = "name",operator = Filter.Operator.like)
     private String name;

    //打印机类型
     @Search(name = "deviceType.id",operator = Filter.Operator.eq)
     private Long deviceType;

    //商家名称
     @Search(name = "shop.name",operator = Filter.Operator.like)
     private String shopName;



}