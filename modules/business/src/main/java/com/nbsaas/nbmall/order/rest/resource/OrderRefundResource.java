package com.nbsaas.nbmall.order.rest.resource;

import com.nbsaas.nbmall.order.api.apis.OrderRefundApi;
import com.nbsaas.nbmall.order.api.domain.list.OrderRefundList;
import com.nbsaas.nbmall.order.api.domain.page.OrderRefundPage;
import com.nbsaas.nbmall.order.api.domain.request.*;
import com.nbsaas.nbmall.order.api.domain.response.OrderRefundResponse;
import com.nbsaas.nbmall.order.data.dao.OrderRefundDao;
import com.nbsaas.nbmall.order.data.entity.OrderRefund;
import com.nbsaas.nbmall.order.rest.convert.OrderRefundResponseConvert;
import com.nbsaas.nbmall.order.rest.convert.OrderRefundSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.haoxuer.bigworld.member.data.dao.TenantUserDao;
import com.nbsaas.nbmall.order.data.dao.OrderFormDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class OrderRefundResource implements OrderRefundApi {

    @Autowired
    private OrderRefundDao dataDao;

    @Autowired
    private TenantUserDao creatorDao;
    @Autowired
    private OrderFormDao orderFormDao;


    @Override
    public OrderRefundResponse create(OrderRefundDataRequest request) {
        OrderRefundResponse result = new OrderRefundResponse();

        OrderRefund bean = new OrderRefund();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new OrderRefundResponseConvert().conver(bean);
        return result;
    }

    @Override
    public OrderRefundResponse update(OrderRefundDataRequest request) {
        OrderRefundResponse result = new OrderRefundResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        OrderRefund bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new OrderRefundResponseConvert().conver(bean);
        return result;
    }

    private void handleData(OrderRefundDataRequest request, OrderRefund bean) {
       TenantBeanUtils.copyProperties(request,bean);
           if(request.getOrderForm()!=null){
              bean.setOrderForm(orderFormDao.findById(request.getOrderForm()));
           }
           if(request.getCreator()!=null){
              bean.setCreator(creatorDao.findById(request.getCreator()));
           }

    }

    @Override
    public OrderRefundResponse delete(OrderRefundDataRequest request) {
        OrderRefundResponse result = new OrderRefundResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public OrderRefundResponse view(OrderRefundDataRequest request) {
        OrderRefundResponse result=new OrderRefundResponse();
        OrderRefund bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new OrderRefundResponseConvert().conver(bean);
        return result;
    }
    @Override
    public OrderRefundList list(OrderRefundSearchRequest request) {
        OrderRefundList result = new OrderRefundList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<OrderRefund> organizations = dataDao.list(0, request.getSize(), filters, orders);

        OrderRefundSimpleConvert convert=new OrderRefundSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public OrderRefundPage search(OrderRefundSearchRequest request) {
        OrderRefundPage result=new OrderRefundPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<OrderRefund> page=dataDao.page(pageable);

        OrderRefundSimpleConvert convert=new OrderRefundSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
