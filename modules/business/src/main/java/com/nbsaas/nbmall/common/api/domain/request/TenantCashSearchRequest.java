package com.nbsaas.nbmall.common.api.domain.request;

import com.haoxuer.bigworld.member.api.domain.request.TenantPageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月27日16:40:36.
*/

@Data
public class TenantCashSearchRequest extends TenantPageRequest {

    //提现配置
     @Search(name = "cashConfig.id",operator = Filter.Operator.eq)
     private Long cashConfig;

    //姓名
     @Search(name = "name",operator = Filter.Operator.like)
     private String name;

    //提现单号
     @Search(name = "no",operator = Filter.Operator.like)
     private String cashNo;



}