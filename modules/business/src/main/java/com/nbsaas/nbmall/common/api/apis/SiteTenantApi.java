package com.nbsaas.nbmall.common.api.apis;


import com.haoxuer.bigworld.pay.api.domain.page.TradeStreamPage;
import com.nbsaas.nbmall.common.api.domain.list.SiteTenantList;
import com.nbsaas.nbmall.common.api.domain.page.SiteTenantPage;
import com.nbsaas.nbmall.common.api.domain.request.*;
import com.nbsaas.nbmall.common.api.domain.response.SiteTenantResponse;

public interface SiteTenantApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    SiteTenantResponse create(SiteTenantDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    SiteTenantResponse update(SiteTenantDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    SiteTenantResponse delete(SiteTenantDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     SiteTenantResponse view(SiteTenantDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    SiteTenantList list(SiteTenantSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    SiteTenantPage search(SiteTenantSearchRequest request);

    TradeStreamPage stream(SiteTenantSearchRequest request) ;

    SiteTenantResponse viewInfo(SiteTenantDataRequest request);


}