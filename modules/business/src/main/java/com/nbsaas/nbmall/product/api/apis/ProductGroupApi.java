package com.nbsaas.nbmall.product.api.apis;


import com.nbsaas.nbmall.product.api.domain.list.ProductGroupList;
import com.nbsaas.nbmall.product.api.domain.page.ProductGroupPage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductGroupResponse;

public interface ProductGroupApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    ProductGroupResponse create(ProductGroupDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    ProductGroupResponse update(ProductGroupDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    ProductGroupResponse delete(ProductGroupDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     ProductGroupResponse view(ProductGroupDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    ProductGroupList list(ProductGroupSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    ProductGroupPage search(ProductGroupSearchRequest request);

}