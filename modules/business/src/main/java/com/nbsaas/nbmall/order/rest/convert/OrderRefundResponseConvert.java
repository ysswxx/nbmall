package com.nbsaas.nbmall.order.rest.convert;

import com.nbsaas.nbmall.order.api.domain.response.OrderRefundResponse;
import com.nbsaas.nbmall.order.data.entity.OrderRefund;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class OrderRefundResponseConvert implements Conver<OrderRefundResponse, OrderRefund> {
    @Override
    public OrderRefundResponse conver(OrderRefund source) {
        OrderRefundResponse result = new OrderRefundResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getOrderForm()!=null){
           result.setOrderForm(source.getOrderForm().getId());
        }
        if(source.getCreator()!=null){
           result.setCreator(source.getCreator().getId());
        }
         if(source.getCreator()!=null){
            result.setCreatorName(source.getCreator().getName());
         }

         result.setShopStateName(source.getShopState()+"");
         result.setRefundStateName(source.getRefundState()+"");
         result.setPlatformStateName(source.getPlatformState()+"");
        return result;
    }
}
