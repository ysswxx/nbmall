package com.nbsaas.nbmall.product.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.nbsaas.nbmall.product.data.entity.ProductSpec;

/**
 * Created by imake on 2021年12月10日17:38:01.
 */
public interface ProductSpecDao extends BaseDao<ProductSpec, Long> {

    ProductSpec findById(Long id);

    ProductSpec save(ProductSpec bean);

    ProductSpec updateByUpdater(Updater<ProductSpec> updater);

    ProductSpec deleteById(Long id);

    ProductSpec findById(Long tenant, Long id);

    ProductSpec deleteById(Long tenant, Long id);

    void deleteByProduct(Long id);
}