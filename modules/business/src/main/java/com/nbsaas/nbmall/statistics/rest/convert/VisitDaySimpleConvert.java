package com.nbsaas.nbmall.statistics.rest.convert;

import com.nbsaas.nbmall.statistics.api.domain.simple.VisitDaySimple;
import com.nbsaas.nbmall.statistics.data.entity.VisitDay;
import com.haoxuer.discover.data.rest.core.Conver;
public class VisitDaySimpleConvert implements Conver<VisitDaySimple, VisitDay> {


    @Override
    public VisitDaySimple conver(VisitDay source) {
        VisitDaySimple result = new VisitDaySimple();

            result.setId(source.getId());
             result.setUserNum(source.getUserNum());
             result.setPageNum(source.getPageNum());
             result.setAddDate(source.getAddDate());
             result.setKey(source.getKey());

        return result;
    }
}
