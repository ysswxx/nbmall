package com.nbsaas.nbmall.cart.data.entity;


import com.haoxuer.bigworld.member.data.entity.TenantUser;
import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.FieldConvert;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.SearchItem;
import com.nbsaas.nbmall.product.data.entity.Product;
import com.nbsaas.nbmall.product.data.entity.ProductSku;
import com.nbsaas.nbmall.product.data.enums.ProductType;
import com.nbsaas.nbmall.shop.data.entity.Shop;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

@Data
@FormAnnotation(title = "购物车管理", model = "购物车", menu = "1,123,124")
@Entity
@Table(name = "bs_tenant_cart")
public class Cart extends TenantEntity {


    @FieldConvert
    @SearchItem(label = "用户", name = "tenantUser", key = "tenantUser.id", operator = "eq", classType = "Long")
    @ManyToOne(fetch = FetchType.LAZY)
    private TenantUser tenantUser;

    @FieldConvert
    @SearchItem(label = "商家", name = "shop", key = "shop.id", operator = "eq", classType = "Long")
    @ManyToOne(fetch = FetchType.LAZY)
    private Shop shop;

    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private ProductSku sku;

    private Boolean checked;

    private Integer amount;

    private BigDecimal price;

    private BigDecimal total;

    private ProductType productType;

}
