package com.nbsaas.nbmall.order.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.math.BigDecimal;
import com.nbsaas.nbmall.order.data.enums.RefundState;
import com.nbsaas.nbmall.order.data.enums.RefundState;
import com.nbsaas.nbmall.order.data.enums.RefundState;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
/**
*
* Created by BigWorld on 2021年12月21日18:00:16.
*/

@Data
public class OrderRefundResponse extends ResponseObject {

    private Long id;

     private String no;
     private RefundState refundState;
     private BigDecimal shopMoney;
     private Long orderForm;
     private Long creator;
     private RefundState platformState;
     private String reason;
     private RefundState shopState;
     private String creatorName;
     private BigDecimal money;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private BigDecimal platformMoney;

     private String shopStateName;
     private String refundStateName;
     private String platformStateName;
}