package com.nbsaas.nbmall.product.data.entity;


import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.FieldConvert;
import com.nbsaas.codemake.annotation.FormAnnotation;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@FormAnnotation(title = "图片管理",model = "图片",menu = "1,107,109")
@Entity
@Table(name = "bs_tenant_product_image")
public class ProductImage extends TenantEntity {

    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;


    private String url;
}
