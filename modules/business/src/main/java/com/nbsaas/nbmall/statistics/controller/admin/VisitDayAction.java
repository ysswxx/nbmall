package com.nbsaas.nbmall.statistics.controller.admin;


import com.haoxuer.bigworld.tenant.controller.admin.TenantBaseAction;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
*
* Created by imake on 2021年12月23日11:45:35.
*/


@Scope("prototype")
@Controller
public class VisitDayAction extends TenantBaseAction {

	@RequiresPermissions("visitday")
	@RequestMapping("/tenant/visitday/view_list")
	public String list(ModelMap model) {
		return getView("visitday/list");
	}

}