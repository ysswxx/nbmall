package com.nbsaas.nbmall.video.rest.convert;

import com.nbsaas.nbmall.video.api.domain.simple.VideoCatalogSimple;
import com.nbsaas.nbmall.video.data.entity.VideoCatalog;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import lombok.Data;


@Data
public class VideoCatalogSimpleConvert implements Conver<VideoCatalogSimple, VideoCatalog> {

    private int fetch;

    @Override
    public VideoCatalogSimple conver(VideoCatalog source) {
        VideoCatalogSimple result = new VideoCatalogSimple();

         result.setLabel(source.getName());
         result.setValue(""+source.getId());
         if (fetch!=0&&source.getChildren()!=null&&source.getChildren().size()>0){
             result.setChildren(ConverResourceUtils.converList(source.getChildren(),this));
         }
            result.setId(source.getId());
            if(source.getParent()!=null){
               result.setParent(source.getParent().getId());
            }
             result.setCode(source.getCode());
             result.setLevelInfo(source.getLevelInfo());
             if(source.getParent()!=null){
                result.setParentName(source.getParent().getName());
             }
             result.setSortNum(source.getSortNum());
             result.setIds(source.getIds());
             result.setLastDate(source.getLastDate());
             result.setName(source.getName());
             result.setAddDate(source.getAddDate());

        return result;
    }
}
