package com.nbsaas.nbmall.product.api.apis;


import com.nbsaas.nbmall.product.api.domain.list.ProductSpecList;
import com.nbsaas.nbmall.product.api.domain.page.ProductSpecPage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductSpecResponse;

public interface ProductSpecApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    ProductSpecResponse create(ProductSpecDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    ProductSpecResponse update(ProductSpecDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    ProductSpecResponse delete(ProductSpecDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     ProductSpecResponse view(ProductSpecDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    ProductSpecList list(ProductSpecSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    ProductSpecPage search(ProductSpecSearchRequest request);

}