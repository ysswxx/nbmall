package com.nbsaas.nbmall.promote.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by BigWorld on 2021年12月25日12:45:48.
*/
@Data
public class FullReductionItemSimple implements Serializable {

    private Long id;

     private Integer reduceMoney;
     private Integer fullMoney;
     private Long reduction;
     private String reductionName;


}
