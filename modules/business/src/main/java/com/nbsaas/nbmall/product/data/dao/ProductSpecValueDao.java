package com.nbsaas.nbmall.product.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.product.data.entity.ProductSpecValue;

/**
* Created by imake on 2021年12月10日17:38:01.
*/
public interface ProductSpecValueDao extends BaseDao<ProductSpecValue,Long>{

	 ProductSpecValue findById(Long id);

	 ProductSpecValue save(ProductSpecValue bean);

	 ProductSpecValue updateByUpdater(Updater<ProductSpecValue> updater);

	 ProductSpecValue deleteById(Long id);

	 ProductSpecValue findById(Long tenant, Long id);

     ProductSpecValue deleteById(Long tenant, Long id);
}