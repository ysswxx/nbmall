package com.nbsaas.nbmall.common.rest.convert;

import com.nbsaas.nbmall.common.api.domain.response.SiteTenantResponse;
import com.nbsaas.nbmall.common.data.entity.SiteTenant;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class SiteTenantResponseConvert implements Conver<SiteTenantResponse, SiteTenant> {
    @Override
    public SiteTenantResponse conver(SiteTenant source) {
        SiteTenantResponse result = new SiteTenantResponse();
        BeanDataUtils.copyProperties(source,result);

        if(source.getTradeAccount()!=null){
           result.setTradeAccount(source.getTradeAccount().getId());
        }
        if(source.getCreator()!=null){
           result.setCreator(source.getCreator().getId());
        }
         if(source.getCreator()!=null){
            result.setCreatorName(source.getCreator().getName());
         }

         result.setStoreStateName(source.getStoreState()+"");

        return result;
    }
}
