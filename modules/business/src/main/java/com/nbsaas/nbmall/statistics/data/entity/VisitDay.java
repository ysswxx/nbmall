package com.nbsaas.nbmall.statistics.data.entity;


import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.ComposeView;
import com.nbsaas.codemake.annotation.FormAnnotation;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@ComposeView
@Data
@FormAnnotation(title = "每日访问", model = "每日访问", menu = "1,101,102")
@Entity
@Table(name = "bs_tenant_visit_day")
public class VisitDay extends TenantEntity {

     //2021-07-12
    @Column(name = "data_key", length = 12)
    private String key;

    private Long userNum;

    private Long pageNum;

}
