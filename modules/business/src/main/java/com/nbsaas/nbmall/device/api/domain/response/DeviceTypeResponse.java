package com.nbsaas.nbmall.device.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by imake on 2021年12月10日17:40:16.
*/

@Data
public class DeviceTypeResponse extends ResponseObject {

    private Long id;

     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date beginDate;

     private String note;

     private String website;

     private Long creator;

     private String doc;

     private String name;

     private String creatorName;

     private String className;

     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;


}