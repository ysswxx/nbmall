package com.nbsaas.nbmall.promote.api.apis;


import com.nbsaas.nbmall.promote.api.domain.list.CouponRuleList;
import com.nbsaas.nbmall.promote.api.domain.page.CouponRulePage;
import com.nbsaas.nbmall.promote.api.domain.request.*;
import com.nbsaas.nbmall.promote.api.domain.response.CouponRuleResponse;

public interface CouponRuleApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    CouponRuleResponse create(CouponRuleDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    CouponRuleResponse update(CouponRuleDataRequest request);

    /**
     * 删除
     *
     * @param request
     * @return
     */
    CouponRuleResponse delete(CouponRuleDataRequest request);


    /**
     * 详情
     *
     * @param request
     * @return
     */
    CouponRuleResponse view(CouponRuleDataRequest request);


    /**
     * @param request
     * @return
     */
    CouponRuleList list(CouponRuleSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    CouponRulePage search(CouponRuleSearchRequest request);

    CouponRulePage searchByReceive(CouponRuleSearchRequest request);

}