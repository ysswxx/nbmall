package com.nbsaas.nbmall.customer.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.customer.data.dao.CustomerAddressDao;
import com.nbsaas.nbmall.customer.data.entity.CustomerAddress;

/**
* Created by imake on 2021年12月10日22:56:19.
*/
@Repository

public class CustomerAddressDaoImpl extends CriteriaDaoImpl<CustomerAddress, Long> implements CustomerAddressDao {

	@Override
	public CustomerAddress findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public CustomerAddress save(CustomerAddress bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public CustomerAddress deleteById(Long id) {
		CustomerAddress entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<CustomerAddress> getEntityClass() {
		return CustomerAddress.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public CustomerAddress findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public CustomerAddress deleteById(Long tenant,Long id) {
		CustomerAddress entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}