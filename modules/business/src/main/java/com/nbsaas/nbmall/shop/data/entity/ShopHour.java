package com.nbsaas.nbmall.shop.data.entity;

import com.nbsaas.codemake.annotation.ComposeView;
import com.nbsaas.codemake.annotation.FormAnnotation;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@ComposeView
@Data
@FormAnnotation(title = "营业时间管理", model = "营业时间", menu = "1,101,102")
@Entity
@Table(name = "bs_tenant_shop_hour")
public class ShopHour extends ShopEntity {


    private String name;

    @Column(length = 10)
    private String beginTime;

    @Column(length = 10)
    private String  endTime;
}
