package com.nbsaas.nbmall.shop.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.shop.data.dao.ShopStaffDao;
import com.nbsaas.nbmall.shop.data.entity.ShopStaff;

/**
* Created by imake on 2021年12月10日17:38:00.
*/
@Repository

public class ShopStaffDaoImpl extends CriteriaDaoImpl<ShopStaff, Long> implements ShopStaffDao {

	@Override
	public ShopStaff findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public ShopStaff save(ShopStaff bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public ShopStaff deleteById(Long id) {
		ShopStaff entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<ShopStaff> getEntityClass() {
		return ShopStaff.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public ShopStaff findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public ShopStaff deleteById(Long tenant,Long id) {
		ShopStaff entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}