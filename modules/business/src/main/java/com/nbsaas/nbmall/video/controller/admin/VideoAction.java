package com.nbsaas.nbmall.video.controller.admin;


import com.haoxuer.bigworld.tenant.controller.admin.TenantBaseAction;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
*
* Created by imake on 2022年03月16日21:26:00.
*/


@Scope("prototype")
@Controller
public class VideoAction extends TenantBaseAction {

	@RequiresPermissions("video")
	@RequestMapping("/tenant/video/view_list")
	public String list(ModelMap model) {
		return getView("video/list");
	}

}