package com.nbsaas.nbmall.product.api.domain.request;

import com.haoxuer.bigworld.member.api.domain.request.TenantPageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月10日17:38:01.
*/

@Data
public class FreightTemplateSearchRequest extends TenantPageRequest {

    //模板名称
     @Search(name = "name",operator = Filter.Operator.like)
     private String name;



}