package com.nbsaas.nbmall.product.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.product.data.entity.ProductSku;

/**
* Created by imake on 2021年12月10日17:38:01.
*/
public interface ProductSkuDao extends BaseDao<ProductSku,Long>{

	 ProductSku findById(Long id);

	 ProductSku save(ProductSku bean);

	 ProductSku updateByUpdater(Updater<ProductSku> updater);

	 ProductSku deleteById(Long id);

	 ProductSku findById(Long tenant, Long id);

     ProductSku deleteById(Long tenant, Long id);
}