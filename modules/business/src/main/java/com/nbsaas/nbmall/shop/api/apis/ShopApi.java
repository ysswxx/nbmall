package com.nbsaas.nbmall.shop.api.apis;


import com.haoxuer.bigworld.pay.api.domain.page.TradeStreamPage;
import com.nbsaas.nbmall.shop.api.domain.list.ShopList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopResponse;

public interface ShopApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    ShopResponse create(ShopDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    ShopResponse update(ShopDataRequest request);

    /**
     * 删除
     *
     * @param request
     * @return
     */
    ShopResponse delete(ShopDataRequest request);


    /**
     * 详情
     *
     * @param request
     * @return
     */
    ShopResponse view(ShopDataRequest request);


    /**
     * @param request
     * @return
     */
    ShopList list(ShopSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    ShopPage search(ShopSearchRequest request);

    ShopResponse audit(ShopDataRequest request);

    /**
     * 商家流水
     * @param request
     * @return
     */
    TradeStreamPage stream(ShopSearchRequest request);

    ShopResponse bind(String code, String state);
}