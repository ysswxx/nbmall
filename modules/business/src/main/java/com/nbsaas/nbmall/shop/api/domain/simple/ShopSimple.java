package com.nbsaas.nbmall.shop.api.domain.simple;


import java.io.Serializable;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.nbsaas.nbmall.shop.data.enums.SubjectType;
import com.nbsaas.nbmall.shop.data.enums.AuditState;
import com.nbsaas.nbmall.shop.data.enums.ReceivingMode;
import com.nbsaas.nbmall.shop.data.enums.ShopState;
import com.nbsaas.nbmall.shop.data.enums.SubjectType;
import com.haoxuer.discover.data.enums.StoreState;

/**
 * Created by BigWorld on 2021年12月10日17:38:00.
 */
@Data
public class ShopSimple implements Serializable {

    private Long id;

    private Integer area;
    private String summary;
    private String address;
    private Long creator;
    private StoreState storeState;
    private Integer dishDeliveryMethod;
    private String contact;
    private String bossName;
    private Boolean transport;
    private Boolean takeaway;
    private Long baseSaleNum;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date addDate;
    private String url;
    private BigDecimal perMoney;
    private Double takeawayRate;
    private String name;
    private String shopCatalogName;
    private Integer dishSaleNum;
    private SubjectType mealSubject;
    private Long dishAmount;
    private Long boss;
    private Integer discountNum;
    private String note;
    private Integer city;
    private Integer deliveryMethod;
    private Integer distance;
    private Integer deliveryTime;
    private ShopState shopState;
    private Long channel;
    private Long sortScore;
    private AuditState auditState;
    private Float score;
    private String beginTime;
    private String tel;
    private Double cashRate;
    private Double lat;
    private Integer balanceMethod;
    private Integer receivingTime;
    private Long ext;
    private BigDecimal startPrice;
    private Boolean hallFood;
    private String thumbnail;
    private Double lng;
    private Integer productNum;
    private Boolean cashModel;
    private BigDecimal amount;
    private Long saleNum;
    private Integer eatOutTime;
    private String endTime;
    private SubjectType freightSubject;
    private Boolean foodModel;
    private Boolean autoOrder;
    private Boolean productModel;
    private Long refundNum;
    private Boolean serviceModel;
    private BigDecimal freight;
    private Double selfRate;
    private Integer staffNum;
    private Integer province;
    private ReceivingMode receivingMode;
    private Integer transportTime;
    private Double hallFoodRate;
    private Integer sortNum;
    private String logo;
    private Integer shopCatalog;

    private String freightSubjectName;
    private String auditStateName;
    private String receivingModeName;
    private String shopStateName;
    private String mealSubjectName;
    private String storeStateName;

    private String style;

    private String basicTxt;

    private String distanceTxt;

    private String saleTxt;

    /**
     * 配送时间
     */

    private BigDecimal tradeAccountAmount;

    private List<String> labels;

}
