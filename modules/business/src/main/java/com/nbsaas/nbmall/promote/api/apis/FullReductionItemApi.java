package com.nbsaas.nbmall.promote.api.apis;


import com.nbsaas.nbmall.promote.api.domain.list.FullReductionItemList;
import com.nbsaas.nbmall.promote.api.domain.page.FullReductionItemPage;
import com.nbsaas.nbmall.promote.api.domain.request.*;
import com.nbsaas.nbmall.promote.api.domain.response.FullReductionItemResponse;

public interface FullReductionItemApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    FullReductionItemResponse create(FullReductionItemDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    FullReductionItemResponse update(FullReductionItemDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    FullReductionItemResponse delete(FullReductionItemDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     FullReductionItemResponse view(FullReductionItemDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    FullReductionItemList list(FullReductionItemSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    FullReductionItemPage search(FullReductionItemSearchRequest request);

}