package com.nbsaas.nbmall.test.api.apis;

import com.haoxuer.bigworld.tenant.api.domain.request.TenantRequest;
import com.haoxuer.discover.rest.base.ResponseObject;

public interface TestApi {


    ResponseObject testTrade(TenantRequest request);
}
