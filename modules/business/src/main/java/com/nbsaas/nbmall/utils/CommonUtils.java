package com.nbsaas.nbmall.utils;

import com.haoxuer.discover.data.utils.DateUtils;

import java.util.Date;

public class CommonUtils {

    public static String dayKey(String key) {
        return DateUtils.simple(new Date()) + "-" + key;
    }
}
