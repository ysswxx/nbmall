package com.nbsaas.nbmall.video.rest.convert;

import com.nbsaas.nbmall.video.api.domain.response.VideoCatalogResponse;
import com.nbsaas.nbmall.video.data.entity.VideoCatalog;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class VideoCatalogResponseConvert implements Conver<VideoCatalogResponse, VideoCatalog> {
    @Override
    public VideoCatalogResponse conver(VideoCatalog source) {
        VideoCatalogResponse result = new VideoCatalogResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getParent()!=null){
           result.setParent(source.getParent().getId());
        }
         if(source.getParent()!=null){
            result.setParentName(source.getParent().getName());
         }

        return result;
    }
}
