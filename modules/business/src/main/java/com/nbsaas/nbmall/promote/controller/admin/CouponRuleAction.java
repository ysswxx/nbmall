package com.nbsaas.nbmall.promote.controller.admin;


import com.haoxuer.bigworld.tenant.controller.admin.TenantBaseAction;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
*
* Created by imake on 2021年12月28日23:18:26.
*/


@Scope("prototype")
@Controller
public class CouponRuleAction extends TenantBaseAction {

	@RequiresPermissions("couponrule")
	@RequestMapping("/tenant/couponrule/view_list")
	public String list(ModelMap model) {
		return getView("couponrule/list");
	}

}