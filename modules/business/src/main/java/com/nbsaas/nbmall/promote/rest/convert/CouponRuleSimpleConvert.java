package com.nbsaas.nbmall.promote.rest.convert;

import com.nbsaas.nbmall.promote.api.domain.simple.CouponRuleSimple;
import com.nbsaas.nbmall.promote.data.entity.CouponRule;
import com.haoxuer.discover.data.rest.core.Conver;
public class CouponRuleSimpleConvert implements Conver<CouponRuleSimple, CouponRule> {


    @Override
    public CouponRuleSimple conver(CouponRule source) {
        CouponRuleSimple result = new CouponRuleSimple();

            result.setId(source.getId());
            if(source.getShop()!=null){
               result.setShop(source.getShop().getId());
            }
             result.setSendEndTime(source.getSendEndTime());
             result.setLimitNum(source.getLimitNum());
             result.setNote(source.getNote());
             result.setStoreState(source.getStoreState());
             result.setExpireType(source.getExpireType());
             if(source.getShop()!=null){
                result.setShopName(source.getShop().getName());
             }
             result.setUseBeginTime(source.getUseBeginTime());
             result.setSendBeginTime(source.getSendBeginTime());
             result.setMoney(source.getMoney());
             result.setCouponCatalog(source.getCouponCatalog());
             result.setAddDate(source.getAddDate());
             result.setCouponState(source.getCouponState());
             result.setUseEndTime(source.getUseEndTime());
             result.setCouponScope(source.getCouponScope());
             result.setTakeawayRate(source.getTakeawayRate());
             result.setUseNum(source.getUseNum());
             result.setMinPrice(source.getMinPrice());
             result.setLogo(source.getLogo());
             result.setShowType(source.getShowType());
             result.setName(source.getName());
             result.setUseDay(source.getUseDay());
             result.setStock(source.getStock());
             result.setSendNum(source.getSendNum());

             result.setCouponStateName(source.getCouponState()+"");
             result.setExpireTypeName(source.getExpireType()+"");
             result.setCouponCatalogName(source.getCouponCatalog()+"");
             result.setShowTypeName(source.getShowType()+"");
             result.setStoreStateName(source.getStoreState()+"");
        return result;
    }
}
