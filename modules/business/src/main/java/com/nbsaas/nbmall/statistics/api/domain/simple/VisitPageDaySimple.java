package com.nbsaas.nbmall.statistics.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by BigWorld on 2021年12月23日11:45:35.
*/
@Data
public class VisitPageDaySimple implements Serializable {

    private Long id;

     private Long user;
     private Long pageNum;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private String key;


}
