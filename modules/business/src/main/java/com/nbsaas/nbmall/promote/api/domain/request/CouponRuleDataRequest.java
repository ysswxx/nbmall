package com.nbsaas.nbmall.promote.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import com.nbsaas.nbmall.promote.data.enums.CouponState;
import com.nbsaas.nbmall.promote.data.enums.ExpireType;
import com.nbsaas.nbmall.promote.data.enums.CouponCatalog;
import com.nbsaas.nbmall.promote.data.enums.ShowType;
import com.haoxuer.discover.data.enums.StoreState;
import java.util.Date;
import java.math.BigDecimal;

/**
*
* Created by imake on 2021年12月28日23:18:26.
*/

@Data
public class CouponRuleDataRequest extends TenantRequest {

    private Long id;

     private Long shop;
     private Date sendEndTime;
     private Integer limitNum;
     private String note;
     private StoreState storeState;
     private ExpireType expireType;
     private Date useBeginTime;
     private Date sendBeginTime;
     private BigDecimal money;
     private CouponCatalog couponCatalog;
     private CouponState couponState;
     private Date useEndTime;
     private Short couponScope;
     private Integer takeawayRate;
     private Long useNum;
     private BigDecimal minPrice;
     private String logo;
     private ShowType showType;
     private String name;
     private Integer useDay;
     private Integer stock;
     private Long sendNum;

}