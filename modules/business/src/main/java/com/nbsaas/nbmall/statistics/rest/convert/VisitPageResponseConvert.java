package com.nbsaas.nbmall.statistics.rest.convert;

import com.nbsaas.nbmall.statistics.api.domain.response.VisitPageResponse;
import com.nbsaas.nbmall.statistics.data.entity.VisitPage;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class VisitPageResponseConvert implements Conver<VisitPageResponse, VisitPage> {
    @Override
    public VisitPageResponse conver(VisitPage source) {
        VisitPageResponse result = new VisitPageResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getUser()!=null){
           result.setUser(source.getUser().getId());
        }

        return result;
    }
}
