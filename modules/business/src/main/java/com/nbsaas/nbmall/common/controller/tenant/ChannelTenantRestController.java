package com.nbsaas.nbmall.common.controller.tenant;

import com.nbsaas.nbmall.common.api.apis.ChannelApi;
import com.nbsaas.nbmall.common.api.domain.list.ChannelList;
import com.nbsaas.nbmall.common.api.domain.page.ChannelPage;
import com.nbsaas.nbmall.common.api.domain.request.*;
import com.nbsaas.nbmall.common.api.domain.response.ChannelResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@RequestMapping("/tenantRest/channel")
@RestController
public class ChannelTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("channel")
    @RequiresUser
    @RequestMapping("create")
    public ChannelResponse create(ChannelDataRequest request) {
        init(request);
        request.setCreator(request.getCreateUser());
        return api.create(request);
    }

	@RequiresPermissions("channel")
    @RequiresUser
    @RequestMapping("update")
    public ChannelResponse update(ChannelDataRequest request) {
        init(request);
        return api.update(request);
    }

	@RequiresPermissions("channel")
    @RequiresUser
    @RequestMapping("delete")
    public ChannelResponse delete(ChannelDataRequest request) {
        init(request);
        ChannelResponse result = new ChannelResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("channel")
    @RequiresUser
    @RequestMapping("view")
    public ChannelResponse view(ChannelDataRequest request) {
       init(request);
       return api.view(request);
   }

	@RequiresPermissions("channel")
    @RequiresUser
    @RequestMapping("list")
    public ChannelList list(ChannelSearchRequest request) {
        init(request);
        return api.list(request);
    }

	@RequiresPermissions("channel")
    @RequiresUser
    @RequestMapping("search")
    public ChannelPage search(ChannelSearchRequest request) {
        init(request);
        return api.search(request);
    }

    @Autowired
    private ChannelApi api;

}
