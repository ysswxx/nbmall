package com.nbsaas.nbmall.product.rest.convert;

import com.nbsaas.nbmall.product.api.domain.response.ProductImageResponse;
import com.nbsaas.nbmall.product.data.entity.ProductImage;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class ProductImageResponseConvert implements Conver<ProductImageResponse, ProductImage> {
    @Override
    public ProductImageResponse conver(ProductImage source) {
        ProductImageResponse result = new ProductImageResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getProduct()!=null){
           result.setProduct(source.getProduct().getId());
        }

        return result;
    }
}
