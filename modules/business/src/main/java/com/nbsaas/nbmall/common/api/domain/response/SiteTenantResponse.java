package com.nbsaas.nbmall.common.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.haoxuer.discover.data.enums.StoreState;

/**
 * Created by imake on 2021年12月23日11:54:02.
 */

@Data
public class SiteTenantResponse extends ResponseObject {

    private Long id;

    private Long tradeAccount;

    private String note;

    private String address;

    private Double lng;

    private Long creator;

    private StoreState storeState;

    private String shopStyle;

    private String creatorName;

    private String demo;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date addDate;

    private String domain;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date beginDate;

    private String phone;

    private String logo;

    private String chatSign;

    private String name;

    private String theme;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date expireDate;

    private String key;

    private Double lat;


    private String storeStateName;

    /**
     * 经营信息
     */
    private BigDecimal today;

    private BigDecimal yesterday;

    private Long todayNum;

    private Long yesterdayNum;

    private Long todayCollect;

    private Long yesterdayCollect;

    private BigDecimal money;
}