package com.nbsaas.nbmall.product.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.product.data.entity.Product;

/**
* Created by imake on 2021年12月28日18:00:18.
*/
public interface ProductDao extends BaseDao<Product,Long>{

	 Product findById(Long id);

	 Product save(Product bean);

	 Product updateByUpdater(Updater<Product> updater);

	 Product deleteById(Long id);

	 Product findById(Long tenant, Long id);

     Product deleteById(Long tenant, Long id);
}