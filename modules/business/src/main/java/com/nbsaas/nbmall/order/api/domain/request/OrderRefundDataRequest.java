package com.nbsaas.nbmall.order.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import com.nbsaas.nbmall.order.data.enums.RefundState;
import com.nbsaas.nbmall.order.data.enums.RefundState;
import com.nbsaas.nbmall.order.data.enums.RefundState;
import java.util.Date;
import java.math.BigDecimal;

/**
*
* Created by imake on 2021年12月21日18:00:16.
*/

@Data
public class OrderRefundDataRequest extends TenantRequest {

    private Long id;

     private String no;
     private RefundState refundState;
     private BigDecimal shopMoney;
     private Long orderForm;
     private Long creator;
     private RefundState platformState;
     private String reason;
     private RefundState shopState;
     private BigDecimal money;
     private BigDecimal platformMoney;

}