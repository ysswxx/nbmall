package com.nbsaas.nbmall.product.controller.rest;

import com.nbsaas.nbmall.product.api.apis.FreightTemplateApi;
import com.nbsaas.nbmall.product.api.domain.list.FreightTemplateList;
import com.nbsaas.nbmall.product.api.domain.page.FreightTemplatePage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.FreightTemplateResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/freighttemplate")
@RestController
public class FreightTemplateRestController extends BaseRestController {


    @RequestMapping("create")
    public FreightTemplateResponse create(FreightTemplateDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

    @RequestMapping("delete")
    public FreightTemplateResponse delete(FreightTemplateDataRequest request) {
        initTenant(request);
        FreightTemplateResponse result = new FreightTemplateResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public FreightTemplateResponse update(FreightTemplateDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public FreightTemplateResponse view(FreightTemplateDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public FreightTemplateList list(FreightTemplateSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public FreightTemplatePage search(FreightTemplateSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private FreightTemplateApi api;

}
