package com.nbsaas.nbmall.common.api.domain.page;


import com.nbsaas.nbmall.common.api.domain.simple.ChannelSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年12月10日15:46:16.
*/

@Data
public class ChannelPage  extends ResponsePage<ChannelSimple> {

}