package com.nbsaas.nbmall.shop.rest.convert;

import com.nbsaas.nbmall.shop.api.domain.simple.ShopExtSimple;
import com.nbsaas.nbmall.shop.data.entity.ShopExt;
import com.haoxuer.discover.data.rest.core.Conver;
public class ShopExtSimpleConvert implements Conver<ShopExtSimple, ShopExt> {


    @Override
    public ShopExtSimple conver(ShopExt source) {
        ShopExtSimple result = new ShopExtSimple();

            result.setId(source.getId());
            if(source.getShop()!=null){
               result.setShop(source.getShop().getId());
            }
             result.setNote(source.getNote());
             result.setCardNoFront(source.getCardNoFront());
             result.setContact(source.getContact());
             result.setCardNo(source.getCardNo());
             result.setAddDate(source.getAddDate());
             result.setLicense(source.getLicense());
             result.setLetter(source.getLetter());
             result.setPhone(source.getPhone());
             result.setCardNoReverse(source.getCardNoReverse());
             result.setName(source.getName());
             result.setPermit(source.getPermit());
             result.setCardNoHand(source.getCardNoHand());

        return result;
    }
}
