package com.nbsaas.nbmall.shop.data.entity;


import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.FieldConvert;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.FormField;
import com.nbsaas.codemake.annotation.InputType;
import lombok.Data;

import javax.persistence.*;

@Data
@FormAnnotation(title = "商家扩展管理", model = "商家扩展信息", menu = "1,101,102")
@Entity
@Table(name = "bs_tenant_shop_ext")
public class ShopExt extends TenantEntity {


    @FieldConvert
    @OneToOne(mappedBy ="ext",fetch = FetchType.LAZY)
    private Shop shop;

    @FormField(title = "商家别名", grid = true, required = true)
    private String name;

    @FormField(title = "联系人姓名", grid = true, required = true)
    private String contact;


    @FormField(title = "联系人电话", grid = true, required = true)
    private String phone;

    @FormField(title = "法人身份证", grid = true, required = true)
    private String cardNo;

    @FormField(title = "法人身份证正面", grid = true, type = InputType.image)
    private String cardNoFront;

    @FormField(title = "法人身份证反面", grid = true, type = InputType.image)
    private String cardNoReverse ;

    @FormField(title = "营业执照", grid = true,type = InputType.image)
    private String license ;

    @FormField(title = "银行开户许可证", grid = true, type = InputType.image)
    private String permit ;


    @FormField(title = "法人手持身份证", grid = true, type = InputType.image)
    private String cardNoHand ;

    @FormField(title = "平台联盟商家承诺书", grid = true, type = InputType.image)
    private String letter ;

    @Column(length = 100)
    @FormField(title = "商家简介", grid = true, required = true,type = InputType.textarea)
    private String note;
}
