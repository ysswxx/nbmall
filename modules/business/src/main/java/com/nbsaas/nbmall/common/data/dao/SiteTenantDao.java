package com.nbsaas.nbmall.common.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.common.data.entity.SiteTenant;

/**
* Created by imake on 2021年12月23日11:54:02.
*/
public interface SiteTenantDao extends BaseDao<SiteTenant,Long>{

	 SiteTenant findById(Long id);

	 SiteTenant save(SiteTenant bean);

	 SiteTenant updateByUpdater(Updater<SiteTenant> updater);

	 SiteTenant deleteById(Long id);
}