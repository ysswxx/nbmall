package com.nbsaas.nbmall.shop.rest.convert;

import com.nbsaas.nbmall.shop.api.domain.response.ShopExtResponse;
import com.nbsaas.nbmall.shop.data.entity.ShopExt;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class ShopExtResponseConvert implements Conver<ShopExtResponse, ShopExt> {
    @Override
    public ShopExtResponse conver(ShopExt source) {
        ShopExtResponse result = new ShopExtResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getShop()!=null){
           result.setShop(source.getShop().getId());
        }

        return result;
    }
}
