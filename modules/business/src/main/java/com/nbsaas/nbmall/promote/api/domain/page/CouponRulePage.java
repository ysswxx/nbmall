package com.nbsaas.nbmall.promote.api.domain.page;


import com.nbsaas.nbmall.promote.api.domain.simple.CouponRuleSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年12月28日23:18:26.
*/

@Data
public class CouponRulePage  extends ResponsePage<CouponRuleSimple> {

}