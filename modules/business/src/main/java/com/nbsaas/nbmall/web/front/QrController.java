package com.nbsaas.nbmall.web.front;

import com.haoxuer.discover.qrcode.view.QRView;
import com.haoxuer.discover.web.controller.front.BaseController;
import com.nbsaas.nbmall.third.service.WxService;
import com.nbsaas.nbmall.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;

@Scope("prototype")
@Controller
public class QrController extends BaseController {

    public static final int SIZE = 100;

    @Autowired
    private WxService wxService;



    @RequestMapping("/dishcoupon/{id}")
    public ModelAndView view(Model model, @PathVariable("id") String id) {
        model.addAttribute("width", 500);
        model.addAttribute("height", 500);

        model.addAttribute("code", "https://cloud.newbyte.ltd/dishcoupon/" + id + ".htm");

        ModelAndView result = new ModelAndView();
        result.setView(new QRView());
        return result;
    }

    @RequestMapping("/bind_code/{tenant}/{id}")
    public ModelAndView bind(Model model,@PathVariable("tenant") Long tenant,  @PathVariable("id") String id) {
        model.addAttribute("width", 500);
        model.addAttribute("height", 500);
        String url = wxService.getWxMpService(tenant).getOAuth2Service().buildAuthorizationUrl("https://nbmall.nbsaas.com/bindShop.htm", "snsapi_userinfo", id);
        model.addAttribute("code", url);
        ModelAndView result = new ModelAndView();
        result.setView(new QRView());
        return result;
    }

    @RequestMapping("/store_code/{id}")
    public ModelAndView store(Model model, @PathVariable("id") String id) {
        model.addAttribute("width", 500);
        model.addAttribute("height", 500);

        model.addAttribute("code", "https://saas.metamall.cn/store/" + id + "");

        ModelAndView result = new ModelAndView();
        result.setView(new QRView());
        return result;
    }

    @RequestMapping("/qrCode/{id}")
    public ModelAndView qrCode(Model model, @PathVariable("id") String id) {
        model.addAttribute("width", 500);
        model.addAttribute("height", 500);

        model.addAttribute("code", "https://saas.metamall.cn/scan/" + id + "");

        ModelAndView result = new ModelAndView();
        result.setView(new QRView());
        return result;
    }
    @RequestMapping("/dish_coupon/{tenant}/{id}")
    public ModelAndView qrCode(Model model,@PathVariable("tenant") Long tenant, @PathVariable("id") String id) {
        model.addAttribute("width", 500);
        model.addAttribute("height", 500);

        HashMap<String,Object> map=new HashMap<>();
        map.put("tenant",tenant);
        map.put("id",id);
        String body= JsonUtils.json(map);

        model.addAttribute("code", body);

        ModelAndView result = new ModelAndView();
        result.setView(new QRView());
        return result;
    }
    @RequestMapping("/dish_coupon2/{tenant}/1")
    public ModelAndView qrCode2(Model model,@PathVariable("tenant") Long tenant, String id) {
        model.addAttribute("width", 500);
        model.addAttribute("height", 500);
        model.addAttribute("code", "https://cloud.newbyte.ltd/dishcoupon/"+tenant+"/1.htm?id="+id);

        ModelAndView result = new ModelAndView();
        result.setView(new QRView());
        return result;
    }
}
