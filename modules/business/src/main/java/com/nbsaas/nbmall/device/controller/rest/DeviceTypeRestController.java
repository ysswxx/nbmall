package com.nbsaas.nbmall.device.controller.rest;

import com.nbsaas.nbmall.device.api.apis.DeviceTypeApi;
import com.nbsaas.nbmall.device.api.domain.list.DeviceTypeList;
import com.nbsaas.nbmall.device.api.domain.page.DeviceTypePage;
import com.nbsaas.nbmall.device.api.domain.request.*;
import com.nbsaas.nbmall.device.api.domain.response.DeviceTypeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.rest.BaseRestController;

@RequestMapping("/rest/devicetype")
@RestController
public class DeviceTypeRestController extends BaseRestController {


    @RequestMapping("create")
    public DeviceTypeResponse create(DeviceTypeDataRequest request) {
        init(request);
        request.setCreator(request.getUser());
        return api.create(request);
    }

    @RequestMapping("update")
    public DeviceTypeResponse update(DeviceTypeDataRequest request) {
        init(request);
        return api.update(request);
    }

    @RequestMapping("delete")
    public DeviceTypeResponse delete(DeviceTypeDataRequest request) {
        init(request);
        DeviceTypeResponse result = new DeviceTypeResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("view")
    public DeviceTypeResponse view(DeviceTypeDataRequest request) {
        init(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public DeviceTypeList list(DeviceTypeSearchRequest request) {
        init(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public DeviceTypePage search(DeviceTypeSearchRequest request) {
        init(request);
        return api.search(request);
    }



    @Autowired
    private DeviceTypeApi api;

}
