package com.nbsaas.nbmall.customer.rest.resource;

import com.nbsaas.nbmall.customer.api.apis.CustomerAddressApi;
import com.nbsaas.nbmall.customer.api.domain.list.CustomerAddressList;
import com.nbsaas.nbmall.customer.api.domain.page.CustomerAddressPage;
import com.nbsaas.nbmall.customer.api.domain.request.*;
import com.nbsaas.nbmall.customer.api.domain.response.CustomerAddressResponse;
import com.nbsaas.nbmall.customer.data.dao.CustomerAddressDao;
import com.nbsaas.nbmall.customer.data.entity.Customer;
import com.nbsaas.nbmall.customer.data.entity.CustomerAddress;
import com.nbsaas.nbmall.customer.rest.convert.CustomerAddressResponseConvert;
import com.nbsaas.nbmall.customer.rest.convert.CustomerAddressSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.nbsaas.nbmall.customer.data.dao.CustomerDao;
import com.haoxuer.discover.area.data.dao.AreaDao;
import com.haoxuer.discover.area.data.dao.AreaDao;
import com.haoxuer.discover.area.data.dao.AreaDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class CustomerAddressResource implements CustomerAddressApi {

    @Autowired
    private CustomerAddressDao dataDao;

    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private AreaDao provinceDao;
    @Autowired
    private AreaDao areaDao;
    @Autowired
    private AreaDao cityDao;


    @Override
    public CustomerAddressResponse create(CustomerAddressDataRequest request) {
        CustomerAddressResponse result = new CustomerAddressResponse();

        if (request.getCustomer() == null) {
            result.setCode(501);
            result.setMsg("无效用户");
            return result;
        }
        if (request.getDefaultAddress() == null) {
            request.setDefaultAddress(false);
        }
        CustomerAddress bean = new CustomerAddress();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new CustomerAddressResponseConvert().conver(bean);
        if (request.getDefaultAddress()) {
            Customer customer = bean.getCustomer();
            if (customer != null) {
                customer.setAddressId(bean.getId());
            }
        }
        return result;
    }

    @Override
    public CustomerAddressResponse update(CustomerAddressDataRequest request) {
        CustomerAddressResponse result = new CustomerAddressResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        CustomerAddress bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new CustomerAddressResponseConvert().conver(bean);
        return result;
    }

    private void handleData(CustomerAddressDataRequest request, CustomerAddress bean) {
        TenantBeanUtils.copyProperties(request, bean);
        if (request.getArea() != null) {
            bean.setArea(areaDao.findById(request.getArea()));
        }
        if (request.getCity() != null) {
            bean.setCity(cityDao.findById(request.getCity()));
        }
        if (request.getProvince() != null) {
            bean.setProvince(provinceDao.findById(request.getProvince()));
        }
        if (request.getCustomer() != null) {
            bean.setCustomer(customerDao.findById(request.getCustomer()));
        }

    }

    @Override
    public CustomerAddressResponse delete(CustomerAddressDataRequest request) {
        CustomerAddressResponse result = new CustomerAddressResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(), request.getId());
        return result;
    }

    @Override
    public CustomerAddressResponse view(CustomerAddressDataRequest request) {
        CustomerAddressResponse result = new CustomerAddressResponse();
        CustomerAddress bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean == null) {
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result = new CustomerAddressResponseConvert().conver(bean);
        return result;
    }

    @Override
    public CustomerAddressList list(CustomerAddressSearchRequest request) {
        CustomerAddressList result = new CustomerAddressList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())) {
            orders.add(Order.asc("" + request.getSortField()));
        } else if ("desc".equals(request.getSortMethod())) {
            orders.add(Order.desc("" + request.getSortField()));
        } else {
            orders.add(Order.desc("id"));
        }
        List<CustomerAddress> organizations = dataDao.list(0, request.getSize(), filters, orders);

        CustomerAddressSimpleConvert convert = new CustomerAddressSimpleConvert();
        ConverResourceUtils.converList(result, organizations, convert);
        return result;
    }

    @Override
    public CustomerAddressPage search(CustomerAddressSearchRequest request) {
        CustomerAddressPage result = new CustomerAddressPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())) {
            pageable.getOrders().add(Order.asc("" + request.getSortField()));
        } else if ("desc".equals(request.getSortMethod())) {
            pageable.getOrders().add(Order.desc("" + request.getSortField()));
        } else {
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<CustomerAddress> page = dataDao.page(pageable);

        CustomerAddressSimpleConvert convert = new CustomerAddressSimpleConvert();
        ConverResourceUtils.converPage(result, page, convert);
        return result;
    }
}
