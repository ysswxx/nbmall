package com.nbsaas.nbmall.shop.rest.resource;

import com.nbsaas.nbmall.shop.api.apis.ShopHourApi;
import com.nbsaas.nbmall.shop.api.domain.list.ShopHourList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopHourPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopHourResponse;
import com.nbsaas.nbmall.shop.data.dao.ShopHourDao;
import com.nbsaas.nbmall.shop.data.entity.ShopHour;
import com.nbsaas.nbmall.shop.rest.convert.ShopHourResponseConvert;
import com.nbsaas.nbmall.shop.rest.convert.ShopHourSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.nbsaas.nbmall.shop.data.dao.ShopDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class ShopHourResource implements ShopHourApi {

    @Autowired
    private ShopHourDao dataDao;

    @Autowired
    private ShopDao shopDao;


    @Override
    public ShopHourResponse create(ShopHourDataRequest request) {
        ShopHourResponse result = new ShopHourResponse();

        ShopHour bean = new ShopHour();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new ShopHourResponseConvert().conver(bean);
        return result;
    }

    @Override
    public ShopHourResponse update(ShopHourDataRequest request) {
        ShopHourResponse result = new ShopHourResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        ShopHour bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new ShopHourResponseConvert().conver(bean);
        return result;
    }

    private void handleData(ShopHourDataRequest request, ShopHour bean) {
       TenantBeanUtils.copyProperties(request,bean);
           if(request.getShop()!=null){
              bean.setShop(shopDao.findById(request.getShop()));
           }

    }

    @Override
    public ShopHourResponse delete(ShopHourDataRequest request) {
        ShopHourResponse result = new ShopHourResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public ShopHourResponse view(ShopHourDataRequest request) {
        ShopHourResponse result=new ShopHourResponse();
        ShopHour bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new ShopHourResponseConvert().conver(bean);
        return result;
    }
    @Override
    public ShopHourList list(ShopHourSearchRequest request) {
        ShopHourList result = new ShopHourList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<ShopHour> organizations = dataDao.list(0, request.getSize(), filters, orders);

        ShopHourSimpleConvert convert=new ShopHourSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public ShopHourPage search(ShopHourSearchRequest request) {
        ShopHourPage result=new ShopHourPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<ShopHour> page=dataDao.page(pageable);

        ShopHourSimpleConvert convert=new ShopHourSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
