package com.nbsaas.nbmall.product.rest.convert;

import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.nbsaas.nbmall.product.api.domain.response.ProductResponse;
import com.nbsaas.nbmall.product.api.domain.simple.ProductImageSimple;
import com.nbsaas.nbmall.product.api.domain.simple.ProductSkuSimple;
import com.nbsaas.nbmall.product.api.domain.simple.ProductSpecSimple;
import com.nbsaas.nbmall.product.data.entity.Product;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.nbsaas.nbmall.product.data.entity.ProductCatalog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProductResponseConvert implements Conver<ProductResponse, Product> {
    @Override
    public ProductResponse conver(Product source) {
        ProductResponse result = new ProductResponse();
        if (source.getSkuEnable() == null) {
            source.setSkuEnable(false);
        }
        TenantBeanUtils.copyProperties(source, result);

        if (source.getCreator() != null) {
            result.setCreator(source.getCreator().getId());
        }
        if (source.getProductGroup() != null) {
            result.setProductGroupName(source.getProductGroup().getName());
        }
        if (source.getProductGroup() != null) {
            result.setProductGroup(source.getProductGroup().getId());
        }
        if (source.getShop() != null) {
            result.setShop(source.getShop().getId());
        }
        if (source.getProductCatalog() != null) {
            result.setProductCatalogName(source.getProductCatalog().getName());
            List<String> catalogs = new ArrayList<>();
            ProductCatalog catalog = source.getProductCatalog();
            catalogs.add(catalog.getId() + "");
            while (catalog.getParent() != null) {
                catalogs.add(catalog.getParent().getId() + "");
                catalog = catalog.getParent();
            }
            Collections.reverse(catalogs);
            result.setCatalogs(catalogs);
        }
        if (source.getShop() != null) {
            result.setShopName(source.getShop().getName());
        }
        if (source.getProductCatalog() != null) {
            result.setProductCatalog(source.getProductCatalog().getId());
        }

        result.setProductStateName(source.getProductState() + "");
        result.setStoreStateName(source.getStoreState() + "");

        if (source.getSkus() != null && source.getSkus().size() > 0) {
            List<ProductSkuSimple> skus = ConverResourceUtils.converList(source.getSkus(), new ProductSkuSimpleConvert());
            result.setSkus(skus);
        }
        if (source.getSpecs() != null && source.getSpecs().size() > 0) {
            List<ProductSpecSimple> skus = ConverResourceUtils.converList(source.getSpecs(), new ProductSpecSimpleConvert());
            result.setSpecs(skus);
        }
        if (source.getImages() != null && source.getImages().size() > 0) {
            List<ProductImageSimple> skus = ConverResourceUtils.converList(source.getImages(), new ProductImageSimpleConvert());
            result.setImages(skus);
        }
        return result;
    }
}
