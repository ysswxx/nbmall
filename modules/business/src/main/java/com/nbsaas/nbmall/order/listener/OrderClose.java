package com.nbsaas.nbmall.order.listener;


import com.nbsaas.nbmall.order.data.entity.OrderForm;

public interface OrderClose {

    void close(OrderForm orderForm);
}
