package com.nbsaas.nbmall.cart.rest.convert;

import com.nbsaas.nbmall.cart.api.domain.response.CartResponse;
import com.nbsaas.nbmall.cart.data.entity.Cart;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class CartResponseConvert implements Conver<CartResponse, Cart> {
    @Override
    public CartResponse conver(Cart source) {
        CartResponse result = new CartResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getTenantUser()!=null){
           result.setTenantUser(source.getTenantUser().getId());
        }
        if(source.getShop()!=null){
           result.setShop(source.getShop().getId());
        }
        if(source.getProduct()!=null){
           result.setProduct(source.getProduct().getId());
        }
        if(source.getSku()!=null){
           result.setSku(source.getSku().getId());
        }

        return result;
    }
}
