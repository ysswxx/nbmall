package com.nbsaas.nbmall.product.controller.tenant;

import com.nbsaas.nbmall.product.api.apis.ProductApi;
import com.nbsaas.nbmall.product.api.domain.list.ProductList;
import com.nbsaas.nbmall.product.api.domain.page.ProductPage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/product")
@RestController
public class ProductTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("product")
    @RequestMapping("create")
    public ProductResponse create(@RequestBody ProductDataRequest request) {
        initTenant(request);
        request.setCreator(request.getCreateUser());
        return api.create(request);
    }

	@RequiresPermissions("product")
    @RequestMapping("delete")
    public ProductResponse delete(ProductDataRequest request) {
        initTenant(request);
        ProductResponse result = new ProductResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("product")
    @RequestMapping("update")
    public ProductResponse update(@RequestBody ProductDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("product")
    @RequestMapping("view")
    public ProductResponse view(ProductDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("product")
    @RequestMapping("list")
    public ProductList list(ProductSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("product")
    @RequestMapping("search")
    public ProductPage search(ProductSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @RequiresPermissions("product")
    @RequestMapping("offLine")
    public ProductResponse offLine(ProductDataRequest request) {
        initTenant(request);
        return api.offLine(request);
    }

    @RequiresPermissions("product")
    @RequestMapping("onLine")
    public ProductResponse onLine(ProductDataRequest request) {
        initTenant(request);
        return api.onLine(request);
    }

    @Autowired
    private ProductApi api;

}
