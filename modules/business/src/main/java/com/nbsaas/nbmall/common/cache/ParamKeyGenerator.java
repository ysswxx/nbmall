package com.nbsaas.nbmall.common.cache;

import com.nbsaas.codemake.fields.FieldBean;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;


@Component("paramKeyGenerator")
public class ParamKeyGenerator implements KeyGenerator {
    @Override
    public Object generate(Object target, Method method, Object... params) {

        StringBuffer buffer = new StringBuffer();
        RequestMapping requestMapping = target.getClass().getAnnotation(RequestMapping.class);
        if (requestMapping != null) {
            String[] value = requestMapping.value();
            if (value != null && value.length > 0) {
                buffer.append(value[0]);
            }
        } else {
            buffer.append(target.getClass().getSimpleName());
        }
        buffer.append("-");

        buffer.append(method.getName());
        buffer.append("-");
        if (params != null) {
            for (Object param : params) {
                buffer.append(key(param));
            }
        }

        return buffer.toString();
    }

    public String key(Object object) {
        StringBuffer buffer = new StringBuffer();
        for (Class<?> clazz = object.getClass(); clazz != Object.class; clazz = clazz.getSuperclass()) {
            Field[] fs = clazz.getDeclaredFields();
            for (Field field : fs) {
                try {
                    field.setAccessible(true);
                    Object value = field.get(object);
                    if (value != null) {
                        buffer.append(field.getName() + "=");
                        buffer.append(value + "");
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return buffer.toString();
    }
}
