package com.nbsaas.nbmall.promote.controller.rest;

import com.nbsaas.nbmall.promote.api.apis.FullReductionApi;
import com.nbsaas.nbmall.promote.api.domain.list.FullReductionList;
import com.nbsaas.nbmall.promote.api.domain.page.FullReductionPage;
import com.nbsaas.nbmall.promote.api.domain.request.*;
import com.nbsaas.nbmall.promote.api.domain.response.FullReductionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/fullreduction")
@RestController
public class FullReductionRestController extends BaseRestController {


    @RequestMapping("create")
    public FullReductionResponse create(FullReductionDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

    @RequestMapping("delete")
    public FullReductionResponse delete(FullReductionDataRequest request) {
        initTenant(request);
        FullReductionResponse result = new FullReductionResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public FullReductionResponse update(FullReductionDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public FullReductionResponse view(FullReductionDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public FullReductionList list(FullReductionSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public FullReductionPage search(FullReductionSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private FullReductionApi api;

}
