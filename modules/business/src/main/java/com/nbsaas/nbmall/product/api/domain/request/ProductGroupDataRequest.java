package com.nbsaas.nbmall.product.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月28日17:59:58.
*/

@Data
public class ProductGroupDataRequest extends TenantRequest {

    private Long id;

     private Long shop;
     private String note;
     private Long num;
     private Integer sortNum;
     private String showState;
     private String name;

}