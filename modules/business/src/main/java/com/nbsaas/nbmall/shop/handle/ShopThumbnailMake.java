package com.nbsaas.nbmall.shop.handle;

import com.nbsaas.nbmall.shop.data.entity.Shop;
import com.nbsaas.nbmall.shop.listener.ShopChangeListener;
import com.nbsaas.nbmall.utils.Im4JavaUtils;
import com.sun.istack.NotNull;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.File;


@Component("shopThumbnailMake")
public class ShopThumbnailMake implements ShopChangeListener, ApplicationContextAware {

    ApplicationContext context;


    @Override
    public void create(Shop shop) {
        if (shop == null) {
            return;
        }
        if (StringUtils.isEmpty(shop.getLogo())) {
            return;
        }
        String path = shop.getLogo();
        String temp = path.substring(path.indexOf("upload"));
        String tempFileStr = "/data200/files/newbyte/" + temp;
        File tempFile = new File(tempFileStr);
        if (tempFile.exists()) {
            System.out.println("缩略图:"+tempFileStr);
            int index = tempFileStr.indexOf(".");
            String name = tempFileStr.substring(index);
            String oldPath = tempFileStr.substring(0, index);
            String tempPath = oldPath + "_thumbnail" + name;
            File tFile = new File(tempPath);

            if (!tFile.exists()) {

                System.out.println("处理中:"+tempFileStr);
                Im4JavaUtils.thumbnail(tempFileStr, 200, 200);
            }
            if (tFile.exists()) {
                int indexPath = path.lastIndexOf(".");
                String urlName = path.substring(indexPath);
                String urlPath = path.substring(0, indexPath);
                String thumbnail = urlPath + "_thumbnail" + urlName;
                shop.setThumbnail(thumbnail);
            }
        }


    }

    public static void main(String[] args) {

        String path = "http://file.nbsaas.com/newbyte/upload/image/202111/8b5d5f76-305d-4492-abf4-35da48467e2d.jpg";
        String temp = path.substring(path.indexOf("upload"));
        String tempFileStr = "/data200/files/newbyte/" + temp;
        System.out.println("tempFileStr" + tempFileStr);
        int index = tempFileStr.indexOf(".");
        String name = tempFileStr.substring(index);
        String oldPath = tempFileStr.substring(0, index);
        System.out.println(oldPath);
        String tempPath = oldPath + "_thumbnail" + name;
        System.out.println(tempPath);
    }

    @Override
    public int sortNum() {
        return 0;
    }

    @Override
    public int compareTo(@NotNull ShopChangeListener o) {
        return new CompareToBuilder().append(sortNum(), o.sortNum()).build();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}
