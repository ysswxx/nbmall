package com.nbsaas.nbmall.common.api.domain.request;

import com.haoxuer.discover.user.api.domain.request.BasePageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;

import java.util.Date;

/**
 * Created by imake on 2021年12月23日11:54:02.
 */

@Data
public class SiteTenantSearchRequest extends BasePageRequest {

    //标识
    @Search(name = "key", operator = Filter.Operator.eq)
    private String key;

    //名称
    @Search(name = "name", operator = Filter.Operator.like)
    private String name;

    private Long tenant;


    private String sortField;


    private String sortMethod;
}