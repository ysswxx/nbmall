package com.nbsaas.nbmall.promote.api.apis;


import com.nbsaas.nbmall.promote.api.domain.list.CouponList;
import com.nbsaas.nbmall.promote.api.domain.page.CouponPage;
import com.nbsaas.nbmall.promote.api.domain.request.*;
import com.nbsaas.nbmall.promote.api.domain.response.CouponResponse;
import com.nbsaas.nbmall.promote.api.domain.simple.CouponSimple;

import java.util.List;

public interface CouponApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    CouponResponse create(CouponDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    CouponResponse update(CouponDataRequest request);

    /**
     * 删除
     *
     * @param request
     * @return
     */
    CouponResponse delete(CouponDataRequest request);


    /**
     * 详情
     *
     * @param request
     * @return
     */
    CouponResponse view(CouponDataRequest request);


    /**
     * @param request
     * @return
     */
    CouponList list(CouponSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    CouponPage search(CouponSearchRequest request);

    List<CouponSimple> platform(Long user);
}