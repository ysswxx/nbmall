package com.nbsaas.nbmall.order.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.order.data.dao.OrderItemDao;
import com.nbsaas.nbmall.order.data.entity.OrderItem;

/**
* Created by imake on 2021年12月10日17:38:01.
*/
@Repository

public class OrderItemDaoImpl extends CriteriaDaoImpl<OrderItem, Long> implements OrderItemDao {

	@Override
	public OrderItem findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public OrderItem save(OrderItem bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public OrderItem deleteById(Long id) {
		OrderItem entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<OrderItem> getEntityClass() {
		return OrderItem.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public OrderItem findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public OrderItem deleteById(Long tenant,Long id) {
		OrderItem entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}