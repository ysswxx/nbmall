package com.nbsaas.nbmall.customer.api.domain.page;


import com.nbsaas.nbmall.customer.api.domain.simple.CustomerAddressSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年12月10日22:56:19.
*/

@Data
public class CustomerAddressPage  extends ResponsePage<CustomerAddressSimple> {

}