package com.nbsaas.nbmall.shop.rest.resource;

import com.github.binarywang.wxpay.bean.entpay.EntPayRequest;
import com.github.binarywang.wxpay.bean.entpay.EntPayResult;
import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.EntPayService;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.binarywang.wxpay.service.impl.EntPayServiceImpl;
import com.github.binarywang.wxpay.service.impl.WxPayServiceImpl;
import com.github.binarywang.wxpay.util.XmlConfig;
import com.haoxuer.bigworld.member.data.entity.TenantUser;
import com.haoxuer.bigworld.pay.data.dao.TenantAccountDao;
import com.haoxuer.bigworld.pay.data.entity.CashConfig;
import com.haoxuer.bigworld.pay.data.enums.SendState;
import com.haoxuer.bigworld.pay.plugins.service.impl.PayServiceImpl;
import com.haoxuer.discover.trade.api.domain.request.TradeRequest;
import com.haoxuer.discover.trade.data.dao.TradeAccountDao;
import com.haoxuer.discover.trade.data.entity.TradeAccount;
import com.haoxuer.discover.trade.data.enums.ChangeType;
import com.nbsaas.nbmall.shop.api.apis.ShopCashApi;
import com.nbsaas.nbmall.shop.api.domain.list.ShopCashList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopCashPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopCashResponse;
import com.nbsaas.nbmall.shop.data.dao.ShopCashDao;
import com.nbsaas.nbmall.shop.data.entity.Shop;
import com.nbsaas.nbmall.shop.data.entity.ShopCash;
import com.nbsaas.nbmall.shop.rest.convert.ShopCashResponseConvert;
import com.nbsaas.nbmall.shop.rest.convert.ShopCashSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.haoxuer.bigworld.pay.data.dao.CashConfigDao;
import com.haoxuer.bigworld.member.data.dao.TenantUserDao;
import com.nbsaas.nbmall.shop.data.dao.ShopDao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class ShopCashResource implements ShopCashApi {
    @Autowired
    private ShopCashDao dataDao;

    @Autowired
    private CashConfigDao cashConfigDao;
    @Autowired
    private TenantUserDao creatorDao;
    @Autowired
    private ShopDao shopDao;

    @Autowired
    private TenantAccountDao basicTradeAccountDao;

    @Autowired
    private TradeAccountDao tradeAccountDao;

    @Override
    public ShopCashResponse create(ShopCashDataRequest request) {
        ShopCashResponse result = new ShopCashResponse();

        ShopCash bean = new ShopCash();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new ShopCashResponseConvert().conver(bean);
        return result;
    }

    @Override
    public ShopCashResponse cash(ShopCashDataRequest request) {
        ShopCashResponse result=new ShopCashResponse();
        if (request.getMoney() == null || request.getMoney().compareTo(new BigDecimal(0)) <= 0) {
            result.setCode(-1);
            result.setMsg("无效金额");
            return result;
        }
        Shop shop = shopDao.findById(request.getShop());
        if (shop == null) {
            result.setCode(1001);
            result.setMsg("无效token");
            return result;
        }
        if (request.getOpenId() == null) {
            result.setCode(1002);
            result.setMsg("无效微信用户");
            return result;
        }

        if (shop.getTradeAccount() == null) {
            shop.setTradeAccount(tradeAccountDao.initNormal());
        }
        if (shop.getTradeAccount().getAmount().compareTo(request.getMoney()) < 0) {
            result.setCode(-303);
            result.setMsg("余额不足");
            return result;
        }

        TradeAccount paymentAccount = basicTradeAccountDao.special(request.getTenant(),"shop_payment");

        TradeAccount feeAccount = basicTradeAccountDao.special(request.getTenant(),"shop_feeAccount");


        CashConfig cashConfig = cashConfigDao.one(Filter.eq("tenant.id", request.getTenant()),
                Filter.eq("key", request.getKey()));
        BigDecimal fee = null;
        BigDecimal money = null;
        if (cashConfig == null) {
            result.setCode(503);
            result.setMsg("提现配置没设置");
            return result;
        }
        fee = cashConfig.getFee();
        if (fee == null) {
            fee = new BigDecimal(3);
        }
        if (fee.compareTo(new BigDecimal(0.001)) > 0) {
            money = request.getMoney().subtract(fee);
            if (money.compareTo(new BigDecimal(0.001)) < 0) {
                result.setCode(-304);
                result.setMsg("手续费大于提现金额");
                return result;
            }

            TradeAccount userAccount = shop.getTradeAccount();
            TradeRequest feeRequest = new TradeRequest();
            feeRequest.setAmount(fee);
            feeRequest.setFrom(userAccount.getId());
            feeRequest.setTo(feeAccount.getId());
            feeRequest.setChangeType(ChangeType.from(3, "手续费"));
            feeRequest.setNote("手续费");
            tradeAccountDao.trade(feeRequest);
        }else{
            money=request.getMoney();
        }

        TradeAccount userAccount = shop.getTradeAccount();
        TradeRequest cashRequest = new TradeRequest();
        cashRequest.setAmount(money);
        cashRequest.setFrom(userAccount.getId());
        cashRequest.setTo(paymentAccount.getId());
        cashRequest.setChangeType(ChangeType.from(2, "提现"));
        cashRequest.setNote("微信提现");
        tradeAccountDao.trade(cashRequest);

        ShopCash shopCash = new ShopCash();
        shopCash.setTradeAccount(userAccount);
        shopCash.setCashConfig(cashConfig);
        shopCash.setName(request.getName());
        shopCash.setMoney(money);
        shopCash.setSendState(SendState.init);
        shopCash.setOpenId(request.getOpenId());
        shopCash.setNo(PayServiceImpl.getOrderNo());
        shopCash.setNote("微信余额提现");
        shopCash.setFee(fee);
        shopCash.setCash(request.getMoney());
        shopCash.setTenant(Tenant.fromId(request.getTenant()));
        shopCash.setShop(shop);
        shopCash.setPhone(request.getPhone());
        shopCash.setIdNo(request.getNo());
        shopCash.setAppId(cashConfig.getAppId());
        shopCash.setTradeAccount(userAccount);
        shopCash.setCreator(TenantUser.fromId(request.getUser()));
        dataDao.save(shopCash);
        result.setId(shopCash.getId());
        return result;
    }

    @Override
    public ShopCashResponse update(ShopCashDataRequest request) {
        ShopCashResponse result = new ShopCashResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        ShopCash bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new ShopCashResponseConvert().conver(bean);
        return result;
    }

    private void handleData(ShopCashDataRequest request, ShopCash bean) {
        TenantBeanUtils.copyProperties(request,bean);
        if(request.getShop()!=null){
            bean.setShop(shopDao.findById(request.getShop()));
        }
        if(request.getCreator()!=null){
            bean.setCreator(creatorDao.findById(request.getCreator()));
        }
        if(request.getCashConfig()!=null){
            bean.setCashConfig(cashConfigDao.findById(request.getCashConfig()));
        }

    }

    @Override
    public ShopCashResponse delete(ShopCashDataRequest request) {
        ShopCashResponse result = new ShopCashResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public ShopCashResponse view(ShopCashDataRequest request) {
        ShopCashResponse result=new ShopCashResponse();
        ShopCash bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new ShopCashResponseConvert().conver(bean);
        return result;
    }

    @Override
    public ShopCashResponse back(ShopCashDataRequest request) {
        ShopCashResponse result=new ShopCashResponse();
        ShopCash temp = dataDao.findById(request.getId());

        if (temp == null) {
            result.setCode(501);
            result.setMsg("无效信息");
            return result;
        }
        if (temp.getSendState() == SendState.send) {
            result.setCode(502);
            result.setMsg("该提现单已经支付");
            return result;
        }
        if (temp.getSendState() == SendState.back) {
            result.setCode(502);
            result.setMsg("该提现单已经退回");
            return result;
        }
        TradeAccount account = temp.getTradeAccount();
        if (account == null) {
            result.setCode(505);
            result.setMsg("单据无付款账号");
            return result;
        }

        TradeAccount paymentAccount = basicTradeAccountDao.special(request.getTenant(),"shop_payment");

        TradeRequest cashRequest = new TradeRequest();
        cashRequest.setAmount(temp.getCash());
        cashRequest.setFrom(paymentAccount.getId());
        cashRequest.setTo(account.getId());
        cashRequest.setChangeType(ChangeType.from(1001, "提现退回"));
        cashRequest.setNote("提现退回");
        tradeAccountDao.trade(cashRequest);

        temp.setSendState(SendState.back);
        return result;
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    @Override
    public ShopCashResponse send(ShopCashDataRequest dataRequest) {
        ShopCashResponse result=new ShopCashResponse();
        ShopCash temp = dataDao.findById(dataRequest.getId());

        if (temp == null) {
            result.setCode(501);
            result.setMsg("无效信息");
            return result;
        }
        CashConfig cashConfig = temp.getCashConfig();
        if (cashConfig == null) {
            result.setCode(502);
            result.setMsg("提现单无配置信息");
            return result;
        }
        if (temp.getSendState() == SendState.send) {
            result.setCode(502);
            result.setMsg("该提现单已经支付");
            return result;
        }
        if (temp.getSendState() == SendState.back) {
            result.setCode(503);
            result.setMsg("该提现单已经退回");
            return result;
        }
        if (temp.getSendState() == SendState.fail) {
            result.setCode(504);
            result.setMsg("该提现单无法处理");
            return result;
        }
        XmlConfig.fastMode=true;
        EntPayRequest request = new EntPayRequest();
        request.setAmount(temp.getMoney().multiply(new BigDecimal(100)).intValue());
        request.setPartnerTradeNo(temp.getNo());
        request.setReUserName(temp.getName());
        request.setCheckName("FORCE_CHECK");
        request.setDescription(temp.getNote());
        request.setOpenid(temp.getOpenId());
        request.setSpbillCreateIp("119.23.149.178");

        WxPayConfig payConfig = new WxPayConfig();
        payConfig.setAppId(cashConfig.getAppId());
        payConfig.setMchId(cashConfig.getMchId());
        payConfig.setMchKey(cashConfig.getMchKey());
        payConfig.setKeyPath(cashConfig.getKeyPath());
        //payConfig.setSignType("MD5");
        WxPayService wxPayService = new WxPayServiceImpl();
        wxPayService.setConfig(payConfig);
        EntPayService entPayService = new EntPayServiceImpl(wxPayService);
        try {
            EntPayResult entPayResult = entPayService.entPay(request);
            temp.setSendState(SendState.send);
            temp.setBussNo(entPayResult.getPaymentNo());
        } catch (WxPayException exception) {
            result.setCode(503);
            result.setMsg(exception.getErrCodeDes());
            temp.setDemo(exception.getErrCodeDes());
            exception.printStackTrace();
        }
        return result;
    }

    @Override
    public ShopCashList list(ShopCashSearchRequest request) {
        ShopCashList result = new ShopCashList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
            orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            orders.add(Order.desc(""+request.getSortField()));
        }else{
            orders.add(Order.desc("id"));
        }
        List<ShopCash> organizations = dataDao.list(0, request.getSize(), filters, orders);

        ShopCashSimpleConvert convert=new ShopCashSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public ShopCashPage search(ShopCashSearchRequest request) {
        ShopCashPage result=new ShopCashPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<ShopCash> page=dataDao.page(pageable);

        ShopCashSimpleConvert convert=new ShopCashSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
