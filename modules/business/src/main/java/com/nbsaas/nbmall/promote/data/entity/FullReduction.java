package com.nbsaas.nbmall.promote.data.entity;


import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.*;
import com.nbsaas.nbmall.promote.data.enums.FullReductionWay;
import com.nbsaas.nbmall.shop.data.entity.Shop;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@FormAnnotation(title = "满减活动管理", model = "满减活动", menu = "1,131,133")
@Entity
@Table(name = "bs_tenant_promote_full_reduction")
public class FullReduction extends TenantEntity {

    @SearchItem(label = "名称", name = "name", key = "name")
    @FormField(title = "名称",  grid = true, col = 20,required = true)
    private String name;

    @SearchItem(label = "商家",name = "shop",key = "shop",operator = "eq",classType = "Long",show = false)
    @FieldConvert
    @FieldName
    @FormField(title = "商家",  grid = true, col = 20,type = InputType.select,option = "shop",required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    private Shop shop;

    @FormField(title = "开始时间",  type = InputType.el_date_time_picker, grid = true, col = 20)
    private Date beginTime;

    @FormField(title = "截止时间",  type = InputType.el_date_time_picker, grid = true, col = 20)
    private Date endTime;



    @FormField(title = "参与设置",  grid = true, col = 20,type = InputType.el_radio_group)
    private FullReductionWay fullReductionWay;


    @OneToMany(fetch = FetchType.LAZY,mappedBy = "reduction")
    private List<FullReductionItem> items;
}
