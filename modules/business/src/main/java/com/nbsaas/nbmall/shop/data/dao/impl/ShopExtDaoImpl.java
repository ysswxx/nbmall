package com.nbsaas.nbmall.shop.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.shop.data.dao.ShopExtDao;
import com.nbsaas.nbmall.shop.data.entity.ShopExt;

/**
* Created by imake on 2021年12月10日17:38:00.
*/
@Repository

public class ShopExtDaoImpl extends CriteriaDaoImpl<ShopExt, Long> implements ShopExtDao {

	@Override
	public ShopExt findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public ShopExt save(ShopExt bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public ShopExt deleteById(Long id) {
		ShopExt entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<ShopExt> getEntityClass() {
		return ShopExt.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public ShopExt findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public ShopExt deleteById(Long tenant,Long id) {
		ShopExt entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}