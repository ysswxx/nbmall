package com.nbsaas.nbmall.product.rest.resource;

import com.nbsaas.nbmall.product.api.apis.ProductSkuApi;
import com.nbsaas.nbmall.product.api.domain.list.ProductSkuList;
import com.nbsaas.nbmall.product.api.domain.page.ProductSkuPage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductSkuResponse;
import com.nbsaas.nbmall.product.data.dao.ProductSkuDao;
import com.nbsaas.nbmall.product.data.entity.ProductSku;
import com.nbsaas.nbmall.product.rest.convert.ProductSkuResponseConvert;
import com.nbsaas.nbmall.product.rest.convert.ProductSkuSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.nbsaas.nbmall.product.data.dao.ProductDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class ProductSkuResource implements ProductSkuApi {

    @Autowired
    private ProductSkuDao dataDao;

    @Autowired
    private ProductDao productDao;


    @Override
    public ProductSkuResponse create(ProductSkuDataRequest request) {
        ProductSkuResponse result = new ProductSkuResponse();

        ProductSku bean = new ProductSku();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new ProductSkuResponseConvert().conver(bean);
        return result;
    }

    @Override
    public ProductSkuResponse update(ProductSkuDataRequest request) {
        ProductSkuResponse result = new ProductSkuResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        ProductSku bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new ProductSkuResponseConvert().conver(bean);
        return result;
    }

    private void handleData(ProductSkuDataRequest request, ProductSku bean) {
       TenantBeanUtils.copyProperties(request,bean);
           if(request.getProduct()!=null){
              bean.setProduct(productDao.findById(request.getProduct()));
           }

    }

    @Override
    public ProductSkuResponse delete(ProductSkuDataRequest request) {
        ProductSkuResponse result = new ProductSkuResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public ProductSkuResponse view(ProductSkuDataRequest request) {
        ProductSkuResponse result=new ProductSkuResponse();
        ProductSku bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new ProductSkuResponseConvert().conver(bean);
        return result;
    }
    @Override
    public ProductSkuList list(ProductSkuSearchRequest request) {
        ProductSkuList result = new ProductSkuList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<ProductSku> organizations = dataDao.list(0, request.getSize(), filters, orders);

        ProductSkuSimpleConvert convert=new ProductSkuSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public ProductSkuPage search(ProductSkuSearchRequest request) {
        ProductSkuPage result=new ProductSkuPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<ProductSku> page=dataDao.page(pageable);

        ProductSkuSimpleConvert convert=new ProductSkuSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
