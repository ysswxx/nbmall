package com.nbsaas.nbmall.statistics.rest.convert;

import com.nbsaas.nbmall.statistics.api.domain.response.VisitPageDayResponse;
import com.nbsaas.nbmall.statistics.data.entity.VisitPageDay;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class VisitPageDayResponseConvert implements Conver<VisitPageDayResponse, VisitPageDay> {
    @Override
    public VisitPageDayResponse conver(VisitPageDay source) {
        VisitPageDayResponse result = new VisitPageDayResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getUser()!=null){
           result.setUser(source.getUser().getId());
        }

        return result;
    }
}
