package com.nbsaas.nbmall.device.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import org.springframework.context.annotation.Scope;
import com.haoxuer.discover.controller.BaseAction;
/**
*
* Created by imake on 2021年12月10日17:40:16.
*/

@Scope("prototype")
@Controller
public class DeviceTypeAction extends BaseAction{


	@RequiresPermissions("devicetype")
	@RequestMapping("/admin/devicetype/view_list")
	public String list() {
		return getView("devicetype/list");
	}

}