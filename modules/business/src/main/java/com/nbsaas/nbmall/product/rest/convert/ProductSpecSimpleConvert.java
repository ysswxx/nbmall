package com.nbsaas.nbmall.product.rest.convert;

import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.nbsaas.nbmall.product.api.domain.simple.ProductSpecSimple;
import com.nbsaas.nbmall.product.api.domain.simple.ProductSpecValueSimple;
import com.nbsaas.nbmall.product.data.entity.ProductSpec;
import com.haoxuer.discover.data.rest.core.Conver;

import java.util.List;

public class ProductSpecSimpleConvert implements Conver<ProductSpecSimple, ProductSpec> {


    @Override
    public ProductSpecSimple conver(ProductSpec source) {
        ProductSpecSimple result = new ProductSpecSimple();

            result.setId(source.getId());
            if(source.getProduct()!=null){
               result.setProduct(source.getProduct().getId());
            }
            if(source.getCreator()!=null){
               result.setCreator(source.getCreator().getId());
            }
             result.setName(source.getName());
             result.setAddDate(source.getAddDate());
             if(source.getProduct()!=null){
                result.setProductName(source.getProduct().getName());
             }
        if (source.getValues() != null && source.getValues().size() > 0) {
            List<ProductSpecValueSimple> skus = ConverResourceUtils.converList(source.getValues(), new ProductSpecValueSimpleConvert());
            result.setValues(skus);
        }
        return result;
    }
}
