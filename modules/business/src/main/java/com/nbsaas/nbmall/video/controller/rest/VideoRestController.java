package com.nbsaas.nbmall.video.controller.rest;

import com.nbsaas.nbmall.video.api.apis.VideoApi;
import com.nbsaas.nbmall.video.api.domain.list.VideoList;
import com.nbsaas.nbmall.video.api.domain.page.VideoPage;
import com.nbsaas.nbmall.video.api.domain.request.*;
import com.nbsaas.nbmall.video.api.domain.response.VideoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/video")
@RestController
public class VideoRestController extends BaseRestController {


    @RequestMapping("create")
    public VideoResponse create(VideoDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

    @RequestMapping("delete")
    public VideoResponse delete(VideoDataRequest request) {
        initTenant(request);
        VideoResponse result = new VideoResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public VideoResponse update(VideoDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public VideoResponse view(VideoDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public VideoList list(VideoSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public VideoPage search(VideoSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private VideoApi api;

}
