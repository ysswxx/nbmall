package com.nbsaas.nbmall.shop.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月10日17:38:00.
*/

@Data
public class ShopStoreDataRequest extends TenantRequest {

    private Long id;

     private Long shop;
     private Integer area;
     private String address;
     private Integer city;
     private Double lng;
     private Long creator;
     private String phone;
     private String logo;
     private String name;
     private Integer province;
     private Double lat;

}