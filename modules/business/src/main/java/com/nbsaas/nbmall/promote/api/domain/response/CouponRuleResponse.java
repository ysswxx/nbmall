package com.nbsaas.nbmall.promote.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.math.BigDecimal;
import com.nbsaas.nbmall.promote.data.enums.CouponState;
import com.nbsaas.nbmall.promote.data.enums.ExpireType;
import com.nbsaas.nbmall.promote.data.enums.CouponCatalog;
import com.nbsaas.nbmall.promote.data.enums.ShowType;
import com.haoxuer.discover.data.enums.StoreState;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
/**
*
* Created by BigWorld on 2021年12月28日23:18:26.
*/

@Data
public class CouponRuleResponse extends ResponseObject {

    private Long id;

     private Long shop;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date sendEndTime;
     private Integer limitNum;
     private String note;
     private StoreState storeState;
     private ExpireType expireType;
     private String shopName;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date useBeginTime;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date sendBeginTime;
     private BigDecimal money;
     private CouponCatalog couponCatalog;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private CouponState couponState;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date useEndTime;
     private Short couponScope;
     private Integer takeawayRate;
     private Long useNum;
     private BigDecimal minPrice;
     private String logo;
     private ShowType showType;
     private String name;
     private Integer useDay;
     private Integer stock;
     private Long sendNum;

     private String couponStateName;
     private String expireTypeName;
     private String couponCatalogName;
     private String showTypeName;
     private String storeStateName;
}