package com.nbsaas.nbmall.product.api.domain.list;


import com.nbsaas.nbmall.product.api.domain.simple.ProductSkuSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年12月10日17:38:01.
*/

@Data
public class ProductSkuList  extends ResponseList<ProductSkuSimple> {

}