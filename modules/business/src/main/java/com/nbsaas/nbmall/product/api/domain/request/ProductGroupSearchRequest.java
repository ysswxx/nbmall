package com.nbsaas.nbmall.product.api.domain.request;

import com.haoxuer.bigworld.member.api.domain.request.TenantPageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月28日17:59:58.
*/

@Data
public class ProductGroupSearchRequest extends TenantPageRequest {

    //商家名称
     @Search(name = "shop.id",operator = Filter.Operator.eq)
     private Long shop;

    //分组名称
     @Search(name = "name",operator = Filter.Operator.like)
     private String name;



}