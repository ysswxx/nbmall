package com.nbsaas.nbmall.web.front;

import cn.binarywang.wx.miniapp.api.WxMaQrcodeService;
import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.impl.WxMaQrcodeServiceImpl;
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import cn.binarywang.wx.miniapp.bean.WxMaCodeLineColor;
import cn.binarywang.wx.miniapp.config.impl.WxMaDefaultConfigImpl;
import com.google.zxing.WriterException;
import com.haoxuer.bigworld.member.api.apis.TenantOauthConfigApi;
import com.haoxuer.bigworld.member.api.domain.request.TenantOauthConfigDataRequest;
import com.haoxuer.bigworld.member.api.domain.response.TenantOauthConfigResponse;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@Scope("prototype")
@Controller
public class AppCodeController extends BaseTenantRestController {

    public static final int SIZE = 100;

    @Autowired
    private TenantOauthConfigApi api;

    @RequestMapping("/code/{path}")
    public ResponseEntity<byte[]> getResponseEntity(@PathVariable("path") String path) throws WriterException, IOException, WxErrorException {

        WxMaService maService = new WxMaServiceImpl();
        WxMaDefaultConfigImpl payConfig = new WxMaDefaultConfigImpl();
        TenantOauthConfigDataRequest request = new TenantOauthConfigDataRequest();
        initTenant(request);
        request.setModel("wxapp");
        TenantOauthConfigResponse response = api.key(request);
        if (response.getCode() == 0) {
            payConfig.setAppid(response.getAppKey());
            payConfig.setSecret(response.getAppSecret());
        }
        maService.setWxMaConfig(payConfig);
        WxMaQrcodeService wxPayService = new WxMaQrcodeServiceImpl(maService);

        StringBuffer buffer = new StringBuffer();
        buffer.append("pages/main/shop/index?visitType=scan&id=");
        buffer.append(path);
        File file = wxPayService.createQrcode(buffer.toString(), 512);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] bs = new byte[fileInputStream.available()];
        fileInputStream.read(bs);
        return new ResponseEntity<byte[]>(bs, headers, HttpStatus.CREATED);
    }

    @RequestMapping("/getUnlimited/{path}")
    public ResponseEntity<byte[]> getUnlimited(@PathVariable("path") String path) throws WriterException, IOException, WxErrorException {

        WxMaService maService = new WxMaServiceImpl();
        WxMaDefaultConfigImpl payConfig = new WxMaDefaultConfigImpl();
        TenantOauthConfigDataRequest request = new TenantOauthConfigDataRequest();
        initTenant(request);
        request.setModel("wxapp");
        TenantOauthConfigResponse response = api.key(request);
        if (response.getCode() == 0) {
            payConfig.setAppid(response.getAppKey());
            payConfig.setSecret(response.getAppSecret());
        }

        maService.setWxMaConfig(payConfig);
        WxMaQrcodeService wxPayService = new WxMaQrcodeServiceImpl(maService);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        WxMaCodeLineColor lineColor=new WxMaCodeLineColor("0","0","0");
        byte[] bs =wxPayService.createWxaCodeUnlimitBytes("visitType=scan&id="+path,
                "pages/main/shop/index",true
        ,"release",512,false,lineColor,false);
        return new ResponseEntity<byte[]>(bs, headers, HttpStatus.CREATED);
    }
}
