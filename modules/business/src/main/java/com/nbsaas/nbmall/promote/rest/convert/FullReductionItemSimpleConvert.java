package com.nbsaas.nbmall.promote.rest.convert;

import com.nbsaas.nbmall.promote.api.domain.simple.FullReductionItemSimple;
import com.nbsaas.nbmall.promote.data.entity.FullReductionItem;
import com.haoxuer.discover.data.rest.core.Conver;
public class FullReductionItemSimpleConvert implements Conver<FullReductionItemSimple, FullReductionItem> {


    @Override
    public FullReductionItemSimple conver(FullReductionItem source) {
        FullReductionItemSimple result = new FullReductionItemSimple();

            result.setId(source.getId());
             result.setReduceMoney(source.getReduceMoney());
             result.setFullMoney(source.getFullMoney());
            if(source.getReduction()!=null){
               result.setReduction(source.getReduction().getId());
            }
             if(source.getReduction()!=null){
                result.setReductionName(source.getReduction().getName());
             }

        return result;
    }
}
