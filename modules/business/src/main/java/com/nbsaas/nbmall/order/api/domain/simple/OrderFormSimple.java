package com.nbsaas.nbmall.order.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.haoxuer.bigworld.pay.data.enums.PayState;
import com.nbsaas.nbmall.order.data.enums.OrderState;
import com.haoxuer.discover.data.enums.StoreState;

/**
*
* Created by BigWorld on 2021年12月24日00:04:24.
*/
@Data
public class OrderFormSimple implements Serializable {

    private Long id;

     private Integer returnCount;
     private String consignee;
     private Integer orderSource;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date editPriceTime;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date sysReceiveTime;
     private String address;
     private Long creator;
     private BigDecimal platformDiscount;
     private StoreState storeState;
     private BigDecimal weight;
     private Integer paySource;
     private BigDecimal payAmount;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private BigDecimal amount;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date receiveTime;
     private Integer commentState;
     private Integer productCount;
     private String outTradeNum;
     private BigDecimal sendIntegral;
     private BigDecimal totalAmount;
     private String phone;
     private Long user;
     private BigDecimal integralDiscount;
     private Long shop;
     private String no;
     private String note;
     private BigDecimal changeAmount;
     private Long newRefundId;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date payTime;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date deliveryTime;
     private BigDecimal freight;
     private String shopName;
     private BigDecimal discount;
     private BigDecimal useIntegral;
     private PayState payState;
     private OrderState orderState;
     private Integer orderType;
     private String mergeOrderCode;
     private BigDecimal useRedPacket;
     private BigDecimal returnAmount;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date remindTime;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date orderTime;
     private Integer userState;
     private Integer state;
     private BigDecimal redPacketDiscount;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date finishTime;

     private String payStateName;
     private String orderStateName;
     private String storeStateName;

}
