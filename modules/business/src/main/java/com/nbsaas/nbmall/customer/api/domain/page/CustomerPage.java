package com.nbsaas.nbmall.customer.api.domain.page;


import com.nbsaas.nbmall.customer.api.domain.simple.CustomerSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年12月10日22:56:19.
*/

@Data
public class CustomerPage  extends ResponsePage<CustomerSimple> {

}