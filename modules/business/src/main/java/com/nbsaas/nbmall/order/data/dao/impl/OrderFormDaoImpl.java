package com.nbsaas.nbmall.order.data.dao.impl;

import com.haoxuer.bigworld.pay.data.enums.PayState;
import com.haoxuer.discover.data.utils.DateUtils;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.order.data.dao.OrderFormDao;
import com.nbsaas.nbmall.order.data.entity.OrderForm;

import java.math.BigDecimal;
import java.util.Date;

/**
* Created by imake on 2021年12月24日00:04:24.
*/
@Repository

public class OrderFormDaoImpl extends CriteriaDaoImpl<OrderForm, Long> implements OrderFormDao {

	@Override
	public OrderForm findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public OrderForm save(OrderForm bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public OrderForm deleteById(Long id) {
		OrderForm entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<OrderForm> getEntityClass() {
		return OrderForm.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public OrderForm findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public OrderForm deleteById(Long tenant,Long id) {
		OrderForm entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}


	@Override
	public BigDecimal day(Long id, Date today) {
		Date begin = DateUtils.begin(today);
		Date end = DateUtils.end(today);
		Filter beginFilter = Filter.ge("addDate", begin);
		beginFilter.setPrefix("begin");
		Filter endFilter = Filter.le("addDate", end);
		endFilter.setPrefix("end");

		BigDecimal money = handle("sum", "payAmount", Filter.eq("shop.id", id),
				Filter.eq("payState", PayState.success), beginFilter, endFilter);
		if (money==null){
			money=new BigDecimal(0);
		}

		return money;
	}

	@Override
	public Long dayNum(Long id, Date date) {
		Date begin = DateUtils.begin(date);
		Date end = DateUtils.end(date);
		Filter beginFilter = Filter.ge("addDate", begin);
		beginFilter.setPrefix("begin");
		Filter endFilter = Filter.le("addDate", end);
		endFilter.setPrefix("end");

		Long money = handle("count", "id", Filter.eq("shop.id", id),
				Filter.eq("payState", PayState.success), beginFilter, endFilter);
		if (money==null){
			money=0L;
		}
		return money;
	}

	@Override
	public BigDecimal dayAll(Long tenant,Date today) {
		Date begin = DateUtils.begin(today);
		Date end = DateUtils.end(today);
		Filter beginFilter = Filter.ge("addDate", begin);
		beginFilter.setPrefix("begin");
		Filter endFilter = Filter.le("addDate", end);
		endFilter.setPrefix("end");

		BigDecimal money = handle("sum", "payAmount",
				Filter.eq("payState", PayState.success),
				Filter.eq("tenant.id",tenant),
				beginFilter, endFilter);
		if (money==null){
			money=new BigDecimal(0);
		}

		return money;
	}

	@Override
	public Long dayNumAll(Long tenant,Date date) {
		Date begin = DateUtils.begin(date);
		Date end = DateUtils.end(date);
		Filter beginFilter = Filter.ge("addDate", begin);
		beginFilter.setPrefix("begin");
		Filter endFilter = Filter.le("addDate", end);
		endFilter.setPrefix("end");

		Long money = handle("count", "id",
				Filter.eq("payState", PayState.success),
				Filter.eq("tenant.id",tenant),
				beginFilter, endFilter);
		if (money==null){
			money=0L;
		}
		return money;
	}
}