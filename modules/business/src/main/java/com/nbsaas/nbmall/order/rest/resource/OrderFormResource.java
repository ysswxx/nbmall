package com.nbsaas.nbmall.order.rest.resource;

import com.nbsaas.nbmall.order.api.apis.OrderFormApi;
import com.nbsaas.nbmall.order.api.domain.list.OrderFormList;
import com.nbsaas.nbmall.order.api.domain.page.OrderFormPage;
import com.nbsaas.nbmall.order.api.domain.request.*;
import com.nbsaas.nbmall.order.api.domain.response.OrderFormResponse;
import com.nbsaas.nbmall.order.data.dao.OrderFormDao;
import com.nbsaas.nbmall.order.data.entity.OrderForm;
import com.nbsaas.nbmall.order.rest.convert.OrderFormResponseConvert;
import com.nbsaas.nbmall.order.rest.convert.OrderFormSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.haoxuer.bigworld.member.data.dao.TenantUserDao;
import com.nbsaas.nbmall.shop.data.dao.ShopDao;
import com.haoxuer.bigworld.member.data.dao.TenantUserDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class OrderFormResource implements OrderFormApi {

    @Autowired
    private OrderFormDao dataDao;

    @Autowired
    private TenantUserDao creatorDao;
    @Autowired
    private ShopDao shopDao;
    @Autowired
    private TenantUserDao userDao;


    @Override
    public OrderFormResponse create(OrderFormDataRequest request) {
        OrderFormResponse result = new OrderFormResponse();

        OrderForm bean = new OrderForm();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new OrderFormResponseConvert().conver(bean);
        return result;
    }

    @Override
    public OrderFormResponse update(OrderFormDataRequest request) {
        OrderFormResponse result = new OrderFormResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        OrderForm bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new OrderFormResponseConvert().conver(bean);
        return result;
    }

    private void handleData(OrderFormDataRequest request, OrderForm bean) {
       TenantBeanUtils.copyProperties(request,bean);
           if(request.getCreator()!=null){
              bean.setCreator(creatorDao.findById(request.getCreator()));
           }
           if(request.getUser()!=null){
              bean.setUser(userDao.findById(request.getUser()));
           }
           if(request.getShop()!=null){
              bean.setShop(shopDao.findById(request.getShop()));
           }

    }

    @Override
    public OrderFormResponse delete(OrderFormDataRequest request) {
        OrderFormResponse result = new OrderFormResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public OrderFormResponse view(OrderFormDataRequest request) {
        OrderFormResponse result=new OrderFormResponse();
        OrderForm bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new OrderFormResponseConvert().conver(bean);
        return result;
    }
    @Override
    public OrderFormList list(OrderFormSearchRequest request) {
        OrderFormList result = new OrderFormList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<OrderForm> organizations = dataDao.list(0, request.getSize(), filters, orders);

        OrderFormSimpleConvert convert=new OrderFormSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public OrderFormPage search(OrderFormSearchRequest request) {
        OrderFormPage result=new OrderFormPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<OrderForm> page=dataDao.page(pageable);

        OrderFormSimpleConvert convert=new OrderFormSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
