package com.nbsaas.nbmall.common.api.apis;


import com.nbsaas.nbmall.common.api.domain.list.ChannelList;
import com.nbsaas.nbmall.common.api.domain.page.ChannelPage;
import com.nbsaas.nbmall.common.api.domain.request.*;
import com.nbsaas.nbmall.common.api.domain.response.ChannelResponse;

public interface ChannelApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    ChannelResponse create(ChannelDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    ChannelResponse update(ChannelDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    ChannelResponse delete(ChannelDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     ChannelResponse view(ChannelDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    ChannelList list(ChannelSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    ChannelPage search(ChannelSearchRequest request);

}