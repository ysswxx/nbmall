package com.nbsaas.nbmall.product.api.domain.page;


import com.nbsaas.nbmall.product.api.domain.simple.ProductGroupSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年12月28日17:59:58.
*/

@Data
public class ProductGroupPage  extends ResponsePage<ProductGroupSimple> {

}