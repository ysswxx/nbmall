package com.nbsaas.nbmall.product.rest.convert;

import com.nbsaas.nbmall.product.api.domain.response.ProductCatalogResponse;
import com.nbsaas.nbmall.product.data.entity.ProductCatalog;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class ProductCatalogResponseConvert implements Conver<ProductCatalogResponse, ProductCatalog> {
    @Override
    public ProductCatalogResponse conver(ProductCatalog source) {
        ProductCatalogResponse result = new ProductCatalogResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getParent()!=null){
           result.setParent(source.getParent().getId());
        }
         if(source.getParent()!=null){
            result.setParentName(source.getParent().getName());
         }

        return result;
    }
}
