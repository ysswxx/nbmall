package com.nbsaas.nbmall.statistics.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.statistics.data.entity.VisitDay;

/**
* Created by imake on 2021年12月23日11:45:35.
*/
public interface VisitDayDao extends BaseDao<VisitDay,Long>{

	 VisitDay findById(Long id);

	 VisitDay save(VisitDay bean);

	 VisitDay updateByUpdater(Updater<VisitDay> updater);

	 VisitDay deleteById(Long id);

	 VisitDay findById(Long tenant, Long id);

     VisitDay deleteById(Long tenant, Long id);
}