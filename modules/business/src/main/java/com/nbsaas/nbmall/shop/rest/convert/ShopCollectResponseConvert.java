package com.nbsaas.nbmall.shop.rest.convert;

import com.nbsaas.nbmall.shop.api.domain.response.ShopCollectResponse;
import com.nbsaas.nbmall.shop.data.entity.ShopCollect;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class ShopCollectResponseConvert implements Conver<ShopCollectResponse, ShopCollect> {
    @Override
    public ShopCollectResponse conver(ShopCollect source) {
        ShopCollectResponse result = new ShopCollectResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getTenantUser()!=null){
           result.setTenantUser(source.getTenantUser().getId());
        }
        if(source.getShop()!=null){
           result.setShop(source.getShop().getId());
        }
         if(source.getShop()!=null){
            result.setShopName(source.getShop().getName());
         }
         if(source.getTenantUser()!=null){
            result.setTenantUserName(source.getTenantUser().getName());
         }

         result.setStoreStateName(source.getStoreState()+"");
        return result;
    }
}
