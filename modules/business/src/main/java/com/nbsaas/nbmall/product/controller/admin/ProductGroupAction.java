package com.nbsaas.nbmall.product.controller.admin;


import com.haoxuer.bigworld.tenant.controller.admin.TenantBaseAction;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
*
* Created by imake on 2021年12月28日17:59:58.
*/


@Scope("prototype")
@Controller
public class ProductGroupAction extends TenantBaseAction {

	@RequiresPermissions("productgroup")
	@RequestMapping("/tenant/productgroup/view_list")
	public String list(ModelMap model) {
		return getView("productgroup/list");
	}

}