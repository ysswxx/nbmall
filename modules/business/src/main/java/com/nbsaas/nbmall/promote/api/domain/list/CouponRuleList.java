package com.nbsaas.nbmall.promote.api.domain.list;


import com.nbsaas.nbmall.promote.api.domain.simple.CouponRuleSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年12月28日23:18:26.
*/

@Data
public class CouponRuleList  extends ResponseList<CouponRuleSimple> {

}