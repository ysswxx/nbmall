package com.nbsaas.nbmall.promote.rest.convert;

import com.nbsaas.nbmall.promote.api.domain.response.CouponResponse;
import com.nbsaas.nbmall.promote.data.entity.Coupon;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class CouponResponseConvert implements Conver<CouponResponse, Coupon> {
    @Override
    public CouponResponse conver(Coupon source) {
        CouponResponse result = new CouponResponse();
        TenantBeanUtils.copyProperties(source,result);

         if(source.getCouponRule()!=null){
            result.setCouponRuleName(source.getCouponRule().getName());
         }
        if(source.getTenantUser()!=null){
           result.setTenantUser(source.getTenantUser().getId());
        }
        if(source.getShop()!=null){
           result.setShop(source.getShop().getId());
        }
        if(source.getCouponRule()!=null){
           result.setCouponRule(source.getCouponRule().getId());
        }
         if(source.getShop()!=null){
            result.setShopName(source.getShop().getName());
         }
         if(source.getTenantUser()!=null){
            result.setTenantUserName(source.getTenantUser().getName());
         }

         result.setCouponStateName(source.getCouponState()+"");
         result.setCouponCatalogName(source.getCouponCatalog()+"");
        return result;
    }
}
