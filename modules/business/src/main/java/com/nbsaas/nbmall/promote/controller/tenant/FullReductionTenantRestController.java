package com.nbsaas.nbmall.promote.controller.tenant;

import com.nbsaas.nbmall.promote.api.apis.FullReductionApi;
import com.nbsaas.nbmall.promote.api.domain.list.FullReductionList;
import com.nbsaas.nbmall.promote.api.domain.page.FullReductionPage;
import com.nbsaas.nbmall.promote.api.domain.request.*;
import com.nbsaas.nbmall.promote.api.domain.response.FullReductionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/fullreduction")
@RestController
public class FullReductionTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("fullreduction")
    @RequestMapping("create")
    public FullReductionResponse create(@RequestBody FullReductionDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

	@RequiresPermissions("fullreduction")
    @RequestMapping("delete")
    public FullReductionResponse delete(FullReductionDataRequest request) {
        initTenant(request);
        FullReductionResponse result = new FullReductionResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("fullreduction")
    @RequestMapping("update")
    public FullReductionResponse update(@RequestBody FullReductionDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("fullreduction")
    @RequestMapping("view")
    public FullReductionResponse view(FullReductionDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("fullreduction")
    @RequestMapping("list")
    public FullReductionList list(FullReductionSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("fullreduction")
    @RequestMapping("search")
    public FullReductionPage search(FullReductionSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private FullReductionApi api;

}
