package com.nbsaas.nbmall.order.data.entity;

import com.haoxuer.bigworld.member.data.entity.TenantUser;
import com.haoxuer.bigworld.pay.data.enums.PayState;
import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.haoxuer.discover.data.enums.StoreState;
import com.nbsaas.codemake.annotation.*;
import com.nbsaas.nbmall.order.data.enums.OrderState;
import com.nbsaas.nbmall.shop.data.entity.Shop;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


@Data
@FormAnnotation(title = "订单管理", model = "订单", menu = "1,123,124")
@Entity
@Table(name = "bs_tenant_order_form")
public class OrderForm extends TenantEntity {

    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private TenantUser user;

    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private TenantUser creator;

    @SearchItem(label = "商家", name = "shop", key = "shop.id", operator = "eq", classType = "Long",type = InputType.select)
    @FormField(title = "商家", grid = false, col = 12,type = InputType.select,option = "shop")
    @FieldConvert
    @FieldName
    @ManyToOne(fetch = FetchType.LAZY)
    private Shop shop;

    @SearchItem(label = "订单号", name = "orderNo")
    @Column(length = 50)
    @FormField(title = "订单号", sortNum = "1", grid = true, col = 12)
    private String no;

    //合并支付订单号（未拆单情况下与订单号一致;已拆单情况下，与订单号不一致，但多笔子订单的合并支付订单号是一致）
    @FormField(title = "合并支付订单号", sortNum = "1", grid = false, col = 12)
    private String mergeOrderCode;

    @FormField(title = "外部交易流水号", sortNum = "1", grid = false, col = 12)
    private String outTradeNum;

    //订单来源 1pc  2 移动端h5 3APP  4 微信客户端H5  5小程序（微信小程序）
    @FormField(title = "订单来源", sortNum = "1", grid = false, col = 12)
    private Integer orderSource;

    //支付来源 1pc  2 移动端h5 3APP  4 微信客户端H5  5小程序（微信小程序）
    @FormField(title = "支付来源", sortNum = "1", grid = false, col = 12)
    private Integer paySource;

    @FormField(title = "最新售后ID", sortNum = "1", grid = false, col = 12)
    private Long newRefundId;

    //订单状态(1待付款 2待发货 3待收货 4已完成 5已取消 6超时交易关闭 )
    private Integer state;

    @FormField(title = "订单状态", sortNum = "1", grid = true, col = 12)
    private OrderState orderState;

    //评价状态 1-待评价  2-已评价  3-对方已评  4-双方已评
    @FormField(title = "评价状态", sortNum = "1", grid = false, col = 12)
    private Integer commentState;

    //用户删除(0未删除 1回收站 2删除)
    @FormField(title = "用户删除", sortNum = "1", grid = false, col = 12)
    private Integer userState;


    @FormField(title = "下单时间", sortNum = "1", grid = false, col = 12)
    private Date orderTime;

    @FormField(title = "支付时间", sortNum = "1", grid = false, col = 12)
    private Date payTime;

    @FormField(title = "发货时间", sortNum = "1", grid = false, col = 12)
    private Date deliveryTime;

    @FormField(title = "收货时间", sortNum = "1", grid = false, col = 12)
    private Date receiveTime;

    //订单类型(1实物订单 2虚拟订单)
    @FormField(title = "订单类型", sortNum = "1", grid = false, col = 12)
    private Integer orderType;

    @FormField(title = "重量", sortNum = "1", grid = false, col = 12)
    private BigDecimal weight;


    //实际付款价格=原价-卖家优惠券总金额-平台优惠券总金额+运费-整单折扣-红包抵扣-积分抵扣
    //
    @FormField(title = "原价", sortNum = "1", grid = true, col = 12)
    private BigDecimal totalAmount;


    //订单优惠后总金额(含运费)
    @FormField(title = "应付金额", sortNum = "1", grid = true, col = 12)
    private BigDecimal amount;

    @FormField(title = "整单折扣", sortNum = "1", grid = true, col = 12)
    private BigDecimal changeAmount;

    //实际支付金额(抵扣红包、积分金额)
    @FormField(title = "实际支付金额", sortNum = "1", grid = true, col = 12)
    private BigDecimal payAmount;

    @FormField(title = "运费", sortNum = "1", grid = false, col = 12)
    private BigDecimal freight;

    @FormField(title = "卖家优惠券总金额", sortNum = "1", grid = false, col = 12)
    private BigDecimal discount;

    @FormField(title = "平台优惠券总金额", sortNum = "1", grid = false, col = 12)
    private BigDecimal platformDiscount;

    @FormField(title = "积分抵扣", sortNum = "1", grid = false, col = 12)
    private BigDecimal integralDiscount;

    @FormField(title = "使用积分数", sortNum = "1", grid = false, col = 12)
    private BigDecimal useIntegral;

    @FormField(title = "使用红包", sortNum = "1", grid = false, col = 12)
    private BigDecimal useRedPacket;

    @FormField(title = "单件商品红包抵扣", sortNum = "1", grid = false, col = 12)
    private BigDecimal redPacketDiscount;

    @FormField(title = "退款金额", sortNum = "1", grid = false, col = 12)
    private BigDecimal returnAmount;

    @FormField(title = "积分赠送", sortNum = "1", grid = false, col = 12)
    private BigDecimal sendIntegral;

    @FormField(title = "系统自动收货时间", sortNum = "1", grid = false, col = 12)
    private Date sysReceiveTime;

    @FormField(title = "是否已延长收货", sortNum = "1", grid = false, col = 12)
    private boolean hasExtendReceive;

    @FormField(title = "商家改价格时间", sortNum = "1", grid = false, col = 12)
    private Date editPriceTime;

    @FormField(title = "完成时间", sortNum = "1", grid = false, col = 12)
    private Date finishTime;

    @FormField(title = "购买数量", sortNum = "1", grid = false, col = 12)
    private Integer productCount;

    @FormField(title = "退货数量", sortNum = "1", grid = false, col = 12)
    private Integer returnCount;

    @FormField(title = "最后提现时间", sortNum = "1", grid = false, col = 12)
    private Date remindTime;

    private StoreState storeState;

    @OneToMany(mappedBy = "orderForm", fetch = FetchType.LAZY)
    private List<OrderItem> items;

    @SearchItem(label = "收货人", name = "consignee")
    @Column(length = 20)
    @FormField(title = "收货人", sortNum = "1", grid = true, col = 12)
    private String consignee;


    @SearchItem(label = "联系电话", name = "phone")
    @Column(length = 20)
    @FormField(title = "联系电话", sortNum = "1", grid = true, col = 12)
    private String phone;

    @Column(length = 100)
    @FormField(title = "收货地址", sortNum = "1", grid = true, col = 12,width = "1000")
    private String address;

    @Column(length = 500)
    @FormField(title = "备注", sortNum = "1", grid = false, col = 12)
    private String note;



    private PayState payState;


}
