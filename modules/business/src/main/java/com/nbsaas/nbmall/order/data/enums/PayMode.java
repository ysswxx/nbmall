package com.nbsaas.nbmall.order.data.enums;

public enum PayMode {
    alipay,weixin,unionpay,balance,cash,other;
    public String toString() {
        if (this.name().equals("alipay")) {
            return "支付宝支付";
        } else if (this.name().equals("weixin")) {
            return "微信支付";
        } else if (this.name().equals("unionpay")) {
            return "银联支付";
        }else if (this.name().equals("balance")) {
            return "余额";
        }else if (this.name().equals("cash")) {
            return "现金";
        }else if (this.name().equals("other")) {
            return "初始化";
        } else {
            return this.name();
        }
    }
}
