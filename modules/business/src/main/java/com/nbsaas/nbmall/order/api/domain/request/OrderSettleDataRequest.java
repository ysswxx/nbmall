package com.nbsaas.nbmall.order.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import com.nbsaas.nbmall.shop.data.enums.SubjectType;
import com.nbsaas.nbmall.order.data.enums.SettleType;
import com.nbsaas.nbmall.shop.data.enums.SubjectType;
import java.util.Date;
import java.math.BigDecimal;

/**
*
* Created by imake on 2021年12月21日18:00:16.
*/

@Data
public class OrderSettleDataRequest extends TenantRequest {

    private Long id;

     private Long shop;
     private SubjectType mealSubject;
     private BigDecimal freight;
     private BigDecimal platformBasic;
     private BigDecimal money;
     private BigDecimal payAmount;
     private BigDecimal platformFee;
     private BigDecimal platformBalance;
     private Long orderForm;
     private BigDecimal balance;
     private BigDecimal platformRate;
     private SubjectType freightSubject;
     private SettleType settleType;

}