package com.nbsaas.nbmall.device.rest.resource;

import com.nbsaas.nbmall.device.api.apis.DeviceTypeApi;
import com.nbsaas.nbmall.device.api.domain.list.DeviceTypeList;
import com.nbsaas.nbmall.device.api.domain.page.DeviceTypePage;
import com.nbsaas.nbmall.device.api.domain.request.*;
import com.nbsaas.nbmall.device.api.domain.response.DeviceTypeResponse;
import com.nbsaas.nbmall.device.data.dao.DeviceTypeDao;
import com.nbsaas.nbmall.device.data.entity.DeviceType;
import com.nbsaas.nbmall.device.rest.convert.DeviceTypeResponseConvert;
import com.nbsaas.nbmall.device.rest.convert.DeviceTypeSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;
import com.haoxuer.discover.user.data.dao.UserInfoDao;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class DeviceTypeResource implements DeviceTypeApi {

    @Autowired
    private DeviceTypeDao dataDao;

    @Autowired
    private UserInfoDao creatorDao;

    @Override
    public DeviceTypeResponse create(DeviceTypeDataRequest request) {
        DeviceTypeResponse result = new DeviceTypeResponse();

        DeviceType bean = new DeviceType();
        handleData(request, bean);
        dataDao.save(bean);
        result = new DeviceTypeResponseConvert().conver(bean);
        return result;
    }

    @Override
    public DeviceTypeResponse update(DeviceTypeDataRequest request) {
        DeviceTypeResponse result = new DeviceTypeResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        DeviceType bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new DeviceTypeResponseConvert().conver(bean);
        return result;
    }

    private void handleData(DeviceTypeDataRequest request, DeviceType bean) {
        BeanDataUtils.copyProperties(request,bean);
            if(request.getCreator()!=null){
               bean.setCreator(creatorDao.findById(request.getCreator()));
            }

    }

    @Override
    public DeviceTypeResponse delete(DeviceTypeDataRequest request) {
        DeviceTypeResponse result = new DeviceTypeResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public DeviceTypeResponse view(DeviceTypeDataRequest request) {
        DeviceTypeResponse result=new DeviceTypeResponse();
        DeviceType bean = dataDao.findById( request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new DeviceTypeResponseConvert().conver(bean);
        return result;
    }
    @Override
    public DeviceTypeList list(DeviceTypeSearchRequest request) {
        DeviceTypeList result = new DeviceTypeList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<DeviceType> organizations = dataDao.list(0, request.getSize(), filters, orders);
        DeviceTypeSimpleConvert convert=new DeviceTypeSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public DeviceTypePage search(DeviceTypeSearchRequest request) {
        DeviceTypePage result=new DeviceTypePage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<DeviceType> page=dataDao.page(pageable);
        DeviceTypeSimpleConvert convert=new DeviceTypeSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
