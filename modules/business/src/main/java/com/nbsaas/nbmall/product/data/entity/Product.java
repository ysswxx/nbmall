package com.nbsaas.nbmall.product.data.entity;

import com.haoxuer.bigworld.member.data.entity.TenantUser;
import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.haoxuer.discover.data.enums.StoreState;
import com.nbsaas.codemake.annotation.*;
import com.nbsaas.nbmall.product.data.enums.ProductState;
import com.nbsaas.nbmall.product.data.enums.ProductType;
import com.nbsaas.nbmall.shop.data.entity.Shop;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@CreateByUser
@ComposeView
@Data
@FormAnnotation(title = "商品管理", model = "商品",menu = "1,125,127")
@Entity
@Table(name = "bs_tenant_product")
public class Product extends TenantEntity {
    public static Product fromId(Long id) {
        Product result = new Product();
        result.setId(id);
        return result;
    }
    @FormField(title = "商品封面",  grid = false, col = 22,type = InputType.image)
    private String logo;

    @SearchItem(label = "商品名称",name = "name")
    @FormField(title = "商品名称",  grid = true, col = 12,required = true)
    private String name;

    @SearchItem(label = "商家名称",name = "shop",key = "shop.id",operator = "eq",classType = "Long",type = InputType.select)
    @FormField(title = "商家名称",  grid = true, col = 12,required = true,type = InputType.select,option = "shop")
    @FieldName
    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private Shop shop;

    @SearchItem(label = "商品分类",name = "productCatalog",key = "productCatalog.id",operator = "eq",classType = "Integer",type = InputType.select)
    @FormField(title = "商品分类",  grid = true, col = 12,type = InputType.select,option = "productCatalog")
    @FieldName
    @FieldConvert(classType = "Integer")
    @ManyToOne(fetch = FetchType.LAZY)
    private ProductCatalog productCatalog;


    @FormField(title = "商品分组",  grid = false, col = 12,type = InputType.select,option = "productGroup")
    @FieldName
    @FieldConvert()
    @ManyToOne(fetch = FetchType.LAZY)
    private ProductGroup productGroup;


    @SearchItem(label = "商品编码",name = "barCode")
    @FormField(title = "商品编码",  grid = true, col = 12)
    private String barCode;





    @FormField(title = "菜品缩略图",  grid = false, col = 12,ignore = true)
    private String thumbnail;


    @FormField(title = "商品简介",  grid = false, col = 12)
    private String summary;


    @FormField(title = "是否开票", grid = true, col = 12)
    private Boolean invoice;

    //商品状态,0-仓库中  1-待审核  2- 审核不通过  3-出售中(审核通过、上架)  4-下架（违规下架）  5 -回收站
    @FormField(title = "商品状态",grid = true, col = 12)
    private ProductState productState;


    @FormField(title = "是否开启规格",  col = 12)
    private Boolean skuEnable;

    @FormField(title = "销售价",  grid = true, col = 12)
    private BigDecimal salePrice;

    @FormField(title = "市场价",  grid = true, col = 12)
    private BigDecimal marketPrice;

    @FormField(title = "成本价",  grid = true, col = 12)
    private BigDecimal costPrice;

    @FormField(title = "积分",  grid = true, col = 12)
    private BigDecimal score;

    @FormField(title = "vip价",  grid = true, col = 12)
    private BigDecimal vipPrice;

    @FormField(title = "库存量",  grid = true, col = 12)
    private Integer inventory;

    @FormField(title = "库存预警值",  grid = true, col = 12)
    private Integer warningValue;


    @FormField(title = "毛重",  grid = false, col = 12)
    private Double weight;

    @FormField(title = "体积",  grid = false, col = 12)
    private Double volume;

    @FormField(title = "净重",  grid = false, col = 12)
    private Double netWeight;

    @FormField(title = "高",  grid = false, col = 12)
    private Double height;

    @FormField(title = "宽",  grid = false, col = 12)
    private Double width;

    @FormField(title = "高",  grid = false, col = 12)
    private Double length;




    private String unit;

    private String demo;

    @FormField(title = "商品介绍",  grid = false, col = 22,type = InputType.richText)
    private String note;

    private Integer sortNum;

    private StoreState storeState;




    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
    private List<ProductSku> skus;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
    private List<ProductImage> images = new ArrayList<>();


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
    private List<ProductSpec> specs = new ArrayList<>();

    private ProductType productType;

    private BigDecimal minPrice;

    private BigDecimal maxPrice;

    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private TenantUser creator;

}
