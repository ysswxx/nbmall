package com.nbsaas.nbmall.common.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.common.data.dao.SiteTenantDao;
import com.nbsaas.nbmall.common.data.entity.SiteTenant;

/**
* Created by imake on 2021年12月23日11:54:02.
*/
@Repository

public class SiteTenantDaoImpl extends CriteriaDaoImpl<SiteTenant, Long> implements SiteTenantDao {

	@Override
	public SiteTenant findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public SiteTenant save(SiteTenant bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public SiteTenant deleteById(Long id) {
		SiteTenant entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<SiteTenant> getEntityClass() {
		return SiteTenant.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}