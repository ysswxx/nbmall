package com.nbsaas.nbmall.device.api.domain.request;

import com.haoxuer.discover.user.api.domain.request.BasePageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月10日17:40:16.
*/

@Data
public class DeviceTypeSearchRequest extends BasePageRequest {

    //厂家名称
     @Search(name = "name",operator = Filter.Operator.like)
     private String name;




    private String sortField;


    private String sortMethod;
}