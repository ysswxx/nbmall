package com.nbsaas.nbmall.product.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
/**
*
* Created by BigWorld on 2021年12月28日17:59:58.
*/

@Data
public class ProductGroupResponse extends ResponseObject {

    private Long id;

     private Long shop;
     private String note;
     private Long num;
     private Integer sortNum;
     private String showState;
     private String shopName;
     private String name;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;

}