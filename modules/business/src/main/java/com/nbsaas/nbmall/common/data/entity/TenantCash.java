package com.nbsaas.nbmall.common.data.entity;

import com.haoxuer.bigworld.member.data.entity.TenantUser;
import com.haoxuer.bigworld.pay.data.entity.CashConfig;
import com.haoxuer.bigworld.pay.data.enums.SendState;
import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.haoxuer.discover.trade.data.entity.TradeAccount;
import com.nbsaas.codemake.annotation.*;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;


@CreateByUser
@ComposeView
@Data
@FormAnnotation(title = "提现记录管理", model = "提现记录", menu = "1,101,104")
@Entity
@Table(name = "bs_tenant_cash")
public class TenantCash extends TenantEntity {

    @SearchItem(label = "提现配置",name = "cashConfig",key = "cashConfig.id",classType = "Long",operator = "eq",show = false)
    @FieldName
    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private CashConfig cashConfig;

    @SearchItem(label = "姓名",name = "name",key = "name")
    @FormField(title = "姓名", sortNum = "2", grid = true, col = 12)
    @Column(length = 10)
    private String name;

    @SearchItem(label = "提现单号",name = "cashNo",key = "no")
    @FormField(title = "提现单号", sortNum = "1", grid = true, col =12,width = "200")
    private String no;


    @FormField(title = "付款金额", sortNum = "3", grid = true, col = 12)
    private BigDecimal money;


    @FormField(title = "手续费", sortNum = "3", grid = true, col = 12)
    private BigDecimal fee;

    @Column(length = 20)
    private String idNo;

    @Column(length = 20)
    private String phone;


    @FormField(title = "提现金额", sortNum = "3", grid = true, col = 12,sort = true)
    private BigDecimal cash;

    private String openId;

    @FormField(title = "备注", sortNum = "5", grid = true, col = 12)
    private String demo;

    private String note;

    @FormField(title = "状态", sortNum = "4", grid = true, col = 12)
    private SendState sendState;

    private String bussNo;

    @Column(length = 50)
    private String appId;

    @ManyToOne(fetch = FetchType.LAZY)
    private TradeAccount tradeAccount;

    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private TenantUser creator;
}
