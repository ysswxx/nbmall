package com.nbsaas.nbmall.shop.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import com.haoxuer.discover.data.enums.StoreState;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
/**
*
* Created by BigWorld on 2021年12月10日17:38:00.
*/

@Data
public class ShopStaffResponse extends ResponseObject {

    private Long id;

     private Long tenantUser;
     private Long shop;
     private StoreState storeState;
     private String shopName;
     private String tenantUserName;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;

     private String storeStateName;
}