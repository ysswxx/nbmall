package com.nbsaas.nbmall.order.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.order.data.entity.OrderItem;

/**
* Created by imake on 2021年12月10日17:38:01.
*/
public interface OrderItemDao extends BaseDao<OrderItem,Long>{

	 OrderItem findById(Long id);

	 OrderItem save(OrderItem bean);

	 OrderItem updateByUpdater(Updater<OrderItem> updater);

	 OrderItem deleteById(Long id);

	 OrderItem findById(Long tenant, Long id);

     OrderItem deleteById(Long tenant, Long id);
}