package com.nbsaas.nbmall.order.controller.rest;

import com.nbsaas.nbmall.order.api.apis.OrderItemApi;
import com.nbsaas.nbmall.order.api.domain.list.OrderItemList;
import com.nbsaas.nbmall.order.api.domain.page.OrderItemPage;
import com.nbsaas.nbmall.order.api.domain.request.*;
import com.nbsaas.nbmall.order.api.domain.response.OrderItemResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/orderitem")
@RestController
public class OrderItemRestController extends BaseRestController {


    @RequestMapping("create")
    public OrderItemResponse create(OrderItemDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

    @RequestMapping("delete")
    public OrderItemResponse delete(OrderItemDataRequest request) {
        initTenant(request);
        OrderItemResponse result = new OrderItemResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public OrderItemResponse update(OrderItemDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public OrderItemResponse view(OrderItemDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public OrderItemList list(OrderItemSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public OrderItemPage search(OrderItemSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private OrderItemApi api;

}
