package com.nbsaas.nbmall.product.controller.rest;

import com.nbsaas.nbmall.product.api.apis.ProductCommentApi;
import com.nbsaas.nbmall.product.api.domain.list.ProductCommentList;
import com.nbsaas.nbmall.product.api.domain.page.ProductCommentPage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductCommentResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/productcomment")
@RestController
public class ProductCommentRestController extends BaseRestController {


    @RequestMapping("create")
    public ProductCommentResponse create(ProductCommentDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

    @RequestMapping("delete")
    public ProductCommentResponse delete(ProductCommentDataRequest request) {
        initTenant(request);
        ProductCommentResponse result = new ProductCommentResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public ProductCommentResponse update(ProductCommentDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public ProductCommentResponse view(ProductCommentDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public ProductCommentList list(ProductCommentSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public ProductCommentPage search(ProductCommentSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private ProductCommentApi api;

}
