package com.nbsaas.nbmall.shop.rest.resource;

import com.nbsaas.nbmall.shop.api.apis.ShopStaffApi;
import com.nbsaas.nbmall.shop.api.domain.list.ShopStaffList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopStaffPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopStaffResponse;
import com.nbsaas.nbmall.shop.data.dao.ShopStaffDao;
import com.nbsaas.nbmall.shop.data.entity.ShopStaff;
import com.nbsaas.nbmall.shop.rest.convert.ShopStaffResponseConvert;
import com.nbsaas.nbmall.shop.rest.convert.ShopStaffSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.nbsaas.nbmall.shop.data.dao.ShopDao;
import com.haoxuer.bigworld.member.data.dao.TenantUserDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class ShopStaffResource implements ShopStaffApi {

    @Autowired
    private ShopStaffDao dataDao;

    @Autowired
    private ShopDao shopDao;
    @Autowired
    private TenantUserDao tenantUserDao;


    @Override
    public ShopStaffResponse create(ShopStaffDataRequest request) {
        ShopStaffResponse result = new ShopStaffResponse();

        ShopStaff bean = new ShopStaff();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new ShopStaffResponseConvert().conver(bean);
        return result;
    }

    @Override
    public ShopStaffResponse update(ShopStaffDataRequest request) {
        ShopStaffResponse result = new ShopStaffResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        ShopStaff bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new ShopStaffResponseConvert().conver(bean);
        return result;
    }

    private void handleData(ShopStaffDataRequest request, ShopStaff bean) {
       TenantBeanUtils.copyProperties(request,bean);
           if(request.getTenantUser()!=null){
              bean.setTenantUser(tenantUserDao.findById(request.getTenantUser()));
           }
           if(request.getShop()!=null){
              bean.setShop(shopDao.findById(request.getShop()));
           }

    }

    @Override
    public ShopStaffResponse delete(ShopStaffDataRequest request) {
        ShopStaffResponse result = new ShopStaffResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public ShopStaffResponse view(ShopStaffDataRequest request) {
        ShopStaffResponse result=new ShopStaffResponse();
        ShopStaff bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new ShopStaffResponseConvert().conver(bean);
        return result;
    }
    @Override
    public ShopStaffList list(ShopStaffSearchRequest request) {
        ShopStaffList result = new ShopStaffList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<ShopStaff> organizations = dataDao.list(0, request.getSize(), filters, orders);

        ShopStaffSimpleConvert convert=new ShopStaffSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public ShopStaffPage search(ShopStaffSearchRequest request) {
        ShopStaffPage result=new ShopStaffPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<ShopStaff> page=dataDao.page(pageable);

        ShopStaffSimpleConvert convert=new ShopStaffSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
