package com.nbsaas.nbmall.promote.controller.tenant;

import com.nbsaas.nbmall.promote.api.apis.CouponRuleApi;
import com.nbsaas.nbmall.promote.api.domain.list.CouponRuleList;
import com.nbsaas.nbmall.promote.api.domain.page.CouponRulePage;
import com.nbsaas.nbmall.promote.api.domain.request.*;
import com.nbsaas.nbmall.promote.api.domain.response.CouponRuleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/couponrule")
@RestController
public class CouponRuleTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("couponrule")
    @RequestMapping("create")
    public CouponRuleResponse create(CouponRuleDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

	@RequiresPermissions("couponrule")
    @RequestMapping("delete")
    public CouponRuleResponse delete(CouponRuleDataRequest request) {
        initTenant(request);
        CouponRuleResponse result = new CouponRuleResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("couponrule")
    @RequestMapping("update")
    public CouponRuleResponse update(CouponRuleDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("couponrule")
    @RequestMapping("view")
    public CouponRuleResponse view(CouponRuleDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("couponrule")
    @RequestMapping("list")
    public CouponRuleList list(CouponRuleSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("couponrule")
    @RequestMapping("search")
    public CouponRulePage search(CouponRuleSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private CouponRuleApi api;

}
