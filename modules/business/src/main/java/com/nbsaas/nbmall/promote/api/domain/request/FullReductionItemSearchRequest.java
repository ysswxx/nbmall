package com.nbsaas.nbmall.promote.api.domain.request;

import com.haoxuer.bigworld.member.api.domain.request.TenantPageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月25日12:45:48.
*/

@Data
public class FullReductionItemSearchRequest extends TenantPageRequest {

    //满减规则
     @Search(name = "reduction.id",operator = Filter.Operator.eq)
     private Long reduction;



}