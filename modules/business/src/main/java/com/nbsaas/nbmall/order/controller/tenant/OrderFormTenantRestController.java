package com.nbsaas.nbmall.order.controller.tenant;

import com.nbsaas.nbmall.order.api.apis.OrderFormApi;
import com.nbsaas.nbmall.order.api.domain.list.OrderFormList;
import com.nbsaas.nbmall.order.api.domain.page.OrderFormPage;
import com.nbsaas.nbmall.order.api.domain.request.*;
import com.nbsaas.nbmall.order.api.domain.response.OrderFormResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/orderform")
@RestController
public class OrderFormTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("orderform")
    @RequestMapping("create")
    public OrderFormResponse create(OrderFormDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

	@RequiresPermissions("orderform")
    @RequestMapping("delete")
    public OrderFormResponse delete(OrderFormDataRequest request) {
        initTenant(request);
        OrderFormResponse result = new OrderFormResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("orderform")
    @RequestMapping("update")
    public OrderFormResponse update(OrderFormDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("orderform")
    @RequestMapping("view")
    public OrderFormResponse view(OrderFormDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("orderform")
    @RequestMapping("list")
    public OrderFormList list(OrderFormSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("orderform")
    @RequestMapping("search")
    public OrderFormPage search(OrderFormSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private OrderFormApi api;

}
