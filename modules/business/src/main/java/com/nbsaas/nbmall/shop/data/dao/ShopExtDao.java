package com.nbsaas.nbmall.shop.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.shop.data.entity.ShopExt;

/**
* Created by imake on 2021年12月10日17:38:00.
*/
public interface ShopExtDao extends BaseDao<ShopExt,Long>{

	 ShopExt findById(Long id);

	 ShopExt save(ShopExt bean);

	 ShopExt updateByUpdater(Updater<ShopExt> updater);

	 ShopExt deleteById(Long id);

	 ShopExt findById(Long tenant, Long id);

     ShopExt deleteById(Long tenant, Long id);
}