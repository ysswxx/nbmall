package com.nbsaas.nbmall.utils;

public class LocationUtils {
    private static double EARTH_RADIUS = 6378.137;

    private static double rad(double d) {
        return d * Math.PI / 180.0;
    }

    /**
     * 通过经纬度获取距离(单位：米)
     *
     * @param lat1
     * @param lng1
     * @param lat2
     * @param lng2
     * @return
     */
    public static double getDistance(double lat1, double lng1, double lat2,
                                     double lng2) {
        double radLat1 = rad(lat1);
        double radLat2 = rad(lat2);
        double a = radLat1 - radLat2;
        double b = rad(lng1) - rad(lng2);
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2)
                + Math.cos(radLat1) * Math.cos(radLat2)
                * Math.pow(Math.sin(b / 2), 2)));
        s = s * EARTH_RADIUS;
        s = Math.round(s * 10000d) / 10000d;
        s = s * 1000;
        return s;
    }

    public static String distance(Double lat1, Double lng1, Double lat2,
                                  Double lng2) {
        if (lat1 == null || lat2 == null || lng1 == null || lng2 == null) {
            return "";
        }
        String result = "";
        double temp = getDistance(lat1, lng1, lat2, lng2);
        if (temp < 1000) {
            return (int) Math.ceil(temp) + "m";
        }
        temp = temp / 1000;
        return result + (int) Math.ceil(temp) + "km";
    }

    public static String mi(Float lat1, Float lng1, Float lat2,
                            Float lng2) {
        if (lat1 == null || lat2 == null || lng1 == null || lng2 == null) {
            return "";
        }
        String result = "";
        double temp = getDistance(lat1, lng1, lat2, lng2);
        return result + (int) Math.ceil(temp) + "米";
    }

}