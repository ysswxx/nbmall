package com.nbsaas.nbmall.customer.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import com.haoxuer.discover.data.enums.StoreState;

import java.util.Date;

/**
 * Created by imake on 2021年12月10日22:56:19.
 */

@Data
public class CustomerAddressDataRequest extends TenantRequest {

    private Long id;

    private Integer area;
    private String note;
    private Date updateDate;
    private String address;
    private Integer city;
    private Double lng;
    private StoreState storeState;
    private String postalCode;
    private Integer province;
    private String label;
    private String phone;
    private String houseNo;
    private String name;
    private String tel;
    private Long customer;
    private Double lat;
    private Boolean defaultAddress;

}