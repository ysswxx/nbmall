package com.nbsaas.nbmall.shop.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.shop.data.entity.Shop;

/**
* Created by imake on 2021年12月10日17:38:00.
*/
public interface ShopDao extends BaseDao<Shop,Long>{

	 Shop findById(Long id);

	 Shop save(Shop bean);

	 Shop updateByUpdater(Updater<Shop> updater);

	 Shop deleteById(Long id);

	 Shop findById(Long tenant, Long id);

     Shop deleteById(Long tenant, Long id);
}