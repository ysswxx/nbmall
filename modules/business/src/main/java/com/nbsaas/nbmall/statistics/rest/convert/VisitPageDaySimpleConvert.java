package com.nbsaas.nbmall.statistics.rest.convert;

import com.nbsaas.nbmall.statistics.api.domain.simple.VisitPageDaySimple;
import com.nbsaas.nbmall.statistics.data.entity.VisitPageDay;
import com.haoxuer.discover.data.rest.core.Conver;
public class VisitPageDaySimpleConvert implements Conver<VisitPageDaySimple, VisitPageDay> {


    @Override
    public VisitPageDaySimple conver(VisitPageDay source) {
        VisitPageDaySimple result = new VisitPageDaySimple();

            result.setId(source.getId());
            if(source.getUser()!=null){
               result.setUser(source.getUser().getId());
            }
             result.setPageNum(source.getPageNum());
             result.setAddDate(source.getAddDate());
             result.setKey(source.getKey());

        return result;
    }
}
