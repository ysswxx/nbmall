package com.nbsaas.nbmall.order.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.order.data.dao.OrderSettleDao;
import com.nbsaas.nbmall.order.data.entity.OrderSettle;

/**
* Created by imake on 2021年12月21日18:00:16.
*/
@Repository

public class OrderSettleDaoImpl extends CriteriaDaoImpl<OrderSettle, Long> implements OrderSettleDao {

	@Override
	public OrderSettle findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public OrderSettle save(OrderSettle bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public OrderSettle deleteById(Long id) {
		OrderSettle entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<OrderSettle> getEntityClass() {
		return OrderSettle.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public OrderSettle findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public OrderSettle deleteById(Long tenant,Long id) {
		OrderSettle entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}