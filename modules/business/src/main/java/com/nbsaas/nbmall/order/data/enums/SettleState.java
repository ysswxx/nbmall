package com.nbsaas.nbmall.order.data.enums;

public enum SettleState {

    init,success;


    @Override
    public String toString() {
        if (name().equals("init")) {
            return "待结算";
        } else if (name().equals("success")) {
            return "已结算";
        }
        return super.toString();
    }
}
