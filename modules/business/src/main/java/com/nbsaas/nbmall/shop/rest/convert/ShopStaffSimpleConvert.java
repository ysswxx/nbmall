package com.nbsaas.nbmall.shop.rest.convert;

import com.nbsaas.nbmall.shop.api.domain.simple.ShopStaffSimple;
import com.nbsaas.nbmall.shop.data.entity.ShopStaff;
import com.haoxuer.discover.data.rest.core.Conver;
public class ShopStaffSimpleConvert implements Conver<ShopStaffSimple, ShopStaff> {


    @Override
    public ShopStaffSimple conver(ShopStaff source) {
        ShopStaffSimple result = new ShopStaffSimple();

            result.setId(source.getId());
            if(source.getTenantUser()!=null){
               result.setTenantUser(source.getTenantUser().getId());
            }
            if(source.getShop()!=null){
               result.setShop(source.getShop().getId());
            }
             result.setStoreState(source.getStoreState());
             if(source.getShop()!=null){
                result.setShopName(source.getShop().getName());
             }
             if(source.getTenantUser()!=null){
                result.setTenantUserName(source.getTenantUser().getName());
             }
             result.setAddDate(source.getAddDate());

             result.setStoreStateName(source.getStoreState()+"");
        return result;
    }
}
