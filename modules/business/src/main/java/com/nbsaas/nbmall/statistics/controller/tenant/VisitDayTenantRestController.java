package com.nbsaas.nbmall.statistics.controller.tenant;

import com.nbsaas.nbmall.statistics.api.apis.VisitDayApi;
import com.nbsaas.nbmall.statistics.api.domain.list.VisitDayList;
import com.nbsaas.nbmall.statistics.api.domain.page.VisitDayPage;
import com.nbsaas.nbmall.statistics.api.domain.request.*;
import com.nbsaas.nbmall.statistics.api.domain.response.VisitDayResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/visitday")
@RestController
public class VisitDayTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("visitday")
    @RequestMapping("create")
    public VisitDayResponse create(VisitDayDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

	@RequiresPermissions("visitday")
    @RequestMapping("delete")
    public VisitDayResponse delete(VisitDayDataRequest request) {
        initTenant(request);
        VisitDayResponse result = new VisitDayResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("visitday")
    @RequestMapping("update")
    public VisitDayResponse update(VisitDayDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("visitday")
    @RequestMapping("view")
    public VisitDayResponse view(VisitDayDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("visitday")
    @RequestMapping("list")
    public VisitDayList list(VisitDaySearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("visitday")
    @RequestMapping("search")
    public VisitDayPage search(VisitDaySearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private VisitDayApi api;

}
