package com.nbsaas.nbmall.shop.data.enums;

public enum VisitType {

    visit,scan;
    @Override
    public String toString() {
        if (name().equals("visit")) {
            return "访问";
        } else if (name().equals("scan")) {
            return "扫码";
        }
        return super.toString();
    }
}
