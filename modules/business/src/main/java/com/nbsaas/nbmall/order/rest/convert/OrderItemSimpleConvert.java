package com.nbsaas.nbmall.order.rest.convert;

import com.nbsaas.nbmall.order.api.domain.simple.OrderItemSimple;
import com.nbsaas.nbmall.order.data.entity.OrderItem;
import com.haoxuer.discover.data.rest.core.Conver;
public class OrderItemSimpleConvert implements Conver<OrderItemSimple, OrderItem> {


    @Override
    public OrderItemSimple conver(OrderItem source) {
        OrderItemSimple result = new OrderItemSimple();

            result.setId(source.getId());
             result.setRefundState(source.getRefundState());
             result.setPlatformDiscount(source.getPlatformDiscount());
             result.setWeight(source.getWeight());
             result.setPayAmount(source.getPayAmount());
             result.setAddDate(source.getAddDate());
             result.setGiveType(source.getGiveType());
             result.setSubtotal(source.getSubtotal());
             result.setSize(source.getSize());
             result.setUnit(source.getUnit());
             result.setReturnNum(source.getReturnNum());
             result.setName(source.getName());
             result.setPaidAmount(source.getPaidAmount());
             result.setRealPrice(source.getRealPrice());
             result.setSkuAttr(source.getSkuAttr());
             result.setIntegralDiscount(source.getIntegralDiscount());
             result.setFreight(source.getFreight());
             result.setDiscountAmount(source.getDiscountAmount());
             result.setUseIntegral(source.getUseIntegral());
             result.setSurplusNum(source.getSurplusNum());
             result.setUseRedPacket(source.getUseRedPacket());
             result.setReturnAmount(source.getReturnAmount());
             result.setRealAmount(source.getRealAmount());
             result.setUseNum(source.getUseNum());
             result.setPrice(source.getPrice());
             result.setLogo(source.getLogo());
             result.setRedPacketDiscount(source.getRedPacketDiscount());

        return result;
    }
}
