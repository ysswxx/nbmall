package com.nbsaas.nbmall.shop.rest.convert;

import com.nbsaas.nbmall.shop.api.domain.response.ShopHourResponse;
import com.nbsaas.nbmall.shop.data.entity.ShopHour;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class ShopHourResponseConvert implements Conver<ShopHourResponse, ShopHour> {
    @Override
    public ShopHourResponse conver(ShopHour source) {
        ShopHourResponse result = new ShopHourResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getShop()!=null){
           result.setShop(source.getShop().getId());
        }
         if(source.getShop()!=null){
            result.setShopName(source.getShop().getName());
         }

        return result;
    }
}
