package com.nbsaas.nbmall.product.rest.resource;

import com.nbsaas.nbmall.product.api.apis.ProductSpecApi;
import com.nbsaas.nbmall.product.api.domain.list.ProductSpecList;
import com.nbsaas.nbmall.product.api.domain.page.ProductSpecPage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductSpecResponse;
import com.nbsaas.nbmall.product.data.dao.ProductSpecDao;
import com.nbsaas.nbmall.product.data.entity.ProductSpec;
import com.nbsaas.nbmall.product.rest.convert.ProductSpecResponseConvert;
import com.nbsaas.nbmall.product.rest.convert.ProductSpecSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.haoxuer.bigworld.member.data.dao.TenantUserDao;
import com.nbsaas.nbmall.product.data.dao.ProductDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class ProductSpecResource implements ProductSpecApi {

    @Autowired
    private ProductSpecDao dataDao;

    @Autowired
    private TenantUserDao creatorDao;
    @Autowired
    private ProductDao productDao;


    @Override
    public ProductSpecResponse create(ProductSpecDataRequest request) {
        ProductSpecResponse result = new ProductSpecResponse();

        ProductSpec bean = new ProductSpec();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new ProductSpecResponseConvert().conver(bean);
        return result;
    }

    @Override
    public ProductSpecResponse update(ProductSpecDataRequest request) {
        ProductSpecResponse result = new ProductSpecResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        ProductSpec bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new ProductSpecResponseConvert().conver(bean);
        return result;
    }

    private void handleData(ProductSpecDataRequest request, ProductSpec bean) {
       TenantBeanUtils.copyProperties(request,bean);
           if(request.getProduct()!=null){
              bean.setProduct(productDao.findById(request.getProduct()));
           }
           if(request.getCreator()!=null){
              bean.setCreator(creatorDao.findById(request.getCreator()));
           }

    }

    @Override
    public ProductSpecResponse delete(ProductSpecDataRequest request) {
        ProductSpecResponse result = new ProductSpecResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public ProductSpecResponse view(ProductSpecDataRequest request) {
        ProductSpecResponse result=new ProductSpecResponse();
        ProductSpec bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new ProductSpecResponseConvert().conver(bean);
        return result;
    }
    @Override
    public ProductSpecList list(ProductSpecSearchRequest request) {
        ProductSpecList result = new ProductSpecList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<ProductSpec> organizations = dataDao.list(0, request.getSize(), filters, orders);

        ProductSpecSimpleConvert convert=new ProductSpecSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public ProductSpecPage search(ProductSpecSearchRequest request) {
        ProductSpecPage result=new ProductSpecPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<ProductSpec> page=dataDao.page(pageable);

        ProductSpecSimpleConvert convert=new ProductSpecSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
