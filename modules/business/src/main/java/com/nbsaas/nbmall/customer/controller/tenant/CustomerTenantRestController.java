package com.nbsaas.nbmall.customer.controller.tenant;

import com.nbsaas.nbmall.customer.api.apis.CustomerApi;
import com.nbsaas.nbmall.customer.api.domain.list.CustomerList;
import com.nbsaas.nbmall.customer.api.domain.page.CustomerPage;
import com.nbsaas.nbmall.customer.api.domain.request.*;
import com.nbsaas.nbmall.customer.api.domain.response.CustomerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/customer")
@RestController
public class CustomerTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("customer")
    @RequestMapping("create")
    public CustomerResponse create(CustomerDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

	@RequiresPermissions("customer")
    @RequestMapping("delete")
    public CustomerResponse delete(CustomerDataRequest request) {
        initTenant(request);
        CustomerResponse result = new CustomerResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("customer")
    @RequestMapping("update")
    public CustomerResponse update(CustomerDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("customer")
    @RequestMapping("view")
    public CustomerResponse view(CustomerDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("customer")
    @RequestMapping("list")
    public CustomerList list(CustomerSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("customer")
    @RequestMapping("search")
    public CustomerPage search(CustomerSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private CustomerApi api;

}
