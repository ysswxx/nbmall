package com.nbsaas.nbmall.common.api.domain.page;


import com.nbsaas.nbmall.common.api.domain.simple.TenantCashSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年12月27日16:40:36.
*/

@Data
public class TenantCashPage  extends ResponsePage<TenantCashSimple> {

}