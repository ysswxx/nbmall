package com.nbsaas.nbmall.product.api.domain.simple;


import java.io.Serializable;

import lombok.Data;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Created by BigWorld on 2021年12月10日17:38:01.
 */
@Data
public class ProductSpecSimple implements Serializable {

    private Long id;

    private Long product;
    private Long creator;
    private String name;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date addDate;
    private String productName;

    private List<ProductSpecValueSimple> values;

}
