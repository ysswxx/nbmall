package com.nbsaas.nbmall.customer.controller.rest;

import com.nbsaas.nbmall.customer.api.apis.CustomerAddressApi;
import com.nbsaas.nbmall.customer.api.domain.list.CustomerAddressList;
import com.nbsaas.nbmall.customer.api.domain.page.CustomerAddressPage;
import com.nbsaas.nbmall.customer.api.domain.request.*;
import com.nbsaas.nbmall.customer.api.domain.response.CustomerAddressResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/customeraddress")
@RestController
public class CustomerAddressRestController extends BaseRestController {


    @RequestMapping("create")
    public CustomerAddressResponse create(CustomerAddressDataRequest request) {
        initTenant(request);
        request.setCustomer(request.getUser());
        return api.create(request);
    }

    @RequestMapping("delete")
    public CustomerAddressResponse delete(CustomerAddressDataRequest request) {
        initTenant(request);
        CustomerAddressResponse result = new CustomerAddressResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public CustomerAddressResponse update(CustomerAddressDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public CustomerAddressResponse view(CustomerAddressDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public CustomerAddressList list(CustomerAddressSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public CustomerAddressPage search(CustomerAddressSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @RequestMapping("my")
    public CustomerAddressPage my(CustomerAddressSearchRequest request) {
        initTenant(request);
        request.setCustomer(request.getUser());
        return api.search(request);
    }

    @Autowired
    private CustomerAddressApi api;

}
