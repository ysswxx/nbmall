package com.nbsaas.nbmall.common.api.domain.list;


import com.nbsaas.nbmall.common.api.domain.simple.SiteTenantSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年12月23日11:54:02.
*/

@Data
public class SiteTenantList  extends ResponseList<SiteTenantSimple> {

}