package com.nbsaas.nbmall.shop.rest.resource;

import com.nbsaas.nbmall.shop.api.apis.ShopCatalogApi;
import com.nbsaas.nbmall.shop.api.domain.list.ShopCatalogList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopCatalogPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopCatalogResponse;
import com.nbsaas.nbmall.shop.data.dao.ShopCatalogDao;
import com.nbsaas.nbmall.shop.data.entity.ShopCatalog;
import com.nbsaas.nbmall.shop.rest.convert.ShopCatalogResponseConvert;
import com.nbsaas.nbmall.shop.rest.convert.ShopCatalogSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.nbsaas.nbmall.shop.data.dao.ShopCatalogDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class ShopCatalogResource implements ShopCatalogApi {

    @Autowired
    private ShopCatalogDao dataDao;

    @Autowired
    private ShopCatalogDao parentDao;


    @Override
    public ShopCatalogResponse create(ShopCatalogDataRequest request) {
        ShopCatalogResponse result = new ShopCatalogResponse();

        ShopCatalog bean = new ShopCatalog();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new ShopCatalogResponseConvert().conver(bean);
        return result;
    }

    @Override
    public ShopCatalogResponse update(ShopCatalogDataRequest request) {
        ShopCatalogResponse result = new ShopCatalogResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        ShopCatalog bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new ShopCatalogResponseConvert().conver(bean);
        return result;
    }

    private void handleData(ShopCatalogDataRequest request, ShopCatalog bean) {
       TenantBeanUtils.copyProperties(request,bean);
           if(request.getParent()!=null){
              bean.setParent(parentDao.findById(request.getParent()));
           }

    }

    @Override
    public ShopCatalogResponse delete(ShopCatalogDataRequest request) {
        ShopCatalogResponse result = new ShopCatalogResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public ShopCatalogResponse view(ShopCatalogDataRequest request) {
        ShopCatalogResponse result=new ShopCatalogResponse();
        ShopCatalog bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new ShopCatalogResponseConvert().conver(bean);
        return result;
    }
    @Override
    public ShopCatalogList list(ShopCatalogSearchRequest request) {
        ShopCatalogList result = new ShopCatalogList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<ShopCatalog> organizations = dataDao.list(0, request.getSize(), filters, orders);

        ShopCatalogSimpleConvert convert=new ShopCatalogSimpleConvert();
        convert.setFetch(request.getFetch());
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public ShopCatalogPage search(ShopCatalogSearchRequest request) {
        ShopCatalogPage result=new ShopCatalogPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<ShopCatalog> page=dataDao.page(pageable);

        ShopCatalogSimpleConvert convert=new ShopCatalogSimpleConvert();
        convert.setFetch(request.getFetch());
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
