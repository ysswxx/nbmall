package com.nbsaas.nbmall.common.api.domain.list;


import com.nbsaas.nbmall.common.api.domain.simple.TenantCashSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年12月27日16:40:36.
*/

@Data
public class TenantCashList  extends ResponseList<TenantCashSimple> {

}