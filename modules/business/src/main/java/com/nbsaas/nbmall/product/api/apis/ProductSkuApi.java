package com.nbsaas.nbmall.product.api.apis;


import com.nbsaas.nbmall.product.api.domain.list.ProductSkuList;
import com.nbsaas.nbmall.product.api.domain.page.ProductSkuPage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductSkuResponse;

public interface ProductSkuApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    ProductSkuResponse create(ProductSkuDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    ProductSkuResponse update(ProductSkuDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    ProductSkuResponse delete(ProductSkuDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     ProductSkuResponse view(ProductSkuDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    ProductSkuList list(ProductSkuSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    ProductSkuPage search(ProductSkuSearchRequest request);

}