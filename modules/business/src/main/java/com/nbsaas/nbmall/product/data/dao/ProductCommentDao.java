package com.nbsaas.nbmall.product.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.product.data.entity.ProductComment;

/**
* Created by imake on 2021年12月10日17:38:01.
*/
public interface ProductCommentDao extends BaseDao<ProductComment,Long>{

	 ProductComment findById(Long id);

	 ProductComment save(ProductComment bean);

	 ProductComment updateByUpdater(Updater<ProductComment> updater);

	 ProductComment deleteById(Long id);

	 ProductComment findById(Long tenant, Long id);

     ProductComment deleteById(Long tenant, Long id);
}