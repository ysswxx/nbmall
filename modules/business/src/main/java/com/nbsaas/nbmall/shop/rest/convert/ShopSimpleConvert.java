package com.nbsaas.nbmall.shop.rest.convert;

import com.haoxuer.discover.data.enums.StoreState;
import com.nbsaas.nbmall.shop.api.domain.simple.ShopSimple;
import com.nbsaas.nbmall.shop.data.entity.Shop;
import com.haoxuer.discover.data.rest.core.Conver;
import com.nbsaas.nbmall.shop.data.entity.ShopHour;
import com.nbsaas.nbmall.shop.data.enums.AuditState;
import com.nbsaas.nbmall.shop.data.enums.ShopState;
import com.nbsaas.nbmall.utils.TimeUtil;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ShopSimpleConvert implements Conver<ShopSimple, Shop> {


    @Override
    public ShopSimple conver(Shop source) {
        ShopSimple result = new ShopSimple();
        if (source.getShopState() == null) {
            source.setShopState(ShopState.disable);
        }
        if (source.getAuditState() == null) {
            source.setAuditState(AuditState.wait);
        }
        result.setId(source.getId());
        if (source.getArea() != null) {
            result.setArea(source.getArea().getId());
        }
        if (source.getExt() != null) {
            result.setExt(source.getExt().getId());
        }
        if (source.getBoss() != null) {
            result.setBoss(source.getBoss().getId());
        }
        result.setAddress(source.getAddress());
        if (source.getCity() != null) {
            result.setCity(source.getCity().getId());
        }
        if (source.getCreator() != null) {
            result.setCreator(source.getCreator().getId());
        }
        result.setLng(source.getLng());
        result.setStaffNum(source.getStaffNum());
        result.setShopState(source.getShopState());
        if (source.getBoss() != null) {
            result.setBossName(source.getBoss().getName());
        }
        result.setProductNum(source.getProductNum());
        if (source.getProvince() != null) {
            result.setProvince(source.getProvince().getId());
        }
        result.setAddDate(source.getAddDate());
        if (source.getTradeAccount() != null) {
            result.setAmount(source.getTradeAccount().getAmount());
        }
        result.setAuditState(source.getAuditState());
        result.setSortNum(source.getSortNum());
        result.setLogo(source.getLogo());
        result.setName(source.getName());
        if (source.getShopCatalog() != null) {
            result.setShopCatalog(source.getShopCatalog().getId());
        }
        result.setTel(source.getTel());
        if (source.getShopCatalog() != null) {
            result.setShopCatalogName(source.getShopCatalog().getName());
        }
        result.setLat(source.getLat());

        result.setAuditStateName(source.getAuditState() + "");
        result.setShopStateName(source.getShopState() + "");
        if (source.getChannel() != null) {
            result.setUrl(source.getChannel().getUrl());
        }
        if (source.getScore() == null) {
            source.setScore(5f);
        }
        result.setScore(source.getScore());
        result.setLabels(new ArrayList<>());
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -5);
        if (source.getAddDate() == null) {
            source.setAddDate(new Date());
        }

        if (source.getAddDate().after(calendar.getTime())) {
            result.getLabels().add("新店开业");
        }
        result.setFreight(source.getFreight());
        if (result.getFreight() == null) {
            result.setFreight(new BigDecimal(0));
        }
        if (source.getDeliveryTime() == null) {
            source.setDeliveryTime(30);
        }
        result.setDeliveryTime(source.getDeliveryTime());
        //月售10 人均18
        String saleTxt = "月售10 人均18";

        if (source.getSaleNum() == null) {
            source.setSaleNum(0L);
        }
        if (source.getBaseSaleNum()==null){
            source.setBaseSaleNum(0L);
        }
        result.setBaseSaleNum(source.getBaseSaleNum());
        result.setSaleNum(source.getSaleNum());
        Long saleNum=source.getSaleNum()+source.getBaseSaleNum();
        saleTxt = "月售" + saleNum + "";
        if (source.getPerMoney() == null) {
            source.setPerMoney(new BigDecimal(15));
        }
        saleTxt = saleTxt + " 人均" + source.getPerMoney();
        //起送￥0元|配送费￥1.0元
        String basicTxt = "";
        //37分钟 3.0km

        if (source.getTransportTime() != null) {

        }
        String distanceTxt = "30分钟 3.0km";

        if (source.getStartPrice() == null) {
            source.setStartPrice(new BigDecimal(0));
        }
        basicTxt = basicTxt + "起送￥" + source.getStartPrice() + "元";
        if (result.getFreight().compareTo(new BigDecimal(0)) <= 0) {
            basicTxt = basicTxt + "|免费配送";
        } else {
            basicTxt = basicTxt + "|配送费￥" + result.getFreight();
        }
        result.setBasicTxt(basicTxt);
        result.setSaleTxt(saleTxt);
        result.setDistanceTxt(distanceTxt);

        if (StringUtils.hasText(source.getSummary())) {
            result.setSummary("“" + source.getSummary() + "“");
        } else {
            result.setSummary("");
        }
        result.setBeginTime(source.getBeginTime());
        result.setEndTime(source.getEndTime());



        List<ShopHour> hourList = source.getHours();
        if (hourList != null&&hourList.size()>0) {
            result.setShopStateName(ShopState.disable+"");
            result.setShopState(ShopState.disable);
            for (ShopHour shopHour : hourList) {
                boolean state = TimeUtil.check(shopHour.getBeginTime(), shopHour.getEndTime());
                if (state) {
                    result.setShopState(ShopState.normal);
                    result.setShopStateName("" + ShopState.normal);
                }
            }
        }else{
            if (StringUtils.hasText(result.getBeginTime()) && StringUtils.hasText(result.getEndTime())) {
                boolean state = TimeUtil.check(result.getBeginTime(), result.getEndTime());
                if (state) {
                    result.setShopState(ShopState.normal);
                    result.setShopStateName("" + ShopState.normal);
                }
            }
        }
        if (source.getShopState()==ShopState.disable){
            result.setShopStateName(ShopState.disable+"");
            result.setShopState(ShopState.disable);
        }


        if (source.getTradeAccount() != null) {
            result.setTradeAccountAmount(source.getTradeAccount().getAmount());
        } else {
            result.setTradeAccountAmount(new BigDecimal(0));
        }
        if (source.getStoreState() == null) {
            source.setStoreState(StoreState.normal);
        }
        result.setStoreState(source.getStoreState());

        if (StringUtils.hasText(source.getThumbnail())) {
            result.setLogo(source.getThumbnail());
        }

        return result;
    }
}
