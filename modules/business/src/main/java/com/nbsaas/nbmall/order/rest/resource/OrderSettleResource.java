package com.nbsaas.nbmall.order.rest.resource;

import com.nbsaas.nbmall.order.api.apis.OrderSettleApi;
import com.nbsaas.nbmall.order.api.domain.list.OrderSettleList;
import com.nbsaas.nbmall.order.api.domain.page.OrderSettlePage;
import com.nbsaas.nbmall.order.api.domain.request.*;
import com.nbsaas.nbmall.order.api.domain.response.OrderSettleResponse;
import com.nbsaas.nbmall.order.data.dao.OrderSettleDao;
import com.nbsaas.nbmall.order.data.entity.OrderSettle;
import com.nbsaas.nbmall.order.rest.convert.OrderSettleResponseConvert;
import com.nbsaas.nbmall.order.rest.convert.OrderSettleSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.nbsaas.nbmall.shop.data.dao.ShopDao;
import com.nbsaas.nbmall.order.data.dao.OrderFormDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class OrderSettleResource implements OrderSettleApi {

    @Autowired
    private OrderSettleDao dataDao;

    @Autowired
    private ShopDao shopDao;
    @Autowired
    private OrderFormDao orderFormDao;


    @Override
    public OrderSettleResponse create(OrderSettleDataRequest request) {
        OrderSettleResponse result = new OrderSettleResponse();

        OrderSettle bean = new OrderSettle();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new OrderSettleResponseConvert().conver(bean);
        return result;
    }

    @Override
    public OrderSettleResponse update(OrderSettleDataRequest request) {
        OrderSettleResponse result = new OrderSettleResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        OrderSettle bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new OrderSettleResponseConvert().conver(bean);
        return result;
    }

    private void handleData(OrderSettleDataRequest request, OrderSettle bean) {
       TenantBeanUtils.copyProperties(request,bean);
           if(request.getShop()!=null){
              bean.setShop(shopDao.findById(request.getShop()));
           }
           if(request.getOrderForm()!=null){
              bean.setOrderForm(orderFormDao.findById(request.getOrderForm()));
           }

    }

    @Override
    public OrderSettleResponse delete(OrderSettleDataRequest request) {
        OrderSettleResponse result = new OrderSettleResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public OrderSettleResponse view(OrderSettleDataRequest request) {
        OrderSettleResponse result=new OrderSettleResponse();
        OrderSettle bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new OrderSettleResponseConvert().conver(bean);
        return result;
    }
    @Override
    public OrderSettleList list(OrderSettleSearchRequest request) {
        OrderSettleList result = new OrderSettleList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<OrderSettle> organizations = dataDao.list(0, request.getSize(), filters, orders);

        OrderSettleSimpleConvert convert=new OrderSettleSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public OrderSettlePage search(OrderSettleSearchRequest request) {
        OrderSettlePage result=new OrderSettlePage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<OrderSettle> page=dataDao.page(pageable);

        OrderSettleSimpleConvert convert=new OrderSettleSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
