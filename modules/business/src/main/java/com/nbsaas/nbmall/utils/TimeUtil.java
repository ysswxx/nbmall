package com.nbsaas.nbmall.utils;

public class TimeUtil {

    public static boolean check(String begin, String end) {
        boolean result = false;
        Time beginTime = Time.valueOf(begin);
        Time endTime = Time.valueOf(end);
        Time now = Time.now();
        if (beginTime.before(now) && endTime.after(now)) {
            result = true;
        }
        return result;
    }

    public static void main(String[] args) {

        System.out.println(check("08:00","14:35"));
    }
}
