package com.nbsaas.nbmall.statistics.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月23日11:45:35.
*/

@Data
public class VisitPageDataRequest extends TenantRequest {

    private Long id;

     private String path;
     private Long num;
     private Long user;
     private String key;

}