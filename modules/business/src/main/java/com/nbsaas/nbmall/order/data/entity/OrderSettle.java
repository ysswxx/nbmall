package com.nbsaas.nbmall.order.data.entity;


import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.FieldConvert;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.FormField;
import com.nbsaas.codemake.annotation.SearchItem;
import com.nbsaas.nbmall.order.data.enums.SettleType;
import com.nbsaas.nbmall.shop.data.entity.Shop;
import com.nbsaas.nbmall.shop.data.enums.SubjectType;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

@Data
@FormAnnotation(title = "结算单管理", model = "结算单", menu = "1,107,112")
@Entity
@Table(name = "bs_tenant_order_settle")
public class OrderSettle extends TenantEntity {

    @SearchItem(label = "订单", name = "dishOrder", classType = "Long", key = "dishOrder.id", operator = "eq")
    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private OrderForm orderForm;

    @SearchItem(label = "商家", name = "shop", classType = "Long", key = "shop.id", operator = "eq")
    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private Shop shop;

    @FormField(title = "付款金额", sortNum = "1", grid = false, col = 12)
    private BigDecimal payAmount;

    @FormField(title = "参与结算金额", sortNum = "1", grid = false, col = 12)
    private BigDecimal money;

    @FormField(title = "结算类型", sortNum = "1", grid = false, col = 12)
    private SettleType settleType;

    @FormField(title = "平台扣点", sortNum = "1", grid = false, col = 12)
    private BigDecimal platformRate;

    @FormField(title = "平台扣点", sortNum = "1", grid = false, col = 12)
    private BigDecimal platformFee;

    @FormField(title = "运费", sortNum = "1", grid = false, col = 12)
    private BigDecimal freight;


    @FormField(title = "入账金额", sortNum = "1", grid = false, col = 12)
    private BigDecimal balance;

    @FormField(title = "平台基础", sortNum = "1", grid = false, col = 12)
    private BigDecimal platformBasic;

    @FormField(title = "平台入账", sortNum = "1", grid = false, col = 12)
    private BigDecimal platformBalance;

    /**
     * 运费承担主体
     */
    private SubjectType freightSubject;

    /**
     * 餐盒费承担主体
     */
    private SubjectType mealSubject;

}
