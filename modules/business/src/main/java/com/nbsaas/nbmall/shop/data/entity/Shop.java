package com.nbsaas.nbmall.shop.data.entity;

import com.haoxuer.bigworld.member.data.entity.TenantUser;
import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.haoxuer.discover.area.data.entity.Area;
import com.haoxuer.discover.data.enums.StoreState;
import com.haoxuer.discover.trade.data.entity.TradeAccount;
import com.nbsaas.codemake.annotation.*;
import com.nbsaas.nbmall.common.data.entity.Channel;
import com.nbsaas.nbmall.shop.data.enums.AuditState;
import com.nbsaas.nbmall.shop.data.enums.ReceivingMode;
import com.nbsaas.nbmall.shop.data.enums.ShopState;
import com.nbsaas.nbmall.shop.data.enums.SubjectType;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


@ComposeView
@Data
@FormAnnotation(title = "商家管理", model = "商家", menu = "1,101,102")
@Entity
@Table(name = "bs_tenant_shop")
public class Shop extends TenantEntity {
    public static Shop fromId(Long id) {
        Shop result = new Shop();
        result.setId(id);
        return result;
    }


    @FormField(title = "门头", grid = false, type = InputType.image, col = 24)
    private String logo;

    private String thumbnail;


    @SearchItem(label = "商家名称", name = "name")
    @FormField(title = "商家名称", grid = true, required = true)
    @Column(length = 20)
    private String name;

    @SearchItem(label = "分类", name = "shopCatalog", key = "shopCatalog.id", classType = "Integer", operator = "eq")
    @FieldName
    @FormField(title = "所属分类", grid = true, type = InputType.select, option = "shopCatalog")
    @FieldConvert(classType = "Integer")
    @ManyToOne(fetch = FetchType.LAZY)
    private ShopCatalog shopCatalog;

    @FormField(title = "排序号", grid = true, sort = true)
    private Integer sortNum;

    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private TenantUser creator;


    @FormField(title = "老板")
    @FieldName
    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private TenantUser boss;

    @SearchItem(label = "地址", name = "address")
    @FormField(title = "地址")
    @Column(length = 200)
    private String address;

    @FormField(title = "商品数量", grid = true, ignore = true)
    private Integer productNum;

    @FormField(title = "员工人数", grid = true, ignore = true)
    private Integer staffNum;

    @FormField(title = "联系人")
    private String contact;

    @FormField(title = "客服电话")
    private String tel;

    private Double lat;

    private Double lng;

    @FieldConvert(classType = "Integer")
    @ManyToOne(fetch = FetchType.LAZY)
    private Area province;

    @FieldConvert(classType = "Integer")
    @ManyToOne(fetch = FetchType.LAZY)
    private Area city;

    @FieldConvert(classType = "Integer")
    @ManyToOne(fetch = FetchType.LAZY)
    private Area area;

    @ManyToOne(fetch = FetchType.LAZY)
    @FormField(title = "账户余额", grid = true, ignore = true)
    @FieldName(classType = "BigDecimal", name = "amount")
    private TradeAccount tradeAccount;

    @SearchItem(label = "状态", name = "shopState", classType = "ShopState", operator = "eq", show = false)
    @FormField(title = "状态", grid = true, ignore = true)
    private ShopState shopState;

    private AuditState auditState;

    @FieldName(name = "url", parent = "url")
    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private Channel channel;

    private Float score;

    @FieldConvert
    @OneToOne()
    private ShopExt ext;

    private Boolean productModel;
    private Boolean foodModel;
    private Boolean serviceModel;
    private Boolean cashModel;

    private Integer balanceMethod;
    private Integer deliveryMethod;
    private Boolean takeaway;
    private Boolean transport;
    private Boolean autoOrder;
    private Integer dishDeliveryMethod;

    private Double takeawayRate;
    private Double selfRate;
    private Double hallFoodRate;
    private Integer transportTime;
    private Integer eatOutTime;
    private Integer dishSaleNum;
    private Boolean hallFood;
    private Double cashRate;

    /**
     * 运费
     */
    private BigDecimal freight;
    /**
     * 最大配送服务范围
     */
    private Integer distance;

    /**
     * 起送价
     */
    private BigDecimal startPrice;

    /**
     * 折扣数量
     */
    private Integer discountNum;

    /**
     * 运费承担主体
     */
    private SubjectType freightSubject;

    /**
     * 餐盒费承担主体
     */
    private SubjectType mealSubject;

    /**
     * 接单方式
     */
    private ReceivingMode receivingMode;

    private String note;

    private Integer receivingTime;

    /**
     * 配送时间
     */
    private Integer deliveryTime;

    /**
     * 店铺说明
     */
    private String summary;

    @Column(length = 10)
    private String beginTime;

    @Column(length = 10)
    private String endTime;

    private StoreState storeState;

    /**
     * 销售数量
     */
    private Long saleNum;


    private Long baseSaleNum;

    /**
     * 退款数量
     */
    private Long refundNum;

    /**
     * 排序分数
     */
    private Long sortScore;

    /**
     * 人均
     */
    private BigDecimal perMoney;

    /**
     * 菜品数量
     */
    private Long dishAmount;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "shop")
    private List<ShopHour> hours;



}
