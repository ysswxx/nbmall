package com.nbsaas.nbmall.video.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2022年03月16日21:26:00.
*/

@Data
public class VideoDataRequest extends TenantRequest {

    private Long id;


}