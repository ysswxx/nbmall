package com.nbsaas.nbmall.product.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import java.util.Date;
import java.math.BigDecimal;

/**
*
* Created by imake on 2021年12月10日17:38:01.
*/

@Data
public class ProductSkuDataRequest extends TenantRequest {

    private Long id;

     private String code;
     private BigDecimal marketPrice;
     private Long product;
     private BigDecimal salePrice;
     private String logo;
     private String name;
     private Integer inventory;
     private Integer warning;
     private BigDecimal costPrice;

}