package com.nbsaas.nbmall.cart.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import java.util.Date;
import java.math.BigDecimal;

/**
*
* Created by imake on 2021年12月30日22:38:35.
*/

@Data
public class CartDataRequest extends TenantRequest {

    private Long id;

     private Long tenantUser;
     private Long shop;
     private Long product;
     private BigDecimal total;
     private BigDecimal price;
     private Boolean checked;
     private Long sku;
     private Integer amount;

}