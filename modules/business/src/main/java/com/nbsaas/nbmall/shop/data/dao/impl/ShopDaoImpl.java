package com.nbsaas.nbmall.shop.data.dao.impl;

import com.haoxuer.discover.data.enums.StoreState;
import com.nbsaas.nbmall.shop.data.enums.AuditState;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.shop.data.dao.ShopDao;
import com.nbsaas.nbmall.shop.data.entity.Shop;

/**
* Created by imake on 2021年12月10日17:38:00.
*/
@Repository

public class ShopDaoImpl extends CriteriaDaoImpl<Shop, Long> implements ShopDao {

	@Override
	public Shop findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public Shop save(Shop bean) {
		bean.setStoreState(StoreState.normal);
		bean.setAuditState(AuditState.wait);
        getSession().save(bean);
		return bean;
	}

    @Override
	public Shop deleteById(Long id) {
		Shop entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<Shop> getEntityClass() {
		return Shop.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public Shop findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public Shop deleteById(Long tenant,Long id) {
		Shop entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}