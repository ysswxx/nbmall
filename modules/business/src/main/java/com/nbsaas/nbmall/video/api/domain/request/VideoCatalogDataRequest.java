package com.nbsaas.nbmall.video.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2022年03月16日21:25:37.
*/

@Data
public class VideoCatalogDataRequest extends TenantRequest {

    private Integer id;

     private Integer parent;
     private String code;
     private Integer levelInfo;
     private Integer sortNum;
     private String ids;
     private Integer lft;
     private Date lastDate;
     private String name;
     private Date addDate;
     private Integer rgt;

}