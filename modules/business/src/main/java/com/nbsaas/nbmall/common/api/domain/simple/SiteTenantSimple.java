package com.nbsaas.nbmall.common.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.haoxuer.discover.data.enums.StoreState;

/**
*
* Created by BigWorld on 2021年12月23日11:54:02.
*/
@Data
public class SiteTenantSimple implements Serializable {

    private Long id;

     private Long tradeAccount;
     private String note;
     private String address;
     private Double lng;
     private Long creator;
     private StoreState storeState;
     private String shopStyle;
     private String creatorName;
     private String demo;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private String domain;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date beginDate;
     private String phone;
     private String logo;
     private String chatSign;
     private String name;
     private String theme;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date expireDate;
     private String key;
     private Double lat;

     private String storeStateName;

}
