package com.nbsaas.nbmall.promote.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import com.nbsaas.nbmall.promote.api.domain.simple.FullReductionItemSimple;
import lombok.Data;
import java.util.Date;
import java.util.List;

/**
*
* Created by imake on 2021年12月25日12:45:48.
*/

@Data
public class FullReductionItemDataRequest extends TenantRequest {

    private Long id;

     private Integer reduceMoney;
     private Integer fullMoney;
     private Long reduction;

}