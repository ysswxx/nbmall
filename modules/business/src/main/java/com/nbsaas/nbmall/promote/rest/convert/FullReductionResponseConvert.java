package com.nbsaas.nbmall.promote.rest.convert;

import com.nbsaas.nbmall.promote.api.domain.response.FullReductionResponse;
import com.nbsaas.nbmall.promote.api.domain.simple.FullReductionItemSimple;
import com.nbsaas.nbmall.promote.data.entity.FullReduction;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.nbsaas.nbmall.promote.data.entity.FullReductionItem;

import java.util.ArrayList;
import java.util.List;

public class FullReductionResponseConvert implements Conver<FullReductionResponse, FullReduction> {
    @Override
    public FullReductionResponse conver(FullReduction source) {
        FullReductionResponse result = new FullReductionResponse();
        TenantBeanUtils.copyProperties(source, result);

        if (source.getShop() != null) {
            result.setShop(source.getShop().getId());
        }
        if (source.getShop() != null) {
            result.setShopName(source.getShop().getName());
        }
        if (source.getItems() != null) {
            List<FullReductionItemSimple> items = new ArrayList<>();
            for (FullReductionItem item : source.getItems()) {
                FullReductionItemSimple simpleItem = new FullReductionItemSimple();
                simpleItem.setId(item.getId());
                simpleItem.setFullMoney(item.getFullMoney());
                simpleItem.setReduceMoney(item.getReduceMoney());
                items.add(simpleItem);
            }
            result.setItems(items);
        }
        result.setFullReductionWayName(source.getFullReductionWay() + "");
        return result;
    }
}
