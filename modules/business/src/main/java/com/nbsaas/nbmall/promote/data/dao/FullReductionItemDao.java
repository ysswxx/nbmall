package com.nbsaas.nbmall.promote.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.promote.data.entity.FullReductionItem;

/**
* Created by imake on 2021年12月25日12:45:48.
*/
public interface FullReductionItemDao extends BaseDao<FullReductionItem,Long>{

	 FullReductionItem findById(Long id);

	 FullReductionItem save(FullReductionItem bean);

	 FullReductionItem updateByUpdater(Updater<FullReductionItem> updater);

	 FullReductionItem deleteById(Long id);

	 FullReductionItem findById(Long tenant, Long id);

     FullReductionItem deleteById(Long tenant, Long id);
}