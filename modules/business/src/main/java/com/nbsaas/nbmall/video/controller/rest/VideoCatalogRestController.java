package com.nbsaas.nbmall.video.controller.rest;

import com.nbsaas.nbmall.video.api.apis.VideoCatalogApi;
import com.nbsaas.nbmall.video.api.domain.list.VideoCatalogList;
import com.nbsaas.nbmall.video.api.domain.page.VideoCatalogPage;
import com.nbsaas.nbmall.video.api.domain.request.*;
import com.nbsaas.nbmall.video.api.domain.response.VideoCatalogResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/videocatalog")
@RestController
public class VideoCatalogRestController extends BaseRestController {


    @RequestMapping("create")
    public VideoCatalogResponse create(VideoCatalogDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

    @RequestMapping("delete")
    public VideoCatalogResponse delete(VideoCatalogDataRequest request) {
        initTenant(request);
        VideoCatalogResponse result = new VideoCatalogResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public VideoCatalogResponse update(VideoCatalogDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public VideoCatalogResponse view(VideoCatalogDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public VideoCatalogList list(VideoCatalogSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public VideoCatalogPage search(VideoCatalogSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private VideoCatalogApi api;

}
