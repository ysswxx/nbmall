package com.nbsaas.nbmall.product.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.product.data.dao.ProductSpecValueDao;
import com.nbsaas.nbmall.product.data.entity.ProductSpecValue;

/**
* Created by imake on 2021年12月10日17:38:01.
*/
@Repository

public class ProductSpecValueDaoImpl extends CriteriaDaoImpl<ProductSpecValue, Long> implements ProductSpecValueDao {

	@Override
	public ProductSpecValue findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public ProductSpecValue save(ProductSpecValue bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public ProductSpecValue deleteById(Long id) {
		ProductSpecValue entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<ProductSpecValue> getEntityClass() {
		return ProductSpecValue.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public ProductSpecValue findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public ProductSpecValue deleteById(Long tenant,Long id) {
		ProductSpecValue entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}