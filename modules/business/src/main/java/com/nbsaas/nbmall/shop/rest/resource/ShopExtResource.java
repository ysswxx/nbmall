package com.nbsaas.nbmall.shop.rest.resource;

import com.nbsaas.nbmall.shop.api.apis.ShopExtApi;
import com.nbsaas.nbmall.shop.api.domain.list.ShopExtList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopExtPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopExtResponse;
import com.nbsaas.nbmall.shop.data.dao.ShopExtDao;
import com.nbsaas.nbmall.shop.data.entity.ShopExt;
import com.nbsaas.nbmall.shop.rest.convert.ShopExtResponseConvert;
import com.nbsaas.nbmall.shop.rest.convert.ShopExtSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.nbsaas.nbmall.shop.data.dao.ShopDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class ShopExtResource implements ShopExtApi {

    @Autowired
    private ShopExtDao dataDao;

    @Autowired
    private ShopDao shopDao;


    @Override
    public ShopExtResponse create(ShopExtDataRequest request) {
        ShopExtResponse result = new ShopExtResponse();

        ShopExt bean = new ShopExt();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new ShopExtResponseConvert().conver(bean);
        return result;
    }

    @Override
    public ShopExtResponse update(ShopExtDataRequest request) {
        ShopExtResponse result = new ShopExtResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        ShopExt bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new ShopExtResponseConvert().conver(bean);
        return result;
    }

    private void handleData(ShopExtDataRequest request, ShopExt bean) {
       TenantBeanUtils.copyProperties(request,bean);
           if(request.getShop()!=null){
              bean.setShop(shopDao.findById(request.getShop()));
           }

    }

    @Override
    public ShopExtResponse delete(ShopExtDataRequest request) {
        ShopExtResponse result = new ShopExtResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public ShopExtResponse view(ShopExtDataRequest request) {
        ShopExtResponse result=new ShopExtResponse();
        ShopExt bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new ShopExtResponseConvert().conver(bean);
        return result;
    }
    @Override
    public ShopExtList list(ShopExtSearchRequest request) {
        ShopExtList result = new ShopExtList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<ShopExt> organizations = dataDao.list(0, request.getSize(), filters, orders);

        ShopExtSimpleConvert convert=new ShopExtSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public ShopExtPage search(ShopExtSearchRequest request) {
        ShopExtPage result=new ShopExtPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<ShopExt> page=dataDao.page(pageable);

        ShopExtSimpleConvert convert=new ShopExtSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
