package com.nbsaas.nbmall.order.rest.convert;

import com.nbsaas.nbmall.order.api.domain.response.OrderFormResponse;
import com.nbsaas.nbmall.order.data.entity.OrderForm;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class OrderFormResponseConvert implements Conver<OrderFormResponse, OrderForm> {
    @Override
    public OrderFormResponse conver(OrderForm source) {
        OrderFormResponse result = new OrderFormResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getCreator()!=null){
           result.setCreator(source.getCreator().getId());
        }
        if(source.getUser()!=null){
           result.setUser(source.getUser().getId());
        }
        if(source.getShop()!=null){
           result.setShop(source.getShop().getId());
        }
         if(source.getShop()!=null){
            result.setShopName(source.getShop().getName());
         }

         result.setPayStateName(source.getPayState()+"");
         result.setOrderStateName(source.getOrderState()+"");
         result.setStoreStateName(source.getStoreState()+"");
        return result;
    }
}
