package com.nbsaas.nbmall.product.rest.convert;

import com.nbsaas.nbmall.product.api.domain.response.ProductSkuResponse;
import com.nbsaas.nbmall.product.data.entity.ProductSku;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class ProductSkuResponseConvert implements Conver<ProductSkuResponse, ProductSku> {
    @Override
    public ProductSkuResponse conver(ProductSku source) {
        ProductSkuResponse result = new ProductSkuResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getProduct()!=null){
           result.setProduct(source.getProduct().getId());
        }
         if(source.getProduct()!=null){
            result.setProductName(source.getProduct().getName());
         }

        return result;
    }
}
