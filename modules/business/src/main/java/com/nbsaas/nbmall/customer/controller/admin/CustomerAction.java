package com.nbsaas.nbmall.customer.controller.admin;


import com.haoxuer.bigworld.tenant.controller.admin.TenantBaseAction;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
*
* Created by imake on 2021年12月10日22:56:18.
*/


@Scope("prototype")
@Controller
public class CustomerAction extends TenantBaseAction {

	@RequiresPermissions("customer")
	@RequestMapping("/tenant/customer/view_list")
	public String list(ModelMap model) {
		return getView("customer/list");
	}

}