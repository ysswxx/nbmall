package com.nbsaas.nbmall.customer.api.domain.request;

import com.haoxuer.bigworld.member.api.domain.request.TenantPageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月10日22:56:19.
*/

@Data
public class CustomerSearchRequest extends TenantPageRequest {

    //姓名
     @Search(name = "name",operator = Filter.Operator.like)
     private String name;

    //电话
     @Search(name = "phone",operator = Filter.Operator.like)
     private String phone;

    //客户经理
     @Search(name = "manager.id",operator = Filter.Operator.eq)
     private Long manager;

    //省份
     @Search(name = "province.id",operator = Filter.Operator.eq)
     private Integer province;

    //城市
     @Search(name = "city.id",operator = Filter.Operator.eq)
     private Integer city;

    //区县
     @Search(name = "area.id",operator = Filter.Operator.eq)
     private Integer area;



}