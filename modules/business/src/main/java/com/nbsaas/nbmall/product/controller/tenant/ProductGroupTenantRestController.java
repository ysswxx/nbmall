package com.nbsaas.nbmall.product.controller.tenant;

import com.nbsaas.nbmall.product.api.apis.ProductGroupApi;
import com.nbsaas.nbmall.product.api.domain.list.ProductGroupList;
import com.nbsaas.nbmall.product.api.domain.page.ProductGroupPage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductGroupResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/productgroup")
@RestController
public class ProductGroupTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("productgroup")
    @RequestMapping("create")
    public ProductGroupResponse create(ProductGroupDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

	@RequiresPermissions("productgroup")
    @RequestMapping("delete")
    public ProductGroupResponse delete(ProductGroupDataRequest request) {
        initTenant(request);
        ProductGroupResponse result = new ProductGroupResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("productgroup")
    @RequestMapping("update")
    public ProductGroupResponse update(ProductGroupDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("productgroup")
    @RequestMapping("view")
    public ProductGroupResponse view(ProductGroupDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("productgroup")
    @RequestMapping("list")
    public ProductGroupList list(ProductGroupSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("productgroup")
    @RequestMapping("search")
    public ProductGroupPage search(ProductGroupSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private ProductGroupApi api;

}
