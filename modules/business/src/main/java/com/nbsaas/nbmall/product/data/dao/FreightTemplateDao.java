package com.nbsaas.nbmall.product.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.product.data.entity.FreightTemplate;

/**
* Created by imake on 2021年12月10日17:38:01.
*/
public interface FreightTemplateDao extends BaseDao<FreightTemplate,Long>{

	 FreightTemplate findById(Long id);

	 FreightTemplate save(FreightTemplate bean);

	 FreightTemplate updateByUpdater(Updater<FreightTemplate> updater);

	 FreightTemplate deleteById(Long id);

	 FreightTemplate findById(Long tenant, Long id);

     FreightTemplate deleteById(Long tenant, Long id);
}