package com.nbsaas.nbmall.product.rest.resource;

import com.nbsaas.nbmall.product.api.apis.ProductCommentApi;
import com.nbsaas.nbmall.product.api.domain.list.ProductCommentList;
import com.nbsaas.nbmall.product.api.domain.page.ProductCommentPage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductCommentResponse;
import com.nbsaas.nbmall.product.data.dao.ProductCommentDao;
import com.nbsaas.nbmall.product.data.entity.ProductComment;
import com.nbsaas.nbmall.product.rest.convert.ProductCommentResponseConvert;
import com.nbsaas.nbmall.product.rest.convert.ProductCommentSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.haoxuer.bigworld.member.data.dao.TenantUserDao;
import com.nbsaas.nbmall.product.data.dao.ProductDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class ProductCommentResource implements ProductCommentApi {

    @Autowired
    private ProductCommentDao dataDao;

    @Autowired
    private TenantUserDao tenantUserDao;
    @Autowired
    private ProductDao productDao;


    @Override
    public ProductCommentResponse create(ProductCommentDataRequest request) {
        ProductCommentResponse result = new ProductCommentResponse();

        ProductComment bean = new ProductComment();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new ProductCommentResponseConvert().conver(bean);
        return result;
    }

    @Override
    public ProductCommentResponse update(ProductCommentDataRequest request) {
        ProductCommentResponse result = new ProductCommentResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        ProductComment bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new ProductCommentResponseConvert().conver(bean);
        return result;
    }

    private void handleData(ProductCommentDataRequest request, ProductComment bean) {
       TenantBeanUtils.copyProperties(request,bean);
           if(request.getTenantUser()!=null){
              bean.setTenantUser(tenantUserDao.findById(request.getTenantUser()));
           }
           if(request.getProduct()!=null){
              bean.setProduct(productDao.findById(request.getProduct()));
           }

    }

    @Override
    public ProductCommentResponse delete(ProductCommentDataRequest request) {
        ProductCommentResponse result = new ProductCommentResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public ProductCommentResponse view(ProductCommentDataRequest request) {
        ProductCommentResponse result=new ProductCommentResponse();
        ProductComment bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new ProductCommentResponseConvert().conver(bean);
        return result;
    }
    @Override
    public ProductCommentList list(ProductCommentSearchRequest request) {
        ProductCommentList result = new ProductCommentList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<ProductComment> organizations = dataDao.list(0, request.getSize(), filters, orders);

        ProductCommentSimpleConvert convert=new ProductCommentSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public ProductCommentPage search(ProductCommentSearchRequest request) {
        ProductCommentPage result=new ProductCommentPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<ProductComment> page=dataDao.page(pageable);

        ProductCommentSimpleConvert convert=new ProductCommentSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
