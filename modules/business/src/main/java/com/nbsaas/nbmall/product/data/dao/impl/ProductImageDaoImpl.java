package com.nbsaas.nbmall.product.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.product.data.dao.ProductImageDao;
import com.nbsaas.nbmall.product.data.entity.ProductImage;

/**
* Created by imake on 2021年12月28日18:04:52.
*/
@Repository

public class ProductImageDaoImpl extends CriteriaDaoImpl<ProductImage, Long> implements ProductImageDao {

	@Override
	public ProductImage findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public ProductImage save(ProductImage bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public ProductImage deleteById(Long id) {
		ProductImage entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<ProductImage> getEntityClass() {
		return ProductImage.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public ProductImage findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public ProductImage deleteById(Long tenant,Long id) {
		ProductImage entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public void deleteByProduct(Long id) {
		getSession().createQuery("delete from ProductImage d where d.product.id="+id).executeUpdate();

	}
}