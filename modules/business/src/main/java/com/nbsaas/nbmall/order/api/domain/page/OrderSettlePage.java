package com.nbsaas.nbmall.order.api.domain.page;


import com.nbsaas.nbmall.order.api.domain.simple.OrderSettleSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年12月21日18:00:16.
*/

@Data
public class OrderSettlePage  extends ResponsePage<OrderSettleSimple> {

}