package com.nbsaas.nbmall.product.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.product.data.dao.ProductDao;
import com.nbsaas.nbmall.product.data.entity.Product;

/**
* Created by imake on 2021年12月28日18:00:18.
*/
@Repository

public class ProductDaoImpl extends CriteriaDaoImpl<Product, Long> implements ProductDao {

	@Override
	public Product findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public Product save(Product bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public Product deleteById(Long id) {
		Product entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<Product> getEntityClass() {
		return Product.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public Product findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public Product deleteById(Long tenant,Long id) {
		Product entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}