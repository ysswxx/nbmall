package com.nbsaas.nbmall.device.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.nbsaas.codemake.annotation.*;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;


@CreateByUser
@ComposeView
@Data
@FormAnnotation(title = "设备厂家管理", model = "设备厂家", menu = "1,176,177")
@Entity
@Table(name = "bs_common_device_type")
public class DeviceType extends AbstractEntity {

    @SearchItem(label = "厂家名称", name = "name")
    @FormField(title = "厂家名称", width = "180",grid = true, col = 22, required = true)
    private String name;

    @FormField(title = "公司网站", width = "260",grid = true, col = 22)
    private String website;

    @FormField(title = "文档网站",width = "260",grid = false, col = 22)
    private String doc;

    @FormField(title = "处理程序",width = "260",grid = false, col = 22)
    private String className;

    @FormField(title = "合作日期", width = "10000",grid = true, col = 22, type = InputType.date)
    private Date beginDate;

    @Column(length = 2000)
    @FormField(title = "公司介绍",grid = false, col = 22, type = InputType.textarea)
    private String note;

    @FieldConvert
    @FieldName
    @ManyToOne(fetch = FetchType.LAZY)
    private UserInfo creator;
}
