package com.nbsaas.nbmall.order.rest.convert;

import com.nbsaas.nbmall.order.api.domain.response.OrderSettleResponse;
import com.nbsaas.nbmall.order.data.entity.OrderSettle;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class OrderSettleResponseConvert implements Conver<OrderSettleResponse, OrderSettle> {
    @Override
    public OrderSettleResponse conver(OrderSettle source) {
        OrderSettleResponse result = new OrderSettleResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getShop()!=null){
           result.setShop(source.getShop().getId());
        }
        if(source.getOrderForm()!=null){
           result.setOrderForm(source.getOrderForm().getId());
        }

         result.setFreightSubjectName(source.getFreightSubject()+"");
         result.setSettleTypeName(source.getSettleType()+"");
         result.setMealSubjectName(source.getMealSubject()+"");
        return result;
    }
}
