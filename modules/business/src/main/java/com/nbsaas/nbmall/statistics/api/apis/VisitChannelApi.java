package com.nbsaas.nbmall.statistics.api.apis;


import com.nbsaas.nbmall.statistics.api.domain.list.VisitChannelList;
import com.nbsaas.nbmall.statistics.api.domain.page.VisitChannelPage;
import com.nbsaas.nbmall.statistics.api.domain.request.*;
import com.nbsaas.nbmall.statistics.api.domain.response.VisitChannelResponse;

public interface VisitChannelApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    VisitChannelResponse create(VisitChannelDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    VisitChannelResponse update(VisitChannelDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    VisitChannelResponse delete(VisitChannelDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     VisitChannelResponse view(VisitChannelDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    VisitChannelList list(VisitChannelSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    VisitChannelPage search(VisitChannelSearchRequest request);

}