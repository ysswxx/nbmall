package com.nbsaas.nbmall.order.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.math.BigDecimal;
import com.nbsaas.nbmall.shop.data.enums.SubjectType;
import com.nbsaas.nbmall.order.data.enums.SettleType;
import com.nbsaas.nbmall.shop.data.enums.SubjectType;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
/**
*
* Created by BigWorld on 2021年12月21日18:00:16.
*/

@Data
public class OrderSettleResponse extends ResponseObject {

    private Long id;

     private Long shop;
     private SubjectType mealSubject;
     private BigDecimal freight;
     private BigDecimal platformBasic;
     private BigDecimal money;
     private BigDecimal payAmount;
     private BigDecimal platformFee;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private BigDecimal platformBalance;
     private Long orderForm;
     private BigDecimal balance;
     private BigDecimal platformRate;
     private SubjectType freightSubject;
     private SettleType settleType;

     private String freightSubjectName;
     private String settleTypeName;
     private String mealSubjectName;
}