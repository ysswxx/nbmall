package com.nbsaas.nbmall.shop.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.nbsaas.nbmall.shop.data.entity.ShopCollect;

import java.util.Date;

/**
 * Created by imake on 2021年12月10日17:38:00.
 */
public interface ShopCollectDao extends BaseDao<ShopCollect, Long> {

    ShopCollect findById(Long id);

    ShopCollect save(ShopCollect bean);

    ShopCollect updateByUpdater(Updater<ShopCollect> updater);

    ShopCollect deleteById(Long id);

    ShopCollect findById(Long tenant, Long id);

    ShopCollect deleteById(Long tenant, Long id);

    Long dayNum(Long id, Date yesterday);

    Long dayNumAll(Long tenant, Date date);
}