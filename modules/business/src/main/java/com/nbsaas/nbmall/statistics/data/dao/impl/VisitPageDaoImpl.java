package com.nbsaas.nbmall.statistics.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.statistics.data.dao.VisitPageDao;
import com.nbsaas.nbmall.statistics.data.entity.VisitPage;

/**
* Created by imake on 2021年12月23日11:45:35.
*/
@Repository

public class VisitPageDaoImpl extends CriteriaDaoImpl<VisitPage, Long> implements VisitPageDao {

	@Override
	public VisitPage findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public VisitPage save(VisitPage bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public VisitPage deleteById(Long id) {
		VisitPage entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<VisitPage> getEntityClass() {
		return VisitPage.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public VisitPage findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public VisitPage deleteById(Long tenant,Long id) {
		VisitPage entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}