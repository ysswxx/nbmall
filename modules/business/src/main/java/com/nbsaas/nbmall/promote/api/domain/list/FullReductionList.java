package com.nbsaas.nbmall.promote.api.domain.list;


import com.nbsaas.nbmall.promote.api.domain.simple.FullReductionSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年12月26日12:04:21.
*/

@Data
public class FullReductionList  extends ResponseList<FullReductionSimple> {

}