package com.nbsaas.nbmall.video.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
/**
*
* Created by BigWorld on 2022年03月16日21:25:37.
*/

@Data
public class VideoCatalogResponse extends ResponseObject {

    private Integer id;

     private Integer parent;
     private Integer levelInfo;
     private String parentName;
     private Integer sortNum;
     private String ids;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date lastDate;
     private String name;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;

}