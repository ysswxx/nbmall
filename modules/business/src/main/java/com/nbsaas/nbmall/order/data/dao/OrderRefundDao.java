package com.nbsaas.nbmall.order.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.order.data.entity.OrderRefund;

/**
* Created by imake on 2021年12月21日18:00:16.
*/
public interface OrderRefundDao extends BaseDao<OrderRefund,Long>{

	 OrderRefund findById(Long id);

	 OrderRefund save(OrderRefund bean);

	 OrderRefund updateByUpdater(Updater<OrderRefund> updater);

	 OrderRefund deleteById(Long id);

	 OrderRefund findById(Long tenant, Long id);

     OrderRefund deleteById(Long tenant, Long id);
}