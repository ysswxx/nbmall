package com.nbsaas.nbmall.product.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月28日18:04:52.
*/

@Data
public class ProductImageDataRequest extends TenantRequest {

    private Long id;

     private Long product;
     private String url;

}