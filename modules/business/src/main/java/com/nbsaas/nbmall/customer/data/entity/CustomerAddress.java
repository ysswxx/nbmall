package com.nbsaas.nbmall.customer.data.entity;

import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.haoxuer.discover.area.data.entity.Area;
import com.haoxuer.discover.data.enums.StoreState;
import com.nbsaas.codemake.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;


@EqualsAndHashCode
@Data
@FormAnnotation(title = "收货地址")
@Entity
@Table(name = "bs_tenant_customer_address")
public class CustomerAddress extends TenantEntity {

    @FormField(title = "收货人姓名", sortNum = "1", grid = true, col = 12)
    @Column(length = 30)
    private String name;

    @FormField(title = "手机号码", sortNum = "1", grid = true, col = 12)
    @Column(length = 50)
    private String phone;

    @FormField(title = "座机", sortNum = "1", grid = true, col = 12)
    @Column(length = 20)
    private String tel;

    @FormField(title = "详细地址", sortNum = "1", grid = true, col = 12)
    @Column(length = 50)
    private String address;

    @FormField(title = "门牌号", sortNum = "1", grid = true, col = 12)
    @Column(length = 20)
    private String houseNo;

    @FormField(title = "标签", sortNum = "1", grid = true, col = 12)
    @Column(length = 20)
    private String label;

    private Double lat;

    private Double lng;


    @SearchItem(
            label = "省份",
            name = "province",
            key = "province.id",
            classType = "Integer",
            operator = "eq",
            show = false
    )
    @FormField(
            title = "省份",
            grid = true,
            ignore = true,
            sort = true
    )
    @FieldName
    @FieldConvert(classType = "Integer")
    @ManyToOne(fetch = FetchType.LAZY)
    private Area province;

    @SearchItem(
            label = "城市",
            name = "city",
            key = "city.id",
            classType = "Integer",
            operator = "eq",
            show = false
    )
    @FormField(
            title = "城市",
            grid = true,
            ignore = true,
            sort = true
    )
    @FieldName
    @FieldConvert(classType = "Integer")
    @ManyToOne(fetch = FetchType.LAZY)
    private Area city;


    @SearchItem(
            label = "区县",
            name = "area",
            key = "area.id",
            classType = "Integer",
            operator = "eq",
            show = false
    )
    @FormField(
            title = "区县",
            grid = true,
            ignore = true,
            type = InputType.select,
            sort = true
    )
    @FieldName
    @FieldConvert(classType = "Integer")
    @ManyToOne(fetch = FetchType.LAZY)
    private Area area;

    @FormField(title = "邮编", sortNum = "1", grid = true, col = 12)
    @Column(length = 20)
    private String postalCode;

    @FormField(title = "备注", sortNum = "1", grid = true, col = 12)
    private String note;

    @SearchItem(
            label = "用户",
            name = "customer",
            key = "customer.id",
            classType = "Long",
            operator = "eq",
            show = false
    )
    @FieldName
    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private Customer customer;

    private Date updateDate;

    private StoreState storeState;
}
