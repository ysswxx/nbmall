package com.nbsaas.nbmall.video.data.entity;

import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.*;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Data
@FormAnnotation(title = "视频")
@Entity
@Table(name = "bs_tenant_video")
public class Video extends TenantEntity {

    @FormField(title = "视频名称", grid = true, col = 24)
    private String name;

    @FormField(title = "视频封面", grid = true, col = 24, type = InputType.image)
    private String logo;


    @FormField(title = "视频类型", grid = true, col = 24, type = InputType.select, option = "videoType")
    @FieldConvert
    @FieldName
    @ManyToOne(fetch = FetchType.LAZY)
    private VideoType videoType;


    @FormField(title = "视频分类", grid = true, col = 24, type = InputType.select, option = "videoCatalog")
    @FieldConvert(classType = "Integer")
    @FieldName
    @ManyToOne(fetch = FetchType.LAZY)
    private VideoCatalog videoCatalog;

    @FormField(title = "视频地址", col = 24)
    private String url;

}
