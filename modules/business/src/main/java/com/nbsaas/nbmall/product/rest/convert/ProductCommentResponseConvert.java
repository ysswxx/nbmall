package com.nbsaas.nbmall.product.rest.convert;

import com.nbsaas.nbmall.product.api.domain.response.ProductCommentResponse;
import com.nbsaas.nbmall.product.data.entity.ProductComment;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class ProductCommentResponseConvert implements Conver<ProductCommentResponse, ProductComment> {
    @Override
    public ProductCommentResponse conver(ProductComment source) {
        ProductCommentResponse result = new ProductCommentResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getTenantUser()!=null){
           result.setTenantUser(source.getTenantUser().getId());
        }
        if(source.getProduct()!=null){
           result.setProduct(source.getProduct().getId());
        }

        return result;
    }
}
