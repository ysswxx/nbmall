package com.nbsaas.nbmall.customer.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.customer.data.entity.CustomerAddress;

/**
* Created by imake on 2021年12月10日22:56:19.
*/
public interface CustomerAddressDao extends BaseDao<CustomerAddress,Long>{

	 CustomerAddress findById(Long id);

	 CustomerAddress save(CustomerAddress bean);

	 CustomerAddress updateByUpdater(Updater<CustomerAddress> updater);

	 CustomerAddress deleteById(Long id);

	 CustomerAddress findById(Long tenant, Long id);

     CustomerAddress deleteById(Long tenant, Long id);
}