package com.nbsaas.nbmall.common.rest.resource;

import com.haoxuer.bigworld.pay.api.domain.page.TradeStreamPage;
import com.haoxuer.bigworld.pay.rest.conver.TradeStreamSimpleConver;
import com.haoxuer.discover.trade.data.dao.TradeStreamDao;
import com.haoxuer.discover.trade.data.entity.TradeAccount;
import com.haoxuer.discover.trade.data.entity.TradeStream;
import com.nbsaas.nbmall.common.api.apis.SiteTenantApi;
import com.nbsaas.nbmall.common.api.domain.list.SiteTenantList;
import com.nbsaas.nbmall.common.api.domain.page.SiteTenantPage;
import com.nbsaas.nbmall.common.api.domain.request.*;
import com.nbsaas.nbmall.common.api.domain.response.SiteTenantResponse;
import com.nbsaas.nbmall.common.data.dao.SiteTenantDao;
import com.nbsaas.nbmall.common.data.entity.SiteTenant;
import com.nbsaas.nbmall.common.rest.convert.SiteTenantResponseConvert;
import com.nbsaas.nbmall.common.rest.convert.SiteTenantSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.nbsaas.nbmall.order.data.dao.OrderFormDao;
import com.nbsaas.nbmall.order.data.entity.OrderForm;
import com.nbsaas.nbmall.shop.data.dao.ShopCollectDao;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;
import com.haoxuer.discover.config.data.dao.UserDao;
import com.haoxuer.discover.trade.data.dao.TradeAccountDao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Transactional
@Component
public class SiteTenantResource implements SiteTenantApi {

    @Autowired
    private SiteTenantDao dataDao;

    @Autowired
    private UserDao creatorDao;
    @Autowired
    private TradeAccountDao tradeAccountDao;

    @Autowired
    private TradeStreamDao streamDao;

    @Autowired
    private OrderFormDao orderDao;

    @Autowired
    private ShopCollectDao collectDao;

    @Override
    public SiteTenantResponse create(SiteTenantDataRequest request) {
        SiteTenantResponse result = new SiteTenantResponse();

        SiteTenant bean = new SiteTenant();
        handleData(request, bean);
        dataDao.save(bean);
        result = new SiteTenantResponseConvert().conver(bean);
        return result;
    }

    @Override
    public SiteTenantResponse update(SiteTenantDataRequest request) {
        SiteTenantResponse result = new SiteTenantResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        SiteTenant bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new SiteTenantResponseConvert().conver(bean);
        return result;
    }

    private void handleData(SiteTenantDataRequest request, SiteTenant bean) {
        BeanDataUtils.copyProperties(request,bean);
            if(request.getTradeAccount()!=null){
               bean.setTradeAccount(tradeAccountDao.findById(request.getTradeAccount()));
            }
            if(request.getCreator()!=null){
               bean.setCreator(creatorDao.findById(request.getCreator()));
            }

    }

    @Override
    public SiteTenantResponse delete(SiteTenantDataRequest request) {
        SiteTenantResponse result = new SiteTenantResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public SiteTenantResponse view(SiteTenantDataRequest request) {
        SiteTenantResponse result=new SiteTenantResponse();
        SiteTenant bean = dataDao.findById( request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new SiteTenantResponseConvert().conver(bean);
        return result;
    }
    @Override
    public SiteTenantList list(SiteTenantSearchRequest request) {
        SiteTenantList result = new SiteTenantList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<SiteTenant> organizations = dataDao.list(0, request.getSize(), filters, orders);
        SiteTenantSimpleConvert convert=new SiteTenantSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public SiteTenantPage search(SiteTenantSearchRequest request) {
        SiteTenantPage result=new SiteTenantPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<SiteTenant> page=dataDao.page(pageable);
        SiteTenantSimpleConvert convert=new SiteTenantSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }

    @Override
    public TradeStreamPage stream(SiteTenantSearchRequest request) {
        TradeStreamPage result = new TradeStreamPage();
        SiteTenant shop = dataDao.findById(request.getTenant());
        if (shop == null) {
            result.setCode(1001);
            result.setMsg("无效token");
            return result;
        }
        TradeAccount account = shop.getTradeAccount();
        if (account == null) {
            account = tradeAccountDao.initNormal();
            shop.setTradeAccount(account);
        }
        result.setMoney(account.getAmount());
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getOrders().add(Order.desc("id"));
        pageable.getFilters().add(Filter.eq("account.id", account.getId()));

        Page<TradeStream> page = streamDao.page(pageable);
        ConverResourceUtils.converPage(result, page, new TradeStreamSimpleConver());
        return result;
    }

    @Override
    public SiteTenantResponse viewInfo(SiteTenantDataRequest request) {
        SiteTenantResponse result = new SiteTenantResponse();
        SiteTenant bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result = new SiteTenantResponseConvert().conver(bean);
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        Date yesterday = calendar.getTime();

        result.setToday(orderDao.dayAll(request.getId(), today));
        result.setYesterday(orderDao.dayAll(request.getId(), yesterday));

        result.setTodayNum(orderDao.dayNumAll(request.getId(), today));
        result.setYesterdayNum(orderDao.dayNumAll(request.getId(), yesterday));

        result.setTodayCollect(collectDao.dayNumAll(request.getId(), today));
        result.setYesterdayCollect(collectDao.dayNumAll(request.getId(), yesterday));
        if (bean.getTradeAccount() == null) {
            result.setMoney(new BigDecimal(0));
        } else {
            result.setMoney(bean.getTradeAccount().getAmount());
        }
        return result;
    }
}
