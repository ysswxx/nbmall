package com.nbsaas.nbmall.shop.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月10日17:38:00.
*/

@Data
public class ShopNotifierDataRequest extends TenantRequest {

    private Long id;

     private Long shop;
     private Long creator;
     private String openId;
     private String name;

}