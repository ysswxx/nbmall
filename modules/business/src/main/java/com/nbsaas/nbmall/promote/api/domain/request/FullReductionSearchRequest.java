package com.nbsaas.nbmall.promote.api.domain.request;

import com.haoxuer.bigworld.member.api.domain.request.TenantPageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月26日12:04:21.
*/

@Data
public class FullReductionSearchRequest extends TenantPageRequest {

    //名称
     @Search(name = "name",operator = Filter.Operator.like)
     private String name;

    //商家
     @Search(name = "shop",operator = Filter.Operator.eq)
     private Long shop;



}