package com.nbsaas.nbmall.shop.rest.convert;

import com.nbsaas.nbmall.shop.api.domain.response.ShopCatalogResponse;
import com.nbsaas.nbmall.shop.data.entity.ShopCatalog;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class ShopCatalogResponseConvert implements Conver<ShopCatalogResponse, ShopCatalog> {
    @Override
    public ShopCatalogResponse conver(ShopCatalog source) {
        ShopCatalogResponse result = new ShopCatalogResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getParent()!=null){
           result.setParent(source.getParent().getId());
        }
         if(source.getParent()!=null){
            result.setParentName(source.getParent().getName());
         }

        return result;
    }
}
