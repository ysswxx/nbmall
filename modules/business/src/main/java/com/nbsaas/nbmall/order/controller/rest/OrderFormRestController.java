package com.nbsaas.nbmall.order.controller.rest;

import com.nbsaas.nbmall.order.api.apis.OrderFormApi;
import com.nbsaas.nbmall.order.api.domain.list.OrderFormList;
import com.nbsaas.nbmall.order.api.domain.page.OrderFormPage;
import com.nbsaas.nbmall.order.api.domain.request.*;
import com.nbsaas.nbmall.order.api.domain.response.OrderFormResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/orderform")
@RestController
public class OrderFormRestController extends BaseRestController {


    @RequestMapping("create")
    public OrderFormResponse create(OrderFormDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

    @RequestMapping("delete")
    public OrderFormResponse delete(OrderFormDataRequest request) {
        initTenant(request);
        OrderFormResponse result = new OrderFormResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public OrderFormResponse update(OrderFormDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public OrderFormResponse view(OrderFormDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public OrderFormList list(OrderFormSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public OrderFormPage search(OrderFormSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private OrderFormApi api;

}
