package com.nbsaas.nbmall.order.data.enums;

public enum SettleType {

    rate, one;

    public String toString() {
        if (this.name().equals("rate")) {
            return "抽成结算";
        } else if (this.name().equals("one")) {
            return "单笔结算";
        }  else {
            return this.name();
        }
    }
}
