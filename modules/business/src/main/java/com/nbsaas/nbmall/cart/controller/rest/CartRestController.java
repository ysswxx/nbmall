package com.nbsaas.nbmall.cart.controller.rest;

import com.haoxuer.bigworld.gather.api.domain.response.MapResponse;
import com.nbsaas.nbmall.cart.api.apis.CartApi;
import com.nbsaas.nbmall.cart.api.domain.list.CartList;
import com.nbsaas.nbmall.cart.api.domain.page.CartPage;
import com.nbsaas.nbmall.cart.api.domain.request.*;
import com.nbsaas.nbmall.cart.api.domain.response.CartResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/cart")
@RestController
public class CartRestController extends BaseRestController {


    @RequestMapping("create")
    public CartResponse create(CartDataRequest request) {
        initTenant(request);
        request.setTenantUser(request.getUser());
        return api.create(request);
    }

    @RequestMapping("delete")
    public CartResponse delete(CartDataRequest request) {
        initTenant(request);
        CartResponse result = new CartResponse();
        try {
            result = api.delete(request);
        } catch (Exception e) {
            result.setCode(501);
            result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("my")
    public MapResponse my(CartSearchRequest request) {
        initTenant(request);
        request.setTenantUser(request.getUser());
        return api.my(request);
    }

    @Autowired
    private CartApi api;

}
