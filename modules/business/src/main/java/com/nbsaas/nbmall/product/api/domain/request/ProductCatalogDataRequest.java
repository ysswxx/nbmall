package com.nbsaas.nbmall.product.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月10日17:38:01.
*/

@Data
public class ProductCatalogDataRequest extends TenantRequest {

    private Integer id;

     private Integer parent;
     private String code;
     private Integer levelInfo;
     private Integer sortNum;
     private String ids;
     private Integer lft;
     private Date lastDate;
     private String name;
     private Date addDate;
     private Integer rgt;

}