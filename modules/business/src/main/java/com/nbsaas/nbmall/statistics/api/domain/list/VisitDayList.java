package com.nbsaas.nbmall.statistics.api.domain.list;


import com.nbsaas.nbmall.statistics.api.domain.simple.VisitDaySimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年12月23日11:45:35.
*/

@Data
public class VisitDayList  extends ResponseList<VisitDaySimple> {

}