package com.nbsaas.nbmall.common.rest.convert;

import com.nbsaas.nbmall.common.api.domain.response.ChannelResponse;
import com.nbsaas.nbmall.common.data.entity.Channel;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class ChannelResponseConvert implements Conver<ChannelResponse, Channel> {
    @Override
    public ChannelResponse conver(Channel source) {
        ChannelResponse result = new ChannelResponse();
        BeanDataUtils.copyProperties(source,result);

        if(source.getCreator()!=null){
           result.setCreator(source.getCreator().getId());
        }
         if(source.getCreator()!=null){
            result.setCreatorName(source.getCreator().getName());
         }


        return result;
    }
}
