package com.nbsaas.nbmall.order.api.domain.list;


import com.nbsaas.nbmall.order.api.domain.simple.OrderRefundSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年12月21日18:00:16.
*/

@Data
public class OrderRefundList  extends ResponseList<OrderRefundSimple> {

}