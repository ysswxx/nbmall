package com.nbsaas.nbmall.product.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.product.data.entity.ProductCatalog;

/**
* Created by imake on 2021年12月10日17:38:01.
*/
public interface ProductCatalogDao extends BaseDao<ProductCatalog,Integer>{

	 ProductCatalog findById(Integer id);

	 ProductCatalog save(ProductCatalog bean);

	 ProductCatalog updateByUpdater(Updater<ProductCatalog> updater);

	 ProductCatalog deleteById(Integer id);

	 ProductCatalog findById(Long tenant, Integer id);

     ProductCatalog deleteById(Long tenant, Integer id);
}