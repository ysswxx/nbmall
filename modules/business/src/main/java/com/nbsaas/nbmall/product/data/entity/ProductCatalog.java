package com.nbsaas.nbmall.product.data.entity;


import com.haoxuer.bigworld.tenant.data.entity.TenantCatalogEntity;
import com.nbsaas.codemake.annotation.*;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@CatalogClass
@Data
@FormAnnotation(title = "商品分类管理",model = "商品分类",menu = "1,107,162")
@Entity
@Table(name = "bs_tenant_product_catalog")
public class ProductCatalog  extends TenantCatalogEntity {

    @SearchItem(label = "分类",name = "parent",key = "parent.id",classType = "Integer",operator = "eq",show = false)
    @FieldName
    @FieldConvert(classType = "Integer")
    @ManyToOne(fetch = FetchType.LAZY)
    private ProductCatalog parent;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
    private List<ProductCatalog> children;

    @Override
    public Integer getParentId() {
        if (parent != null) {
            return parent.getId();
        }
        return null;
    }
}
