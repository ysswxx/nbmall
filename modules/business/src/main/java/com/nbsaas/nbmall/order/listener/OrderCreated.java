package com.nbsaas.nbmall.order.listener;

import com.nbsaas.nbmall.order.data.entity.OrderForm;

public interface OrderCreated {

    void pay(OrderForm dish);

}
