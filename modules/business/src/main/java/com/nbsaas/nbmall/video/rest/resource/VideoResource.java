package com.nbsaas.nbmall.video.rest.resource;

import com.nbsaas.nbmall.video.api.apis.VideoApi;
import com.nbsaas.nbmall.video.api.domain.list.VideoList;
import com.nbsaas.nbmall.video.api.domain.page.VideoPage;
import com.nbsaas.nbmall.video.api.domain.request.*;
import com.nbsaas.nbmall.video.api.domain.response.VideoResponse;
import com.nbsaas.nbmall.video.data.dao.VideoDao;
import com.nbsaas.nbmall.video.data.entity.Video;
import com.nbsaas.nbmall.video.rest.convert.VideoResponseConvert;
import com.nbsaas.nbmall.video.rest.convert.VideoSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class VideoResource implements VideoApi {

    @Autowired
    private VideoDao dataDao;



    @Override
    public VideoResponse create(VideoDataRequest request) {
        VideoResponse result = new VideoResponse();

        Video bean = new Video();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new VideoResponseConvert().conver(bean);
        return result;
    }

    @Override
    public VideoResponse update(VideoDataRequest request) {
        VideoResponse result = new VideoResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        Video bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new VideoResponseConvert().conver(bean);
        return result;
    }

    private void handleData(VideoDataRequest request, Video bean) {
       TenantBeanUtils.copyProperties(request,bean);

    }

    @Override
    public VideoResponse delete(VideoDataRequest request) {
        VideoResponse result = new VideoResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public VideoResponse view(VideoDataRequest request) {
        VideoResponse result=new VideoResponse();
        Video bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new VideoResponseConvert().conver(bean);
        return result;
    }
    @Override
    public VideoList list(VideoSearchRequest request) {
        VideoList result = new VideoList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<Video> organizations = dataDao.list(0, request.getSize(), filters, orders);

        VideoSimpleConvert convert=new VideoSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public VideoPage search(VideoSearchRequest request) {
        VideoPage result=new VideoPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<Video> page=dataDao.page(pageable);

        VideoSimpleConvert convert=new VideoSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
