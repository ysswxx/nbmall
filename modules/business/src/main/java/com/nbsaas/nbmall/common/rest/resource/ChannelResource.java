package com.nbsaas.nbmall.common.rest.resource;

import com.nbsaas.nbmall.common.api.apis.ChannelApi;
import com.nbsaas.nbmall.common.api.domain.list.ChannelList;
import com.nbsaas.nbmall.common.api.domain.page.ChannelPage;
import com.nbsaas.nbmall.common.api.domain.request.*;
import com.nbsaas.nbmall.common.api.domain.response.ChannelResponse;
import com.nbsaas.nbmall.common.data.dao.ChannelDao;
import com.nbsaas.nbmall.common.data.entity.Channel;
import com.nbsaas.nbmall.common.rest.convert.ChannelResponseConvert;
import com.nbsaas.nbmall.common.rest.convert.ChannelSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;
import com.haoxuer.discover.user.data.dao.UserInfoDao;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class ChannelResource implements ChannelApi {

    @Autowired
    private ChannelDao dataDao;

    @Autowired
    private UserInfoDao creatorDao;

    @Override
    public ChannelResponse create(ChannelDataRequest request) {
        ChannelResponse result = new ChannelResponse();

        Channel bean = new Channel();
        handleData(request, bean);
        dataDao.save(bean);
        result = new ChannelResponseConvert().conver(bean);
        return result;
    }

    @Override
    public ChannelResponse update(ChannelDataRequest request) {
        ChannelResponse result = new ChannelResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        Channel bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new ChannelResponseConvert().conver(bean);
        return result;
    }

    private void handleData(ChannelDataRequest request, Channel bean) {
        BeanDataUtils.copyProperties(request,bean);
            if(request.getCreator()!=null){
               bean.setCreator(creatorDao.findById(request.getCreator()));
            }

    }

    @Override
    public ChannelResponse delete(ChannelDataRequest request) {
        ChannelResponse result = new ChannelResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public ChannelResponse view(ChannelDataRequest request) {
        ChannelResponse result=new ChannelResponse();
        Channel bean = dataDao.findById( request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new ChannelResponseConvert().conver(bean);
        return result;
    }
    @Override
    public ChannelList list(ChannelSearchRequest request) {
        ChannelList result = new ChannelList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<Channel> organizations = dataDao.list(0, request.getSize(), filters, orders);
        ChannelSimpleConvert convert=new ChannelSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public ChannelPage search(ChannelSearchRequest request) {
        ChannelPage result=new ChannelPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<Channel> page=dataDao.page(pageable);
        ChannelSimpleConvert convert=new ChannelSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
