package com.nbsaas.nbmall.statistics.api.apis;


import com.nbsaas.nbmall.statistics.api.domain.list.VisitDayList;
import com.nbsaas.nbmall.statistics.api.domain.page.VisitDayPage;
import com.nbsaas.nbmall.statistics.api.domain.request.*;
import com.nbsaas.nbmall.statistics.api.domain.response.VisitDayResponse;

public interface VisitDayApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    VisitDayResponse create(VisitDayDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    VisitDayResponse update(VisitDayDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    VisitDayResponse delete(VisitDayDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     VisitDayResponse view(VisitDayDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    VisitDayList list(VisitDaySearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    VisitDayPage search(VisitDaySearchRequest request);

}