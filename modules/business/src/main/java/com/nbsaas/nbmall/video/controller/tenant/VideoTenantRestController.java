package com.nbsaas.nbmall.video.controller.tenant;

import com.nbsaas.nbmall.video.api.apis.VideoApi;
import com.nbsaas.nbmall.video.api.domain.list.VideoList;
import com.nbsaas.nbmall.video.api.domain.page.VideoPage;
import com.nbsaas.nbmall.video.api.domain.request.*;
import com.nbsaas.nbmall.video.api.domain.response.VideoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/video")
@RestController
public class VideoTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("video")
    @RequestMapping("create")
    public VideoResponse create(VideoDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

	@RequiresPermissions("video")
    @RequestMapping("delete")
    public VideoResponse delete(VideoDataRequest request) {
        initTenant(request);
        VideoResponse result = new VideoResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("video")
    @RequestMapping("update")
    public VideoResponse update(VideoDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("video")
    @RequestMapping("view")
    public VideoResponse view(VideoDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("video")
    @RequestMapping("list")
    public VideoList list(VideoSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("video")
    @RequestMapping("search")
    public VideoPage search(VideoSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private VideoApi api;

}
