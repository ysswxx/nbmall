package com.nbsaas.nbmall.product.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.product.data.dao.ProductGroupDao;
import com.nbsaas.nbmall.product.data.entity.ProductGroup;

/**
* Created by imake on 2021年12月28日17:59:58.
*/
@Repository

public class ProductGroupDaoImpl extends CriteriaDaoImpl<ProductGroup, Long> implements ProductGroupDao {

	@Override
	public ProductGroup findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public ProductGroup save(ProductGroup bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public ProductGroup deleteById(Long id) {
		ProductGroup entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<ProductGroup> getEntityClass() {
		return ProductGroup.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public ProductGroup findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public ProductGroup deleteById(Long tenant,Long id) {
		ProductGroup entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}