package com.nbsaas.nbmall.device.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月10日17:40:16.
*/

@Data
public class DeviceTypeDataRequest extends BaseRequest {

    private Long id;

     private Date beginDate;

     private String note;

     private String website;

     private Long creator;

     private String doc;

     private String name;

     private String className;


}