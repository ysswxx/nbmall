package com.nbsaas.nbmall.statistics.controller.rest;

import com.nbsaas.nbmall.statistics.api.apis.VisitChannelApi;
import com.nbsaas.nbmall.statistics.api.domain.list.VisitChannelList;
import com.nbsaas.nbmall.statistics.api.domain.page.VisitChannelPage;
import com.nbsaas.nbmall.statistics.api.domain.request.*;
import com.nbsaas.nbmall.statistics.api.domain.response.VisitChannelResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/visitchannel")
@RestController
public class VisitChannelRestController extends BaseRestController {


    @RequestMapping("create")
    public VisitChannelResponse create(VisitChannelDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

    @RequestMapping("delete")
    public VisitChannelResponse delete(VisitChannelDataRequest request) {
        initTenant(request);
        VisitChannelResponse result = new VisitChannelResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public VisitChannelResponse update(VisitChannelDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public VisitChannelResponse view(VisitChannelDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public VisitChannelList list(VisitChannelSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public VisitChannelPage search(VisitChannelSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private VisitChannelApi api;

}
