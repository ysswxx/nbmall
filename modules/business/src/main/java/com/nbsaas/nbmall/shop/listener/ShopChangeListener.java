package com.nbsaas.nbmall.shop.listener;


import com.nbsaas.nbmall.shop.data.entity.Shop;

public interface ShopChangeListener extends Comparable<ShopChangeListener> {

    /**
     * @param shop 菜品
     */
    void create(Shop shop);

    /**
     * 排序号
     *
     * @return
     */
    int sortNum();
}
