package com.nbsaas.nbmall.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtils {

    public static String json(Object object) {
        ObjectMapper mapper = new ObjectMapper();
        String body = "";
        try {
            body = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        } catch (Exception e) {

        }
        return body;
    }
}
