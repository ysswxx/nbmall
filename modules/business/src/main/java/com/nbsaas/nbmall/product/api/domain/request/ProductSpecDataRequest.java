package com.nbsaas.nbmall.product.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月10日17:38:01.
*/

@Data
public class ProductSpecDataRequest extends TenantRequest {

    private Long id;

     private Long product;
     private Long creator;
     private String name;

}