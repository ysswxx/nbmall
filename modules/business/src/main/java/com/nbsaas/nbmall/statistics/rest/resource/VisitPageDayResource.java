package com.nbsaas.nbmall.statistics.rest.resource;

import com.nbsaas.nbmall.statistics.api.apis.VisitPageDayApi;
import com.nbsaas.nbmall.statistics.api.domain.list.VisitPageDayList;
import com.nbsaas.nbmall.statistics.api.domain.page.VisitPageDayPage;
import com.nbsaas.nbmall.statistics.api.domain.request.*;
import com.nbsaas.nbmall.statistics.api.domain.response.VisitPageDayResponse;
import com.nbsaas.nbmall.statistics.data.dao.VisitPageDayDao;
import com.nbsaas.nbmall.statistics.data.entity.VisitPageDay;
import com.nbsaas.nbmall.statistics.rest.convert.VisitPageDayResponseConvert;
import com.nbsaas.nbmall.statistics.rest.convert.VisitPageDaySimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.haoxuer.bigworld.member.data.dao.TenantUserDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class VisitPageDayResource implements VisitPageDayApi {

    @Autowired
    private VisitPageDayDao dataDao;

    @Autowired
    private TenantUserDao userDao;


    @Override
    public VisitPageDayResponse create(VisitPageDayDataRequest request) {
        VisitPageDayResponse result = new VisitPageDayResponse();

        VisitPageDay bean = new VisitPageDay();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new VisitPageDayResponseConvert().conver(bean);
        return result;
    }

    @Override
    public VisitPageDayResponse update(VisitPageDayDataRequest request) {
        VisitPageDayResponse result = new VisitPageDayResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        VisitPageDay bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new VisitPageDayResponseConvert().conver(bean);
        return result;
    }

    private void handleData(VisitPageDayDataRequest request, VisitPageDay bean) {
       TenantBeanUtils.copyProperties(request,bean);
           if(request.getUser()!=null){
              bean.setUser(userDao.findById(request.getUser()));
           }

    }

    @Override
    public VisitPageDayResponse delete(VisitPageDayDataRequest request) {
        VisitPageDayResponse result = new VisitPageDayResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public VisitPageDayResponse view(VisitPageDayDataRequest request) {
        VisitPageDayResponse result=new VisitPageDayResponse();
        VisitPageDay bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new VisitPageDayResponseConvert().conver(bean);
        return result;
    }
    @Override
    public VisitPageDayList list(VisitPageDaySearchRequest request) {
        VisitPageDayList result = new VisitPageDayList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<VisitPageDay> organizations = dataDao.list(0, request.getSize(), filters, orders);

        VisitPageDaySimpleConvert convert=new VisitPageDaySimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public VisitPageDayPage search(VisitPageDaySearchRequest request) {
        VisitPageDayPage result=new VisitPageDayPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<VisitPageDay> page=dataDao.page(pageable);

        VisitPageDaySimpleConvert convert=new VisitPageDaySimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
