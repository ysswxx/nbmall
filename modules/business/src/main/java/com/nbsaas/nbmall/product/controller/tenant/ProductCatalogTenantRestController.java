package com.nbsaas.nbmall.product.controller.tenant;

import com.nbsaas.nbmall.product.api.apis.ProductCatalogApi;
import com.nbsaas.nbmall.product.api.domain.list.ProductCatalogList;
import com.nbsaas.nbmall.product.api.domain.page.ProductCatalogPage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductCatalogResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/productcatalog")
@RestController
public class ProductCatalogTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("productcatalog")
    @RequestMapping("create")
    public ProductCatalogResponse create(ProductCatalogDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

	@RequiresPermissions("productcatalog")
    @RequestMapping("delete")
    public ProductCatalogResponse delete(ProductCatalogDataRequest request) {
        initTenant(request);
        ProductCatalogResponse result = new ProductCatalogResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("productcatalog")
    @RequestMapping("update")
    public ProductCatalogResponse update(ProductCatalogDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("productcatalog")
    @RequestMapping("view")
    public ProductCatalogResponse view(ProductCatalogDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("productcatalog")
    @RequestMapping("list")
    public ProductCatalogList list(ProductCatalogSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("productcatalog")
    @RequestMapping("search")
    public ProductCatalogPage search(ProductCatalogSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private ProductCatalogApi api;

}
