package com.nbsaas.nbmall.utils;

import org.im4java.core.ImageCommand;

public class ConvertCmdWin extends ImageCommand {

    public ConvertCmdWin(boolean var1) {
        if (var1) {
            this.setCommand(new String[]{"gm", "magick"});
        } else {
            this.setCommand(new String[]{"magick"});
        }

    }
}
