package com.nbsaas.nbmall.promote.controller.rest;

import com.nbsaas.nbmall.promote.api.apis.CouponRuleApi;
import com.nbsaas.nbmall.promote.api.domain.list.CouponRuleList;
import com.nbsaas.nbmall.promote.api.domain.page.CouponRulePage;
import com.nbsaas.nbmall.promote.api.domain.request.*;
import com.nbsaas.nbmall.promote.api.domain.response.CouponRuleResponse;
import com.nbsaas.nbmall.promote.api.domain.simple.CouponSimple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/rest/couponrule")
@RestController
public class CouponRuleRestController extends BaseRestController {


    @RequestMapping("create")
    public CouponRuleResponse create(CouponRuleDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

    @RequestMapping("delete")
    public CouponRuleResponse delete(CouponRuleDataRequest request) {
        initTenant(request);
        CouponRuleResponse result = new CouponRuleResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public CouponRuleResponse update(CouponRuleDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public CouponRuleResponse view(CouponRuleDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public CouponRuleList list(CouponRuleSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public CouponRulePage search(CouponRuleSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @RequestMapping("searchByReceive")
    public CouponRulePage searchByReceive(CouponRuleSearchRequest request) {
        initTenant(request);
        return api.searchByReceive(request);
    }

    @Autowired
    private CouponRuleApi api;

}
