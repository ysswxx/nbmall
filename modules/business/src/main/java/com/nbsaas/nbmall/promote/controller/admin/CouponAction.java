package com.nbsaas.nbmall.promote.controller.admin;


import com.haoxuer.bigworld.tenant.controller.admin.TenantBaseAction;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
*
* Created by imake on 2021年12月25日12:45:48.
*/


@Scope("prototype")
@Controller
public class CouponAction extends TenantBaseAction {

	@RequiresPermissions("coupon")
	@RequestMapping("/tenant/coupon/view_list")
	public String list(ModelMap model) {
		return getView("coupon/list");
	}

}