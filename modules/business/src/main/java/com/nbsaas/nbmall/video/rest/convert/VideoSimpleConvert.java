package com.nbsaas.nbmall.video.rest.convert;

import com.nbsaas.nbmall.video.api.domain.simple.VideoSimple;
import com.nbsaas.nbmall.video.data.entity.Video;
import com.haoxuer.discover.data.rest.core.Conver;
public class VideoSimpleConvert implements Conver<VideoSimple, Video> {


    @Override
    public VideoSimple conver(Video source) {
        VideoSimple result = new VideoSimple();

            result.setId(source.getId());
             result.setAddDate(source.getAddDate());

        return result;
    }
}
