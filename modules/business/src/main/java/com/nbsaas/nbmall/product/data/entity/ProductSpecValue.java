package com.nbsaas.nbmall.product.data.entity;

import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.FieldConvert;
import com.nbsaas.codemake.annotation.FormAnnotation;
import lombok.Data;

import javax.persistence.*;


@Data
@FormAnnotation(title = "规格值", model = "规格值",menu = "1,107,108")
@Entity
@Table(name = "bs_tenant_product_spec_value")
public class ProductSpecValue extends TenantEntity {

    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private ProductSpec productSpec;

    @Column(name = "data_value")
    private String value;
}
