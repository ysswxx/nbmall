package com.nbsaas.nbmall.order.controller.rest;

import com.nbsaas.nbmall.order.api.apis.OrderSettleApi;
import com.nbsaas.nbmall.order.api.domain.list.OrderSettleList;
import com.nbsaas.nbmall.order.api.domain.page.OrderSettlePage;
import com.nbsaas.nbmall.order.api.domain.request.*;
import com.nbsaas.nbmall.order.api.domain.response.OrderSettleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/ordersettle")
@RestController
public class OrderSettleRestController extends BaseRestController {


    @RequestMapping("create")
    public OrderSettleResponse create(OrderSettleDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

    @RequestMapping("delete")
    public OrderSettleResponse delete(OrderSettleDataRequest request) {
        initTenant(request);
        OrderSettleResponse result = new OrderSettleResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public OrderSettleResponse update(OrderSettleDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public OrderSettleResponse view(OrderSettleDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public OrderSettleList list(OrderSettleSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public OrderSettlePage search(OrderSettleSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private OrderSettleApi api;

}
