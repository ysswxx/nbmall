package com.nbsaas.nbmall.product.rest.convert;

import com.nbsaas.nbmall.product.api.domain.simple.ProductGroupSimple;
import com.nbsaas.nbmall.product.data.entity.ProductGroup;
import com.haoxuer.discover.data.rest.core.Conver;
public class ProductGroupSimpleConvert implements Conver<ProductGroupSimple, ProductGroup> {


    @Override
    public ProductGroupSimple conver(ProductGroup source) {
        ProductGroupSimple result = new ProductGroupSimple();

            result.setId(source.getId());
            if(source.getShop()!=null){
               result.setShop(source.getShop().getId());
            }
             result.setNote(source.getNote());
             result.setNum(source.getNum());
             result.setSortNum(source.getSortNum());
             result.setShowState(source.getShowState());
             if(source.getShop()!=null){
                result.setShopName(source.getShop().getName());
             }
             result.setName(source.getName());
             result.setAddDate(source.getAddDate());

        return result;
    }
}
