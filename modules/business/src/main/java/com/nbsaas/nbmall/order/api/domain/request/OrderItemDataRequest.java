package com.nbsaas.nbmall.order.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import java.util.Date;
import java.math.BigDecimal;

/**
*
* Created by imake on 2021年12月10日17:38:01.
*/

@Data
public class OrderItemDataRequest extends TenantRequest {

    private Long id;

     private Integer refundState;
     private BigDecimal platformDiscount;
     private BigDecimal weight;
     private BigDecimal payAmount;
     private Boolean giveType;
     private BigDecimal subtotal;
     private Integer size;
     private String unit;
     private Integer returnNum;
     private String name;
     private BigDecimal paidAmount;
     private BigDecimal realPrice;
     private String skuAttr;
     private BigDecimal integralDiscount;
     private BigDecimal freight;
     private BigDecimal discountAmount;
     private BigDecimal useIntegral;
     private Integer surplusNum;
     private BigDecimal useRedPacket;
     private BigDecimal returnAmount;
     private BigDecimal realAmount;
     private Integer useNum;
     private BigDecimal price;
     private String logo;
     private BigDecimal redPacketDiscount;

}