package com.nbsaas.nbmall.product.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.List;

/**
*
* Created by BigWorld on 2021年12月10日17:38:01.
*/

@Data
public class ProductCatalogSimple implements Serializable {
    private Integer id;
    private String value;
    private String label;
    private List<ProductCatalogSimple> children;

     private Integer parent;
     private String code;
     private Integer levelInfo;
     private String parentName;
     private Integer sortNum;
     private String ids;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date lastDate;
     private String name;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;

}
