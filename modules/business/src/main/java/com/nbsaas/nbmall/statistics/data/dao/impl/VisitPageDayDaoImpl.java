package com.nbsaas.nbmall.statistics.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.statistics.data.dao.VisitPageDayDao;
import com.nbsaas.nbmall.statistics.data.entity.VisitPageDay;

/**
* Created by imake on 2021年12月23日11:45:35.
*/
@Repository

public class VisitPageDayDaoImpl extends CriteriaDaoImpl<VisitPageDay, Long> implements VisitPageDayDao {

	@Override
	public VisitPageDay findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public VisitPageDay save(VisitPageDay bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public VisitPageDay deleteById(Long id) {
		VisitPageDay entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<VisitPageDay> getEntityClass() {
		return VisitPageDay.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public VisitPageDay findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public VisitPageDay deleteById(Long tenant,Long id) {
		VisitPageDay entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}