package com.nbsaas.nbmall.statistics.api.apis;


import com.nbsaas.nbmall.statistics.api.domain.list.VisitPageList;
import com.nbsaas.nbmall.statistics.api.domain.page.VisitPagePage;
import com.nbsaas.nbmall.statistics.api.domain.request.*;
import com.nbsaas.nbmall.statistics.api.domain.response.VisitPageResponse;

public interface VisitPageApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    VisitPageResponse create(VisitPageDataRequest request);

    /**
     *  用户访问记录
     * @param request
     * @return
     */
    VisitPageResponse visit(VisitPageDataRequest request);


    /**
     * 更新
     *
     * @param request
     * @return
     */
    VisitPageResponse update(VisitPageDataRequest request);

    /**
     * 删除
     *
     * @param request
     * @return
     */
    VisitPageResponse delete(VisitPageDataRequest request);


    /**
     * 详情
     *
     * @param request
     * @return
     */
    VisitPageResponse view(VisitPageDataRequest request);


    /**
     * @param request
     * @return
     */
    VisitPageList list(VisitPageSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    VisitPagePage search(VisitPageSearchRequest request);

}