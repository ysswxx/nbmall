package com.nbsaas.nbmall.product.controller.rest;

import com.nbsaas.nbmall.product.api.apis.ProductSpecApi;
import com.nbsaas.nbmall.product.api.domain.list.ProductSpecList;
import com.nbsaas.nbmall.product.api.domain.page.ProductSpecPage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductSpecResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/productspec")
@RestController
public class ProductSpecRestController extends BaseRestController {


    @RequestMapping("create")
    public ProductSpecResponse create(ProductSpecDataRequest request) {
        initTenant(request);
        request.setCreator(request.getUser());
        return api.create(request);
    }

    @RequestMapping("delete")
    public ProductSpecResponse delete(ProductSpecDataRequest request) {
        initTenant(request);
        ProductSpecResponse result = new ProductSpecResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public ProductSpecResponse update(ProductSpecDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public ProductSpecResponse view(ProductSpecDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public ProductSpecList list(ProductSpecSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public ProductSpecPage search(ProductSpecSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private ProductSpecApi api;

}
