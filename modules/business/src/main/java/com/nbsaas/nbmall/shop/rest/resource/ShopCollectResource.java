package com.nbsaas.nbmall.shop.rest.resource;

import com.nbsaas.nbmall.shop.api.apis.ShopCollectApi;
import com.nbsaas.nbmall.shop.api.domain.list.ShopCollectList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopCollectPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopCollectResponse;
import com.nbsaas.nbmall.shop.data.dao.ShopCollectDao;
import com.nbsaas.nbmall.shop.data.entity.ShopCollect;
import com.nbsaas.nbmall.shop.rest.convert.ShopCollectResponseConvert;
import com.nbsaas.nbmall.shop.rest.convert.ShopCollectSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.nbsaas.nbmall.shop.data.dao.ShopDao;
import com.haoxuer.bigworld.member.data.dao.TenantUserDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class ShopCollectResource implements ShopCollectApi {

    @Autowired
    private ShopCollectDao dataDao;

    @Autowired
    private ShopDao shopDao;
    @Autowired
    private TenantUserDao tenantUserDao;


    @Override
    public ShopCollectResponse create(ShopCollectDataRequest request) {
        ShopCollectResponse result = new ShopCollectResponse();

        ShopCollect bean = new ShopCollect();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new ShopCollectResponseConvert().conver(bean);
        return result;
    }

    @Override
    public ShopCollectResponse update(ShopCollectDataRequest request) {
        ShopCollectResponse result = new ShopCollectResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        ShopCollect bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new ShopCollectResponseConvert().conver(bean);
        return result;
    }

    private void handleData(ShopCollectDataRequest request, ShopCollect bean) {
       TenantBeanUtils.copyProperties(request,bean);
           if(request.getTenantUser()!=null){
              bean.setTenantUser(tenantUserDao.findById(request.getTenantUser()));
           }
           if(request.getShop()!=null){
              bean.setShop(shopDao.findById(request.getShop()));
           }

    }

    @Override
    public ShopCollectResponse delete(ShopCollectDataRequest request) {
        ShopCollectResponse result = new ShopCollectResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public ShopCollectResponse view(ShopCollectDataRequest request) {
        ShopCollectResponse result=new ShopCollectResponse();
        ShopCollect bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new ShopCollectResponseConvert().conver(bean);
        return result;
    }
    @Override
    public ShopCollectList list(ShopCollectSearchRequest request) {
        ShopCollectList result = new ShopCollectList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<ShopCollect> organizations = dataDao.list(0, request.getSize(), filters, orders);

        ShopCollectSimpleConvert convert=new ShopCollectSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public ShopCollectPage search(ShopCollectSearchRequest request) {
        ShopCollectPage result=new ShopCollectPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<ShopCollect> page=dataDao.page(pageable);

        ShopCollectSimpleConvert convert=new ShopCollectSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
