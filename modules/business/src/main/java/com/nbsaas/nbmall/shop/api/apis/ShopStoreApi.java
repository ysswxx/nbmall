package com.nbsaas.nbmall.shop.api.apis;


import com.nbsaas.nbmall.shop.api.domain.list.ShopStoreList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopStorePage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopStoreResponse;

public interface ShopStoreApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    ShopStoreResponse create(ShopStoreDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    ShopStoreResponse update(ShopStoreDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    ShopStoreResponse delete(ShopStoreDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     ShopStoreResponse view(ShopStoreDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    ShopStoreList list(ShopStoreSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    ShopStorePage search(ShopStoreSearchRequest request);

}