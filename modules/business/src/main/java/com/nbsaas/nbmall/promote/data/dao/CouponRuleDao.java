package com.nbsaas.nbmall.promote.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.promote.data.entity.CouponRule;

/**
* Created by imake on 2021年12月28日23:18:26.
*/
public interface CouponRuleDao extends BaseDao<CouponRule,Long>{

	 CouponRule findById(Long id);

	 CouponRule save(CouponRule bean);

	 CouponRule updateByUpdater(Updater<CouponRule> updater);

	 CouponRule deleteById(Long id);

	 CouponRule findById(Long tenant, Long id);

     CouponRule deleteById(Long tenant, Long id);
}