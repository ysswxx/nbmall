package com.nbsaas.nbmall.customer.api.apis;


import com.haoxuer.bigworld.pay.api.domain.page.TradeStreamPage;
import com.nbsaas.nbmall.customer.api.domain.list.CustomerList;
import com.nbsaas.nbmall.customer.api.domain.page.CustomerPage;
import com.nbsaas.nbmall.customer.api.domain.request.*;
import com.nbsaas.nbmall.customer.api.domain.response.CustomerResponse;

public interface CustomerApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    CustomerResponse create(CustomerDataRequest request);



    CustomerResponse register(CustomerDataRequest request);


    TradeStreamPage stream(CustomerSearchRequest request);

    TradeStreamPage scores(CustomerSearchRequest request);


    CustomerResponse rechargeScore(CustomerDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    CustomerResponse update(CustomerDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    CustomerResponse delete(CustomerDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     CustomerResponse view(CustomerDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    CustomerList list(CustomerSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    CustomerPage search(CustomerSearchRequest request);

}