package com.nbsaas.nbmall.promote.rest.resource;

import com.nbsaas.nbmall.promote.api.apis.FullReductionApi;
import com.nbsaas.nbmall.promote.api.domain.list.FullReductionList;
import com.nbsaas.nbmall.promote.api.domain.page.FullReductionPage;
import com.nbsaas.nbmall.promote.api.domain.request.*;
import com.nbsaas.nbmall.promote.api.domain.response.FullReductionResponse;
import com.nbsaas.nbmall.promote.api.domain.simple.FullReductionItemSimple;
import com.nbsaas.nbmall.promote.data.dao.FullReductionDao;
import com.nbsaas.nbmall.promote.data.dao.FullReductionItemDao;
import com.nbsaas.nbmall.promote.data.entity.FullReduction;
import com.nbsaas.nbmall.promote.data.entity.FullReductionItem;
import com.nbsaas.nbmall.promote.rest.convert.FullReductionResponseConvert;
import com.nbsaas.nbmall.promote.rest.convert.FullReductionSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.nbsaas.nbmall.shop.data.dao.ShopDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class FullReductionResource implements FullReductionApi {

    @Autowired
    private FullReductionDao dataDao;

    @Autowired
    private ShopDao shopDao;

    @Autowired
    private FullReductionItemDao reductionItemDao;

    @Override
    public FullReductionResponse create(FullReductionDataRequest request) {
        FullReductionResponse result = new FullReductionResponse();

        FullReduction bean = new FullReduction();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        if (request.getItems() != null && request.getItems() != null) {
            for (FullReductionItemSimple item : request.getItems()) {
                if (item.getFullMoney() != null) {
                    FullReductionItem reductionItem = new FullReductionItem();
                    reductionItem.setFullMoney(item.getFullMoney());
                    reductionItem.setReduceMoney(item.getReduceMoney());
                    reductionItem.setReduction(bean);
                    reductionItem.setTenant(bean.getTenant());
                    reductionItemDao.save(reductionItem);
                }
            }
        }
        result = new FullReductionResponseConvert().conver(bean);
        return result;
    }

    @Override
    public FullReductionResponse update(FullReductionDataRequest request) {
        FullReductionResponse result = new FullReductionResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        FullReduction bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        List<FullReductionItem> values = bean.getItems();

        List<FullReductionItem> addList=new ArrayList<>();
        List<FullReductionItem> allList=new ArrayList<>();

        if (request.getItems() != null && request.getItems().size()>0) {
            for (FullReductionItemSimple attr : request.getItems()) {
                FullReductionItem item;
                if (attr.getId()==null) {
                    item = new FullReductionItem();
                    item.setReduction(bean);
                    item.setFullMoney(attr.getFullMoney());
                    item.setReduceMoney(attr.getReduceMoney());
                    item.setTenant(bean.getTenant());
                    addList.add(item);
                }else{
                    item = value(values, attr.getId());
                    if (item!=null){
                        item.setFullMoney(attr.getFullMoney());
                        item.setReduceMoney(attr.getReduceMoney());
                    }
                }
                allList.add(item);
            }
        }

        for (FullReductionItem dishAttrValue : addList) {
            reductionItemDao.save(dishAttrValue);
        }
        values.removeAll(allList);
        if (values.size()>0){
            for (FullReductionItem item : values) {
                reductionItemDao.delete(item);
            }
        }
        handleData(request, bean);
        result = new FullReductionResponseConvert().conver(bean);
        return result;
    }
    public FullReductionItem value(List<FullReductionItem> values,Long id){
        FullReductionItem result=null;
        if (values!=null&&values.size()>0){
            for (FullReductionItem value : values) {
                if (value.getId().equals(id)){
                    return value;
                }
            }
        }
        return result;
    }
    private void handleData(FullReductionDataRequest request, FullReduction bean) {
        TenantBeanUtils.copyProperties(request, bean);
        if (request.getShop() != null) {
            bean.setShop(shopDao.findById(request.getShop()));
        }

    }

    @Override
    public FullReductionResponse delete(FullReductionDataRequest request) {
        FullReductionResponse result = new FullReductionResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(), request.getId());
        return result;
    }

    @Override
    public FullReductionResponse view(FullReductionDataRequest request) {
        FullReductionResponse result = new FullReductionResponse();
        FullReduction bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean == null) {
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result = new FullReductionResponseConvert().conver(bean);
        return result;
    }

    @Override
    public FullReductionList list(FullReductionSearchRequest request) {
        FullReductionList result = new FullReductionList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())) {
            orders.add(Order.asc("" + request.getSortField()));
        } else if ("desc".equals(request.getSortMethod())) {
            orders.add(Order.desc("" + request.getSortField()));
        } else {
            orders.add(Order.desc("id"));
        }
        List<FullReduction> organizations = dataDao.list(0, request.getSize(), filters, orders);

        FullReductionSimpleConvert convert = new FullReductionSimpleConvert();
        ConverResourceUtils.converList(result, organizations, convert);
        return result;
    }

    @Override
    public FullReductionPage search(FullReductionSearchRequest request) {
        FullReductionPage result = new FullReductionPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())) {
            pageable.getOrders().add(Order.asc("" + request.getSortField()));
        } else if ("desc".equals(request.getSortMethod())) {
            pageable.getOrders().add(Order.desc("" + request.getSortField()));
        } else {
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<FullReduction> page = dataDao.page(pageable);

        FullReductionSimpleConvert convert = new FullReductionSimpleConvert();
        ConverResourceUtils.converPage(result, page, convert);
        return result;
    }
}
