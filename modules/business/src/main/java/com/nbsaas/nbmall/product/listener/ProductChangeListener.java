package com.nbsaas.nbmall.product.listener;


import com.nbsaas.nbmall.product.data.entity.Product;

public interface ProductChangeListener extends Comparable<ProductChangeListener> {

    /**
     * @param product 商品
     */
    void create(Product product);

    /**
     * 排序号
     *
     * @return
     */
    int sortNum();
}
