package com.nbsaas.nbmall.promote.api.domain.list;


import com.nbsaas.nbmall.promote.api.domain.simple.CouponSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年12月25日12:45:48.
*/

@Data
public class CouponList  extends ResponseList<CouponSimple> {

}