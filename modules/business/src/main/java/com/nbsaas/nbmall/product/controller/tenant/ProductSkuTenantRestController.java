package com.nbsaas.nbmall.product.controller.tenant;

import com.nbsaas.nbmall.product.api.apis.ProductSkuApi;
import com.nbsaas.nbmall.product.api.domain.list.ProductSkuList;
import com.nbsaas.nbmall.product.api.domain.page.ProductSkuPage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductSkuResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/productsku")
@RestController
public class ProductSkuTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("productsku")
    @RequestMapping("create")
    public ProductSkuResponse create(ProductSkuDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

	@RequiresPermissions("productsku")
    @RequestMapping("delete")
    public ProductSkuResponse delete(ProductSkuDataRequest request) {
        initTenant(request);
        ProductSkuResponse result = new ProductSkuResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("productsku")
    @RequestMapping("update")
    public ProductSkuResponse update(ProductSkuDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("productsku")
    @RequestMapping("view")
    public ProductSkuResponse view(ProductSkuDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("productsku")
    @RequestMapping("list")
    public ProductSkuList list(ProductSkuSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("productsku")
    @RequestMapping("search")
    public ProductSkuPage search(ProductSkuSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private ProductSkuApi api;

}
