package com.nbsaas.nbmall.promote.rest.convert;

import com.nbsaas.nbmall.promote.api.domain.response.FullReductionItemResponse;
import com.nbsaas.nbmall.promote.data.entity.FullReductionItem;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class FullReductionItemResponseConvert implements Conver<FullReductionItemResponse, FullReductionItem> {
    @Override
    public FullReductionItemResponse conver(FullReductionItem source) {
        FullReductionItemResponse result = new FullReductionItemResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getReduction()!=null){
           result.setReduction(source.getReduction().getId());
        }
         if(source.getReduction()!=null){
            result.setReductionName(source.getReduction().getName());
         }

        return result;
    }
}
