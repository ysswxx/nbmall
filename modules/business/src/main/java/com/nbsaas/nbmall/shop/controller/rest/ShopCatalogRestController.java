package com.nbsaas.nbmall.shop.controller.rest;

import com.nbsaas.nbmall.shop.api.apis.ShopCatalogApi;
import com.nbsaas.nbmall.shop.api.domain.list.ShopCatalogList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopCatalogPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopCatalogResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/shopcatalog")
@RestController
public class ShopCatalogRestController extends BaseRestController {


    @RequestMapping("create")
    public ShopCatalogResponse create(ShopCatalogDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

    @RequestMapping("delete")
    public ShopCatalogResponse delete(ShopCatalogDataRequest request) {
        initTenant(request);
        ShopCatalogResponse result = new ShopCatalogResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public ShopCatalogResponse update(ShopCatalogDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public ShopCatalogResponse view(ShopCatalogDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public ShopCatalogList list(ShopCatalogSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public ShopCatalogPage search(ShopCatalogSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private ShopCatalogApi api;

}
