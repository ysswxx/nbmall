package com.nbsaas.nbmall.statistics.api.domain.request;

import com.haoxuer.bigworld.member.api.domain.request.TenantPageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月23日11:45:35.
*/

@Data
public class VisitPageSearchRequest extends TenantPageRequest {



}