package com.nbsaas.nbmall.product.data.entity;

import com.haoxuer.bigworld.member.data.entity.TenantUser;
import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.*;
import lombok.Data;

import javax.persistence.*;
import java.util.List;


@CreateByUser
@ComposeView
@Data
@FormAnnotation(title = "商品规格", model = "商品规格",menu = "1,107,108")
@Entity
@Table(name = "bs_tenant_product_spec")
public class ProductSpec extends TenantEntity {

    @FieldConvert
    @FieldName
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;


    @SearchItem(label = "规格名称",name = "name")
    @FormField(title = "规格名称", sortNum = "2", grid = true, col = 12,required = true)
    private String name;


    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private TenantUser creator;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "productSpec")
    private List<ProductSpecValue> values;
}
