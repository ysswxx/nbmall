package com.nbsaas.nbmall.video.rest.resource;

import com.nbsaas.nbmall.video.api.apis.VideoCatalogApi;
import com.nbsaas.nbmall.video.api.domain.list.VideoCatalogList;
import com.nbsaas.nbmall.video.api.domain.page.VideoCatalogPage;
import com.nbsaas.nbmall.video.api.domain.request.*;
import com.nbsaas.nbmall.video.api.domain.response.VideoCatalogResponse;
import com.nbsaas.nbmall.video.data.dao.VideoCatalogDao;
import com.nbsaas.nbmall.video.data.entity.VideoCatalog;
import com.nbsaas.nbmall.video.rest.convert.VideoCatalogResponseConvert;
import com.nbsaas.nbmall.video.rest.convert.VideoCatalogSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.nbsaas.nbmall.video.data.dao.VideoCatalogDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class VideoCatalogResource implements VideoCatalogApi {

    @Autowired
    private VideoCatalogDao dataDao;

    @Autowired
    private VideoCatalogDao parentDao;


    @Override
    public VideoCatalogResponse create(VideoCatalogDataRequest request) {
        VideoCatalogResponse result = new VideoCatalogResponse();

        VideoCatalog bean = new VideoCatalog();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new VideoCatalogResponseConvert().conver(bean);
        return result;
    }

    @Override
    public VideoCatalogResponse update(VideoCatalogDataRequest request) {
        VideoCatalogResponse result = new VideoCatalogResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        VideoCatalog bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new VideoCatalogResponseConvert().conver(bean);
        return result;
    }

    private void handleData(VideoCatalogDataRequest request, VideoCatalog bean) {
       TenantBeanUtils.copyProperties(request,bean);
           if(request.getParent()!=null){
              bean.setParent(parentDao.findById(request.getParent()));
           }

    }

    @Override
    public VideoCatalogResponse delete(VideoCatalogDataRequest request) {
        VideoCatalogResponse result = new VideoCatalogResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public VideoCatalogResponse view(VideoCatalogDataRequest request) {
        VideoCatalogResponse result=new VideoCatalogResponse();
        VideoCatalog bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new VideoCatalogResponseConvert().conver(bean);
        return result;
    }
    @Override
    public VideoCatalogList list(VideoCatalogSearchRequest request) {
        VideoCatalogList result = new VideoCatalogList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<VideoCatalog> organizations = dataDao.list(0, request.getSize(), filters, orders);

        VideoCatalogSimpleConvert convert=new VideoCatalogSimpleConvert();
        convert.setFetch(request.getFetch());
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public VideoCatalogPage search(VideoCatalogSearchRequest request) {
        VideoCatalogPage result=new VideoCatalogPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<VideoCatalog> page=dataDao.page(pageable);

        VideoCatalogSimpleConvert convert=new VideoCatalogSimpleConvert();
        convert.setFetch(request.getFetch());
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
