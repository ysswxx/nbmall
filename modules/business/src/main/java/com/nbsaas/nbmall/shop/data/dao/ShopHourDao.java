package com.nbsaas.nbmall.shop.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.shop.data.entity.ShopHour;

/**
* Created by imake on 2021年12月10日17:38:01.
*/
public interface ShopHourDao extends BaseDao<ShopHour,Long>{

	 ShopHour findById(Long id);

	 ShopHour save(ShopHour bean);

	 ShopHour updateByUpdater(Updater<ShopHour> updater);

	 ShopHour deleteById(Long id);

	 ShopHour findById(Long tenant, Long id);

     ShopHour deleteById(Long tenant, Long id);
}