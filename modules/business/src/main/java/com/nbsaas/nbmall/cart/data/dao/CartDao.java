package com.nbsaas.nbmall.cart.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.cart.data.entity.Cart;

/**
* Created by imake on 2021年12月30日22:38:35.
*/
public interface CartDao extends BaseDao<Cart,Long>{

	 Cart findById(Long id);

	 Cart save(Cart bean);

	 Cart updateByUpdater(Updater<Cart> updater);

	 Cart deleteById(Long id);

	 Cart findById(Long tenant, Long id);

     Cart deleteById(Long tenant, Long id);
}