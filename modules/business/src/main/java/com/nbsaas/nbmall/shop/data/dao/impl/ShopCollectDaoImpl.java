package com.nbsaas.nbmall.shop.data.dao.impl;

import com.haoxuer.discover.data.utils.DateUtils;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.shop.data.dao.ShopCollectDao;
import com.nbsaas.nbmall.shop.data.entity.ShopCollect;

import java.util.Date;

/**
* Created by imake on 2021年12月10日17:38:00.
*/
@Repository

public class ShopCollectDaoImpl extends CriteriaDaoImpl<ShopCollect, Long> implements ShopCollectDao {

	@Override
	public ShopCollect findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public ShopCollect save(ShopCollect bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public ShopCollect deleteById(Long id) {
		ShopCollect entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<ShopCollect> getEntityClass() {
		return ShopCollect.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public ShopCollect findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public ShopCollect deleteById(Long tenant,Long id) {
		ShopCollect entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Long dayNum(Long id, Date date) {
		Date begin = DateUtils.begin(date);
		Date end = DateUtils.end(date);
		Filter beginFilter = Filter.ge("addDate", begin);
		beginFilter.setPrefix("begin");
		Filter endFilter = Filter.ne("addDate", end);
		endFilter.setPrefix("end");

		Long num = handle("count", "id", Filter.eq("shop.id", id), beginFilter, endFilter);
		if (num == null) {
			num = 0L;
		}
		return num;
	}

	@Override
	public Long dayNumAll(Long tenant,Date date) {
		Date begin = DateUtils.begin(date);
		Date end = DateUtils.end(date);
		Filter beginFilter = Filter.ge("addDate", begin);
		beginFilter.setPrefix("begin");
		Filter endFilter = Filter.ne("addDate", end);
		endFilter.setPrefix("end");

		Long num = handle("count", "id",
				Filter.eq("tenant.id",tenant),
				beginFilter, endFilter);
		if (num == null) {
			num = 0L;
		}
		return num;
	}
}