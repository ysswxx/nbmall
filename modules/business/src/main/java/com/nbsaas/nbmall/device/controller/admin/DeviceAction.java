package com.nbsaas.nbmall.device.controller.admin;


import com.haoxuer.bigworld.tenant.controller.admin.TenantBaseAction;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
*
* Created by imake on 2021年12月10日17:38:01.
*/


@Scope("prototype")
@Controller
public class DeviceAction extends TenantBaseAction {

	@RequiresPermissions("device")
	@RequestMapping("/tenant/device/view_list")
	public String list(ModelMap model) {
		return getView("device/list");
	}

}