package com.nbsaas.nbmall.video.rest.convert;

import com.nbsaas.nbmall.video.api.domain.response.VideoResponse;
import com.nbsaas.nbmall.video.data.entity.Video;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class VideoResponseConvert implements Conver<VideoResponse, Video> {
    @Override
    public VideoResponse conver(Video source) {
        VideoResponse result = new VideoResponse();
        TenantBeanUtils.copyProperties(source,result);


        return result;
    }
}
