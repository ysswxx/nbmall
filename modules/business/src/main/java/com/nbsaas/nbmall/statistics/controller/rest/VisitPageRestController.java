package com.nbsaas.nbmall.statistics.controller.rest;

import com.nbsaas.nbmall.statistics.api.apis.VisitPageApi;
import com.nbsaas.nbmall.statistics.api.domain.list.VisitPageList;
import com.nbsaas.nbmall.statistics.api.domain.page.VisitPagePage;
import com.nbsaas.nbmall.statistics.api.domain.request.*;
import com.nbsaas.nbmall.statistics.api.domain.response.VisitPageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/visitpage")
@RestController
public class VisitPageRestController extends BaseRestController {


    @RequestMapping("visit")
    public VisitPageResponse visit(VisitPageDataRequest request) {
        initTenant(request);
        return api.visit(request);
    }

    @Autowired
    private VisitPageApi api;

}
