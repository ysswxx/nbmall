package com.nbsaas.nbmall.order.rest.resource;

import com.nbsaas.nbmall.order.api.apis.OrderItemApi;
import com.nbsaas.nbmall.order.api.domain.list.OrderItemList;
import com.nbsaas.nbmall.order.api.domain.page.OrderItemPage;
import com.nbsaas.nbmall.order.api.domain.request.*;
import com.nbsaas.nbmall.order.api.domain.response.OrderItemResponse;
import com.nbsaas.nbmall.order.data.dao.OrderItemDao;
import com.nbsaas.nbmall.order.data.entity.OrderItem;
import com.nbsaas.nbmall.order.rest.convert.OrderItemResponseConvert;
import com.nbsaas.nbmall.order.rest.convert.OrderItemSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class OrderItemResource implements OrderItemApi {

    @Autowired
    private OrderItemDao dataDao;



    @Override
    public OrderItemResponse create(OrderItemDataRequest request) {
        OrderItemResponse result = new OrderItemResponse();

        OrderItem bean = new OrderItem();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new OrderItemResponseConvert().conver(bean);
        return result;
    }

    @Override
    public OrderItemResponse update(OrderItemDataRequest request) {
        OrderItemResponse result = new OrderItemResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        OrderItem bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new OrderItemResponseConvert().conver(bean);
        return result;
    }

    private void handleData(OrderItemDataRequest request, OrderItem bean) {
       TenantBeanUtils.copyProperties(request,bean);

    }

    @Override
    public OrderItemResponse delete(OrderItemDataRequest request) {
        OrderItemResponse result = new OrderItemResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public OrderItemResponse view(OrderItemDataRequest request) {
        OrderItemResponse result=new OrderItemResponse();
        OrderItem bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new OrderItemResponseConvert().conver(bean);
        return result;
    }
    @Override
    public OrderItemList list(OrderItemSearchRequest request) {
        OrderItemList result = new OrderItemList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<OrderItem> organizations = dataDao.list(0, request.getSize(), filters, orders);

        OrderItemSimpleConvert convert=new OrderItemSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public OrderItemPage search(OrderItemSearchRequest request) {
        OrderItemPage result=new OrderItemPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<OrderItem> page=dataDao.page(pageable);

        OrderItemSimpleConvert convert=new OrderItemSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
