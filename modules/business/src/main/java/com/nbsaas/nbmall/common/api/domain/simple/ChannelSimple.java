package com.nbsaas.nbmall.common.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by BigWorld on 2021年12月10日15:46:16.
*/
@Data
public class ChannelSimple implements Serializable {

    private Long id;

     private String note;
     private Long creator;
     private String logo;
     private String creatorName;
     private String name;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private String url;


}
