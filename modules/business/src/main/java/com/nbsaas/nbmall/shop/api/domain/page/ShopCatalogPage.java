package com.nbsaas.nbmall.shop.api.domain.page;


import com.nbsaas.nbmall.shop.api.domain.simple.ShopCatalogSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年12月10日17:38:00.
*/

@Data
public class ShopCatalogPage  extends ResponsePage<ShopCatalogSimple> {

}