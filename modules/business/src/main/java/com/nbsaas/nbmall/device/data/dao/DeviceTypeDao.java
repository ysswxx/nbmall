package com.nbsaas.nbmall.device.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.device.data.entity.DeviceType;

/**
* Created by imake on 2021年12月10日17:40:16.
*/
public interface DeviceTypeDao extends BaseDao<DeviceType,Long>{

	 DeviceType findById(Long id);

	 DeviceType save(DeviceType bean);

	 DeviceType updateByUpdater(Updater<DeviceType> updater);

	 DeviceType deleteById(Long id);
}