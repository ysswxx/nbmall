package com.nbsaas.nbmall.product.rest.convert;

import com.nbsaas.nbmall.product.api.domain.response.ProductSpecResponse;
import com.nbsaas.nbmall.product.data.entity.ProductSpec;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class ProductSpecResponseConvert implements Conver<ProductSpecResponse, ProductSpec> {
    @Override
    public ProductSpecResponse conver(ProductSpec source) {
        ProductSpecResponse result = new ProductSpecResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getProduct()!=null){
           result.setProduct(source.getProduct().getId());
        }
        if(source.getCreator()!=null){
           result.setCreator(source.getCreator().getId());
        }
         if(source.getProduct()!=null){
            result.setProductName(source.getProduct().getName());
         }

        return result;
    }
}
