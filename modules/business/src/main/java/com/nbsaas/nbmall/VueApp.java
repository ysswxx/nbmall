package com.nbsaas.nbmall;

import com.haoxuer.bigworld.article.data.entity.Article;
import com.haoxuer.bigworld.article.data.entity.ArticleCatalog;
import com.haoxuer.bigworld.gather.data.entity.GatherSql;
import com.haoxuer.bigworld.generate.template.hibernate.BigWorldHibernateDir;
import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.templates.vue.VueDir;
import com.nbsaas.nbmall.product.data.entity.Product;
import com.nbsaas.nbmall.product.data.entity.ProductGroup;
import com.nbsaas.nbmall.promote.data.entity.CouponRule;
import com.nbsaas.nbmall.promote.data.entity.FullReduction;

import java.io.File;

public class VueApp {
    public static void main(String[] args) {

        vue().make(FullReduction.class);
    }
    private static CodeMake vue() {
        CodeMake make = vueMake();
        make.setDao(false);
        make.setService(false);
        make.setAction(false);
        make.setApi(false);
        make.setRest(false);
        make.setView(true);
        make.put("restDomain",false);
        make.put("componentList",true);

        return make;
    }
    private static CodeMake vueMake() {
        CodeMake make = new CodeMake(VueDir.class, BigWorldHibernateDir.class);
        File view = new File("E:\\codes\\vue\\\\nbmall_mg_vue\\src\\views");
        make.setView(view);
        make.setCodeStyle("vue");
        make.put("theme","default");
        return make;
    }
}
