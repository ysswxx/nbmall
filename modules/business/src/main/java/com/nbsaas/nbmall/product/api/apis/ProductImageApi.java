package com.nbsaas.nbmall.product.api.apis;


import com.nbsaas.nbmall.product.api.domain.list.ProductImageList;
import com.nbsaas.nbmall.product.api.domain.page.ProductImagePage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductImageResponse;

public interface ProductImageApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    ProductImageResponse create(ProductImageDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    ProductImageResponse update(ProductImageDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    ProductImageResponse delete(ProductImageDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     ProductImageResponse view(ProductImageDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    ProductImageList list(ProductImageSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    ProductImagePage search(ProductImageSearchRequest request);

}