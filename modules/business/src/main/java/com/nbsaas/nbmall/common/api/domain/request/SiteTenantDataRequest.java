package com.nbsaas.nbmall.common.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;
import com.haoxuer.discover.data.enums.StoreState;
import java.util.Date;

/**
*
* Created by imake on 2021年12月23日11:54:02.
*/

@Data
public class SiteTenantDataRequest extends BaseRequest {

    private Long id;

     private Long tradeAccount;

     private String note;

     private String address;

     private Double lng;

     private Long creator;

     private StoreState storeState;

     private String shopStyle;

     private String demo;

     private String domain;

     private Date beginDate;

     private String phone;

     private String logo;

     private String chatSign;

     private String name;

     private String theme;

     private Date expireDate;

     private String key;

     private Double lat;


}