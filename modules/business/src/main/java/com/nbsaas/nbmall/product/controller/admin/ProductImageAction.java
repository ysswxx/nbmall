package com.nbsaas.nbmall.product.controller.admin;


import com.haoxuer.bigworld.tenant.controller.admin.TenantBaseAction;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
*
* Created by imake on 2021年12月28日18:04:52.
*/


@Scope("prototype")
@Controller
public class ProductImageAction extends TenantBaseAction {

	@RequiresPermissions("productimage")
	@RequestMapping("/tenant/productimage/view_list")
	public String list(ModelMap model) {
		return getView("productimage/list");
	}

}