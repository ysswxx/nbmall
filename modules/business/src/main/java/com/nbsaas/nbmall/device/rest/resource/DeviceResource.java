package com.nbsaas.nbmall.device.rest.resource;

import com.nbsaas.nbmall.device.api.apis.DeviceApi;
import com.nbsaas.nbmall.device.api.domain.list.DeviceList;
import com.nbsaas.nbmall.device.api.domain.page.DevicePage;
import com.nbsaas.nbmall.device.api.domain.request.*;
import com.nbsaas.nbmall.device.api.domain.response.DeviceResponse;
import com.nbsaas.nbmall.device.data.dao.DeviceDao;
import com.nbsaas.nbmall.device.data.entity.Device;
import com.nbsaas.nbmall.device.rest.convert.DeviceResponseConvert;
import com.nbsaas.nbmall.device.rest.convert.DeviceSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.haoxuer.bigworld.member.data.dao.TenantUserDao;
import com.nbsaas.nbmall.shop.data.dao.ShopDao;
import com.nbsaas.nbmall.device.data.dao.DeviceTypeDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class DeviceResource implements DeviceApi {

    @Autowired
    private DeviceDao dataDao;

    @Autowired
    private TenantUserDao creatorDao;
    @Autowired
    private ShopDao shopDao;
    @Autowired
    private DeviceTypeDao deviceTypeDao;


    @Override
    public DeviceResponse create(DeviceDataRequest request) {
        DeviceResponse result = new DeviceResponse();

        Device bean = new Device();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new DeviceResponseConvert().conver(bean);
        return result;
    }

    @Override
    public DeviceResponse update(DeviceDataRequest request) {
        DeviceResponse result = new DeviceResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        Device bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new DeviceResponseConvert().conver(bean);
        return result;
    }

    private void handleData(DeviceDataRequest request, Device bean) {
       TenantBeanUtils.copyProperties(request,bean);
           if(request.getShop()!=null){
              bean.setShop(shopDao.findById(request.getShop()));
           }
           if(request.getCreator()!=null){
              bean.setCreator(creatorDao.findById(request.getCreator()));
           }
           if(request.getDeviceType()!=null){
              bean.setDeviceType(deviceTypeDao.findById(request.getDeviceType()));
           }

    }

    @Override
    public DeviceResponse delete(DeviceDataRequest request) {
        DeviceResponse result = new DeviceResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public DeviceResponse view(DeviceDataRequest request) {
        DeviceResponse result=new DeviceResponse();
        Device bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new DeviceResponseConvert().conver(bean);
        return result;
    }
    @Override
    public DeviceList list(DeviceSearchRequest request) {
        DeviceList result = new DeviceList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<Device> organizations = dataDao.list(0, request.getSize(), filters, orders);

        DeviceSimpleConvert convert=new DeviceSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public DevicePage search(DeviceSearchRequest request) {
        DevicePage result=new DevicePage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<Device> page=dataDao.page(pageable);

        DeviceSimpleConvert convert=new DeviceSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
