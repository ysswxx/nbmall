package com.nbsaas.nbmall.product.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
/**
*
* Created by BigWorld on 2021年12月28日18:04:52.
*/

@Data
public class ProductImageResponse extends ResponseObject {

    private Long id;

     private Long product;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private String url;

}