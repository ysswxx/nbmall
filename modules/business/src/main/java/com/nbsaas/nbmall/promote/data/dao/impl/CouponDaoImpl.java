package com.nbsaas.nbmall.promote.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.promote.data.dao.CouponDao;
import com.nbsaas.nbmall.promote.data.entity.Coupon;

/**
* Created by imake on 2021年12月25日12:45:48.
*/
@Repository

public class CouponDaoImpl extends CriteriaDaoImpl<Coupon, Long> implements CouponDao {

	@Override
	public Coupon findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public Coupon save(Coupon bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public Coupon deleteById(Long id) {
		Coupon entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<Coupon> getEntityClass() {
		return Coupon.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public Coupon findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public Coupon deleteById(Long tenant,Long id) {
		Coupon entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}