package com.nbsaas.nbmall.order.rest.convert;

import com.nbsaas.nbmall.order.api.domain.simple.OrderRefundSimple;
import com.nbsaas.nbmall.order.data.entity.OrderRefund;
import com.haoxuer.discover.data.rest.core.Conver;
public class OrderRefundSimpleConvert implements Conver<OrderRefundSimple, OrderRefund> {


    @Override
    public OrderRefundSimple conver(OrderRefund source) {
        OrderRefundSimple result = new OrderRefundSimple();

            result.setId(source.getId());
             result.setNo(source.getNo());
             result.setRefundState(source.getRefundState());
             result.setShopMoney(source.getShopMoney());
            if(source.getOrderForm()!=null){
               result.setOrderForm(source.getOrderForm().getId());
            }
            if(source.getCreator()!=null){
               result.setCreator(source.getCreator().getId());
            }
             result.setPlatformState(source.getPlatformState());
             result.setReason(source.getReason());
             result.setShopState(source.getShopState());
             if(source.getCreator()!=null){
                result.setCreatorName(source.getCreator().getName());
             }
             result.setMoney(source.getMoney());
             result.setAddDate(source.getAddDate());
             result.setPlatformMoney(source.getPlatformMoney());

             result.setShopStateName(source.getShopState()+"");
             result.setRefundStateName(source.getRefundState()+"");
             result.setPlatformStateName(source.getPlatformState()+"");
        return result;
    }
}
