package com.nbsaas.nbmall.order.api.apis;


import com.nbsaas.nbmall.order.api.domain.list.OrderFormList;
import com.nbsaas.nbmall.order.api.domain.page.OrderFormPage;
import com.nbsaas.nbmall.order.api.domain.request.*;
import com.nbsaas.nbmall.order.api.domain.response.OrderFormResponse;

public interface OrderFormApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    OrderFormResponse create(OrderFormDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    OrderFormResponse update(OrderFormDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    OrderFormResponse delete(OrderFormDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     OrderFormResponse view(OrderFormDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    OrderFormList list(OrderFormSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    OrderFormPage search(OrderFormSearchRequest request);

}