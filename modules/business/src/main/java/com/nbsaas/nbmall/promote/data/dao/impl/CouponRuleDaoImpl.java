package com.nbsaas.nbmall.promote.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.promote.data.dao.CouponRuleDao;
import com.nbsaas.nbmall.promote.data.entity.CouponRule;

/**
* Created by imake on 2021年12月28日23:18:26.
*/
@Repository

public class CouponRuleDaoImpl extends CriteriaDaoImpl<CouponRule, Long> implements CouponRuleDao {

	@Override
	public CouponRule findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public CouponRule save(CouponRule bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public CouponRule deleteById(Long id) {
		CouponRule entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<CouponRule> getEntityClass() {
		return CouponRule.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public CouponRule findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public CouponRule deleteById(Long tenant,Long id) {
		CouponRule entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}