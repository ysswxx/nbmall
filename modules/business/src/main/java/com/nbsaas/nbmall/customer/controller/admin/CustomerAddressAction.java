package com.nbsaas.nbmall.customer.controller.admin;


import com.haoxuer.bigworld.tenant.controller.admin.TenantBaseAction;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
*
* Created by imake on 2021年12月10日22:56:19.
*/


@Scope("prototype")
@Controller
public class CustomerAddressAction extends TenantBaseAction {

	@RequiresPermissions("customeraddress")
	@RequestMapping("/tenant/customeraddress/view_list")
	public String list(ModelMap model) {
		return getView("customeraddress/list");
	}

}