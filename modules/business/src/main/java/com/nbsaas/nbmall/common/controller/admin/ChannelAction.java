package com.nbsaas.nbmall.common.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import org.springframework.context.annotation.Scope;
import com.haoxuer.discover.controller.BaseAction;
/**
*
* Created by imake on 2021年12月10日15:46:15.
*/

@Scope("prototype")
@Controller
public class ChannelAction extends BaseAction{


	@RequiresPermissions("channel")
	@RequestMapping("/admin/channel/view_list")
	public String list() {
		return getView("channel/list");
	}

}