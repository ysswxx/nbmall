package com.nbsaas.nbmall.shop.data.entity;

import com.haoxuer.bigworld.tenant.data.entity.TenantCatalogEntity;
import com.nbsaas.codemake.annotation.*;
import lombok.Data;

import javax.persistence.*;
import java.util.List;


@CatalogClass
@ComposeView
@Data
@FormAnnotation(title = "商家分类管理", model = "商家分类", menu = "1,101,103")
@Entity
@Table(name = "bs_tenant_shop_catalog")
public class ShopCatalog extends TenantCatalogEntity {

    @FieldName
    @FieldConvert(classType = "Integer")
    @ManyToOne(fetch = FetchType.LAZY)
    private ShopCatalog parent;


    @OneToMany(fetch =  FetchType.LAZY,mappedBy = "parent")
    private List<ShopCatalog> children;

    @Override
    public Integer getParentId() {
        if (parent!=null){
            return parent.getId();
        }
        return null;
    }
}
