package com.nbsaas.nbmall.shop.data.entity;

import com.haoxuer.bigworld.member.data.entity.TenantUser;
import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.haoxuer.discover.area.data.entity.Area;
import com.nbsaas.codemake.annotation.*;
import lombok.Data;

import javax.persistence.*;

@CreateByUser
@ComposeView
@Data
@FormAnnotation(title = "门店管理", model = "门店", menu = "1,101,122")
@Entity
@Table(name = "bs_tenant_shop_store")
public class ShopStore extends TenantEntity {


    @SearchItem(label = "商家名称",name = "shop",key = "shop.id",classType = "Long",type = InputType.select)
    @FormField(title = "商家名称",  grid = true, type = InputType.select,option = "shop",width = "200")
    @FieldConvert
    @FieldName
    @ManyToOne(fetch = FetchType.LAZY)
    private Shop shop;


    @FormField(title = "门头", grid = false, type = InputType.image,col = 24)
    private String logo;


    @SearchItem(label = "门店名称",name = "name")
    @FormField(title = "门店名称", grid = true, required = true,col = 24)
    @Column(length = 20)
    private String name;

    @FormField(title = "联系电话", grid = true,col = 24)
    private String phone;


    @FormField(title = "详细地址", grid = true,col = 24,width = "1000")
    private String address;

    private Double lat;

    private Double lng;

    @FieldConvert(classType = "Integer")
    @ManyToOne(fetch = FetchType.LAZY)
    private Area province;

    @FieldConvert(classType = "Integer")
    @ManyToOne(fetch = FetchType.LAZY)
    private Area city;

    @FieldConvert(classType = "Integer")
    @ManyToOne(fetch = FetchType.LAZY)
    private Area area;

    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private TenantUser creator;


}
