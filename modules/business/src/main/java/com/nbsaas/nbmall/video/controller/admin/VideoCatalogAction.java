package com.nbsaas.nbmall.video.controller.admin;


import com.haoxuer.bigworld.tenant.controller.admin.TenantBaseAction;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
*
* Created by codeMake on 2022年03月16日21:25:37.
*/


@Scope("prototype")
@Controller
public class VideoCatalogAction extends TenantBaseAction {

	@RequiresPermissions("videocatalog")
	@RequestMapping("/tenant/videocatalog/view_list")
	public String list(ModelMap model) {
		return getView("videocatalog/list");
	}

}