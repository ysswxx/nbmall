package com.nbsaas.nbmall.shop.rest.convert;

import com.nbsaas.nbmall.shop.api.domain.response.ShopStoreResponse;
import com.nbsaas.nbmall.shop.data.entity.ShopStore;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class ShopStoreResponseConvert implements Conver<ShopStoreResponse, ShopStore> {
    @Override
    public ShopStoreResponse conver(ShopStore source) {
        ShopStoreResponse result = new ShopStoreResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getShop()!=null){
           result.setShop(source.getShop().getId());
        }
        if(source.getArea()!=null){
           result.setArea(source.getArea().getId());
        }
        if(source.getCity()!=null){
           result.setCity(source.getCity().getId());
        }
        if(source.getCreator()!=null){
           result.setCreator(source.getCreator().getId());
        }
         if(source.getShop()!=null){
            result.setShopName(source.getShop().getName());
         }
        if(source.getProvince()!=null){
           result.setProvince(source.getProvince().getId());
        }

        return result;
    }
}
