package com.nbsaas.nbmall.statistics.controller.rest;

import com.nbsaas.nbmall.statistics.api.apis.VisitPageDayApi;
import com.nbsaas.nbmall.statistics.api.domain.list.VisitPageDayList;
import com.nbsaas.nbmall.statistics.api.domain.page.VisitPageDayPage;
import com.nbsaas.nbmall.statistics.api.domain.request.*;
import com.nbsaas.nbmall.statistics.api.domain.response.VisitPageDayResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/visitpageday")
@RestController
public class VisitPageDayRestController extends BaseRestController {


    @RequestMapping("create")
    public VisitPageDayResponse create(VisitPageDayDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

    @RequestMapping("delete")
    public VisitPageDayResponse delete(VisitPageDayDataRequest request) {
        initTenant(request);
        VisitPageDayResponse result = new VisitPageDayResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public VisitPageDayResponse update(VisitPageDayDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public VisitPageDayResponse view(VisitPageDayDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public VisitPageDayList list(VisitPageDaySearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public VisitPageDayPage search(VisitPageDaySearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private VisitPageDayApi api;

}
