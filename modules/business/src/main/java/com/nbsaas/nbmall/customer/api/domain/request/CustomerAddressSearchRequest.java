package com.nbsaas.nbmall.customer.api.domain.request;

import com.haoxuer.bigworld.member.api.domain.request.TenantPageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月10日22:56:19.
*/

@Data
public class CustomerAddressSearchRequest extends TenantPageRequest {

    //省份
     @Search(name = "province.id",operator = Filter.Operator.eq)
     private Integer province;

    //城市
     @Search(name = "city.id",operator = Filter.Operator.eq)
     private Integer city;

    //区县
     @Search(name = "area.id",operator = Filter.Operator.eq)
     private Integer area;

    //用户
     @Search(name = "customer.id",operator = Filter.Operator.eq)
     private Long customer;



}