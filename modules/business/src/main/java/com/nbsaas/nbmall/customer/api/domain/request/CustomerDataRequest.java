package com.nbsaas.nbmall.customer.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;

import java.util.Date;
import java.math.BigDecimal;

/**
 * Created by imake on 2021年12月10日22:56:19.
 */

@Data
public class CustomerDataRequest extends TenantRequest {

    private Long id;

    private String no;
    private Integer area;
    private String note;
    private String address;
    private Integer city;
    private Long manager;
    private Double lng;
    private Long visitNum;
    private String avatar;
    private String mobile;
    private Integer province;
    private Long addressId;
    private String phone;
    private String name;
    private String tel;
    private Double lat;
    private Long couponNum;
    private BigDecimal score;

    private String password;

}