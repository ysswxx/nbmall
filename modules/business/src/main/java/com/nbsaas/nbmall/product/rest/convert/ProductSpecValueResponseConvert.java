package com.nbsaas.nbmall.product.rest.convert;

import com.nbsaas.nbmall.product.api.domain.response.ProductSpecValueResponse;
import com.nbsaas.nbmall.product.data.entity.ProductSpecValue;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class ProductSpecValueResponseConvert implements Conver<ProductSpecValueResponse, ProductSpecValue> {
    @Override
    public ProductSpecValueResponse conver(ProductSpecValue source) {
        ProductSpecValueResponse result = new ProductSpecValueResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getProductSpec()!=null){
           result.setProductSpec(source.getProductSpec().getId());
        }

        return result;
    }
}
