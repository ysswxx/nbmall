package com.nbsaas.nbmall.order.api.domain.list;


import com.nbsaas.nbmall.order.api.domain.simple.OrderItemSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年12月10日17:38:01.
*/

@Data
public class OrderItemList  extends ResponseList<OrderItemSimple> {

}