package com.nbsaas.nbmall.product.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by BigWorld on 2021年12月10日17:38:01.
*/
@Data
public class FreightTemplateSimple implements Serializable {

    private Long id;

     private String name;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;


}
