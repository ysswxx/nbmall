package com.nbsaas.nbmall.shop.api.domain.page;


import com.nbsaas.nbmall.shop.api.domain.simple.ShopCashSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年12月10日17:38:01.
*/

@Data
public class ShopCashPage  extends ResponsePage<ShopCashSimple> {

}