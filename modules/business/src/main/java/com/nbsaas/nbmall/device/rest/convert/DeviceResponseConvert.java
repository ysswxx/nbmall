package com.nbsaas.nbmall.device.rest.convert;

import com.nbsaas.nbmall.device.api.domain.response.DeviceResponse;
import com.nbsaas.nbmall.device.data.entity.Device;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class DeviceResponseConvert implements Conver<DeviceResponse, Device> {
    @Override
    public DeviceResponse conver(Device source) {
        DeviceResponse result = new DeviceResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getShop()!=null){
           result.setShop(source.getShop().getId());
        }
        if(source.getCreator()!=null){
           result.setCreator(source.getCreator().getId());
        }
         if(source.getShop()!=null){
            result.setShopName(source.getShop().getName());
         }
         if(source.getDeviceType()!=null){
            result.setDeviceTypeName(source.getDeviceType().getName());
         }
         if(source.getCreator()!=null){
            result.setCreatorName(source.getCreator().getName());
         }
        if(source.getDeviceType()!=null){
           result.setDeviceType(source.getDeviceType().getId());
        }

        return result;
    }
}
