package com.nbsaas.nbmall.shop.api.domain.request;

import com.haoxuer.bigworld.member.api.domain.request.TenantPageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月10日17:38:00.
*/

@Data
public class ShopCatalogSearchRequest extends TenantPageRequest {



     private int fetch;

     @Search(name = "levelInfo",operator = Filter.Operator.eq)
     private Integer level;
}