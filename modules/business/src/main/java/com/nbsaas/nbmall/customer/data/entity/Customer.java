package com.nbsaas.nbmall.customer.data.entity;

import com.haoxuer.bigworld.company.data.entity.Employee;
import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.haoxuer.discover.area.data.entity.Area;
import com.haoxuer.discover.trade.data.entity.TradeAccount;
import com.nbsaas.codemake.annotation.*;
import lombok.Data;

import javax.persistence.*;


@ComposeView
@Data
@FormAnnotation(title = "客户管理", model = "客户", menu = "1,135,138")
@Entity
@Table(name = "bs_tenant_user")
public class Customer extends TenantEntity {

    public static Customer fromId(Long id) {
        Customer result = new Customer();
        result.setId(id);
        return result;
    }

    @SearchItem(label = "姓名", name = "name")
    @FormField(title = "姓名", sortNum = "2", grid = true, col = 12, required = true)
    private String name;


    @SearchItem(label = "电话", name = "phone")
    @FormField(title = "电话", sortNum = "4", grid = true, col = 12, required = true)
    private String phone;

    @SearchItem(label = "客户经理", type = InputType.select, name = "manager", key = "manager.id", classType = "Long", operator = "eq")
    @FieldName
    @FieldConvert
    @FormField(title = "客户经理", sortNum = "5", grid = true, col = 12, type = InputType.select, option = "manager")
    @ManyToOne(fetch = FetchType.LAZY)
    private Employee manager;


    @FormField(title = "备注", sortNum = "7", grid = false, col = 12, type = InputType.textarea)
    private String note;


    @FormField(title = "详细地址", sortNum = "6", grid = false, col = 12)
    private String address;

    private Double lat;

    private Double lng;

    private Long addressId;


    private String avatar;

    private String no;

    @Column(length = 20)
    private String tel;

    @Column(length = 20)
    private String mobile;


    private Long visitNum;
    private Long couponNum;

    @FieldName(classType = "BigDecimal",name = "score",parent = "amount")
    @FormField(title = "积分", sortNum = "6", grid = true, col = 12,ignore = true)
    @ManyToOne(fetch = FetchType.LAZY)
    private TradeAccount score;

    @FieldName(classType = "BigDecimal",name = "account",parent = "amount")
    @FormField(title = "余额", sortNum = "6", grid = true, col = 12,ignore = true)
    @ManyToOne(fetch = FetchType.LAZY)
    private TradeAccount tradeAccount;


    @SearchItem(label = "省份", name = "province", key = "province.id", classType = "Integer", operator = "eq", show = false)
    @FormField(title = "省份", grid = false, ignore = true, sort = true)
    @FieldName
    @FieldConvert(classType = "Integer")
    @ManyToOne(fetch = FetchType.LAZY)
    private Area province;

    @SearchItem(label = "城市", name = "city", key = "city.id", classType = "Integer", operator = "eq", show = false)
    @FormField(title = "城市", grid = false, ignore = true, sort = true)
    @FieldName
    @FieldConvert(classType = "Integer")
    @ManyToOne(fetch = FetchType.LAZY)
    private Area city;


    @SearchItem(label = "区县", name = "area", key = "area.id", classType = "Integer", operator = "eq", show = false)
    @FormField(title = "区县", grid = false, ignore = true, type = InputType.select, sort = true)
    @FieldName
    @FieldConvert(classType = "Integer")
    @ManyToOne(fetch = FetchType.LAZY)
    private Area area;
}
