package com.nbsaas.nbmall.shop.controller.tenant;

import com.nbsaas.nbmall.shop.api.apis.ShopStoreApi;
import com.nbsaas.nbmall.shop.api.domain.list.ShopStoreList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopStorePage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopStoreResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/shopstore")
@RestController
public class ShopStoreTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("shopstore")
    @RequestMapping("create")
    public ShopStoreResponse create(ShopStoreDataRequest request) {
        initTenant(request);
        request.setCreator(request.getCreateUser());
        return api.create(request);
    }

	@RequiresPermissions("shopstore")
    @RequestMapping("delete")
    public ShopStoreResponse delete(ShopStoreDataRequest request) {
        initTenant(request);
        ShopStoreResponse result = new ShopStoreResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("shopstore")
    @RequestMapping("update")
    public ShopStoreResponse update(ShopStoreDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("shopstore")
    @RequestMapping("view")
    public ShopStoreResponse view(ShopStoreDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("shopstore")
    @RequestMapping("list")
    public ShopStoreList list(ShopStoreSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("shopstore")
    @RequestMapping("search")
    public ShopStorePage search(ShopStoreSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private ShopStoreApi api;

}
