package com.nbsaas.nbmall.promote.controller.rest;

import com.nbsaas.nbmall.promote.api.apis.CouponApi;
import com.nbsaas.nbmall.promote.api.domain.list.CouponList;
import com.nbsaas.nbmall.promote.api.domain.page.CouponPage;
import com.nbsaas.nbmall.promote.api.domain.request.*;
import com.nbsaas.nbmall.promote.api.domain.response.CouponResponse;
import com.nbsaas.nbmall.promote.api.domain.simple.CouponSimple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/rest/coupon")
@RestController
public class CouponRestController extends BaseRestController {


    @RequestMapping("create")
    public CouponResponse create(CouponDataRequest request) {
        initTenant(request);
        request.setTenantUser(request.getUser());
        return api.create(request);
    }

    @RequestMapping("delete")
    public CouponResponse delete(CouponDataRequest request) {
        initTenant(request);
        CouponResponse result = new CouponResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public CouponResponse update(CouponDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public CouponResponse view(CouponDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public CouponList list(CouponSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public CouponPage search(CouponSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }
    @RequestMapping("searchByMy")
    public CouponPage searchByMy(CouponSearchRequest request) {
        CouponPage result=new CouponPage();
        initTenant(request);
        request.setTenantUser(request.getUser());
        result=api.search(request);

        if (request.getNo() == 1) {
            List<CouponSimple> simples = api.platform(request.getUser());
            if (result.getList()==null){
                result.setList(new ArrayList<>());
            }
            if (simples!=null&&simples.size()>0){
                result.getList().addAll(simples);
            }
        }
        return result;
    }
    @Autowired
    private CouponApi api;

}
