package com.nbsaas.nbmall.common.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.common.data.dao.ChannelDao;
import com.nbsaas.nbmall.common.data.entity.Channel;

/**
* Created by imake on 2021年12月10日15:46:15.
*/
@Repository

public class ChannelDaoImpl extends CriteriaDaoImpl<Channel, Long> implements ChannelDao {

	@Override
	public Channel findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public Channel save(Channel bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public Channel deleteById(Long id) {
		Channel entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<Channel> getEntityClass() {
		return Channel.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}