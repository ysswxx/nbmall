package com.nbsaas.nbmall.device.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.device.data.dao.DeviceDao;
import com.nbsaas.nbmall.device.data.entity.Device;

/**
* Created by imake on 2021年12月10日17:38:01.
*/
@Repository

public class DeviceDaoImpl extends CriteriaDaoImpl<Device, Long> implements DeviceDao {

	@Override
	public Device findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public Device save(Device bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public Device deleteById(Long id) {
		Device entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<Device> getEntityClass() {
		return Device.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public Device findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public Device deleteById(Long tenant,Long id) {
		Device entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}