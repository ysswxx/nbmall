package com.nbsaas.nbmall.order.api.domain.request;

import com.haoxuer.bigworld.member.api.domain.request.TenantPageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月21日18:00:16.
*/

@Data
public class OrderSettleSearchRequest extends TenantPageRequest {

    //订单
     @Search(name = "dishOrder.id",operator = Filter.Operator.eq)
     private Long dishOrder;

    //商家
     @Search(name = "shop.id",operator = Filter.Operator.eq)
     private Long shop;



}