package com.nbsaas.nbmall.product.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import com.nbsaas.nbmall.product.api.domain.simple.ProductImageSimple;
import com.nbsaas.nbmall.product.api.domain.simple.ProductSkuSimple;
import com.nbsaas.nbmall.product.api.domain.vo.ProductSpecVo;
import lombok.Data;
import com.nbsaas.nbmall.product.data.enums.ProductState;
import com.haoxuer.discover.data.enums.StoreState;

import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by imake on 2021年12月28日18:00:18.
 */

@Data
public class ProductDataRequest extends TenantRequest {

    private Long id;

    private String summary;
    private String thumbnail;
    private StoreState storeState;
    private Long creator;
    private BigDecimal salePrice;
    private Double weight;
    private Long productGroup;
    private Double length;
    private ProductState productState;
    private BigDecimal costPrice;
    private Boolean skuEnable;
    private Double volume;
    private Double netWeight;
    private String unit;
    private String name;
    private Integer warningValue;
    private Boolean invoice;
    private Double width;
    private Long shop;
    private String note;
    private BigDecimal marketPrice;
    private Integer inventory;
    private Integer productCatalog;
    private String demo;
    private String barCode;
    private BigDecimal score;
    private Integer sortNum;
    private String logo;
    private Double height;
    private BigDecimal vipPrice;

    private List<ProductSpecVo> specs;

    private List<ProductImageSimple> images;

    private List<ProductSkuSimple> skus;
}