package com.nbsaas.nbmall.cart.controller.admin;


import com.haoxuer.bigworld.tenant.controller.admin.TenantBaseAction;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
*
* Created by imake on 2021年12月30日22:38:35.
*/


@Scope("prototype")
@Controller
public class CartAction extends TenantBaseAction {

	@RequiresPermissions("cart")
	@RequestMapping("/tenant/cart/view_list")
	public String list(ModelMap model) {
		return getView("cart/list");
	}

}