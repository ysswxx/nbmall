package com.nbsaas.nbmall.shop.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.shop.data.entity.ShopStore;

/**
* Created by imake on 2021年12月10日17:38:00.
*/
public interface ShopStoreDao extends BaseDao<ShopStore,Long>{

	 ShopStore findById(Long id);

	 ShopStore save(ShopStore bean);

	 ShopStore updateByUpdater(Updater<ShopStore> updater);

	 ShopStore deleteById(Long id);

	 ShopStore findById(Long tenant, Long id);

     ShopStore deleteById(Long tenant, Long id);
}