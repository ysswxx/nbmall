package com.nbsaas.nbmall.customer.rest.convert;

import com.nbsaas.nbmall.customer.api.domain.response.CustomerAddressResponse;
import com.nbsaas.nbmall.customer.data.entity.CustomerAddress;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class CustomerAddressResponseConvert implements Conver<CustomerAddressResponse, CustomerAddress> {
    @Override
    public CustomerAddressResponse conver(CustomerAddress source) {
        CustomerAddressResponse result = new CustomerAddressResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getArea()!=null){
           result.setArea(source.getArea().getId());
        }
        if(source.getCity()!=null){
           result.setCity(source.getCity().getId());
        }
        if(source.getProvince()!=null){
           result.setProvince(source.getProvince().getId());
        }
         if(source.getCustomer()!=null){
            result.setCustomerName(source.getCustomer().getName());
         }
         if(source.getCity()!=null){
            result.setCityName(source.getCity().getName());
         }
         if(source.getProvince()!=null){
            result.setProvinceName(source.getProvince().getName());
         }
         if(source.getArea()!=null){
            result.setAreaName(source.getArea().getName());
         }
        if(source.getCustomer()!=null){
           result.setCustomer(source.getCustomer().getId());
        }

         result.setStoreStateName(source.getStoreState()+"");
        return result;
    }
}
