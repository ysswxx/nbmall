package com.nbsaas.nbmall.product.controller.tenant;

import com.nbsaas.nbmall.product.api.apis.ProductImageApi;
import com.nbsaas.nbmall.product.api.domain.list.ProductImageList;
import com.nbsaas.nbmall.product.api.domain.page.ProductImagePage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductImageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/productimage")
@RestController
public class ProductImageTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("productimage")
    @RequestMapping("create")
    public ProductImageResponse create(ProductImageDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

	@RequiresPermissions("productimage")
    @RequestMapping("delete")
    public ProductImageResponse delete(ProductImageDataRequest request) {
        initTenant(request);
        ProductImageResponse result = new ProductImageResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("productimage")
    @RequestMapping("update")
    public ProductImageResponse update(ProductImageDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("productimage")
    @RequestMapping("view")
    public ProductImageResponse view(ProductImageDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("productimage")
    @RequestMapping("list")
    public ProductImageList list(ProductImageSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("productimage")
    @RequestMapping("search")
    public ProductImagePage search(ProductImageSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private ProductImageApi api;

}
