package com.nbsaas.nbmall.product.api.apis;


import com.nbsaas.nbmall.product.api.domain.list.ProductList;
import com.nbsaas.nbmall.product.api.domain.page.ProductPage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductResponse;

public interface ProductApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    ProductResponse create(ProductDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    ProductResponse update(ProductDataRequest request);

    ProductResponse offLine(ProductDataRequest request);

    ProductResponse onLine(ProductDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    ProductResponse delete(ProductDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     ProductResponse view(ProductDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    ProductList list(ProductSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    ProductPage search(ProductSearchRequest request);

}