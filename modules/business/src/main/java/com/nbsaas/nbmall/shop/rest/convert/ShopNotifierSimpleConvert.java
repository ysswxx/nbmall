package com.nbsaas.nbmall.shop.rest.convert;

import com.nbsaas.nbmall.shop.api.domain.simple.ShopNotifierSimple;
import com.nbsaas.nbmall.shop.data.entity.ShopNotifier;
import com.haoxuer.discover.data.rest.core.Conver;
public class ShopNotifierSimpleConvert implements Conver<ShopNotifierSimple, ShopNotifier> {


    @Override
    public ShopNotifierSimple conver(ShopNotifier source) {
        ShopNotifierSimple result = new ShopNotifierSimple();

            result.setId(source.getId());
            if(source.getShop()!=null){
               result.setShop(source.getShop().getId());
            }
            if(source.getCreator()!=null){
               result.setCreator(source.getCreator().getId());
            }
             result.setOpenId(source.getOpenId());
             if(source.getShop()!=null){
                result.setShopName(source.getShop().getName());
             }
             result.setName(source.getName());
             result.setAddDate(source.getAddDate());

        return result;
    }
}
