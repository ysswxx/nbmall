package com.nbsaas.nbmall.promote.api.domain.request;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import com.nbsaas.nbmall.promote.api.domain.simple.FullReductionItemSimple;
import lombok.Data;
import com.nbsaas.nbmall.promote.data.enums.FullReductionWay;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * Created by imake on 2021年12月26日12:04:21.
 */

@Data
public class FullReductionDataRequest extends TenantRequest {

    private Long id;

    private Long shop;
    private FullReductionWay fullReductionWay;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date endTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date beginTime;
    private String name;
    private List<FullReductionItemSimple> items;

}