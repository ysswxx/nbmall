package com.nbsaas.nbmall.shop.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.shop.data.entity.ShopCash;

/**
* Created by imake on 2021年12月10日17:38:01.
*/
public interface ShopCashDao extends BaseDao<ShopCash,Long>{

	 ShopCash findById(Long id);

	 ShopCash save(ShopCash bean);

	 ShopCash updateByUpdater(Updater<ShopCash> updater);

	 ShopCash deleteById(Long id);

	 ShopCash findById(Long tenant, Long id);

     ShopCash deleteById(Long tenant, Long id);
}