package com.nbsaas.nbmall.device.data.entity;

import com.haoxuer.bigworld.member.data.entity.TenantUser;
import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.*;
import com.nbsaas.nbmall.shop.data.entity.Shop;
import lombok.Data;

import javax.persistence.*;


@SearchBean(items = {@SearchItem(label = "商家名称", name = "shopName", key = "shop.name",  operator = "like")
})
@CreateByUser
@ComposeView
@Data
@FormAnnotation(title = "打印机管理", model = "打印机", menu = "1,79,175",searchWidth = "100")
@Entity
@Table(name = "bs_tenant_device")
public class Device extends TenantEntity {


    @SearchItem(label = "商家名称", name = "shop", key = "shop.id", type = InputType.select, operator = "eq", classType = "Long", show = false)
    @FormField(title = "商家名称", grid = true, col = 22, type = InputType.selectRemote, option = "shop", width = "200", required = true)
    @FieldConvert
    @FieldName
    @ManyToOne(fetch = FetchType.LAZY)
    private Shop shop;


    @SearchItem(label = "打印机名称", name = "name")
    @FormField(title = "打印机名称", grid = true, col = 22, required = true)
    private String name;

    @SearchItem(label = "打印机类型", name = "deviceType", key = "deviceType.id", operator = "eq", classType = "Long", type = InputType.select)
    @FieldName
    @FieldConvert
    @FormField(title = "打印机类型", grid = true, col = 22, required = true, type = InputType.select, option = "deviceType")
    @ManyToOne(fetch = FetchType.LAZY)
    private DeviceType deviceType;

    @FormField(title = "底部机械码", grid = true, col = 22, required = true)
    private String deviceCode;

    @FormField(title = "底部机械密钥", grid = true, col = 22)
    private String secretKey;

    @Column(length = 20)
    @FormField(title = "终端模式", grid = true, col = 22, required = true, type = InputType.select, option = "model")
    private String model;


    @Column(length = 20)
    @FormField(title = "状态", grid = true, col = 22, ignore = true)
    private String state;

    @FieldConvert
    @FieldName
    @ManyToOne(fetch = FetchType.LAZY)
    private TenantUser creator;

    @FormField(title = "打印次数", grid = true, col = 22, ignore = true)
    private Integer printNum;

}
