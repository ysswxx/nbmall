package com.nbsaas.nbmall.product.controller.tenant;

import com.nbsaas.nbmall.product.api.apis.FreightTemplateApi;
import com.nbsaas.nbmall.product.api.domain.list.FreightTemplateList;
import com.nbsaas.nbmall.product.api.domain.page.FreightTemplatePage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.FreightTemplateResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/freighttemplate")
@RestController
public class FreightTemplateTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("freighttemplate")
    @RequestMapping("create")
    public FreightTemplateResponse create(FreightTemplateDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

	@RequiresPermissions("freighttemplate")
    @RequestMapping("delete")
    public FreightTemplateResponse delete(FreightTemplateDataRequest request) {
        initTenant(request);
        FreightTemplateResponse result = new FreightTemplateResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("freighttemplate")
    @RequestMapping("update")
    public FreightTemplateResponse update(FreightTemplateDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("freighttemplate")
    @RequestMapping("view")
    public FreightTemplateResponse view(FreightTemplateDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("freighttemplate")
    @RequestMapping("list")
    public FreightTemplateList list(FreightTemplateSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("freighttemplate")
    @RequestMapping("search")
    public FreightTemplatePage search(FreightTemplateSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private FreightTemplateApi api;

}
