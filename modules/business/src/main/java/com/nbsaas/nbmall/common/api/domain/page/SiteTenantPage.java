package com.nbsaas.nbmall.common.api.domain.page;


import com.nbsaas.nbmall.common.api.domain.simple.SiteTenantSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年12月23日11:54:02.
*/

@Data
public class SiteTenantPage  extends ResponsePage<SiteTenantSimple> {

}