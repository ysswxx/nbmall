package com.nbsaas.nbmall.customer.rest.convert;

import com.nbsaas.nbmall.customer.api.domain.response.CustomerResponse;
import com.nbsaas.nbmall.customer.data.entity.Customer;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

import java.math.BigDecimal;

public class CustomerResponseConvert implements Conver<CustomerResponse, Customer> {
    @Override
    public CustomerResponse conver(Customer source) {
        CustomerResponse result = new CustomerResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getArea()!=null){
           result.setArea(source.getArea().getId());
        }
         if(source.getManager()!=null){
            result.setManagerName(source.getManager().getName());
         }
        if(source.getCity()!=null){
           result.setCity(source.getCity().getId());
        }
        if(source.getManager()!=null){
           result.setManager(source.getManager().getId());
        }
        if(source.getProvince()!=null){
           result.setProvince(source.getProvince().getId());
        }
         if(source.getScore()!=null){
            result.setScore(source.getScore().getAmount());
         }
         if(source.getCity()!=null){
            result.setCityName(source.getCity().getName());
         }
         if(source.getProvince()!=null){
            result.setProvinceName(source.getProvince().getName());
         }
         if(source.getArea()!=null){
            result.setAreaName(source.getArea().getName());
         }
         if(source.getTradeAccount()!=null){
            result.setAccount(source.getTradeAccount().getAmount());
         }
        if (result.getAccount() == null) {
            result.setAccount(new BigDecimal(0));
        }
        if (result.getScore() == null) {
            result.setScore(new BigDecimal(0));
        }
        if (result.getVisitNum() == null) {
            result.setVisitNum(0L);
        }
        if (result.getCouponNum() == null) {
            result.setCouponNum(0L);
        }
        return result;
    }
}
