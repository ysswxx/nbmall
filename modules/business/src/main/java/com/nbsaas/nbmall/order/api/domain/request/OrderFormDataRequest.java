package com.nbsaas.nbmall.order.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import com.haoxuer.bigworld.pay.data.enums.PayState;
import com.nbsaas.nbmall.order.data.enums.OrderState;
import com.haoxuer.discover.data.enums.StoreState;
import java.util.Date;
import java.math.BigDecimal;

/**
*
* Created by imake on 2021年12月24日00:04:24.
*/

@Data
public class OrderFormDataRequest extends TenantRequest {

    private Long id;

     private Integer returnCount;
     private String consignee;
     private Integer orderSource;
     private Date editPriceTime;
     private Date sysReceiveTime;
     private String address;
     private Long creator;
     private BigDecimal platformDiscount;
     private StoreState storeState;
     private BigDecimal weight;
     private Integer paySource;
     private BigDecimal payAmount;
     private BigDecimal amount;
     private Date receiveTime;
     private Integer commentState;
     private Integer productCount;
     private String outTradeNum;
     private BigDecimal sendIntegral;
     private BigDecimal totalAmount;
     private String phone;
     private Long user;
     private BigDecimal integralDiscount;
     private Long shop;
     private String no;
     private String note;
     private BigDecimal changeAmount;
     private Long newRefundId;
     private Date payTime;
     private Date deliveryTime;
     private BigDecimal freight;
     private BigDecimal discount;
     private BigDecimal useIntegral;
     private PayState payState;
     private OrderState orderState;
     private Integer orderType;
     private String mergeOrderCode;
     private BigDecimal useRedPacket;
     private BigDecimal returnAmount;
     private Date remindTime;
     private Date orderTime;
     private Integer userState;
     private Integer state;
     private BigDecimal redPacketDiscount;
     private Date finishTime;

}