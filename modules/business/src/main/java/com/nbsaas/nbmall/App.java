package com.nbsaas.nbmall;

import com.haoxuer.bigworld.generate.template.hibernate.BigWorldHibernateDir;
import com.haoxuer.bigworld.tenant.data.entity.Application;
import com.haoxuer.bigworld.tenant.data.entity.ApplicationMenu;
import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.template.hibernateSimple.TemplateHibernateSimpleDir;
import com.nbsaas.codemake.templates.elementuiForm.ElementUIFormDir;
import com.nbsaas.codemake.templates.vue.VueDir;
import com.nbsaas.nbmall.video.data.entity.Video;
import com.nbsaas.nbmall.video.data.entity.VideoCatalog;

import java.io.File;

/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args )
    {
        code().make(Video.class);

    }

    private static CodeMake code() {
        CodeMake make = tenantMake();
        make.setDao(true);
        make.setService(false);
        make.setView(true);
        make.setAction(true);
        make.setApi(true);
        make.setRest(true);
        return make;
    }
    private static CodeMake rest() {
        CodeMake make = tenantMake();
        make.setDao(true);
        make.setService(false);
        make.setView(false);
        make.setAction(true);
        make.setApi(true);
        make.setRest(true);
        return make;
    }
    private static CodeMake dao() {
        CodeMake make = tenantMake();
        make.setDao(true);
        make.setService(false);
        make.setView(false);
        make.setAction(false);
        make.setApi(false);
        make.setRest(false);
        make.put("restDomain",false);
        return make;
    }
    private static CodeMake domain() {
        CodeMake make = tenantMake();
        make.setDao(false);
        make.setService(false);
        make.setView(false);
        make.setAction(false);
        make.setApi(false);
        make.setRest(false);
        make.put("restDomain",true);
        return make;
    }
    private static CodeMake view() {
        CodeMake make = tenantMake();
        make.setDao(false);
        make.setService(false);
        make.setAction(false);
        make.setApi(false);
        make.setRest(false);
        make.setView(true);
        make.put("restDomain",false);
        make.put("componentList",true);

        return make;
    }
    private static CodeMake vue() {
        CodeMake make = vueMake();
        make.setDao(false);
        make.setService(false);
        make.setAction(false);
        make.setApi(false);
        make.setRest(false);
        make.setView(true);
        make.put("restDomain",false);
        make.put("componentList",true);

        return make;
    }
    private static CodeMake tenantMake() {
        CodeMake make = new CodeMake(ElementUIFormDir.class, BigWorldHibernateDir.class);
        File view = new File("E:\\codes\\clouds\\nbmall\\web\\src\\main\\webapp\\WEB-INF\\ftl\\tenant\\default");
        make.setView(view);
        make.put("theme","default");
        return make;
    }
    private static CodeMake vueMake() {
        CodeMake make = new CodeMake(VueDir.class, BigWorldHibernateDir.class);
        File view = new File("E:\\codes\\vue\\\\nbmall_mg_vue\\src\\views");
        make.setView(view);
        make.put("theme","default");
        return make;
    }
    private static CodeMake basicView() {
        CodeMake make = new CodeMake(ElementUIFormDir.class, TemplateHibernateSimpleDir.class);
        File view = new File("E:\\codes\\clouds\\nbmall\\web\\src\\main\\webapp\\WEB-INF\\ftl\\tenant\\admin");
        make.setView(view);
        make.put("theme","admin");
        make.setDao(false);
        make.setService(false);
        make.setAction(false);
        make.setApi(false);
        make.setRest(false);
        make.setView(true);
        make.put("restDomain",false);

        return make;
    }
}
