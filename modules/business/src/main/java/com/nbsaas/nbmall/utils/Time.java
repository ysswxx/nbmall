package com.nbsaas.nbmall.utils;

import lombok.Data;

import java.util.Date;

@Data
public class Time {

    private int hour;

    private int minute;

    public static Time now() {
        Time time = new Time();
        Date date = new Date();
        time.setHour(date.getHours());
        time.setMinute(date.getMinutes());
        return time;
    }

    public static Time valueOf(String str) {
        Time time = new Time();
        try {
            int firstColon = str.indexOf(':');
            int hour = Integer.parseInt(str.substring(0, firstColon));
            int minute = Integer.parseInt(str.substring(firstColon + 1));
            time.setHour(hour);
            time.setMinute(minute);
        } catch (Exception e) {

        }
        return time;
    }

    public static int getMillisOf(Time date) {
        int time = date.getHour() * 60;
        time = time + date.getMinute();
        return time;
    }

    public boolean before(Time time) {
        return getMillisOf(this) < getMillisOf(time);
    }
    public boolean after(Time time) {
        return getMillisOf(this) > getMillisOf(time);
    }
}
