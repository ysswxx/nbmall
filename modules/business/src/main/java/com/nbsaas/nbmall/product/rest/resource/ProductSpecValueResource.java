package com.nbsaas.nbmall.product.rest.resource;

import com.nbsaas.nbmall.product.api.apis.ProductSpecValueApi;
import com.nbsaas.nbmall.product.api.domain.list.ProductSpecValueList;
import com.nbsaas.nbmall.product.api.domain.page.ProductSpecValuePage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductSpecValueResponse;
import com.nbsaas.nbmall.product.data.dao.ProductSpecValueDao;
import com.nbsaas.nbmall.product.data.entity.ProductSpecValue;
import com.nbsaas.nbmall.product.rest.convert.ProductSpecValueResponseConvert;
import com.nbsaas.nbmall.product.rest.convert.ProductSpecValueSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.nbsaas.nbmall.product.data.dao.ProductSpecDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class ProductSpecValueResource implements ProductSpecValueApi {

    @Autowired
    private ProductSpecValueDao dataDao;

    @Autowired
    private ProductSpecDao productSpecDao;


    @Override
    public ProductSpecValueResponse create(ProductSpecValueDataRequest request) {
        ProductSpecValueResponse result = new ProductSpecValueResponse();

        ProductSpecValue bean = new ProductSpecValue();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new ProductSpecValueResponseConvert().conver(bean);
        return result;
    }

    @Override
    public ProductSpecValueResponse update(ProductSpecValueDataRequest request) {
        ProductSpecValueResponse result = new ProductSpecValueResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        ProductSpecValue bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new ProductSpecValueResponseConvert().conver(bean);
        return result;
    }

    private void handleData(ProductSpecValueDataRequest request, ProductSpecValue bean) {
       TenantBeanUtils.copyProperties(request,bean);
           if(request.getProductSpec()!=null){
              bean.setProductSpec(productSpecDao.findById(request.getProductSpec()));
           }

    }

    @Override
    public ProductSpecValueResponse delete(ProductSpecValueDataRequest request) {
        ProductSpecValueResponse result = new ProductSpecValueResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public ProductSpecValueResponse view(ProductSpecValueDataRequest request) {
        ProductSpecValueResponse result=new ProductSpecValueResponse();
        ProductSpecValue bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new ProductSpecValueResponseConvert().conver(bean);
        return result;
    }
    @Override
    public ProductSpecValueList list(ProductSpecValueSearchRequest request) {
        ProductSpecValueList result = new ProductSpecValueList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<ProductSpecValue> organizations = dataDao.list(0, request.getSize(), filters, orders);

        ProductSpecValueSimpleConvert convert=new ProductSpecValueSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public ProductSpecValuePage search(ProductSpecValueSearchRequest request) {
        ProductSpecValuePage result=new ProductSpecValuePage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<ProductSpecValue> page=dataDao.page(pageable);

        ProductSpecValueSimpleConvert convert=new ProductSpecValueSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
