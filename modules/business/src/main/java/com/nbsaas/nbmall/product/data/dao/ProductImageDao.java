package com.nbsaas.nbmall.product.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.nbsaas.nbmall.product.data.entity.ProductImage;

/**
 * Created by imake on 2021年12月28日18:04:52.
 */
public interface ProductImageDao extends BaseDao<ProductImage, Long> {

    ProductImage findById(Long id);

    ProductImage save(ProductImage bean);

    ProductImage updateByUpdater(Updater<ProductImage> updater);

    ProductImage deleteById(Long id);

    ProductImage findById(Long tenant, Long id);

    ProductImage deleteById(Long tenant, Long id);

    void deleteByProduct(Long id);
}