package com.nbsaas.nbmall.shop.controller.rest;

import com.nbsaas.nbmall.shop.api.apis.ShopExtApi;
import com.nbsaas.nbmall.shop.api.domain.list.ShopExtList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopExtPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopExtResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/shopext")
@RestController
public class ShopExtRestController extends BaseRestController {


    @RequestMapping("create")
    public ShopExtResponse create(ShopExtDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

    @RequestMapping("delete")
    public ShopExtResponse delete(ShopExtDataRequest request) {
        initTenant(request);
        ShopExtResponse result = new ShopExtResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public ShopExtResponse update(ShopExtDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public ShopExtResponse view(ShopExtDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public ShopExtList list(ShopExtSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public ShopExtPage search(ShopExtSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private ShopExtApi api;

}
