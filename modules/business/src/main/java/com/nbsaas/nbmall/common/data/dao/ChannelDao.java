package com.nbsaas.nbmall.common.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.common.data.entity.Channel;

/**
* Created by imake on 2021年12月10日15:46:15.
*/
public interface ChannelDao extends BaseDao<Channel,Long>{

	 Channel findById(Long id);

	 Channel save(Channel bean);

	 Channel updateByUpdater(Updater<Channel> updater);

	 Channel deleteById(Long id);
}