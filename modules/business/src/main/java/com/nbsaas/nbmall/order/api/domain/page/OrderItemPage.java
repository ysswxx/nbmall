package com.nbsaas.nbmall.order.api.domain.page;


import com.nbsaas.nbmall.order.api.domain.simple.OrderItemSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年12月10日17:38:01.
*/

@Data
public class OrderItemPage  extends ResponsePage<OrderItemSimple> {

}