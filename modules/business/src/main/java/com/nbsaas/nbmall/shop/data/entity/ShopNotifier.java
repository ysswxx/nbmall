package com.nbsaas.nbmall.shop.data.entity;


import com.haoxuer.bigworld.member.data.entity.TenantUser;
import com.nbsaas.codemake.annotation.CreateByUser;
import com.nbsaas.codemake.annotation.FieldConvert;
import com.nbsaas.codemake.annotation.FormAnnotation;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@CreateByUser
@Data
@FormAnnotation(title = "商家通知者", model = "商家通知者", menu = "1,101,102")
@Entity
@Table(name = "bs_tenant_shop_notifier")
public class ShopNotifier extends ShopEntity {

    private String name;

    private String openId;

    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private TenantUser creator;
}
