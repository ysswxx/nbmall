package com.nbsaas.nbmall.shop.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by BigWorld on 2021年12月10日17:38:00.
*/
@Data
public class ShopNotifierSimple implements Serializable {

    private Long id;

     private Long shop;
     private Long creator;
     private String openId;
     private String shopName;
     private String name;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;


}
