package com.nbsaas.nbmall.shop.rest.convert;

import com.nbsaas.nbmall.shop.api.domain.simple.ShopHourSimple;
import com.nbsaas.nbmall.shop.data.entity.ShopHour;
import com.haoxuer.discover.data.rest.core.Conver;
public class ShopHourSimpleConvert implements Conver<ShopHourSimple, ShopHour> {


    @Override
    public ShopHourSimple conver(ShopHour source) {
        ShopHourSimple result = new ShopHourSimple();

            result.setId(source.getId());
            if(source.getShop()!=null){
               result.setShop(source.getShop().getId());
            }
             result.setEndTime(source.getEndTime());
             result.setBeginTime(source.getBeginTime());
             if(source.getShop()!=null){
                result.setShopName(source.getShop().getName());
             }
             result.setName(source.getName());
             result.setAddDate(source.getAddDate());

        return result;
    }
}
