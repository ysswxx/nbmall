package com.nbsaas.nbmall.order.api.domain.page;


import com.nbsaas.nbmall.order.api.domain.simple.OrderFormSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年12月24日00:04:24.
*/

@Data
public class OrderFormPage  extends ResponsePage<OrderFormSimple> {

}