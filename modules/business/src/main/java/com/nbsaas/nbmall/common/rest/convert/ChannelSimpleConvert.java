package com.nbsaas.nbmall.common.rest.convert;

import com.nbsaas.nbmall.common.api.domain.simple.ChannelSimple;
import com.nbsaas.nbmall.common.data.entity.Channel;
import com.haoxuer.discover.data.rest.core.Conver;
public class ChannelSimpleConvert implements Conver<ChannelSimple, Channel> {


    @Override
    public ChannelSimple conver(Channel source) {
        ChannelSimple result = new ChannelSimple();

            result.setId(source.getId());
             result.setNote(source.getNote());
            if(source.getCreator()!=null){
               result.setCreator(source.getCreator().getId());
            }
             result.setLogo(source.getLogo());
             if(source.getCreator()!=null){
                result.setCreatorName(source.getCreator().getName());
             }
             result.setName(source.getName());
             result.setAddDate(source.getAddDate());
             result.setUrl(source.getUrl());

        return result;
    }
}
