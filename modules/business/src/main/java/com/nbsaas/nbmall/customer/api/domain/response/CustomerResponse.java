package com.nbsaas.nbmall.customer.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
/**
*
* Created by BigWorld on 2021年12月10日22:56:19.
*/

@Data
public class CustomerResponse extends ResponseObject {

    private Long id;

     private String no;
     private Integer area;
     private String note;
     private String managerName;
     private String address;
     private Integer city;
     private Long manager;
     private Double lng;
     private Long visitNum;
     private String avatar;
     private String mobile;
     private Integer province;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private Long addressId;
     private BigDecimal score;
     private String phone;
     private String cityName;
     private String name;
     private String provinceName;
     private String areaName;
     private String tel;
     private Double lat;
     private Long couponNum;
     private BigDecimal account;

}