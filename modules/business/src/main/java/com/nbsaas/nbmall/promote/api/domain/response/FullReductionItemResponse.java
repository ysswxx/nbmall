package com.nbsaas.nbmall.promote.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
/**
*
* Created by BigWorld on 2021年12月25日12:45:48.
*/

@Data
public class FullReductionItemResponse extends ResponseObject {

    private Long id;

     private Integer reduceMoney;
     private Integer fullMoney;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private Long reduction;
     private String reductionName;

}