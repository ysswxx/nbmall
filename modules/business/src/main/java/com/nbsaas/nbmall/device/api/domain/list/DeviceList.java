package com.nbsaas.nbmall.device.api.domain.list;


import com.nbsaas.nbmall.device.api.domain.simple.DeviceSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年12月10日17:38:01.
*/

@Data
public class DeviceList  extends ResponseList<DeviceSimple> {

}