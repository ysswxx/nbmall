package com.nbsaas.nbmall.promote.api.domain.request;

import com.haoxuer.bigworld.member.api.domain.request.TenantPageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import com.nbsaas.nbmall.promote.data.enums.CouponCatalog;
import com.nbsaas.nbmall.promote.data.enums.CouponState;
import com.nbsaas.nbmall.shop.data.enums.AuditState;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月25日12:45:48.
*/

@Data
public class CouponSearchRequest extends TenantPageRequest {

    //优惠券
    @Search(name = "couponRule.id", operator = Filter.Operator.eq)
    private Long couponRule;

    //用户
    @Search(name = "tenantUser.id", operator = Filter.Operator.eq)
    private Long tenantUser;

    //商家
    @Search(name = "shop.id", operator = Filter.Operator.eq)
    private Long shop;

    @Search(name = "shop.auditState", operator = Filter.Operator.eq)
    private AuditState auditState = AuditState.checked;

    @Search(name = "couponCatalog", operator = Filter.Operator.eq)
    private CouponCatalog couponCatalog;

    @Search(name = "couponState", operator = Filter.Operator.eq)
    private CouponState couponState;

    private String useType;



}