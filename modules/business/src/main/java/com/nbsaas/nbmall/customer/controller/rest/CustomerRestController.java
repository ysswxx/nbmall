package com.nbsaas.nbmall.customer.controller.rest;

import com.haoxuer.bigworld.pay.api.domain.page.TradeStreamPage;
import com.nbsaas.nbmall.customer.api.apis.CustomerApi;
import com.nbsaas.nbmall.customer.api.domain.list.CustomerList;
import com.nbsaas.nbmall.customer.api.domain.page.CustomerPage;
import com.nbsaas.nbmall.customer.api.domain.request.*;
import com.nbsaas.nbmall.customer.api.domain.response.CustomerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/customer")
@RestController
public class CustomerRestController extends BaseRestController {


    @RequestMapping("create")
    public CustomerResponse create(CustomerDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

    @RequestMapping("delete")
    public CustomerResponse delete(CustomerDataRequest request) {
        initTenant(request);
        CustomerResponse result = new CustomerResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public CustomerResponse update(CustomerDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public CustomerResponse view(CustomerDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public CustomerList list(CustomerSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public CustomerPage search(CustomerSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @RequestMapping("stream")
    public TradeStreamPage stream(CustomerSearchRequest request) {
        initTenant(request);
        return api.stream(request);
    }

    @RequestMapping("scores")
    public TradeStreamPage scores(CustomerSearchRequest request) {
        initTenant(request);
        return api.scores(request);
    }

    @RequestMapping("rechargeScore")
    public CustomerResponse rechargeScore(CustomerDataRequest request) {
        initTenant(request);
        return api.rechargeScore(request);
    }

    @RequestMapping("updateMy")
    public CustomerResponse updateMy(CustomerDataRequest request) {
        initTenant(request);
        request.setId(request.getUser());
        return api.update(request);
    }

    @RequestMapping("my")
    public CustomerResponse my(CustomerDataRequest request) {
        initTenant(request);
        request.setId(request.getUser());
        return api.view(request);
    }

    @Autowired
    private CustomerApi api;

}
