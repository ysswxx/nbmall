package com.nbsaas.nbmall.common.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import org.springframework.context.annotation.Scope;
import com.haoxuer.discover.controller.BaseAction;
/**
*
* Created by imake on 2021年12月23日11:54:02.
*/

@Scope("prototype")
@Controller
public class SiteTenantAction extends BaseAction{


	@RequiresPermissions("sitetenant")
	@RequestMapping("/admin/sitetenant/view_list")
	public String list() {
		return getView("sitetenant/list");
	}

}