package com.nbsaas.nbmall.cart.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by BigWorld on 2021年12月30日22:38:35.
*/
@Data
public class CartSimple implements Serializable {

    private Long id;

     private Long tenantUser;
     private Long shop;
     private Long product;
     private BigDecimal total;
     private BigDecimal price;
     private Boolean checked;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private Long sku;
     private Integer amount;


}
