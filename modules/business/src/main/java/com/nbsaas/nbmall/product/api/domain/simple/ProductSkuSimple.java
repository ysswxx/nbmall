package com.nbsaas.nbmall.product.api.domain.simple;


import java.io.Serializable;

import com.nbsaas.codemake.annotation.FormField;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Created by BigWorld on 2021年12月10日17:38:01.
 */
@Data
public class ProductSkuSimple implements Serializable {

    private Long id;

    private String code;
    private BigDecimal marketPrice;
    private Long product;
    private BigDecimal salePrice;
    private String logo;
    private String name;
    private Integer inventory;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date addDate;
    private String productName;
    private Integer warning;
    private BigDecimal costPrice;
    @FormField(title = "规格", sortNum = "2", grid = true, col = 12)
    private String spec;

}
