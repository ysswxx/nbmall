package com.nbsaas.nbmall.promote.rest.resource;

import com.nbsaas.nbmall.promote.api.apis.CouponRuleApi;
import com.nbsaas.nbmall.promote.api.domain.list.CouponRuleList;
import com.nbsaas.nbmall.promote.api.domain.page.CouponRulePage;
import com.nbsaas.nbmall.promote.api.domain.request.*;
import com.nbsaas.nbmall.promote.api.domain.response.CouponRuleResponse;
import com.nbsaas.nbmall.promote.api.domain.simple.CouponRuleSimple;
import com.nbsaas.nbmall.promote.data.dao.CouponDao;
import com.nbsaas.nbmall.promote.data.dao.CouponRuleDao;
import com.nbsaas.nbmall.promote.data.entity.CouponRule;
import com.nbsaas.nbmall.promote.rest.convert.CouponRuleResponseConvert;
import com.nbsaas.nbmall.promote.rest.convert.CouponRuleSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.nbsaas.nbmall.shop.data.dao.ShopDao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Transactional
@Component
public class CouponRuleResource implements CouponRuleApi {

    @Autowired
    private CouponRuleDao dataDao;

    @Autowired
    private ShopDao shopDao;

    @Autowired
    private CouponDao couponDao;


    @Override
    public CouponRuleResponse create(CouponRuleDataRequest request) {
        CouponRuleResponse result = new CouponRuleResponse();

        CouponRule bean = new CouponRule();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new CouponRuleResponseConvert().conver(bean);
        return result;
    }

    @Override
    public CouponRuleResponse update(CouponRuleDataRequest request) {
        CouponRuleResponse result = new CouponRuleResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        CouponRule bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new CouponRuleResponseConvert().conver(bean);
        return result;
    }

    private void handleData(CouponRuleDataRequest request, CouponRule bean) {
       TenantBeanUtils.copyProperties(request,bean);
           if(request.getShop()!=null){
              bean.setShop(shopDao.findById(request.getShop()));
           }

    }

    @Override
    public CouponRuleResponse delete(CouponRuleDataRequest request) {
        CouponRuleResponse result = new CouponRuleResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public CouponRuleResponse view(CouponRuleDataRequest request) {
        CouponRuleResponse result=new CouponRuleResponse();
        CouponRule bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new CouponRuleResponseConvert().conver(bean);
        return result;
    }
    @Override
    public CouponRuleList list(CouponRuleSearchRequest request) {
        CouponRuleList result = new CouponRuleList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<CouponRule> organizations = dataDao.list(0, request.getSize(), filters, orders);

        CouponRuleSimpleConvert convert=new CouponRuleSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public CouponRulePage search(CouponRuleSearchRequest request) {
        CouponRulePage result=new CouponRulePage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<CouponRule> page=dataDao.page(pageable);

        CouponRuleSimpleConvert convert=new CouponRuleSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }

    @Override
    public CouponRulePage searchByReceive(CouponRuleSearchRequest request) {
        request.setSendBeginTime(new Date());
        request.setSendEndTime(new Date());
        CouponRulePage result = search(request);
        List<CouponRuleSimple> simples = result.getList();
        if (simples != null && simples.size() > 0) {
            for (CouponRuleSimple simple : simples) {
                if (simple.getLimitNum() == null) {
                    simple.setLimitNum(0);
                    simple.setCanReceive(true);
                }

                if (simple.getLimitNum() != 0) {
                    Long num = couponDao.handle("count", "id",
                            Filter.eq("tenantUser.id", request.getUser()),
                            Filter.eq("couponRule.id",simple.getId()));
                    if (num == null) {
                        num = 0L;
                    }
                    if (num < simple.getLimitNum()) {
                        simple.setCanReceive(true);
                    } else {
                        simple.setCanReceive(false);
                    }
                }
                else{
                    simple.setCanReceive(true);
                }
            }
        }
        return result;
    }
}
