package com.nbsaas.nbmall.shop.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.shop.data.dao.ShopStoreDao;
import com.nbsaas.nbmall.shop.data.entity.ShopStore;

/**
* Created by imake on 2021年12月10日17:38:00.
*/
@Repository

public class ShopStoreDaoImpl extends CriteriaDaoImpl<ShopStore, Long> implements ShopStoreDao {

	@Override
	public ShopStore findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public ShopStore save(ShopStore bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public ShopStore deleteById(Long id) {
		ShopStore entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<ShopStore> getEntityClass() {
		return ShopStore.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public ShopStore findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public ShopStore deleteById(Long tenant,Long id) {
		ShopStore entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}