package com.nbsaas.nbmall.shop.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月10日17:38:01.
*/

@Data
public class ShopExtDataRequest extends TenantRequest {

    private Long id;

     private Long shop;
     private String license;
     private String note;
     private String cardNoFront;
     private String letter;
     private String phone;
     private String cardNoReverse;
     private String contact;
     private String name;
     private String permit;
     private String cardNo;
     private String cardNoHand;

}