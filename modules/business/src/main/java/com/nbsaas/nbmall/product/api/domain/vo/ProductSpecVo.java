package com.nbsaas.nbmall.product.api.domain.vo;


import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Created by BigWorld on 2021年07月02日11:09:24.
 */
@Data
public class ProductSpecVo implements Serializable {

    private Long id;

    private String name;

    private List<ProductSpecValueVo> values;


}
