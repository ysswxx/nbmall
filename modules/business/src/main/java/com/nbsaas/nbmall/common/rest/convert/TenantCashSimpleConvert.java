package com.nbsaas.nbmall.common.rest.convert;

import com.nbsaas.nbmall.common.api.domain.simple.TenantCashSimple;
import com.nbsaas.nbmall.common.data.entity.TenantCash;
import com.haoxuer.discover.data.rest.core.Conver;
public class TenantCashSimpleConvert implements Conver<TenantCashSimple, TenantCash> {


    @Override
    public TenantCashSimple conver(TenantCash source) {
        TenantCashSimple result = new TenantCashSimple();

            result.setId(source.getId());
             if(source.getCashConfig()!=null){
                result.setCashConfigName(source.getCashConfig().getName());
             }
             result.setNo(source.getNo());
             result.setNote(source.getNote());
             result.setFee(source.getFee());
            if(source.getCreator()!=null){
               result.setCreator(source.getCreator().getId());
            }
             result.setSendState(source.getSendState());
             result.setOpenId(source.getOpenId());
             result.setIdNo(source.getIdNo());
             result.setMoney(source.getMoney());
             result.setDemo(source.getDemo());
             result.setAddDate(source.getAddDate());
             result.setAppId(source.getAppId());
             result.setBussNo(source.getBussNo());
             result.setPhone(source.getPhone());
             result.setName(source.getName());
            if(source.getCashConfig()!=null){
               result.setCashConfig(source.getCashConfig().getId());
            }
             result.setCash(source.getCash());

             result.setSendStateName(source.getSendState()+"");
        return result;
    }
}
