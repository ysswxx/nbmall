package com.nbsaas.nbmall.shop.controller.tenant;

import com.haoxuer.bigworld.pay.api.domain.page.TradeStreamPage;
import com.nbsaas.nbmall.shop.api.apis.ShopApi;
import com.nbsaas.nbmall.shop.api.domain.list.ShopList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/shop")
@RestController
public class ShopTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("shop")
    @RequestMapping("create")
    public ShopResponse create(ShopDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

	@RequiresPermissions("shop")
    @RequestMapping("delete")
    public ShopResponse delete(ShopDataRequest request) {
        initTenant(request);
        ShopResponse result = new ShopResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("shop")
    @RequestMapping("update")
    public ShopResponse update(ShopDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("shop")
    @RequestMapping("view")
    public ShopResponse view(ShopDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("shop")
    @RequestMapping("list")
    public ShopList list(ShopSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("shop")
    @RequestMapping("search")
    public ShopPage search(ShopSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @RequiresPermissions("shop")
    @RequestMapping("stream")
    public TradeStreamPage stream(ShopSearchRequest request) {
        initTenant(request);
        return api.stream(request);
    }

    @RequiresPermissions("shop")
    @RequestMapping("audit")
    public ShopResponse audit(ShopDataRequest request) {
        initTenant(request);
        return api.audit(request);
    }

    @Autowired
    private ShopApi api;

}
