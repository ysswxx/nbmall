package com.nbsaas.nbmall.order.data.enums;

public enum  OrderState {
    wait,waitPay,pay,cancel,waitConfirm;
    public String toString() {
        if (this.name().equals("wait")) {
            return "待审核";
        } else if (this.name().equals("waitPay")) {
            return "待支付";
        } else if (this.name().equals("pay")) {
            return "已支付";
        }else if (this.name().equals("cancel")) {
            return "取消";
        }else if (this.name().equals("waitConfirm")) {
            return "转账待确认";
        } else {
            return this.name();
        }
    }
}
