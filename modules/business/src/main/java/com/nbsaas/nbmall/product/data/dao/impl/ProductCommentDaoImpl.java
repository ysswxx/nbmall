package com.nbsaas.nbmall.product.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.product.data.dao.ProductCommentDao;
import com.nbsaas.nbmall.product.data.entity.ProductComment;

/**
* Created by imake on 2021年12月10日17:38:01.
*/
@Repository

public class ProductCommentDaoImpl extends CriteriaDaoImpl<ProductComment, Long> implements ProductCommentDao {

	@Override
	public ProductComment findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public ProductComment save(ProductComment bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public ProductComment deleteById(Long id) {
		ProductComment entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<ProductComment> getEntityClass() {
		return ProductComment.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public ProductComment findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public ProductComment deleteById(Long tenant,Long id) {
		ProductComment entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}