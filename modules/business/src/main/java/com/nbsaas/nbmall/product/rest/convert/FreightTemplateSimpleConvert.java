package com.nbsaas.nbmall.product.rest.convert;

import com.nbsaas.nbmall.product.api.domain.simple.FreightTemplateSimple;
import com.nbsaas.nbmall.product.data.entity.FreightTemplate;
import com.haoxuer.discover.data.rest.core.Conver;
public class FreightTemplateSimpleConvert implements Conver<FreightTemplateSimple, FreightTemplate> {


    @Override
    public FreightTemplateSimple conver(FreightTemplate source) {
        FreightTemplateSimple result = new FreightTemplateSimple();

            result.setId(source.getId());
             result.setName(source.getName());
             result.setAddDate(source.getAddDate());

        return result;
    }
}
