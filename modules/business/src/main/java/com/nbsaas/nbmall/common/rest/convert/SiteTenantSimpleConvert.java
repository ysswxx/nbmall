package com.nbsaas.nbmall.common.rest.convert;

import com.nbsaas.nbmall.common.api.domain.simple.SiteTenantSimple;
import com.nbsaas.nbmall.common.data.entity.SiteTenant;
import com.haoxuer.discover.data.rest.core.Conver;
public class SiteTenantSimpleConvert implements Conver<SiteTenantSimple, SiteTenant> {


    @Override
    public SiteTenantSimple conver(SiteTenant source) {
        SiteTenantSimple result = new SiteTenantSimple();

            result.setId(source.getId());
            if(source.getTradeAccount()!=null){
               result.setTradeAccount(source.getTradeAccount().getId());
            }
             result.setNote(source.getNote());
             result.setAddress(source.getAddress());
             result.setLng(source.getLng());
            if(source.getCreator()!=null){
               result.setCreator(source.getCreator().getId());
            }
             result.setStoreState(source.getStoreState());
             result.setShopStyle(source.getShopStyle());
             if(source.getCreator()!=null){
                result.setCreatorName(source.getCreator().getName());
             }
             result.setDemo(source.getDemo());
             result.setAddDate(source.getAddDate());
             result.setDomain(source.getDomain());
             result.setBeginDate(source.getBeginDate());
             result.setPhone(source.getPhone());
             result.setLogo(source.getLogo());
             result.setChatSign(source.getChatSign());
             result.setName(source.getName());
             result.setTheme(source.getTheme());
             result.setExpireDate(source.getExpireDate());
             result.setKey(source.getKey());
             result.setLat(source.getLat());

             result.setStoreStateName(source.getStoreState()+"");
        return result;
    }
}
