package com.nbsaas.nbmall.shop.api.domain.request;

import com.haoxuer.bigworld.member.api.domain.request.TenantPageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月10日17:38:01.
*/

@Data
public class ShopCashSearchRequest extends TenantPageRequest {

    //提现配置
     @Search(name = "cashConfig.id",operator = Filter.Operator.eq)
     private Long cashConfig;

    //商家名称
     @Search(name = "shop.id",operator = Filter.Operator.eq)
     private Long shop;

    //姓名
     @Search(name = "name",operator = Filter.Operator.like)
     private String name;

    //提现单号
     @Search(name = "no",operator = Filter.Operator.like)
     private String cashNo;



}