package com.nbsaas.nbmall.product.data.entity;

import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.*;
import com.nbsaas.nbmall.shop.data.entity.Shop;
import lombok.Data;

import javax.persistence.*;


@Data
@FormAnnotation(title = "产品分组管理",model = "产品分组",menu = "1,125,137")
@Entity
@Table(name = "bs_tenant_product_group")
public class ProductGroup extends TenantEntity {

    @SearchItem(label = "商家名称", name = "shop",key = "shop.id",type = InputType.select,operator = "eq",classType = "Long")
    @FormField(title = "商家名称", grid = true, col = 22,type = InputType.select,option = "shop",width ="200",required = true )
    @FieldConvert
    @FieldName
    @ManyToOne(fetch = FetchType.LAZY)
    private Shop shop;

    @SearchItem(label = "分组名称", name = "name")
    @FormField(title = "分组名称", grid = true, col = 22,required = true,width ="200")
    private String name;

    @FormField(title = "菜品数量", grid = true, col = 22,ignore = true)
    private Long num;

    @Column(length = 6)
    @FormField(title = "是否显示", grid = true, col = 22,type = InputType.el_radio_group)
    private String showState;

    @FormField(title = "排序号", grid = true, col = 22, type = InputType.el_input_number,width = "1000")
    private Integer sortNum;


    @FormField(title = "备注", sortNum = "111", grid = false, col = 22, type = InputType.textarea)
    private String note;
}
