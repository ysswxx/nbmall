package com.nbsaas.nbmall.common.api.apis;


import com.nbsaas.nbmall.common.api.domain.list.TenantCashList;
import com.nbsaas.nbmall.common.api.domain.page.TenantCashPage;
import com.nbsaas.nbmall.common.api.domain.request.*;
import com.nbsaas.nbmall.common.api.domain.response.TenantCashResponse;

public interface TenantCashApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    TenantCashResponse create(TenantCashDataRequest request);

    TenantCashResponse back(TenantCashDataRequest request);

    TenantCashResponse send(TenantCashDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    TenantCashResponse update(TenantCashDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    TenantCashResponse delete(TenantCashDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     TenantCashResponse view(TenantCashDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    TenantCashList list(TenantCashSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    TenantCashPage search(TenantCashSearchRequest request);

}