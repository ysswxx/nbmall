package com.nbsaas.nbmall.shop.data.enums;

public enum AuditState {
    wait,checked,fail;
    @Override
    public String toString() {
        if (name().equals("wait")) {
            return "等待审核";
        } else if (name().equals("checked")) {
            return "审核成功";
        }else if (name().equals("fail")) {
            return "审核失败";
        }
        return super.toString();
    }
}
