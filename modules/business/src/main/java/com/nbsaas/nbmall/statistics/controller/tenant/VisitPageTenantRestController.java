package com.nbsaas.nbmall.statistics.controller.tenant;

import com.nbsaas.nbmall.statistics.api.apis.VisitPageApi;
import com.nbsaas.nbmall.statistics.api.domain.list.VisitPageList;
import com.nbsaas.nbmall.statistics.api.domain.page.VisitPagePage;
import com.nbsaas.nbmall.statistics.api.domain.request.*;
import com.nbsaas.nbmall.statistics.api.domain.response.VisitPageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/visitpage")
@RestController
public class VisitPageTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("visitpage")
    @RequestMapping("create")
    public VisitPageResponse create(VisitPageDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

	@RequiresPermissions("visitpage")
    @RequestMapping("delete")
    public VisitPageResponse delete(VisitPageDataRequest request) {
        initTenant(request);
        VisitPageResponse result = new VisitPageResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("visitpage")
    @RequestMapping("update")
    public VisitPageResponse update(VisitPageDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("visitpage")
    @RequestMapping("view")
    public VisitPageResponse view(VisitPageDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("visitpage")
    @RequestMapping("list")
    public VisitPageList list(VisitPageSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("visitpage")
    @RequestMapping("search")
    public VisitPagePage search(VisitPageSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private VisitPageApi api;

}
