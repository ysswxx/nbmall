package com.nbsaas.nbmall.shop.rest.convert;

import com.nbsaas.nbmall.shop.api.domain.simple.ShopCashSimple;
import com.nbsaas.nbmall.shop.data.entity.ShopCash;
import com.haoxuer.discover.data.rest.core.Conver;
public class ShopCashSimpleConvert implements Conver<ShopCashSimple, ShopCash> {


    @Override
    public ShopCashSimple conver(ShopCash source) {
        ShopCashSimple result = new ShopCashSimple();

            result.setId(source.getId());
             if(source.getCashConfig()!=null){
                result.setCashConfigName(source.getCashConfig().getName());
             }
            if(source.getShop()!=null){
               result.setShop(source.getShop().getId());
            }
             result.setNo(source.getNo());
             result.setNote(source.getNote());
             result.setFee(source.getFee());
            if(source.getCreator()!=null){
               result.setCreator(source.getCreator().getId());
            }
             result.setSendState(source.getSendState());
             result.setOpenId(source.getOpenId());
             if(source.getShop()!=null){
                result.setShopName(source.getShop().getName());
             }
             result.setIdNo(source.getIdNo());
             result.setMoney(source.getMoney());
             result.setDemo(source.getDemo());
             result.setAddDate(source.getAddDate());
             result.setAppId(source.getAppId());
             result.setBussNo(source.getBussNo());
             result.setPhone(source.getPhone());
             result.setName(source.getName());
            if(source.getCashConfig()!=null){
               result.setCashConfig(source.getCashConfig().getId());
            }
             result.setCash(source.getCash());

             result.setSendStateName(source.getSendState()+"");
        return result;
    }
}
