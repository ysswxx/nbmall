package com.nbsaas.nbmall.product.rest.resource;

import com.nbsaas.nbmall.product.api.apis.ProductCatalogApi;
import com.nbsaas.nbmall.product.api.domain.list.ProductCatalogList;
import com.nbsaas.nbmall.product.api.domain.page.ProductCatalogPage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductCatalogResponse;
import com.nbsaas.nbmall.product.data.dao.ProductCatalogDao;
import com.nbsaas.nbmall.product.data.entity.ProductCatalog;
import com.nbsaas.nbmall.product.rest.convert.ProductCatalogResponseConvert;
import com.nbsaas.nbmall.product.rest.convert.ProductCatalogSimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;
import com.nbsaas.nbmall.product.data.dao.ProductCatalogDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class ProductCatalogResource implements ProductCatalogApi {

    @Autowired
    private ProductCatalogDao dataDao;

    @Autowired
    private ProductCatalogDao parentDao;


    @Override
    public ProductCatalogResponse create(ProductCatalogDataRequest request) {
        ProductCatalogResponse result = new ProductCatalogResponse();

        ProductCatalog bean = new ProductCatalog();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new ProductCatalogResponseConvert().conver(bean);
        return result;
    }

    @Override
    public ProductCatalogResponse update(ProductCatalogDataRequest request) {
        ProductCatalogResponse result = new ProductCatalogResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        ProductCatalog bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new ProductCatalogResponseConvert().conver(bean);
        return result;
    }

    private void handleData(ProductCatalogDataRequest request, ProductCatalog bean) {
       TenantBeanUtils.copyProperties(request,bean);
           if(request.getParent()!=null){
              bean.setParent(parentDao.findById(request.getParent()));
           }

    }

    @Override
    public ProductCatalogResponse delete(ProductCatalogDataRequest request) {
        ProductCatalogResponse result = new ProductCatalogResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public ProductCatalogResponse view(ProductCatalogDataRequest request) {
        ProductCatalogResponse result=new ProductCatalogResponse();
        ProductCatalog bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new ProductCatalogResponseConvert().conver(bean);
        return result;
    }
    @Override
    public ProductCatalogList list(ProductCatalogSearchRequest request) {
        ProductCatalogList result = new ProductCatalogList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<ProductCatalog> organizations = dataDao.list(0, request.getSize(), filters, orders);

        ProductCatalogSimpleConvert convert=new ProductCatalogSimpleConvert();
        convert.setFetch(request.getFetch());
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public ProductCatalogPage search(ProductCatalogSearchRequest request) {
        ProductCatalogPage result=new ProductCatalogPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<ProductCatalog> page=dataDao.page(pageable);

        ProductCatalogSimpleConvert convert=new ProductCatalogSimpleConvert();
        convert.setFetch(request.getFetch());
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
