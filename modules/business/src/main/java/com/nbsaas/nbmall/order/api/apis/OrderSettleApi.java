package com.nbsaas.nbmall.order.api.apis;


import com.nbsaas.nbmall.order.api.domain.list.OrderSettleList;
import com.nbsaas.nbmall.order.api.domain.page.OrderSettlePage;
import com.nbsaas.nbmall.order.api.domain.request.*;
import com.nbsaas.nbmall.order.api.domain.response.OrderSettleResponse;

public interface OrderSettleApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    OrderSettleResponse create(OrderSettleDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    OrderSettleResponse update(OrderSettleDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    OrderSettleResponse delete(OrderSettleDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     OrderSettleResponse view(OrderSettleDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    OrderSettleList list(OrderSettleSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    OrderSettlePage search(OrderSettleSearchRequest request);

}