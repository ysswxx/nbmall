package com.nbsaas.nbmall.shop.api.apis;


import com.nbsaas.nbmall.shop.api.domain.list.ShopNotifierList;
import com.nbsaas.nbmall.shop.api.domain.page.ShopNotifierPage;
import com.nbsaas.nbmall.shop.api.domain.request.*;
import com.nbsaas.nbmall.shop.api.domain.response.ShopNotifierResponse;

public interface ShopNotifierApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    ShopNotifierResponse create(ShopNotifierDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    ShopNotifierResponse update(ShopNotifierDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    ShopNotifierResponse delete(ShopNotifierDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     ShopNotifierResponse view(ShopNotifierDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    ShopNotifierList list(ShopNotifierSearchRequest request);

    ShopNotifierResponse test(ShopNotifierDataRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    ShopNotifierPage search(ShopNotifierSearchRequest request);

}