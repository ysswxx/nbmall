package com.nbsaas.nbmall.shop.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by BigWorld on 2021年12月10日17:38:01.
*/
@Data
public class ShopHourSimple implements Serializable {

    private Long id;

     private Long shop;
     private String endTime;
     private String beginTime;
     private String shopName;
     private String name;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;


}
