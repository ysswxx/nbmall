package com.nbsaas.nbmall.video.api.apis;


import com.nbsaas.nbmall.video.api.domain.list.VideoCatalogList;
import com.nbsaas.nbmall.video.api.domain.page.VideoCatalogPage;
import com.nbsaas.nbmall.video.api.domain.request.*;
import com.nbsaas.nbmall.video.api.domain.response.VideoCatalogResponse;

public interface VideoCatalogApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    VideoCatalogResponse create(VideoCatalogDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    VideoCatalogResponse update(VideoCatalogDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    VideoCatalogResponse delete(VideoCatalogDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     VideoCatalogResponse view(VideoCatalogDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    VideoCatalogList list(VideoCatalogSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    VideoCatalogPage search(VideoCatalogSearchRequest request);

}