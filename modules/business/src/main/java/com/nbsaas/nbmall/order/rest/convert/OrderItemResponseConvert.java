package com.nbsaas.nbmall.order.rest.convert;

import com.nbsaas.nbmall.order.api.domain.response.OrderItemResponse;
import com.nbsaas.nbmall.order.data.entity.OrderItem;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class OrderItemResponseConvert implements Conver<OrderItemResponse, OrderItem> {
    @Override
    public OrderItemResponse conver(OrderItem source) {
        OrderItemResponse result = new OrderItemResponse();
        TenantBeanUtils.copyProperties(source,result);


        return result;
    }
}
