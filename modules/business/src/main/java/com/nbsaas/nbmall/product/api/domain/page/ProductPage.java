package com.nbsaas.nbmall.product.api.domain.page;


import com.nbsaas.nbmall.product.api.domain.simple.ProductSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年12月28日18:00:18.
*/

@Data
public class ProductPage  extends ResponsePage<ProductSimple> {

}