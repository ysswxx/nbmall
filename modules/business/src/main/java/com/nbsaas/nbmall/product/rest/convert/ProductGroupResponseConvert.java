package com.nbsaas.nbmall.product.rest.convert;

import com.nbsaas.nbmall.product.api.domain.response.ProductGroupResponse;
import com.nbsaas.nbmall.product.data.entity.ProductGroup;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class ProductGroupResponseConvert implements Conver<ProductGroupResponse, ProductGroup> {
    @Override
    public ProductGroupResponse conver(ProductGroup source) {
        ProductGroupResponse result = new ProductGroupResponse();
        TenantBeanUtils.copyProperties(source,result);

        if(source.getShop()!=null){
           result.setShop(source.getShop().getId());
        }
         if(source.getShop()!=null){
            result.setShopName(source.getShop().getName());
         }

        return result;
    }
}
