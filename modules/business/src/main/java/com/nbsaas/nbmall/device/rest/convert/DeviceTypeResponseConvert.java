package com.nbsaas.nbmall.device.rest.convert;

import com.nbsaas.nbmall.device.api.domain.response.DeviceTypeResponse;
import com.nbsaas.nbmall.device.data.entity.DeviceType;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class DeviceTypeResponseConvert implements Conver<DeviceTypeResponse, DeviceType> {
    @Override
    public DeviceTypeResponse conver(DeviceType source) {
        DeviceTypeResponse result = new DeviceTypeResponse();
        BeanDataUtils.copyProperties(source,result);

        if(source.getCreator()!=null){
           result.setCreator(source.getCreator().getId());
        }
         if(source.getCreator()!=null){
            result.setCreatorName(source.getCreator().getName());
         }


        return result;
    }
}
