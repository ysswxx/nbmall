package com.nbsaas.nbmall.customer.rest.convert;

import com.nbsaas.nbmall.customer.api.domain.simple.CustomerSimple;
import com.nbsaas.nbmall.customer.data.entity.Customer;
import com.haoxuer.discover.data.rest.core.Conver;
public class CustomerSimpleConvert implements Conver<CustomerSimple, Customer> {


    @Override
    public CustomerSimple conver(Customer source) {
        CustomerSimple result = new CustomerSimple();

            result.setId(source.getId());
             result.setNo(source.getNo());
            if(source.getArea()!=null){
               result.setArea(source.getArea().getId());
            }
             result.setNote(source.getNote());
             if(source.getManager()!=null){
                result.setManagerName(source.getManager().getName());
             }
             result.setAddress(source.getAddress());
            if(source.getCity()!=null){
               result.setCity(source.getCity().getId());
            }
            if(source.getManager()!=null){
               result.setManager(source.getManager().getId());
            }
             result.setLng(source.getLng());
             result.setVisitNum(source.getVisitNum());
             result.setAvatar(source.getAvatar());
             result.setMobile(source.getMobile());
            if(source.getProvince()!=null){
               result.setProvince(source.getProvince().getId());
            }
             result.setAddDate(source.getAddDate());
             result.setAddressId(source.getAddressId());
             if(source.getScore()!=null){
                result.setScore(source.getScore().getAmount());
             }
             result.setPhone(source.getPhone());
             if(source.getCity()!=null){
                result.setCityName(source.getCity().getName());
             }
             result.setName(source.getName());
             if(source.getProvince()!=null){
                result.setProvinceName(source.getProvince().getName());
             }
             if(source.getArea()!=null){
                result.setAreaName(source.getArea().getName());
             }
             result.setTel(source.getTel());
             result.setLat(source.getLat());
             result.setCouponNum(source.getCouponNum());
             if(source.getTradeAccount()!=null){
                result.setAccount(source.getTradeAccount().getAmount());
             }

        return result;
    }
}
