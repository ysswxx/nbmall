package com.nbsaas.nbmall.product.api.domain.vo;


import lombok.Data;

import java.io.Serializable;

/**
 * Created by BigWorld on 2021年07月02日11:09:25.
 */
@Data
public class ProductSpecValueVo implements Serializable {

    private Long id;

    private String name;


}
