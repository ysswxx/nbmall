package com.nbsaas.nbmall.common.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.nbsaas.codemake.annotation.*;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@CreateByUser
@Data
@FormAnnotation(title = "展示方式管理", model = "店铺展示方式", menu = "1,104,109")
@Entity
@Table(name = "bs_common_channel")
public class Channel extends AbstractEntity {


    @FieldConvert
    @FieldName
    @ManyToOne(fetch = FetchType.LAZY)
    private UserInfo creator;

    @SearchItem(label = "名称", name = "name")
    @FormField(title = "名称", grid = true, width = "200", sort = true,required = true,col = 22)
    private String name;

    @FormField(title = "展示样式", grid = true, width = "200",type = InputType.image,col = 22)
    private String logo;

    @FormField(title = "导航路径", grid = true, width = "20000", sort = true,required = true,col = 22)
    private String url;

    @FormField(title = "备注", grid = false, width = "20000", type = InputType.textarea,col = 22)
    private String note;
}
