package com.nbsaas.nbmall.common.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.common.data.dao.TenantCashDao;
import com.nbsaas.nbmall.common.data.entity.TenantCash;

/**
* Created by imake on 2021年12月27日16:40:36.
*/
@Repository

public class TenantCashDaoImpl extends CriteriaDaoImpl<TenantCash, Long> implements TenantCashDao {

	@Override
	public TenantCash findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public TenantCash save(TenantCash bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public TenantCash deleteById(Long id) {
		TenantCash entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<TenantCash> getEntityClass() {
		return TenantCash.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public TenantCash findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public TenantCash deleteById(Long tenant,Long id) {
		TenantCash entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}