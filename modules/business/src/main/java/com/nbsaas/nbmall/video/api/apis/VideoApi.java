package com.nbsaas.nbmall.video.api.apis;


import com.nbsaas.nbmall.video.api.domain.list.VideoList;
import com.nbsaas.nbmall.video.api.domain.page.VideoPage;
import com.nbsaas.nbmall.video.api.domain.request.*;
import com.nbsaas.nbmall.video.api.domain.response.VideoResponse;

public interface VideoApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    VideoResponse create(VideoDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    VideoResponse update(VideoDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    VideoResponse delete(VideoDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     VideoResponse view(VideoDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    VideoList list(VideoSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    VideoPage search(VideoSearchRequest request);

}