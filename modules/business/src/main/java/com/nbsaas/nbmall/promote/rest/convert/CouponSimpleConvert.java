package com.nbsaas.nbmall.promote.rest.convert;

import com.nbsaas.nbmall.promote.api.domain.simple.CouponSimple;
import com.nbsaas.nbmall.promote.data.entity.Coupon;
import com.haoxuer.discover.data.rest.core.Conver;
public class CouponSimpleConvert implements Conver<CouponSimple, Coupon> {


    @Override
    public CouponSimple conver(Coupon source) {
        CouponSimple result = new CouponSimple();

            result.setId(source.getId());
             if(source.getCouponRule()!=null){
                result.setCouponRuleName(source.getCouponRule().getName());
             }
            if(source.getTenantUser()!=null){
               result.setTenantUser(source.getTenantUser().getId());
            }
            if(source.getShop()!=null){
               result.setShop(source.getShop().getId());
            }
             result.setUseEndTime(source.getUseEndTime());
            if(source.getCouponRule()!=null){
               result.setCouponRule(source.getCouponRule().getId());
            }
             result.setUseBeginTime(source.getUseBeginTime());
             if(source.getShop()!=null){
                result.setShopName(source.getShop().getName());
             }
             result.setMoney(source.getMoney());
             if(source.getTenantUser()!=null){
                result.setTenantUserName(source.getTenantUser().getName());
             }
             result.setCouponCatalog(source.getCouponCatalog());
             result.setAddDate(source.getAddDate());
             result.setCouponState(source.getCouponState());

             result.setCouponStateName(source.getCouponState()+"");
             result.setCouponCatalogName(source.getCouponCatalog()+"");
        return result;
    }
}
