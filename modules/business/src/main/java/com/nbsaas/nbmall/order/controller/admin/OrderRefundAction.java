package com.nbsaas.nbmall.order.controller.admin;


import com.haoxuer.bigworld.tenant.controller.admin.TenantBaseAction;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
*
* Created by imake on 2021年12月21日18:00:16.
*/


@Scope("prototype")
@Controller
public class OrderRefundAction extends TenantBaseAction {

	@RequiresPermissions("orderrefund")
	@RequestMapping("/tenant/orderrefund/view_list")
	public String list(ModelMap model) {
		return getView("orderrefund/list");
	}

}