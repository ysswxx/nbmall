package com.nbsaas.nbmall.device.controller.rest;

import com.nbsaas.nbmall.device.api.apis.DeviceApi;
import com.nbsaas.nbmall.device.api.domain.list.DeviceList;
import com.nbsaas.nbmall.device.api.domain.page.DevicePage;
import com.nbsaas.nbmall.device.api.domain.request.*;
import com.nbsaas.nbmall.device.api.domain.response.DeviceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/device")
@RestController
public class DeviceRestController extends BaseRestController {


    @RequestMapping("create")
    public DeviceResponse create(DeviceDataRequest request) {
        initTenant(request);
        request.setCreator(request.getUser());
        return api.create(request);
    }

    @RequestMapping("delete")
    public DeviceResponse delete(DeviceDataRequest request) {
        initTenant(request);
        DeviceResponse result = new DeviceResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public DeviceResponse update(DeviceDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public DeviceResponse view(DeviceDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public DeviceList list(DeviceSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public DevicePage search(DeviceSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private DeviceApi api;

}
