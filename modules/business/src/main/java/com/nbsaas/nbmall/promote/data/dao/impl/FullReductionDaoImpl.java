package com.nbsaas.nbmall.promote.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.promote.data.dao.FullReductionDao;
import com.nbsaas.nbmall.promote.data.entity.FullReduction;

/**
* Created by imake on 2021年12月26日12:04:21.
*/
@Repository

public class FullReductionDaoImpl extends CriteriaDaoImpl<FullReduction, Long> implements FullReductionDao {

	@Override
	public FullReduction findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public FullReduction save(FullReduction bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public FullReduction deleteById(Long id) {
		FullReduction entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<FullReduction> getEntityClass() {
		return FullReduction.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public FullReduction findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public FullReduction deleteById(Long tenant,Long id) {
		FullReduction entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}