package com.nbsaas.nbmall.statistics.data.entity;


import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.ComposeView;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.FormField;
import com.nbsaas.codemake.annotation.SearchItem;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@ComposeView
@Data
@FormAnnotation(title = "访问渠道", model = "访问渠道", menu = "1,101,102")
@Entity
@Table(name = "bs_tenant_visit_channel")
public class VisitChannel extends TenantEntity {

    @SearchItem(label = "渠道名称",name = "name")
    @FormField(title = "渠道名称", grid = true, required = true)
    @Column(length = 20)
    private String name;

    private Long userNum;

    private Long pageNum;


}
