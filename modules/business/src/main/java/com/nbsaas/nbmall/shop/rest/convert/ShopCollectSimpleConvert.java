package com.nbsaas.nbmall.shop.rest.convert;

import com.nbsaas.nbmall.shop.api.domain.simple.ShopCollectSimple;
import com.nbsaas.nbmall.shop.data.entity.ShopCollect;
import com.haoxuer.discover.data.rest.core.Conver;
public class ShopCollectSimpleConvert implements Conver<ShopCollectSimple, ShopCollect> {


    @Override
    public ShopCollectSimple conver(ShopCollect source) {
        ShopCollectSimple result = new ShopCollectSimple();

            result.setId(source.getId());
            if(source.getTenantUser()!=null){
               result.setTenantUser(source.getTenantUser().getId());
            }
            if(source.getShop()!=null){
               result.setShop(source.getShop().getId());
            }
             result.setStoreState(source.getStoreState());
             if(source.getShop()!=null){
                result.setShopName(source.getShop().getName());
             }
             if(source.getTenantUser()!=null){
                result.setTenantUserName(source.getTenantUser().getName());
             }
             result.setAddDate(source.getAddDate());

             result.setStoreStateName(source.getStoreState()+"");
        return result;
    }
}
