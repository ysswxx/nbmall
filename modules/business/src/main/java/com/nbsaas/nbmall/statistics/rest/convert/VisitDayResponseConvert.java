package com.nbsaas.nbmall.statistics.rest.convert;

import com.nbsaas.nbmall.statistics.api.domain.response.VisitDayResponse;
import com.nbsaas.nbmall.statistics.data.entity.VisitDay;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class VisitDayResponseConvert implements Conver<VisitDayResponse, VisitDay> {
    @Override
    public VisitDayResponse conver(VisitDay source) {
        VisitDayResponse result = new VisitDayResponse();
        TenantBeanUtils.copyProperties(source,result);


        return result;
    }
}
