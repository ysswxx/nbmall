package com.nbsaas.nbmall.product.data.entity;

import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.FieldConvert;
import com.nbsaas.codemake.annotation.FieldName;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.FormField;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;


@Data
@FormAnnotation(title = "商品sku")
@Entity
@Table(name = "bs_tenant_product_sku")
public class ProductSku extends TenantEntity {


    private String code;

    private BigDecimal salePrice;

    private BigDecimal marketPrice;

    private BigDecimal costPrice;

    private Integer inventory;

    private Integer warning;

    private String name;

    private String logo;

    @FieldConvert
    @FieldName
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @FormField(title = "规格", sortNum = "2", grid = true, col = 12)
    private String spec;
}
