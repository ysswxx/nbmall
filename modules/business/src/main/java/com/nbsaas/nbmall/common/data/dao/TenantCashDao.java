package com.nbsaas.nbmall.common.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.common.data.entity.TenantCash;

/**
* Created by imake on 2021年12月27日16:40:36.
*/
public interface TenantCashDao extends BaseDao<TenantCash,Long>{

	 TenantCash findById(Long id);

	 TenantCash save(TenantCash bean);

	 TenantCash updateByUpdater(Updater<TenantCash> updater);

	 TenantCash deleteById(Long id);

	 TenantCash findById(Long tenant, Long id);

     TenantCash deleteById(Long tenant, Long id);
}