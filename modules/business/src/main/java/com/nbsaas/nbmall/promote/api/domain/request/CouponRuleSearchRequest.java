package com.nbsaas.nbmall.promote.api.domain.request;

import com.haoxuer.bigworld.member.api.domain.request.TenantPageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import com.nbsaas.nbmall.promote.data.enums.CouponCatalog;
import com.nbsaas.nbmall.promote.data.enums.ShowType;
import com.nbsaas.nbmall.shop.data.enums.AuditState;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月28日23:18:26.
*/

@Data
public class CouponRuleSearchRequest extends TenantPageRequest {

    //名称
    //名称
    @Search(name = "name", operator = Filter.Operator.like)
    private String name;

    //商家
    @Search(name = "shop", operator = Filter.Operator.eq)
    private Long shop;

    //名称
    @Search(name = "showType", operator = Filter.Operator.like)
    private ShowType showType;

    @Search(name = "shop.auditState", operator = Filter.Operator.eq)
    private AuditState auditState = AuditState.checked;

    //优惠券类型
    @Search(name = "couponCatalog", operator = Filter.Operator.eq)
    private CouponCatalog couponCatalog;

    @Search(name = "sendBeginTime", operator = Filter.Operator.le)
    private Date sendBeginTime;

    @Search(name = "sendEndTime", operator = Filter.Operator.ge)
    private Date sendEndTime;



}