package com.nbsaas.nbmall.common.controller.rest;

import com.nbsaas.nbmall.common.api.apis.ChannelApi;
import com.nbsaas.nbmall.common.api.domain.list.ChannelList;
import com.nbsaas.nbmall.common.api.domain.page.ChannelPage;
import com.nbsaas.nbmall.common.api.domain.request.*;
import com.nbsaas.nbmall.common.api.domain.response.ChannelResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.rest.BaseRestController;

@RequestMapping("/rest/channel")
@RestController
public class ChannelRestController extends BaseRestController {


    @RequestMapping("create")
    public ChannelResponse create(ChannelDataRequest request) {
        init(request);
        request.setCreator(request.getUser());
        return api.create(request);
    }

    @RequestMapping("update")
    public ChannelResponse update(ChannelDataRequest request) {
        init(request);
        return api.update(request);
    }

    @RequestMapping("delete")
    public ChannelResponse delete(ChannelDataRequest request) {
        init(request);
        ChannelResponse result = new ChannelResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("view")
    public ChannelResponse view(ChannelDataRequest request) {
        init(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public ChannelList list(ChannelSearchRequest request) {
        init(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public ChannelPage search(ChannelSearchRequest request) {
        init(request);
        return api.search(request);
    }



    @Autowired
    private ChannelApi api;

}
