package com.nbsaas.nbmall.common.controller.rest;

import com.nbsaas.nbmall.common.api.apis.TenantCashApi;
import com.nbsaas.nbmall.common.api.domain.list.TenantCashList;
import com.nbsaas.nbmall.common.api.domain.page.TenantCashPage;
import com.nbsaas.nbmall.common.api.domain.request.*;
import com.nbsaas.nbmall.common.api.domain.response.TenantCashResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/tenantcash")
@RestController
public class TenantCashRestController extends BaseRestController {


    @RequestMapping("create")
    public TenantCashResponse create(TenantCashDataRequest request) {
        initTenant(request);
        request.setCreator(request.getUser());
        return api.create(request);
    }


    @RequestMapping("list")
    public TenantCashList list(TenantCashSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public TenantCashPage search(TenantCashSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private TenantCashApi api;

}
