package com.nbsaas.nbmall.statistics.controller.tenant;

import com.nbsaas.nbmall.statistics.api.apis.VisitChannelApi;
import com.nbsaas.nbmall.statistics.api.domain.list.VisitChannelList;
import com.nbsaas.nbmall.statistics.api.domain.page.VisitChannelPage;
import com.nbsaas.nbmall.statistics.api.domain.request.*;
import com.nbsaas.nbmall.statistics.api.domain.response.VisitChannelResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/visitchannel")
@RestController
public class VisitChannelTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("visitchannel")
    @RequestMapping("create")
    public VisitChannelResponse create(VisitChannelDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

	@RequiresPermissions("visitchannel")
    @RequestMapping("delete")
    public VisitChannelResponse delete(VisitChannelDataRequest request) {
        initTenant(request);
        VisitChannelResponse result = new VisitChannelResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("visitchannel")
    @RequestMapping("update")
    public VisitChannelResponse update(VisitChannelDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("visitchannel")
    @RequestMapping("view")
    public VisitChannelResponse view(VisitChannelDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("visitchannel")
    @RequestMapping("list")
    public VisitChannelList list(VisitChannelSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("visitchannel")
    @RequestMapping("search")
    public VisitChannelPage search(VisitChannelSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private VisitChannelApi api;

}
