package com.nbsaas.nbmall.statistics.rest.resource;

import com.nbsaas.nbmall.statistics.api.apis.VisitDayApi;
import com.nbsaas.nbmall.statistics.api.domain.list.VisitDayList;
import com.nbsaas.nbmall.statistics.api.domain.page.VisitDayPage;
import com.nbsaas.nbmall.statistics.api.domain.request.*;
import com.nbsaas.nbmall.statistics.api.domain.response.VisitDayResponse;
import com.nbsaas.nbmall.statistics.data.dao.VisitDayDao;
import com.nbsaas.nbmall.statistics.data.entity.VisitDay;
import com.nbsaas.nbmall.statistics.rest.convert.VisitDayResponseConvert;
import com.nbsaas.nbmall.statistics.rest.convert.VisitDaySimpleConvert;
import com.haoxuer.bigworld.tenant.data.entity.Tenant;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.bigworld.member.rest.conver.PageableConver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class VisitDayResource implements VisitDayApi {

    @Autowired
    private VisitDayDao dataDao;



    @Override
    public VisitDayResponse create(VisitDayDataRequest request) {
        VisitDayResponse result = new VisitDayResponse();

        VisitDay bean = new VisitDay();
        bean.setTenant(Tenant.fromId(request.getTenant()));
        handleData(request, bean);
        dataDao.save(bean);
        result = new VisitDayResponseConvert().conver(bean);
        return result;
    }

    @Override
    public VisitDayResponse update(VisitDayDataRequest request) {
        VisitDayResponse result = new VisitDayResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        VisitDay bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new VisitDayResponseConvert().conver(bean);
        return result;
    }

    private void handleData(VisitDayDataRequest request, VisitDay bean) {
       TenantBeanUtils.copyProperties(request,bean);

    }

    @Override
    public VisitDayResponse delete(VisitDayDataRequest request) {
        VisitDayResponse result = new VisitDayResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getTenant(),request.getId());
        return result;
    }

    @Override
    public VisitDayResponse view(VisitDayDataRequest request) {
        VisitDayResponse result=new VisitDayResponse();
        VisitDay bean = dataDao.findById(request.getTenant(), request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new VisitDayResponseConvert().conver(bean);
        return result;
    }
    @Override
    public VisitDayList list(VisitDaySearchRequest request) {
        VisitDayList result = new VisitDayList();

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("tenant.id", request.getTenant()));
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<VisitDay> organizations = dataDao.list(0, request.getSize(), filters, orders);

        VisitDaySimpleConvert convert=new VisitDaySimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public VisitDayPage search(VisitDaySearchRequest request) {
        VisitDayPage result=new VisitDayPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        pageable.getFilters().add(Filter.eq("tenant.id", request.getTenant()));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<VisitDay> page=dataDao.page(pageable);

        VisitDaySimpleConvert convert=new VisitDaySimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
