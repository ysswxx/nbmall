package com.nbsaas.nbmall.product.rest.convert;

import com.nbsaas.nbmall.product.api.domain.simple.ProductCommentSimple;
import com.nbsaas.nbmall.product.data.entity.ProductComment;
import com.haoxuer.discover.data.rest.core.Conver;
public class ProductCommentSimpleConvert implements Conver<ProductCommentSimple, ProductComment> {


    @Override
    public ProductCommentSimple conver(ProductComment source) {
        ProductCommentSimple result = new ProductCommentSimple();

            result.setId(source.getId());
            if(source.getTenantUser()!=null){
               result.setTenantUser(source.getTenantUser().getId());
            }
             result.setNote(source.getNote());
             result.setScore(source.getScore());
            if(source.getProduct()!=null){
               result.setProduct(source.getProduct().getId());
            }
             result.setService(source.getService());
             result.setLogistics(source.getLogistics());
             result.setAddDate(source.getAddDate());

        return result;
    }
}
