package com.nbsaas.nbmall.device.api.domain.request;


import com.haoxuer.bigworld.member.api.domain.request.TenantRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月10日17:38:01.
*/

@Data
public class DeviceDataRequest extends TenantRequest {

    private Long id;

     private Long shop;
     private Integer printNum;
     private String secretKey;
     private Long creator;
     private String name;
     private String deviceCode;
     private String state;
     private String model;
     private Long deviceType;

}