package com.nbsaas.nbmall.web.front;

import com.haoxuer.bigworld.tenant.api.domain.request.TenantRequest;
import com.haoxuer.discover.generate.utils.RandomValue;
import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.web.controller.front.BaseController;
import com.nbsaas.nbmall.customer.api.apis.CustomerApi;
import com.nbsaas.nbmall.customer.api.domain.list.CustomerList;
import com.nbsaas.nbmall.customer.api.domain.request.CustomerDataRequest;
import com.nbsaas.nbmall.customer.api.domain.request.CustomerSearchRequest;
import com.nbsaas.nbmall.customer.data.entity.Customer;
import com.nbsaas.nbmall.shop.api.apis.ShopApi;
import com.nbsaas.nbmall.shop.api.domain.response.ShopResponse;
import com.nbsaas.nbmall.shop.data.entity.Shop;
import com.nbsaas.nbmall.test.api.apis.TestApi;
import net.sf.ehcache.Statistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.ehcache.EhCacheCache;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Scope(scopeName = "prototype")
@Controller
public class WebController extends BaseController {


    @Autowired
    private ShopApi shopApi;

    @Autowired
    private CustomerApi customerApi;

    @ResponseBody
    @RequestMapping("testTrade")
    public ResponseObject testTrade(Long tenant,Integer num) {
        ResponseObject result=new ResponseObject();
        if (tenant==null){
            tenant=3L;
        }
        if (num==null){
            num=1000;
        }
        Integer finalNum = num;
        Long finalTenant = tenant;
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < finalNum; i++) {
                    TenantRequest request=new TenantRequest();
                    request.setTenant(finalTenant);
                    testApi.testTrade(request);
                }
            }
        }).start();

        return result;
    }

    @Autowired
    private TestApi testApi;

    @RequestMapping("index")
    public String index(){
        return redirect("login.htm");
    }

    @RequestMapping("bindShop")
    public String bindShop(String code, String state, Model model){
        try {
            ShopResponse response= shopApi.bind(code,state);
            model.addAttribute("msg",response.getMsg());
        }catch (Exception e){
            e.printStackTrace();
        }
        return getView("bindShop");
    }
    @ResponseBody
    @RequestMapping("random")
    public String random(Long tenant){
        if (tenant==null){
            tenant=3L;
        }
        CustomerDataRequest request=new CustomerDataRequest();
        request.setAddress(RandomValue.getRoad());
        request.setName(RandomValue.getChineseName());
        request.setPhone(RandomValue.getTel());
        request.setTenant(tenant);
        request.setPassword(RandomValue.getTel());
        customerApi.register(request);
        return "ok";
    }
    @ResponseBody
    @RequestMapping("randomThread")
    public String randomThread(Long tenant,Integer num){
        if (tenant==null){
            tenant=3L;
        }
        if (num==null){
            num=1000;
        }
        Integer finalNum = num;
        Long finalTenant = tenant;
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < finalNum; i++) {
                    CustomerDataRequest request=new CustomerDataRequest();
                    request.setAddress(RandomValue.getRoad());
                    request.setName(RandomValue.getChineseName());
                    request.setPhone(RandomValue.getTel());
                    request.setTenant(finalTenant);
                    request.setPassword(RandomValue.getTel());
                    customerApi.register(request);
                }
            }
        }).start();

        return "ok";
    }
    @ResponseBody
    @RequestMapping("dataList")
    public CustomerList dataList(CustomerSearchRequest request){
        if (request.getTenant()==null){
            request.setTenant(3L);
        }
        return customerApi.list(request);
    }

    @ResponseBody
    @RequestMapping("stringList")
    public String stringList(CustomerSearchRequest request){
        if (request.getTenant()==null){
            request.setTenant(3L);
        }
        customerApi.list(request);
        return "ok";
    }

    @Cacheable( value = "userCache",keyGenerator = "paramKeyGenerator")
    @ResponseBody
    @RequestMapping("stringList1")
    public String stringList1(CustomerSearchRequest request){
        if (request.getTenant()==null){
            request.setTenant(3L);
        }
        customerApi.list(request);
        return "ok";
    }

    @Cacheable( value = "userCache")
    @ResponseBody
    @RequestMapping("stringList2")
    public String stringList2(CustomerSearchRequest request){
        if (request.getTenant()==null){
            request.setTenant(3L);
        }
        customerApi.list(request);
        return "ok";
    }

    @Cacheable( value = "userCache",keyGenerator = "paramKeyGenerator")
    @ResponseBody
    @RequestMapping("cacheList")
    public CustomerList cacheList(CustomerSearchRequest request){
        if (request.getTenant()==null){
            request.setTenant(3L);
        }
        return customerApi.list(request);
    }

    @Autowired
    private EhCacheCacheManager cacheCacheManager;

    @GetMapping(value = "/cache")
    public String cache(Model model) {
        Collection<String> names = cacheCacheManager.getCacheNames();
        List list = new ArrayList();
        List<Statistics> statisticsList = new ArrayList<>();
        for (String name : names) {
            Cache cache = cacheCacheManager.getCache(name);
            if (cache instanceof EhCacheCache) {
                EhCacheCache ehCacheCache = (EhCacheCache) cache;
                if (ehCacheCache == null) {
                    continue;
                }
                List ls = ehCacheCache.getNativeCache().getKeys();
                if (ls != null && ls.size() > 0) {
                    list.addAll(ls);
                }
                Statistics statistics = ehCacheCache.getNativeCache().getStatistics();
                if (statistics != null) {
                    statisticsList.add(statistics);
                }
            }
        }
        model.addAttribute("list", list);
        model.addAttribute("statisticsList", statisticsList);
        return getView("cache");
    }

}
