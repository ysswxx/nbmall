package com.nbsaas.nbmall.promote.data.entity;

import com.haoxuer.bigworld.member.data.entity.TenantUser;
import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.*;
import com.nbsaas.nbmall.promote.data.enums.CouponCatalog;
import com.nbsaas.nbmall.promote.data.enums.CouponState;
import com.nbsaas.nbmall.shop.data.entity.Shop;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;


@Data
@FormAnnotation(title = "优惠券", model = "优惠券", menu = "1,123,169")
@Entity
@Table(name = "bs_tenant_promote_coupon")
public class Coupon extends TenantEntity {

    @FieldConvert
    @SearchItem(label = "优惠券", name = "couponRule", key = "couponRule.id", operator = "eq", classType = "Long")
    @FieldName
    @FormField
    @ManyToOne(fetch = FetchType.LAZY)
    private CouponRule couponRule;


    @FieldConvert
    @SearchItem(label = "用户", name = "tenantUser", key = "tenantUser.id", operator = "eq", classType = "Long")
    @FieldName
    @FormField
    @ManyToOne(fetch = FetchType.LAZY)
    private TenantUser tenantUser;

    private BigDecimal money;

    @FormField(title = "优惠券开始时间", type = InputType.el_date_time_picker, grid = false, col = 20)
    private Date useBeginTime;

    @FormField(title = "优惠券截止时间", type = InputType.el_date_time_picker, grid = false, col = 20)
    private Date useEndTime;

    @SearchItem(label = "商家", name = "shop", key = "shop", operator = "eq", classType = "Long", show = false)
    @FieldConvert
    @FieldName
    @FormField(title = "商家", grid = true, col = 20, type = InputType.select, option = "shop", required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    private Shop shop;


    /**
     * 优惠券类型
     */
    @SearchItem(label = "优惠券类型",name = "couponCatalog",operator = "eq",show = false)
    private CouponCatalog couponCatalog;

    private CouponState couponState;
}
