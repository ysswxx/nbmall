package com.nbsaas.nbmall.product.api.domain.request;

import com.haoxuer.bigworld.member.api.domain.request.TenantPageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import com.nbsaas.nbmall.product.data.enums.ProductState;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月28日18:00:18.
*/

@Data
public class ProductSearchRequest extends TenantPageRequest {

    //商品名称
     @Search(name = "name",operator = Filter.Operator.like)
     private String name;

    //商家名称
     @Search(name = "shop.id",operator = Filter.Operator.eq)
     private Long shop;

    //商品分类
     @Search(name = "productCatalog.id",operator = Filter.Operator.eq)
     private Integer productCatalog;

    //商品编码
     @Search(name = "barCode",operator = Filter.Operator.like)
     private String barCode;

    @Search(name = "productState",operator = Filter.Operator.eq)
    private ProductState productState;



}