package com.nbsaas.nbmall.product.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.product.data.dao.ProductSkuDao;
import com.nbsaas.nbmall.product.data.entity.ProductSku;

/**
* Created by imake on 2021年12月10日17:38:01.
*/
@Repository

public class ProductSkuDaoImpl extends CriteriaDaoImpl<ProductSku, Long> implements ProductSkuDao {

	@Override
	public ProductSku findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public ProductSku save(ProductSku bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public ProductSku deleteById(Long id) {
		ProductSku entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<ProductSku> getEntityClass() {
		return ProductSku.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public ProductSku findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public ProductSku deleteById(Long tenant,Long id) {
		ProductSku entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}