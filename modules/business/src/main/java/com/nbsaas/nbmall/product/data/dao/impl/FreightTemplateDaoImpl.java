package com.nbsaas.nbmall.product.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.product.data.dao.FreightTemplateDao;
import com.nbsaas.nbmall.product.data.entity.FreightTemplate;

/**
* Created by imake on 2021年12月10日17:38:01.
*/
@Repository

public class FreightTemplateDaoImpl extends CriteriaDaoImpl<FreightTemplate, Long> implements FreightTemplateDao {

	@Override
	public FreightTemplate findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public FreightTemplate save(FreightTemplate bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public FreightTemplate deleteById(Long id) {
		FreightTemplate entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<FreightTemplate> getEntityClass() {
		return FreightTemplate.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public FreightTemplate findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public FreightTemplate deleteById(Long tenant,Long id) {
		FreightTemplate entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}