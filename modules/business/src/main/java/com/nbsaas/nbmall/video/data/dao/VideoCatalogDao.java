package com.nbsaas.nbmall.video.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.video.data.entity.VideoCatalog;

/**
* Created by imake on 2022年03月16日21:25:37.
*/
public interface VideoCatalogDao extends BaseDao<VideoCatalog,Integer>{

	 VideoCatalog findById(Integer id);

	 VideoCatalog save(VideoCatalog bean);

	 VideoCatalog updateByUpdater(Updater<VideoCatalog> updater);

	 VideoCatalog deleteById(Integer id);

	 VideoCatalog findById(Long tenant, Integer id);

     VideoCatalog deleteById(Long tenant, Integer id);
}