package com.nbsaas.nbmall.promote.data.entity;


import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.*;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@FormAnnotation(title = "菜品满减", model = "菜品满减", menu = "1,203,169")
@Entity
@Table(name = "bs_tenant_promote_reduction_item")
public class FullReductionItem extends TenantEntity {

    @SearchItem(label = "满减规则",name = "reduction",key = "reduction.id",operator = "eq",classType = "Long",show = false)
    @FieldConvert
    @FieldName
    @FormField(title = "满减规则",  grid = true, col = 20,type = InputType.select,option = "reduction",required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    private FullReduction reduction;

    private Integer fullMoney;

    private Integer reduceMoney;

}
