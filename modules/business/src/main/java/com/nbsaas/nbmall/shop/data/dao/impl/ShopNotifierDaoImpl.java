package com.nbsaas.nbmall.shop.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.haoxuer.discover.data.page.Filter;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.shop.data.dao.ShopNotifierDao;
import com.nbsaas.nbmall.shop.data.entity.ShopNotifier;

/**
* Created by imake on 2021年12月10日17:38:00.
*/
@Repository

public class ShopNotifierDaoImpl extends CriteriaDaoImpl<ShopNotifier, Long> implements ShopNotifierDao {

	@Override
	public ShopNotifier findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public ShopNotifier save(ShopNotifier bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public ShopNotifier deleteById(Long id) {
		ShopNotifier entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<ShopNotifier> getEntityClass() {
		return ShopNotifier.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}

	@Override
	public ShopNotifier findById(Long tenant,Long id) {
	    if (id == null) {
           return null;
        }
        return one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
	}

    @Override
	public ShopNotifier deleteById(Long tenant,Long id) {
		ShopNotifier entity =  one(Filter.eq("tenant.id", tenant), Filter.eq("id", id));
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
}