package com.nbsaas.nbmall.device.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.nbsaas.nbmall.device.data.dao.DeviceTypeDao;
import com.nbsaas.nbmall.device.data.entity.DeviceType;

/**
* Created by imake on 2021年12月10日17:40:16.
*/
@Repository

public class DeviceTypeDaoImpl extends CriteriaDaoImpl<DeviceType, Long> implements DeviceTypeDao {

	@Override
	public DeviceType findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public DeviceType save(DeviceType bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public DeviceType deleteById(Long id) {
		DeviceType entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<DeviceType> getEntityClass() {
		return DeviceType.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}