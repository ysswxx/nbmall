package com.nbsaas.nbmall.product.api.domain.list;


import com.nbsaas.nbmall.product.api.domain.simple.ProductImageSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年12月28日18:04:52.
*/

@Data
public class ProductImageList  extends ResponseList<ProductImageSimple> {

}