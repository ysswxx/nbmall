package com.nbsaas.nbmall.product.data.entity;

import com.haoxuer.bigworld.tenant.data.entity.TenantEntity;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.FormField;
import com.nbsaas.codemake.annotation.SearchItem;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Data
@FormAnnotation(title = "运费模板管理",model = "运费模板",menu = "1,107,110")
@Entity
@Table(name = "bs_tenant_product_freight_template")
public class FreightTemplate extends TenantEntity {

    @SearchItem(label = "模板名称",name="name")
    @Column(length = 50)
    @FormField(title = "模板名称", grid = true)
    private String name;
}
