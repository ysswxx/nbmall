package com.nbsaas.nbmall.order.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.order.data.entity.OrderSettle;

/**
* Created by imake on 2021年12月21日18:00:16.
*/
public interface OrderSettleDao extends BaseDao<OrderSettle,Long>{

	 OrderSettle findById(Long id);

	 OrderSettle save(OrderSettle bean);

	 OrderSettle updateByUpdater(Updater<OrderSettle> updater);

	 OrderSettle deleteById(Long id);

	 OrderSettle findById(Long tenant, Long id);

     OrderSettle deleteById(Long tenant, Long id);
}