package com.nbsaas.nbmall.common.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.haoxuer.bigworld.pay.data.enums.SendState;

/**
*
* Created by BigWorld on 2021年12月27日16:40:36.
*/
@Data
public class TenantCashSimple implements Serializable {

    private Long id;

     private String cashConfigName;
     private String no;
     private String note;
     private BigDecimal fee;
     private Long creator;
     private SendState sendState;
     private String openId;
     private String idNo;
     private BigDecimal money;
     private String demo;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private String appId;
     private String bussNo;
     private String phone;
     private String name;
     private Long cashConfig;
     private BigDecimal cash;

     private String sendStateName;

}
