package com.nbsaas.nbmall.product.controller.tenant;

import com.nbsaas.nbmall.product.api.apis.ProductSpecApi;
import com.nbsaas.nbmall.product.api.domain.list.ProductSpecList;
import com.nbsaas.nbmall.product.api.domain.page.ProductSpecPage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductSpecResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping("/tenantRest/productspec")
@RestController
public class ProductSpecTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("productspec")
    @RequestMapping("create")
    public ProductSpecResponse create(ProductSpecDataRequest request) {
        initTenant(request);
        request.setCreator(request.getCreateUser());
        return api.create(request);
    }

	@RequiresPermissions("productspec")
    @RequestMapping("delete")
    public ProductSpecResponse delete(ProductSpecDataRequest request) {
        initTenant(request);
        ProductSpecResponse result = new ProductSpecResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("productspec")
    @RequestMapping("update")
    public ProductSpecResponse update(ProductSpecDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

	@RequiresPermissions("productspec")
    @RequestMapping("view")
    public ProductSpecResponse view(ProductSpecDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

	@RequiresPermissions("productspec")
    @RequestMapping("list")
    public ProductSpecList list(ProductSpecSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

	@RequiresPermissions("productspec")
    @RequestMapping("search")
    public ProductSpecPage search(ProductSpecSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }


    @Autowired
    private ProductSpecApi api;

}
