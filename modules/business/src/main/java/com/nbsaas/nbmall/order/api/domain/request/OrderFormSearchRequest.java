package com.nbsaas.nbmall.order.api.domain.request;

import com.haoxuer.bigworld.member.api.domain.request.TenantPageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月24日00:04:24.
*/

@Data
public class OrderFormSearchRequest extends TenantPageRequest {

    //商家
     @Search(name = "shop.id",operator = Filter.Operator.eq)
     private Long shop;

    //订单号
     @Search(name = "no",operator = Filter.Operator.like)
     private String orderNo;

    //收货人
     @Search(name = "consignee",operator = Filter.Operator.like)
     private String consignee;

    //联系电话
     @Search(name = "phone",operator = Filter.Operator.like)
     private String phone;



}