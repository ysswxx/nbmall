package com.nbsaas.nbmall.shop.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.nbsaas.nbmall.shop.data.entity.ShopStaff;

/**
* Created by imake on 2021年12月10日17:38:00.
*/
public interface ShopStaffDao extends BaseDao<ShopStaff,Long>{

	 ShopStaff findById(Long id);

	 ShopStaff save(ShopStaff bean);

	 ShopStaff updateByUpdater(Updater<ShopStaff> updater);

	 ShopStaff deleteById(Long id);

	 ShopStaff findById(Long tenant, Long id);

     ShopStaff deleteById(Long tenant, Long id);
}