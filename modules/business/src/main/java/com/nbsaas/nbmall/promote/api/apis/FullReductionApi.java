package com.nbsaas.nbmall.promote.api.apis;


import com.nbsaas.nbmall.promote.api.domain.list.FullReductionList;
import com.nbsaas.nbmall.promote.api.domain.page.FullReductionPage;
import com.nbsaas.nbmall.promote.api.domain.request.*;
import com.nbsaas.nbmall.promote.api.domain.response.FullReductionResponse;

public interface FullReductionApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    FullReductionResponse create(FullReductionDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    FullReductionResponse update(FullReductionDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    FullReductionResponse delete(FullReductionDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     FullReductionResponse view(FullReductionDataRequest request);


    /**
     *
     * @param request
     * @return
     */
    FullReductionList list(FullReductionSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    FullReductionPage search(FullReductionSearchRequest request);

}