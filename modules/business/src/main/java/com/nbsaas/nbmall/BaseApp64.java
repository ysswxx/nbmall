package com.nbsaas.nbmall;

import com.nbsaas.nbmall.common.cache.ParamKeyGenerator;
import com.nbsaas.nbmall.customer.api.domain.request.CustomerSearchRequest;
import jodd.util.Base64;

public class BaseApp64 {
    public static void main(String[] args) {

        CustomerSearchRequest request=new CustomerSearchRequest();
        request.setSize(10);
        ParamKeyGenerator generator=new ParamKeyGenerator();
        System.out.println(generator.key(request));
        System.out.println(Base64.decodeToString("SGgyMDEyMDkxMw=="));
        System.out.println(Base64.decodeToString("SGgyMDEyMDkxM0A="));
        System.out.println(Base64.decodeToString("SGgyMDEyMDkxMys="));

    }
}
