package com.nbsaas.nbmall.product.controller.rest;

import com.nbsaas.nbmall.product.api.apis.ProductSpecValueApi;
import com.nbsaas.nbmall.product.api.domain.list.ProductSpecValueList;
import com.nbsaas.nbmall.product.api.domain.page.ProductSpecValuePage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductSpecValueResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/productspecvalue")
@RestController
public class ProductSpecValueRestController extends BaseRestController {


    @RequestMapping("create")
    public ProductSpecValueResponse create(ProductSpecValueDataRequest request) {
        initTenant(request);
        return api.create(request);
    }

    @RequestMapping("delete")
    public ProductSpecValueResponse delete(ProductSpecValueDataRequest request) {
        initTenant(request);
        ProductSpecValueResponse result = new ProductSpecValueResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("update")
    public ProductSpecValueResponse update(ProductSpecValueDataRequest request) {
        initTenant(request);
        return api.update(request);
    }

    @RequestMapping("view")
    public ProductSpecValueResponse view(ProductSpecValueDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public ProductSpecValueList list(ProductSpecValueSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public ProductSpecValuePage search(ProductSpecValueSearchRequest request) {
        initTenant(request);
        return api.search(request);
    }

    @Autowired
    private ProductSpecValueApi api;

}
