package com.nbsaas.nbmall.product.controller.rest;

import com.nbsaas.nbmall.product.api.apis.ProductApi;
import com.nbsaas.nbmall.product.api.domain.list.ProductList;
import com.nbsaas.nbmall.product.api.domain.page.ProductPage;
import com.nbsaas.nbmall.product.api.domain.request.*;
import com.nbsaas.nbmall.product.api.domain.response.ProductResponse;
import com.nbsaas.nbmall.product.data.enums.ProductState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.bigworld.member.controller.rest.BaseRestController;

@RequestMapping("/rest/product")
@RestController
public class ProductRestController extends BaseRestController {


    @RequestMapping("view")
    public ProductResponse view(ProductDataRequest request) {
        initTenant(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public ProductList list(ProductSearchRequest request) {
        initTenant(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public ProductPage search(ProductSearchRequest request) {
        initTenant(request);
        request.setProductState(ProductState.onSale);
        return api.search(request);
    }

    @Autowired
    private ProductApi api;

}
