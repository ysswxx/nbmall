package com.nbsaas.nbmall.order.controller.admin;


import com.haoxuer.bigworld.tenant.controller.admin.TenantBaseAction;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
*
* Created by imake on 2021年12月24日00:04:24.
*/


@Scope("prototype")
@Controller
public class OrderFormAction extends TenantBaseAction {

	@RequiresPermissions("orderform")
	@RequestMapping("/tenant/orderform/view_list")
	public String list(ModelMap model) {
		return getView("orderform/list");
	}

}