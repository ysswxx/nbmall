package com.nbsaas.nbmall.promote.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.nbsaas.nbmall.promote.data.enums.CouponState;
import com.nbsaas.nbmall.promote.data.enums.CouponCatalog;

/**
*
* Created by BigWorld on 2021年12月25日12:45:48.
*/
@Data
public class CouponSimple implements Serializable {

    private Long id;

     private String couponRuleName;
     private Long tenantUser;
     private Long shop;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date useEndTime;
     private Long couponRule;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date useBeginTime;
     private String shopName;
     private BigDecimal money;
     private String tenantUserName;
     private CouponCatalog couponCatalog;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private CouponState couponState;

     private String couponStateName;
     private String couponCatalogName;

}
