package com.nbsaas.nbmall.product.rest.convert;

import com.nbsaas.nbmall.product.api.domain.response.FreightTemplateResponse;
import com.nbsaas.nbmall.product.data.entity.FreightTemplate;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.bigworld.tenant.util.TenantBeanUtils;

public class FreightTemplateResponseConvert implements Conver<FreightTemplateResponse, FreightTemplate> {
    @Override
    public FreightTemplateResponse conver(FreightTemplate source) {
        FreightTemplateResponse result = new FreightTemplateResponse();
        TenantBeanUtils.copyProperties(source,result);


        return result;
    }
}
