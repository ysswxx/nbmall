package com.nbsaas.nbmall.third.service.impl;

import com.haoxuer.bigworld.member.data.dao.TenantOauthConfigDao;
import com.haoxuer.bigworld.member.data.entity.TenantOauthConfig;
import com.nbsaas.nbmall.third.service.WxService;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.config.impl.WxMpDefaultConfigImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Component
public class WxServiceImpl implements WxService {


    @Autowired
    private TenantOauthConfigDao configDao;


    @Override
    public WxMpService getWxMpService(Long tenant) {
        WxMpService wxMpService = new WxMpServiceImpl();
        TenantOauthConfig config = configDao.config(tenant, "notice");
        if (config!=null){
            WxMpDefaultConfigImpl configStorage = new WxMpDefaultConfigImpl();
            configStorage.setAppId(config.getAppKey());
            configStorage.setSecret(config.getAppSecret());
            configStorage.setToken("newbyte");
            configStorage.setAesKey("rtTsfmBfVSRxFcfXLKjV2TzaxpNE0JkJXr2ke79NvOP");
            wxMpService.setWxMpConfigStorage(configStorage);
        }
        return wxMpService;
    }
}
