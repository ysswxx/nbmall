# nbmall
[![maven](https://img.shields.io/maven-central/v/com.haoxuer.discover/discover-website.svg)](http://mvnrepository.com/artifact/com.haoxuer.discover/discover-website/)
[![QQ](https://img.shields.io/badge/chat-on%20QQ-ff69b4.svg?style=flat-square)](https://qm.qq.com/cgi-bin/qm/qr?k=E2cDL8DExMk0c9oH5C6SX6Po-KJuTsOZ&jump_from=webapi)
[![Apache-2.0](https://img.shields.io/hexpm/l/plug.svg)](https://www.apache.org/licenses/LICENSE-2.0.html)
[![使用IntelliJ IDEA开发维护](https://img.shields.io/badge/IntelliJ%20IDEA-提供支持-blue.svg)](https://www.jetbrains.com/idea/)
[![GitHub forks](https://img.shields.io/github/stars/cng1985/adminstore.svg?style=social&logo=github&label=Stars)](https://github.com/cng1985/adminstore)

#### 介绍

nbmall 是一款多商家saas电商系统,通过域名来区分租户。

* [vue平台管理端](https://gitee.com/cng1985/nbmall_mg_vue)，
  [vue商家端](https://gitee.com/cng1985/nbmall_shop_vue),
  [小程序多商家端](https://gitee.com/cng1985/nbmall_wxapp)

本源码包采用maven结构



## 环境要求

- JDK6或更高版本（支持JDK7、JDK8）。建议使用JDK8，有更好的内存管理。更低版本的JDK6、JDK7可能需要设置Java内存`-XX:PermSize=128M -XX:MaxPermSize=512M`，否则可能出现这种类型的内存溢出：`java.lang.OutOfMemoryError: PermGen space`。
- Servlet2.5或更高版本（如Tomcat6或更高版本）。
- MySQL5.0或更高版本
- Maven3.0或更高版本。

## 技术选型：

* SSH (Spring、SpringMVC、Hibernate）
* 安全权限 Shiro
* 缓存 Ehcache
* 视图模板 freemarker
* 定时任务  quartz
* [discover](https://gitee.com/cng1985/discover)
* AdminLTE
* bootstrap
* [VUE](https://cn.vuejs.org/)
* [element-ui](https://element.eleme.cn/)
## 搭建步骤

1. 创建数据库。如使用MySQL，字符集选择为`utf8`或者`utf8mb4`（支持更多特殊字符，推荐）。
2. 执行数据库脚本。数据库脚本在`document`目录下。
3. 创建mysql数据库，导入`cloud.sql`
4. 在idea中导入maven项目。点击idea菜单`File` - `open`，选择`项目路径`。创建好maven项目后，会开始从maven服务器下载第三方jar包（如spring等），需要一定时间，请耐心等待。
5. 修改数据库连接。打开`/web/src/main/resources/jdbc.propertis`文件，根据实际情况修改`jdbc.url`、`jdbc.username`、`jdbc.password`的值。
6. 运行程序。在idea中，右键点击项目名，选择`Run` - `Run`-`Edit Configurations`-`+`-`Maven`，在`Working directory`选择项目路径 ，在`Command Line`填入`jetty:run`，然后点击`Run`。
7. 访问系统。前台地址：[http://localhost:8080/web/index.htm](http://localhost:8080/web/index.htm)，手机站地址：[http://127.0.0.1:8080/web/](http://127.0.0.1:8080/web/)；后台地址：[http://localhost:8080/web/login.htm](http://localhost:8080/web/login.htm)，用户名：admin，密码：123456。

## 交流方式

* QQ群:140960480   [nbmall开源QQ群](https://qm.qq.com/cgi-bin/qm/qr?k=E2cDL8DExMk0c9oH5C6SX6Po-KJuTsOZ&jump_from=webapi)

## 界面效果

[在线实例http://demo.mall.nbsaas.com](http://demo.mall.nbsaas.com) 账号admin密码123456

[在线实例http://nb.mall.nbsaas.com](http://nb.mall.nbsaas.com) 账号admin密码123456

[在线实例http://newbyte.mall.nbsaas.com](http://newbyte.mall.nbsaas.com) 账号demo密码123456


![部分er图](http://cdn.haoxuer.com/shop.png "部分er图")

